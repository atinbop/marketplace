using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MarketPlace.Model;
using Microsoft.Extensions.Options;
using MarketPlace.Services;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Razor;
using MarketPlace.Resources;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace MarketPlace
{
    public class Startup
    {
        public Microsoft.AspNetCore.Hosting.IWebHostEnvironment   _environment;

        public Startup(IConfiguration configuration, Microsoft.AspNetCore.Hosting.IWebHostEnvironment environment)
        {
            Configuration = configuration;
            _environment = environment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        [Obsolete]
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddDistributedMemoryCache();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });



            services.AddSession(options =>
            {
                options.Cookie.Name = ".Pharmashopcenter.Session";
                options.IdleTimeout = TimeSpan.FromDays(30);
            
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
              .AddCookie(options =>
              {
                  options.Cookie.HttpOnly = true;
                  options.Cookie.SecurePolicy = _environment.IsDevelopment()
              ? CookieSecurePolicy.None : CookieSecurePolicy.Always;
                  options.Cookie.SameSite = SameSiteMode.Lax;

                  options.Cookie.Name = ".Pharmashopcenter.Cookie";
               
                  options.LoginPath = "/Account/Login";
                  options.LogoutPath = "/Account/Logout";

              });


            Action<MDUOptions> mduOptions = (opt =>
            {
                opt.ImageRepository = Configuration["SolutionAppSettings:ImageRepository"];
                opt.ConnectedUserEmail = Configuration["SolutionAppSettings:connectedUserEmail"];
                opt.UserCompanyType = Configuration["SolutionAppSettings:UserCompanyType"];
                opt.ConnectedUserId = Configuration["SolutionAppSettings:ConnectedUserId"];
                opt.UserCompanyId = Int32.Parse(Configuration["SolutionAppSettings:UserCompanyId"]);
                opt.UserCompanyName = Configuration["SolutionAppSettings:UserCompanyName"]; 
                opt.BrandImageRepository = Configuration["SolutionAppSettings:BrandImageRepository"];
                opt.EmployeeImageRepository = Configuration["SolutionAppSettings:EmployeeImageRepository"];
            opt.ConnectedUserPseudo = Configuration["SolutionAppSettings:ConnectedUserPseudo"];
                opt.UserCurrency = Configuration["SolutionAppSettings:UserCurrency"];
                opt.UserLanguage = Configuration["SolutionAppSettings:UserLanguage"]; 
            });


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            
       

            services.AddControllersWithViews();

            #region snippet1
            services.AddSingleton<IdentityLocalizationService>();
            services.AddSingleton<SharedLocalizationService>();
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization();
            #endregion

            // requires
            // using Microsoft.AspNetCore.Identity.UI.Services;
            // using WebPWrecover.Services;
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();


            services.Configure(mduOptions);
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<MDUOptions>>().Value);
            services.AddHttpContextAccessor();

            services.Configure<AuthMessageSenderOptions>(Configuration);



            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[] { "fr", "en", "es", "de" };
                options.SetDefaultCulture(supportedCultures[0])
                    .AddSupportedCultures(supportedCultures)
                    .AddSupportedUICultures(supportedCultures);
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
         .AddViewLocalization()
         .AddDataAnnotationsLocalization(options =>
         {
             options.DataAnnotationLocalizerProvider = (type, factory) =>
             {
                 var assemblyName = new AssemblyName(typeof(IdentityResource).GetTypeInfo().Assembly.FullName);
                 return factory.Create("IdentityResource", assemblyName.Name);
             };
         });


            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
 

            app.Use(async (context, next) =>
            {
                var principal = context.User as ClaimsPrincipal;
                var accessToken = principal?.Claims
                  .FirstOrDefault(c => c.Type == "access_token");

                if (accessToken != null)
                {
                    //_logger.LogDebug(accessToken.Value);
                }

                await next();
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                try
                {
                    using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                        .CreateScope())
                    {
                        serviceScope.ServiceProvider.GetService<ApplicationDbContext>()
                            .Database.Migrate();
                    }
                }
                catch { }

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

      
            var supportedCultures = new[] { "fr", "en", "es", "de" };
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);

            app.UseRequestLocalization(localizationOptions);


            app.UseHttpsRedirection();
        

            app.UseRouting();
            app.UseStaticFiles();

            app.UseRouting();
   
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCookiePolicy();
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseRequestLocalization();
        }
    }
}
