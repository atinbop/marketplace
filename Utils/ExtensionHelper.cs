﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MarketPlace.Utils
{
    public static class ExtensionHelper
    {
        public static string GetStringSafe(this IDataReader reader, int colIndex)
        {
            return GetStringSafe(reader, colIndex, null);
        }

        public static string GetStringSafe(this IDataReader reader, int colIndex, string defaultValue)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            else
                return defaultValue;
        }

        public static string GetStringSafe(this IDataReader reader, string indexName)
        {
            return GetStringSafe(reader, reader.GetOrdinal(indexName));
        }


        public static string GetStringSafe(this IDataReader reader, string indexName, string defaultValue)
        {
            return GetStringSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }

        public static DateTime GetDateTimeSafe(this IDataReader reader, int colIndex)
        {
            return GetDateTimeSafe(reader, colIndex, new DateTime(1960, 01, 01));
        }

        public static DateTime GetDateTimeSafe(this IDataReader reader, int colIndex, DateTime defaultValue)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDateTime(colIndex);
            else
                return defaultValue;
        }

        public static DateTime GetDateTimeSafe(this IDataReader reader, string indexName)
        {
            return GetDateTimeSafe(reader, reader.GetOrdinal(indexName));
        }

        public static bool IsDBNull(this IDataReader reader, string indexName)
        {
            return reader.IsDBNull(reader.GetOrdinal(indexName));
        }

        public static string GetStringSafe(string value)
        {
            return string.IsNullOrEmpty(value) ? "" : value;
        }
    }
}
