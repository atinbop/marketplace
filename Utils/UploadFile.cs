﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Utils
{
    public class UploadFile
    {
        public int FileType { get; set; }
        public int OwnerID { get; set; }
        public string OwnerName { get; set; }
        public string FileName { get; set; }
        public string FileData { get; set; }
    }
}
