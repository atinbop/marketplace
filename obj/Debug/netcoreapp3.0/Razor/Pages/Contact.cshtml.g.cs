#pragma checksum "D:\MarketPlace\MarketPlace\Pages\Contact.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fff6ce108df3e6acaddfc505efe4fa660e76547b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(MarketPlace.Pages.Pages_Contact), @"mvc.1.0.razor-page", @"/Pages/Contact.cshtml")]
namespace MarketPlace.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\MarketPlace\MarketPlace\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\MarketPlace\MarketPlace\Pages\_ViewImports.cshtml"
using MarketPlace;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\MarketPlace\MarketPlace\Pages\_ViewImports.cshtml"
using MarketPlace.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\MarketPlace\MarketPlace\Pages\_ViewImports.cshtml"
using MarketPlace.Resources;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fff6ce108df3e6acaddfc505efe4fa660e76547b", @"/Pages/Contact.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"608931ff387901f75f64dec1455a5a29dae583a4", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Contact : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/carousel1.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\MarketPlace\MarketPlace\Pages\Contact.cshtml"
  
    ViewData["Title"] = "Contact";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n<div id=\"carouselExampleControls\" class=\"carousel slide slideshow\" data-ride=\"carousel\">\r\n    <div class=\"slideshow-item\">\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "fff6ce108df3e6acaddfc505efe4fa660e76547b4636", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
        <div class=""slideshow-item-text"" id=""main_place"">
            <h5> Contactez <strong style=""color: #76B72A; font-family: Saira"">Nous</strong> </h5>
            <p>
                Contactez pharmashopcenter
            </p>

        </div>
    </div>
</div>

<br />






<hr class=""my-5"">
<div class=""container"">
    <!--Section: Contact v.1 -->
    <section class=""section"">

        <div class=""card card-cascade narrower"">
            <!-- Card image -->
            <div class=""view view-cascade gradient-card-header copyright lighten-3"">
                <h5 class=""mb-0"">Contactez-nous</h5>
            </div>
            <div class=""card-body card-body-cascade"">

                <!-- Google map -->
                <div id=""map-container-google-12"" class=""z-depth-1-half map-container-7"" style=""height: 200px"">
                    <iframe src=""https://maps.google.com/maps?q=Miami&t=&z=13&ie=UTF8&iwloc=&output=embed"" frameborder=""0""
                            style=""borde");
            WriteLiteral("r:0\" allowfullscreen></iframe>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n\r\n                    <!-- Grid column -->\r\n                    <div class=\"col-md-6 mb-4\">\r\n                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "fff6ce108df3e6acaddfc505efe4fa660e76547b7062", async() => {
                WriteLiteral("\r\n\r\n                            <div class=\"md-form\">\r\n                                <input type=\"text\" id=\"contact-name\" class=\"form-control\">\r\n                                <label for=\"contact-name\"");
                BeginWriteAttribute("class", " class=\"", 1694, "\"", 1702, 0);
                EndWriteAttribute();
                WriteLiteral(@">Votre nom</label>
                            </div>

                            <div class=""md-form"">
                                <input type=""text"" id=""contact-email"" class=""form-control"">
                                <label for=""contact-email""");
                BeginWriteAttribute("class", " class=\"", 1963, "\"", 1971, 0);
                EndWriteAttribute();
                WriteLiteral(@">Votre adresse email</label>
                            </div>

                            <div class=""md-form"">
                                <input type=""text"" id=""contact-Subject"" class=""form-control"">
                                <label for=""contact-Subject""");
                BeginWriteAttribute("class", " class=\"", 2246, "\"", 2254, 0);
                EndWriteAttribute();
                WriteLiteral(">Sujet</label>\r\n                            </div>\r\n\r\n                        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class=""col-md-6 mb-4"">

                        <div class=""md-form primary-textarea"">
                            <textarea id=""contact-message"" class=""md-textarea form-control mb-0"" rows=""5"" style=""padding-bottom: 1.2rem;""></textarea>
                            <label for=""contact-message"">Votre message</label>
                        </div>

                    </div>
                    <!-- Grid column -->
                    <!-- Grid column -->
                    <div class=""col-md-12"">

                        <div class=""text-center"">
                            <a href=""https://mdbootstrap.com/docs/jquery/forms/basic/"" target=""_blank"" class=""btn text-white copyright"">
                               Envoyer
                            </a>
                        </div>

                    </div>
                    <!-- Grid column -->

  ");
            WriteLiteral(@"              </div>

            </div>

        </div>

    </section>
    <!--Section: Contact v.1 -->
    <!--Section: Docs link -->
    <hr class=""my-5"">

    <section class=""pb-4"">

        <!--Panel -->
        <div class=""card text-center"">
            <h3 class=""card-header copyright white-text"">Conditions générales de la Market Place</h3>
            <div class=""card-body"">
                <p class=""card-text"">Télécharger les CGV et CGU.</p>
                <a href=""https://mdbootstrap.com/docs/jquery/forms/basic/"" target=""_blank"" class=""btn text-white copyright"">
                    En savoir plus
                </a>
            </div>
        </div>
        <!--Panel -->


    </section>
    <!--Section: Docs link -->
</div>




");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ContactModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<ContactModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<ContactModel>)PageContext?.ViewData;
        public ContactModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
