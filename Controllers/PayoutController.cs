﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Controllers
{
    [Route("api/Payout")]
    [ApiController]
    public class PayoutController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        public PayoutController(Data.ApplicationDbContext db, IConfiguration iConfig)
        {
            _db = db;
            configuration = iConfig;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {



            return Json(new { data = await (from art in _db.Payout where art.Deleted != 1 select art).ToListAsync() });
        }


        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            ViewBag.ImageRepository = configuration.GetSection("SolutionAppSettings").GetSection("ImageRepository").Value;


            var bookFromDb = await _db.Payout.FirstOrDefaultAsync(u => u.Id == id);
            if (bookFromDb == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }
            //_db.Payout.Remove(bookFromDb);
            int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update Payout set Deleted=1 where id = " + id);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Delete successful" });
        }





    }


}