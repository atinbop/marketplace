﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace MarketPlace.Controllers
{
    [Route("api/chart")]
    [ApiController]
    public class ChartController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;
        public List<Chart> ChartList { get; set; }
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public  int _NbProductInChart { get; set; }
        public static String _UserCompanyName;
        public IEnumerable<Article> Articles { get; set; }
        public static String _ConnectedUserPseudo;

        public Article Article { get; set; }
        [BindProperty]
        public Chart Chart { get; set; }

        [BindProperty]
        public IEnumerable<ConnectedUser> ConnectedUsers { get; set; }
        public class ConnectedUser
        {
            public int EmployeeId { get; set; }
            public DateTime LoginDate { get; set; }
            public string Pseudo { get; set; }
            public string Picture { get; set; }
        }

        public ChartController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;
            _ConnectedUserPseudo = options.ConnectedUserPseudo;
            _NbProductInChart = options.NbProductInChart;

        }


        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
          

            ViewBag.ImageRepository = configuration.GetSection("SolutionAppSettings").GetSection("ImageRepository").Value;

            if (_UserCompanyType == "B2B")
            {

                var bookFromDb = await _db.Article.FirstOrDefaultAsync(u => u.Id == id);
                if (bookFromDb == null)
                {
                    return Json(new { success = false, message = "Error while Deleting" });
                }
                //_db.Article.Remove(bookFromDb);
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update article set Deleted=1 , LastUpdate=getdate(), B2CSellerCompanyId=null, UpdateByUserId='" + _ConnectedUserId + "' where id = " + id);
                await _db.SaveChangesAsync();
                return Json(new { success = true, message = "Suppression réussie." });
            }
            else
            {
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update ProductSubscriptions set cancelsubscription=1 , endValidityDate=getdate(),   employeeId='" + _ConnectedUserId + "' where articleid = " + id + " and contactid=" + _UserCompanyId);
                await _db.SaveChangesAsync();
                return Json(new { success = true, message = "Retrait réussie." });

            }
        }


        [HttpGet]
        public async Task<IActionResult> Post(

              string articleVendorsId, string articleId,
      string quantity,
        string price,
       string currency,
      string pricetype,
        string image1,
     string productName,
      string productBrand,
       string brandId,
        string supplierName,
      string lot,
      string MinimumOrderQuantity,
      string MaximumOrderQuantity,
string id, 
string vat

            )
        {

            CultureInfo[] cultures = { new CultureInfo("en-US")/*,
            new CultureInfo("fr-FR") */};

            if (id == null)
            {
                Chart NewCharts = new Chart();

                try
                {
                    NewCharts.ArticleVendorsId = Int32.Parse(articleVendorsId);
                    NewCharts.articleId = Int32.Parse(articleId);
                    foreach (CultureInfo culture in cultures)
                            {

                        NewCharts.price = Math.Round(Convert.ToDecimal(price, culture), 2);
                         
                         }

                       
                    NewCharts.currency = currency;
                    NewCharts.pricetype = pricetype;
                    NewCharts.Image1 = image1;
                    NewCharts.ProductName = productName;
                    NewCharts.ProductBrand = productBrand;
                    NewCharts.BrandId = Int32.Parse(brandId);
                    NewCharts.supplierName = supplierName;
                    NewCharts.quantity = Int32.Parse(quantity);
                    NewCharts.Lot = Int32.Parse(lot);

                    NewCharts.TotalAmount = NewCharts.quantity * NewCharts.price;
                    NewCharts.employeeId = Int32.Parse(_ConnectedUserId);
                    NewCharts.chartDate = DateTime.Now;
                    NewCharts.LastUpdate = DateTime.Now;
                    NewCharts.chartstate = "StandBy";

                    if(MinimumOrderQuantity!=null)

                    { NewCharts.MinimumOrderQuantity = Int32.Parse(MinimumOrderQuantity); }
                    
                    if(MaximumOrderQuantity!=null)
                    { 
                        NewCharts.MaximumOrderQuantity = Int32.Parse(MaximumOrderQuantity);
                    }

                    NewCharts.vat = Decimal.Parse(vat);
                    await _db.Chart.AddAsync(NewCharts);
                    await _db.SaveChangesAsync();
                }
                catch (Exception ex){

                    string t = ex.Message;
                }

            }
            else {
                spUpdateChartQuantity(id, quantity);
            }

       


            ChartList = _db.Chart.FromSqlRaw<Chart>("spGetChartProductEmployee {0} ", _ConnectedUserId).ToList();


            int FlagChart = ChartList.Count();

            return Json(new { success = true, message = "Article enregistré dans votre panier", nbItem = FlagChart });

        }

        [HttpPost]
        public async Task<IActionResult> GetNbItemChart(int id, int quantity)
        {
            ChartList = _db.Chart.FromSqlRaw<Chart>("spUpdateChartQuantity {0}, {1} ", id, quantity).ToList();
            ChartList = _db.Chart.FromSqlRaw<Chart>("spGetChartProductEmployee {0} ", _ConnectedUserId).ToList();
            int FlagChart = ChartList.Count();
            _NbProductInChart = FlagChart;
             return Json(new { success = true, message = "Article enregistré dans votre panier", nbItem = FlagChart });
        }


       
        public async Task<IActionResult> spUpdateChartQuantity(string id, string quantity)
        {
            ChartList = _db.Chart.FromSqlRaw<Chart>("spUpdateChartQuantity {0}, {1} ", id, quantity).ToList();
            
            return Json(new { success = true, message = "Article enregistré dans votre panier" });
        }

        public ViewResult Details()
    {

        return View();
    }





}

}

