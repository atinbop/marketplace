﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Controllers
{
    [Route("api/preferedLanguage")]
    [ApiController]
    // [Route("api/brand")]
    //[ApiController]
    public class PreferedlanguageController : Controller
    //public class BrandController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;


        [BindProperty]
        public int _NbProductAutorized { get; set; }

        [BindProperty]
        public int _NbProductAlreadyChoose { get; set; }
        public IEnumerable<String> BrandsName { get; set; }

        public PreferedlanguageController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;
            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;


        }


        [HttpGet]   
        public async Task<IActionResult> setLanguage(int employeeId , string language)
        {
             
                    Employees Employee = await (from art in _db.Employees
                                                 where art.Id == employeeId
                                                 select art).FirstOrDefaultAsync();

            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = DateTime.Now.AddDays(7);
            HttpContext.Response.Cookies.Append("UserPreferedLanguage", language, cookieOptions);
            Employee.PreferedLanguage = language;

            await _db.SaveChangesAsync();
            return View();


        }

    }


}