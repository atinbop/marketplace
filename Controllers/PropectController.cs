﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace MarketPlace.Controllers
{
    [Route("api/prospect")]
    [ApiController]
    public class PropectController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        public IEnumerable<Article> Articles { get; set; }
        public PropectController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;

        }

 

        [HttpGet]
        public async Task<IActionResult> Post(string country, string Message, string ActivitySector, string Company, string TelephoneNumber, string emailAddress, string FirstName, string Name)
        {
            Prospects Prospect = new Prospects();
            Prospect.country = country;
            Prospect.Message = Message;
            Prospect.ActivitySector = ActivitySector;
            Prospect.Company = Company;
            Prospect.TelephoneNumber = TelephoneNumber;
            Prospect.emailAddress = emailAddress;
            Prospect.FirstName = FirstName;
            Prospect.Name = Name;
       

            await _db.Prospects.AddAsync(Prospect);
            await _db.SaveChangesAsync();
            try
            {
                SendMailPharmashopCenter(Prospect);
            }
            catch (Exception e)
            {
                string t = e.Message;
            }

            return Json(new { success = true, message = "Votre message a bien été pris en compte. Nous revenons vers vous dans les plus brefs délais." });

        }


     

        private void SendMailPharmashopCenter(Prospects Prospect)
        {

            if (Prospect.Remarque is null)
            {
                System.Net.Mail.
                SmtpClient smtp = new System.Net.Mail.SmtpClient("mail.gandi.net", 993);
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential("contact@pharmashopcenter.com", "U7|Plg8xF4wQ0tfgv");
                smtp.EnableSsl = true;


                MailMessage email = new MailMessage();
                email.From = new MailAddress(Prospect.emailAddress);
                email.To.Add(new MailAddress("contact@pharmashopcenter.com"));
                email.Subject = "Demande de partenariat Pharmashopcenter";
                email.Body = Prospect.Message;
                email.Priority = MailPriority.High;
                email.Priority = MailPriority.High;

                MailMessage emailRetour = new MailMessage();
                emailRetour.From = new MailAddress("contact@pharmashopcenter.com");
                emailRetour.To.Add(Prospect.emailAddress);
                emailRetour.Subject = "Re: Demande de partenariat Pharmashopcenter";
                emailRetour.Body = "Bonjour, " +
               "Nous vous remercions pour votre message, un représentant Pharmashopcenter prendra" +
               "contact avec vous dans les meilleurs délais Cordialement";
                emailRetour.Priority = MailPriority.High;

                smtp.Send(email);
                //smtp.Send(emailRetour); A Activer lors du passag en production et configurer le bon message avec l'adresse de pharmashopcenter
                // MessageBox.Show("mail Send");
                try
                {
                    smtp.Send(email);
                }
                catch (Exception ex)
                {
                    emailRetour.Body = ex.Message;
                    smtp.Send(email);
                }
            }
        }


    }

}

