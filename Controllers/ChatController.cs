﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace MarketPlace.Controllers
{
    [Route("api/chat")]
    [ApiController]
    public class ChatController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        public IEnumerable<Article> Articles { get; set; }
        public static String _ConnectedUserPseudo;

        public Article Article { get; set; }
        [BindProperty]
        public List<Posts> Posts { get; set; }

        [BindProperty]
        public IEnumerable<ConnectedUser> ConnectedUsers { get; set; }
        public class ConnectedUser
        {
            public int EmployeeId { get; set; }
            public DateTime LoginDate { get; set; }
            public string Pseudo { get; set; }
            public string Picture { get; set; }
        }

        public ChatController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;

            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;
            _ConnectedUserPseudo = options.ConnectedUserPseudo;
        }

        /*
      [HttpGet]
        public async Task<IActionResult> GetAll()
        {

   

            if (_UserCompanyType == "B2B")
            {
                 return  Json(new {
                    data = await (from art in _db.Article
                                  where art.B2BSellerCompanyId == _UserCompanyId &&
                                  art.Deleted != 1
                                  select art).ToListAsync()

            });
         
        }
        else
            {
               

                Articles = await (from art in _db.Article
                                  join p in _db.ProductSubscriptions on art.Id equals p.articleid
                                  where p.contactid==_UserCompanyId &&
                                  art.Deleted != 1 && p.cancelsubscription !=1
                                  select art).ToListAsync();


                using (var sequenceEnum = Articles.GetEnumerator())
                {
                    while (sequenceEnum.MoveNext())
                    {
                        sequenceEnum.Current.Price = (decimal)sequenceEnum.Current.B2CPrice;
                        sequenceEnum.Current.Lot = sequenceEnum.Current.B2CLot;
                        sequenceEnum.Current.Inventory = sequenceEnum.Current.B2CInventory;
                        sequenceEnum.Current.PriceType = "TTC";
                    }
                }
                return Json(new {data = Articles });
                }
        }*/
   

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {


            ViewBag.ImageRepository = configuration.GetSection("SolutionAppSettings").GetSection("ImageRepository").Value;

            if (_UserCompanyType == "B2B")
            {

                var bookFromDb = await _db.Article.FirstOrDefaultAsync(u => u.Id == id);
                if (bookFromDb == null)
                {
                    return Json(new { success = false, message = "Error while Deleting" });
                }
                //_db.Article.Remove(bookFromDb);
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update article set Deleted=1 , LastUpdate=getdate(), B2CSellerCompanyId=null, UpdateByUserId='" + _ConnectedUserId + "' where id = " + id);
                await _db.SaveChangesAsync();
                return Json(new { success = true, message = "Suppression réussie." });
            }
            else
            {
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update ProductSubscriptions set cancelsubscription=1 , endValidityDate=getdate(),   employeeId='" + _ConnectedUserId + "' where articleid = " + id + " and contactid="+ _UserCompanyId);
                await _db.SaveChangesAsync();
                return Json(new { success = true, message = "Retrait réussie." });

            }
        }


        [HttpGet]
        public async Task<IActionResult> Post(int articleId, string postContent)
        {
            Article = await (_db.Article.FindAsync(articleId));



            ConnectedUsers = (from z in _db.LogConnexions
                              where z.LogoutDate == null
                              join f in _db.Employees on z.EmployeeId equals f.Id
                              select new { f.Pseudo, z.EmployeeId, z.LoginDate, f.Picture } into x
                              group x by new { x.Pseudo, x.EmployeeId, x.Picture } into g
                              select new ConnectedUser
                              {
                                  Picture = g.Key.Picture,
                                  Pseudo = g.Key.Pseudo,
                                  EmployeeId = g.Key.EmployeeId,
                                  LoginDate = (DateTime)g.Max(i => i.LoginDate)
                              }).OrderByDescending(g => g.LoginDate).Take(8);


            if (postContent != null)
            {
                Posts ThePost = new Posts();

                ThePost.ArticleId = articleId;
                ThePost.PostContent = postContent;
                ThePost.UpdateByUserId = _ConnectedUserId;
                ThePost.LastUpdate = DateTime.Now;
                ThePost.PartnerEmployeeId = -1; //Group Post
                ThePost.UpdateByPseudo = _ConnectedUserPseudo;
                await _db.Posts.AddAsync(ThePost);
                await _db.SaveChangesAsync();

                //Mise à jour du nombre de post de l'utilisateur
                //var article = await _db.Article.FindAsync(id);
                //article.Note = Convert.ToInt32(await _db.Notes.Where(x => x.ArticleId == id).AverageAsync(x => x.Note));
                //await _db.SaveChangesAsync();
            }
            Posts = await (from art in _db.Posts
                           where art.ArticleId == articleId && art.Deleted != 1 && art.PartnerEmployeeId == -1
                           select art).ToListAsync();


            //string result = "";
            //foreach (Posts prop in Posts)
            //{

            //    if (prop.UpdateByUserId == _ConnectedUserId)
            //    {


            //        result = result + "< li class=\"clearfix-chat\">" +
            //                    "<div class=\"message-data align-right\" > " +
            //                        "<span class=\"message-data-time\" >" + prop.LastUpdate.Value.ToString("dddd, dd MMMM yyyy HH:mm:ss") + "</span> &nbsp; &nbsp;" +
            //                        "<span class=\"message-data-name\" > " + prop.UpdateByPseudo + "</span> <i class=\"fa fa-circle me\"></i>" +

            //                    "</div>" +
            //                    "<div class=\"message other-message float-right\" > " +
            //                        prop.PostContent +
            //                    "</div>" +
            //                "</li>";
            //    }
            //    else
            //    {
            //        result = result + "<li>" +
            //                    "<div class=\"message-data\" > " +
            //                        "<span class=\"message-data-name\" ><i class=\"fa fa-circle online\" ></i>" + prop.UpdateByPseudo + "</span>" +
            //                        "<span class=\"message-data-time\" > " + prop.LastUpdate.Value.ToString("dddd, dd MMMM yyyy HH:mm:ss") + "</span>" +
            //                    "</div>" +
            //                    "<div class=\"message my-message\">" +
            //                        prop.PostContent +
            //                    "</div>" +
            //                "</li>";
            //    }

            //}
            return Json(Posts);
           
      
        }
           

        public ViewResult Details()
        {
            
            return View();
        }



     

    }

}

 