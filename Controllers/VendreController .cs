﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Controllers
{
    [Route("api/Vendre")]
    [ApiController]
    public class VendreController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        

        [BindProperty]
        public   int _NbProductAutorized { get; set; }

        [BindProperty]
        public   int _NbProductAlreadyChoose { get; set; }
        public IEnumerable<Article> Articles { get; set; }
       
        public VendreController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;
            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;

           
        }
               
       [HttpGet]
        public async Task<IActionResult> GetAll()
        {
      
            if (_UserCompanyType == "B2C")
            {
                DateTime localDate = DateTime.Now;

                //Subscriptions en cours
                try
                {
                    Subscriptions currentSubscriptions = await (from art in _db.Subscriptions
                                                                where art.Contact.Id == _UserCompanyId && art.endValidityDate > localDate && art.cancelsubscription != 1
                                                                orderby art.startvalidityDate descending
                                                                select art).FirstOrDefaultAsync();

                    Offers offre = await (from art in _db.Offers
                                          where art.id == currentSubscriptions.OfferId
                                          select art).FirstOrDefaultAsync();

                    if (offre.NumberOfDisplayProduct > 1)
                    {
                        Articles = (from e1 in _db.Article
                                    where !_db.ProductSubscriptions.Where(c => c.cancelsubscription != 1).Any(e2 => e2.articleid == e1.Id) && e1.Deleted != 1
                                    select e1).Take((int)offre.NumberOfDisplayProduct).ToList();
                    }
                    else
                    {
                        Articles = (from e1 in _db.Article
                                    where !_db.ProductSubscriptions.Where(c => c.cancelsubscription != 1).Any(e2 => e2.articleid == e1.Id) && e1.Deleted != 1
                                    select e1).ToList();

                    }



                    _NbProductAutorized = offre.numberofProduct;
                    _NbProductAlreadyChoose = Articles.Count();




                    using (var sequenceEnum = Articles.GetEnumerator())
                    {
                        while (sequenceEnum.MoveNext())
                        {
                            sequenceEnum.Current.Price = (decimal)sequenceEnum.Current.B2CPrice;
                            sequenceEnum.Current.Lot = sequenceEnum.Current.B2CLot;
                            sequenceEnum.Current.Inventory = sequenceEnum.Current.B2CInventory;
                            sequenceEnum.Current.PriceType = "TTC";
                        }
                    }
                
                   }
                catch (Exception ex)
            {
                var t = ex;
              

            }

            ViewBag.Message = _NbProductAutorized;
                return Json(new { data = Articles });
            }

            
            return Json(new { success = false, message = "Erreur - Vous pouvez vous abonner pour vendre des produits sur la plateforme" });  
        }

        
        [HttpDelete]
        public async Task<IActionResult> Add(int id)
        {
            ViewBag.ImageRepository = configuration.GetSection("SolutionAppSettings").GetSection("ImageRepository").Value;

            DateTime localDate = DateTime.Now;

            Subscriptions currentSubscriptions = await (from art in _db.Subscriptions
                                                        where art.Contact.Id == _UserCompanyId && art.endValidityDate > localDate && art.cancelsubscription != 1
                                                        orderby art.startvalidityDate descending
                                                        select art).FirstOrDefaultAsync();

            Offers offre = await (from art in _db.Offers
                                  where art.id == currentSubscriptions.OfferId
                                  select art).FirstOrDefaultAsync();

            IEnumerable<ProductSubscriptions> productSubscriptions = await (from art in _db.ProductSubscriptions
                                                                            where art.contactid == _UserCompanyId && art.Subscriptionid == currentSubscriptions.id
                                                                            select art).ToListAsync();



            if (productSubscriptions.Count() <= offre.numberofProduct)
            {
                //_db.Article.Remove(bookFromDb);
                string sql = "insert into ProductSubscriptions (contactid, articleid, Subscriptionid,startvalidityDate,employeeId)  values (" + _UserCompanyId + "," + id + "," + currentSubscriptions.id + ",'" + localDate + "'," + _ConnectedUserId + ")";

         
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);

                await _db.SaveChangesAsync();
                return Json(new { success = true, message = "Félicitation! Ce produit est désormais présent dans votre catalogue et sera affiché sur la Market Place au nom de votre enseigne " + _UserCompanyName + "." });
            }
            else
            {
                return Json(new { success = false, message = "Vous avez atteint le nombre maximal de produit autorisé dans votre offre. Vous pouvez changer votre offre pour bénéficier d'autres produits" });

            }

        }

    }


}