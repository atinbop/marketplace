﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nancy.Json;

namespace MarketPlace.Controllers
{ 
    public class SetLanguageController : Controller
    {
        private readonly Data.ApplicationDbContext _db;

        public SetLanguageController( Data.ApplicationDbContext db 
        )
        {
          
            _db = db;
 
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            string preferedCountry = GetCountryByIP();

            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = DateTime.Now.AddYears(1);
            HttpContext.Response.Cookies.Append("PreferedLanguage", culture, cookieOptions);

            if (Request.Cookies["ConnectedUserId"] !=null) { 
          int  noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update employees  set  PreferedLanguage ='"+ culture + "' , LastUpdate=getdate()  where  id= '"+ Request.Cookies["ConnectedUserId"] + "'");
            }

            return LocalRedirect(returnUrl);


        }

        public   string GetCountryByIP()
        {
            IpInfo ipInfo = new IpInfo();

            string info = new WebClient().DownloadString("http://ipinfo.io");

            JavaScriptSerializer jsonObject = new JavaScriptSerializer();
            ipInfo = jsonObject.Deserialize<IpInfo>(info);

            RegionInfo region = new RegionInfo(ipInfo.Country);
 
           return region.EnglishName;
            

        }
        public class IpInfo
        {
            //country
            public string Country { get; set; }
        }

    }
}