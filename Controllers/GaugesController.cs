﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Data;
using Microsoft.AspNetCore.Mvc;

namespace MarketPlace.Controllers
{

    public class GaugesController : Controller
    {
        public ActionResult UpdateCircularGaugeDataAtRuntime()
        {
            return View(SampleData.GaugeSeasonsData);
        }
    }
}
 