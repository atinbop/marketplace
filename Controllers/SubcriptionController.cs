﻿using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.IO;

using Microsoft.Extensions.Configuration;

namespace MarketPlace.Controllers
{
    [Route("api/Subscription")]
    [ApiController]
    public class SubcriptionController : Controller
    {

        [BindProperty]
        public Subscriptions Subscription { get; set; }

        [BindProperty]
        public Offers Offer { get; set; }
        public Contacts Contact { get; set; }
        public Employees Employee { get; set; }

        private readonly Data.ApplicationDbContext _db;
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;




        private readonly IWebHostEnvironment webHostEnvironment;
        public SubcriptionController(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;

        }

        public void OnGet()
        {

        }

       /* [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
           


            var bookFromDb = await _db.Article.FirstOrDefaultAsync(u => u.Id == id);
            if (bookFromDb == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }
            //_db.Article.Remove(bookFromDb);
            int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update article set Deleted=1 , LastUpdate=getdate(), B2CSellerCompanyId=null, UpdateByUserId='" + _ConnectedUserId + "' where id = " + id);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Suppression réussie." });
        }
       */
         [HttpPost]
         public async Task<IActionResult> Subscribe(  Offers offer)
         {
            DateTime localDate = DateTime.Now;

            Contact = await (from art in _db.Contacts
                              where art.Id ==  _UserCompanyId
                              select art).FirstAsync();

             Employee = await (from art in _db.Employees
                               where art.Id == Int16.Parse(_ConnectedUserId)
                               select art).FirstAsync();

             Offer = await (from art in _db.Offers
                            where art.SupplierType == offer.SupplierType && art.Period == offer.Period && art.offerName == offer.offerName
                            select art).FirstAsync();

            IEnumerable<Subscriptions> currentSubscriptions = await (from art in _db.Subscriptions
                                                 where   art.Contact.Id == Contact.Id && art.endValidityDate> localDate && art.cancelsubscription !=1 orderby art.startvalidityDate  descending
                                                                     select art).ToListAsync();

            
           int  maxOfferOrder = 0;
            int maxIdOffer = 0;
            foreach (var prop in currentSubscriptions)
            {
                Offers OfferTmp = await (from art in _db.Offers
                               where art.id == prop.OfferId
                               select art).FirstAsync();

                if (OfferTmp.offerOrder > maxOfferOrder)
                    maxOfferOrder = OfferTmp.offerOrder;


                if (OfferTmp.id > maxIdOffer)
                    maxIdOffer = OfferTmp.id;
            }


            if (currentSubscriptions.Count() > 0 && maxOfferOrder < Offer.offerOrder) {
                var sql = "Update Subscriptions set cancelsubscription = 1, endValidityDate = '" + localDate + "' where Offerid = " + maxIdOffer;
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
                await _db.SaveChangesAsync();
            }

            if (currentSubscriptions.Count() > 0  && maxOfferOrder >= Offer.offerOrder)
            {
                return Json(new { success = false, message = "Vous avez déjà un abonnement en cours. Pour changer d'abonnement veillez supprimer au préalable l'abonnement en cours dans votre tableau de board" });
            }
            else {

            if (ModelState.IsValid)
             {
                 TempData["Message"] = "Client Details Edited Successfully";
                 Subscription.Employee = Employee;
                 Subscription.Contact = Contact;
                 Subscription.Offer = Offer;
                 Subscription.startvalidityDate = localDate;

                 if (Offer.Period == "Month")
                 {
                     Subscription.endValidityDate = localDate.AddMonths(1);
                 }

                 if (Offer.Period == "Year")
                 {
                     Subscription.endValidityDate = localDate.AddYears(1);
                 }


                 await _db.Subscriptions.AddAsync(Subscription);
                 await _db.SaveChangesAsync();

                 return Json(new { success = true, message = "Félicitation! Vous bénéficiez à présent des services " + offer.offerName  + " de Pharmashopcenter" });
                 // return RedirectToPage("Index");
             }
             else
             {
                 return Json(new { success = false, message = "Une erreur est survenue au moment de l'enregistrement de votre souscription" });
             }

       }
        }
    }
}

