﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Controllers
{
    [Route("api/address")]
    [ApiController]
    // [Route("api/brand")]
    //[ApiController]
    public class AdresseController : Controller
    //public class BrandController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;


        [BindProperty]
        public int _NbProductAutorized { get; set; }

        [BindProperty]
        public int _NbProductAlreadyChoose { get; set; }
        public IEnumerable<String> BrandsName { get; set; }

        public AdresseController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;
            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;

        }


        [HttpGet]
        public async Task<IActionResult> Search(string country, string codePostal, string city, string addresse)
        {
    
            string term = HttpContext.Request.Query["term"].ToString();
            if (term.Length > 3 )
            {
                try
                {
                  var postTitle = _db.ContactsStock.Where(p => p.address1 .Contains(term) && (p.PostalCode == codePostal )).Select(p => p.address1).ToList().Distinct();
                    if (postTitle == null)
                    {
                          postTitle = _db.Adresse.Where(p => p.adresse.Contains(term) && (p.postalecode == codePostal)).Select(p => p.fullAdress).ToList().Distinct();
                    }

                    return Ok(postTitle);
                }
                catch (Exception e)
                {
                    string t = e.Message;
                    return BadRequest();
                }


            }
            else
                return BadRequest();

        }


    }


}