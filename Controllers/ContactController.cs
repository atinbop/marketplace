﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace MarketPlace.Controllers
{
    [Route("api/contact")]
    [ApiController]
    public class ContactController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;
        public const string SessionKeyConnectedUserEmail = "_ConnectedUserEmail";
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        public IEnumerable<Article> Articles { get; set; }
        public ContactController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;

        }


        

        [HttpPost]
        public async Task<IActionResult> Post(int id, string note)
        {
            Notes Thenote = new Notes();
            Thenote.ArticleId = id;
            Thenote.Note = Int32.Parse(note);
            Thenote.UpdateByUserId = _ConnectedUserId;
            Thenote.LastUpdate = DateTime.Now;
            VendorsNotes notefromDb =null;

            try {
                  notefromDb = await (from art in _db.VendorsNotes
                                      where art.UpdateByUserId == _ConnectedUserId && art.ContactId == id select art).FirstOrDefaultAsync();
            }
            catch (Exception e){

               string  t= e.Message;


            }

            if (notefromDb != null) {

                notefromDb.Note = Thenote.Note;

                notefromDb.LastUpdate = Thenote.LastUpdate;
                await _db.SaveChangesAsync();
            }
            else {
                await _db.Notes.AddAsync(Thenote);
                await _db.SaveChangesAsync();
            }
            
           
            //Mise à jour de la note moyenne de l'article
            var contact = await _db.Contacts.FindAsync(id);
            contact.Note = Convert.ToInt32(await _db.Notes.Where(x => x.Id == id).AverageAsync(x => x.Note));

            await _db.SaveChangesAsync();

            return Json(new { success = true, message = "Note enregistrée" });

        }


        public ViewResult Details()
        {

            return View();
        }





    }

}

