﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace MarketPlace.Controllers
{
    [Route("api/article")]
    [ApiController]
    public class ArticleController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;
        public const string SessionKeyConnectedUserEmail = "_ConnectedUserEmail";
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static string _UserCompanyId;
        public static String _UserCompanyName;
        public static String _UserCurrency;
        public static String _UserLanguage;

        public IEnumerable<Article> Articles { get; set; }
        public ArticleController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
      
            _imageRepository = options.ImageRepository;

        }


        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
            _ConnectedUserId = Request.Cookies["ConnectedUserId"];
            _UserCompanyType = Request.Cookies["UserCompanyType"];
            _UserCompanyId = Request.Cookies["UserCompanyId"];
            _UserCompanyName = Request.Cookies["UserCompanyName"];
            _UserCurrency = Request.Cookies["UserCurrency"];
            _UserLanguage = Request.Cookies["PreferedLanguage"];

            //Articles = await _db.Article.ToListAsync(); 
            if (User.Identity.IsAuthenticated)
            {
                if (_UserCompanyType == "B2B" || _UserCompanyType == "B2C")
                {
                    try
                    {

                        Articles = _db.Article.FromSqlRaw<Article>("spGetAutorizedArticlesB2B_B2C {0} , {1}", _UserCompanyType, _UserCompanyId).ToList();
                    }
                    catch (Exception ex)
                    {
                        string t = ex.Message;
                        t = "";

                    }
                }


                if (_UserCompanyType == "PRO")
                {

                    Articles = _db.Article.FromSqlRaw<Article>("spGetAutorizedArticlesPRO {0} , {1}", _UserCompanyType, _UserCompanyId).ToList();
                }


                //Listes des Posts Non lus
                try
                {
                    List<Parameters> UnreadMessages = _db.Parameters.FromSqlRaw<Parameters>("spGetUnreadMessagesNumber {0}", _UserCompanyType).ToList();

                    foreach (Article prop in Articles)
                    {
                        foreach (Parameters prop1 in UnreadMessages)
                        {
                            if (prop.Id == prop1.Id)
                            { prop.UnreadMessagesNumber = prop1.Value; }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string t = ex.Message;
                }



                return Json(new { data = Articles });
            }
            else
                return Json(new { data = Articles });


        }


        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
            {


                ViewBag.ImageRepository = configuration.GetSection("SolutionAppSettings").GetSection("ImageRepository").Value;
                int noOfRowUpdated;
                if (_UserCompanyType == "PRO")
                {

                    var bookFromDb = await _db.Article.FirstOrDefaultAsync(u => u.Id == id);
                    if (bookFromDb == null)
                    {
                        return Json(new { success = false, message = "Error while Deleting" });
                    }
                    //_db.Article.Remove(bookFromDb);
                    noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update article set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _UserCompanyId + "' where id = " + id);
                    await _db.SaveChangesAsync();
                    return Json(new { success = true, message = "Suppression réussie." });
                }

                noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update ProductSubscriptions set cancelsubscription=1 , endValidityDate=getdate(),   employeeId='" + _UserCompanyId + "' where articleid = " + id + " and contactid=" + _UserCompanyId);

                noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update ArticleVendors set Deleted=1 , LastUpdate=getdate(),    employeeId='" + _UserCompanyId + "' where articleid = " + id + " and contactid=" + _UserCompanyId);


                await _db.SaveChangesAsync();
                return Json(new { success = true, message = "Retrait réussie." });
           
        }


        [HttpPost]
        public async Task<IActionResult> Post(int id, string note)
        {
            Notes Thenote = new Notes();
            Thenote.ArticleId = id;
            Thenote.Note = Int32.Parse(note);
            Thenote.UpdateByUserId = _ConnectedUserId;
            Thenote.LastUpdate = DateTime.Now;
            Notes notefromDb =null;

            try {
                  notefromDb = await (from art in _db.Notes
                                          where art.UpdateByUserId == _ConnectedUserId && art.ArticleId == id select art).FirstOrDefaultAsync();
            }
            catch (Exception e){

               string  t= e.Message;


            }

            if (notefromDb != null) {

                notefromDb.Note = Thenote.Note;

                notefromDb.LastUpdate = Thenote.LastUpdate;
                await _db.SaveChangesAsync();
            }
            else {
                await _db.Notes.AddAsync(Thenote);
                await _db.SaveChangesAsync();
            }
            
           
            //Mise à jour de la note moyenne de l'article
            var article = await _db.Article.FindAsync(id);
            article.Note = Convert.ToInt32(await _db.Notes.Where(x => x.ArticleId == id).AverageAsync(x => x.Note));

            await _db.SaveChangesAsync();

            return Json(new { success = true, message = "Note enregistrée" });

        }


        public ViewResult Details()
        {

            return View();
        }





    }

}

