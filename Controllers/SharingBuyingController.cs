﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using iText.IO.Util;
using MarketPlace.Model;
using MarketPlace.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Update;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace MarketPlace.Controllers
{
    [Route("api/sharingbuying")]
    [ApiController]
    public class SharingBuyingController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static string _UserCompanyId;
        public static String _UserCompanyName;
        public static string _UserCurrency;
        public static string _UserLanguage;
        public List<ArticleVendors> ArticleVendors { get; set; }
        private readonly IStringLocalizer _identityLocalizer;
        private readonly IStringLocalizer _localizer2;
        public List<Parameters> Parameters { get; set; }
        public SharingBuyingController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options, IStringLocalizerFactory factory)
        {
            _db = db;

            configuration = iConfig;

            _imageRepository = options.ImageRepository;


            var type = typeof(IdentityResource);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _identityLocalizer = factory.Create("IdentityResource", assemblyName.Name);
            _localizer2 = factory.Create("SharedResource", assemblyName.Name);

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAllDiscountDate(int id)
        {

            _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
            _ConnectedUserId = Request.Cookies["ConnectedUserId"];
            _UserCompanyType = Request.Cookies["UserCompanyType"];
            _UserCompanyId = Request.Cookies["UserCompanyId"];
            _UserCompanyName = Request.Cookies["UserCompanyName"];
            _UserCurrency = Request.Cookies["UserCurrency"];
            _UserLanguage = Request.Cookies["PreferedLanguage"];

            Contacts contacts = null;
            Currency SupplierCurrency = null;
            Currency UserCurrencyObj = null;
            List<ExchangeRate> ExchangeRates = null;

            List<Discounts> ParamNextDiscountRateTarget = null;
            try
            {

                ParamNextDiscountRateTarget = _db.Discounts.FromSqlRaw<Discounts>("spGetActiveDiscountRate ").ToList();

                UserCurrencyObj = (from art in _db.Currency
                                   where art.AlphabeticCode == _UserCurrency
                                   select art).FirstOrDefault();

                foreach (Discounts pro in ParamNextDiscountRateTarget)
                {

                    contacts = (from art in _db.Contacts
                                where art.Id == pro.ContactId
                                select art).FirstOrDefault();

                    SupplierCurrency = (from art in _db.Currency
                                        where art.AlphabeticCode == contacts.Currency
                                        select art).FirstOrDefault();



                    ExchangeRates = _db.ExchangeRate.FromSqlRaw<ExchangeRate>("spGetExchangerate {0}, {1}", SupplierCurrency.AlphabeticCode, _UserCurrency).ToList();


                    pro.BasePrice = Math.Round((decimal)(pro.BasePrice * ExchangeRates.First().Rate), 2);
                    pro.DiscountPrice1 = Math.Round((decimal)((Decimal.Parse(pro.DiscountPrice1) * ExchangeRates.First().Rate)), 2).ToString();
                    pro.DiscountPrice2 = Math.Round((decimal)((Decimal.Parse(pro.DiscountPrice2) * ExchangeRates.First().Rate)), 2).ToString();
                    pro.DiscountPrice3 = Math.Round((decimal)((Decimal.Parse(pro.DiscountPrice3) * ExchangeRates.First().Rate)), 2).ToString();
                }



            }
            catch (Exception e)
            {

                string t = e.Message;
            }


            return Json(ParamNextDiscountRateTarget);
        }


        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
            _ConnectedUserId = Request.Cookies["ConnectedUserId"];
            _UserCompanyType = Request.Cookies["UserCompanyType"];
            _UserCompanyId = Request.Cookies["UserCompanyId"];
            _UserCompanyName = Request.Cookies["UserCompanyName"];
            _UserCurrency = Request.Cookies["UserCurrency"];
            _UserLanguage = Request.Cookies["PreferedLanguage"];

            try
            {
                ArticleVendors = _db.ArticleVendors.FromSqlRaw<ArticleVendors>("spGetSharingBuyingArticles {0}", Request.Cookies["ConnectedUserId"]).ToList();
            }
            catch (Exception ex)
            {

                string t = ex.Message;

            }


            int nbArticleOutput = 0;
            Parameters = new List<Parameters>();
            //Premiere colonne




            for (int i = 0; i < ArticleVendors.Count() - ArticleVendors.Count() % 3; i = i + 3)
            {
                Parameters param = new Parameters();
                param.Id = nbArticleOutput;



                param.Name = "<div class=\"container\"><div class=\"row pt-4\"><div class=\"col-lg-12\"><div class=\"row\" > "
                        + DrawProduct(ArticleVendors[i])
                        + DrawProduct(ArticleVendors[i + 1])
                        + DrawProduct(ArticleVendors[i + 2]) +

        "                        </div>" +
        "                        </div>" +
        "                        </div>" +
        "                        </div>"
                        ;


                nbArticleOutput = nbArticleOutput + 1;
                Parameters.Add(param);
            }


            int NbArticleRestant = 3 - ArticleVendors.Count() % 3;

            //1er cas si le reste = 1
            if (NbArticleRestant == 1)
            {
                int i = ArticleVendors.Count() - 1;
                Parameters param = new Parameters();
                param.Id = i;



                param.Name =
                    "  <div class=\"container\"><div class=\"row pt-4\"><div class=\"col-lg-12\"><div class=\"row\" > " +
                        DrawProduct(ArticleVendors[i]) +
   "                        </div>" +
   "                        </div>" +
   "                        </div>" +
   "                        </div>";
                Parameters.Add(param);

            }


            //1er cas si le reste = 2
            if (NbArticleRestant == 2)
            {
                int i = ArticleVendors.Count() - 2;
                Parameters param = new Parameters();
                param.Id = i;

                param.Name =
                   "  <div class=\"container\"><div class=\"row pt-4\"><div class=\"col-lg-12\"><div class=\"row\" > " +
                        DrawProduct(ArticleVendors[i])
                        + DrawProduct(ArticleVendors[i + 1]) +
 "                        </div>" +
   "                        </div>" +
   "                        </div>" +
   "                        </div>";
                Parameters.Add(param);
            }



            return Json(new { data = Parameters });

        }


        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {

            _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
            _ConnectedUserId = Request.Cookies["ConnectedUserId"];
            _UserCompanyType = Request.Cookies["UserCompanyType"];
            _UserCompanyId = Request.Cookies["UserCompanyId"];
            _UserCompanyName = Request.Cookies["UserCompanyName"];
            _UserCurrency = Request.Cookies["UserCurrency"];
            _UserLanguage = Request.Cookies["PreferedLanguage"];

            ViewBag.ImageRepository = configuration.GetSection("SolutionAppSettings").GetSection("ImageRepository").Value;
            int noOfRowUpdated;
            if (_UserCompanyType == "PRO")
            {

                var bookFromDb = await _db.Article.FirstOrDefaultAsync(u => u.Id == id);
                if (bookFromDb == null)
                {
                    return Json(new { success = false, message = "Error while Deleting" });
                }
                //_db.Article.Remove(bookFromDb);
                noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update article set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "' where id = " + id);
                await _db.SaveChangesAsync();
                return Json(new { success = true, message = "Suppression réussie." });
            }

            noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update ProductSubscriptions set cancelsubscription=1 , endValidityDate=getdate(),   employeeId='" + _ConnectedUserId + "' where articleid = " + id + " and contactid=" + _UserCompanyId);

            noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update ArticleVendors set Deleted=1 , LastUpdate=getdate(),    employeeId='" + _ConnectedUserId + "' where articleid = " + id + " and contactid=" + _UserCompanyId);


            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Retrait réussie." });

        }


        [HttpPost]
        public async Task<IActionResult> Post(int id, string note)
        {
        

            Notes Thenote = new Notes();
            Thenote.ArticleId = id;
            Thenote.Note = Int32.Parse(note);
            Thenote.UpdateByUserId = _ConnectedUserId;
            Thenote.LastUpdate = DateTime.Now;
            Notes notefromDb = null;

            try
            {
                notefromDb = await (from art in _db.Notes
                                    where art.UpdateByUserId == _ConnectedUserId && art.ArticleId == id
                                    select art).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {

                string t = e.Message;


            }

            if (notefromDb != null)
            {

                notefromDb.Note = Thenote.Note;

                notefromDb.LastUpdate = Thenote.LastUpdate;
                await _db.SaveChangesAsync();
            }
            else
            {
                await _db.Notes.AddAsync(Thenote);
                await _db.SaveChangesAsync();
            }


            //Mise à jour de la note moyenne de l'article
            var article = await _db.Article.FindAsync(id);
            article.Note = Convert.ToInt32(await _db.Notes.Where(x => x.ArticleId == id).AverageAsync(x => x.Note));



            await _db.SaveChangesAsync();

            return Json(new { success = true, message = "Note enregistrée" });

        }


        public ViewResult Details()
        {

            return View();
        }

        public string DrawProduct(ArticleVendors ArticleVendors/*, List<Parameters> ParamNextDiscountRateTarget*/)
        {

           

            string result="";
            int nbdiscount = 0;
            //Ya il une remise sur le produit?

            try
            {
                List<Discounts> DiscountsCount = _db.Discounts.FromSqlRaw<Discounts>("spGetActiveDiscountRateDetails {0}", ArticleVendors.Id).ToList();
                nbdiscount = DiscountsCount.Count();
            }
            catch (Exception ex)
            {
                string t = ex.Message;

            }
           
            string etoile;
            string max;
            string min;
            Contacts contacts = null;
            Currency SupplierCurrency = null;
            Currency UserCurrencyObj = null;
            List<ExchangeRate> ExchangeRates = null;
           
            try
            {
                contacts = (from art in _db.Contacts
                            where art.Id == ArticleVendors.contactId
                            select art).FirstOrDefault();

                SupplierCurrency = (from art in _db.Currency
                                    where art.AlphabeticCode == contacts.Currency
                                    select art).FirstOrDefault();

                UserCurrencyObj = (from art in _db.Currency
                                   where art.AlphabeticCode == _UserCurrency
                                   select art).FirstOrDefault();

                ExchangeRates = _db.ExchangeRate.FromSqlRaw<ExchangeRate>("spGetExchangerate {0}, {1}", SupplierCurrency.AlphabeticCode, UserCurrencyObj.AlphabeticCode).ToList();

                ArticleVendors.Price = Math.Round((decimal)(ArticleVendors.Price * ExchangeRates.First().Rate), 2);
         

            if (ArticleVendors.Lot == null || ArticleVendors.Lot == 0)
            {
                ArticleVendors.Lot = 1;
            }

            if (ArticleVendors.Lot == null || ArticleVendors.Lot == 0)
            {
                ArticleVendors.Lot = 1;
            }
            string step = "step=" + ArticleVendors.Lot;

            if (ArticleVendors.MaximumOrderQuantity == null || ArticleVendors.MaximumOrderQuantity == 0)
            {
                max = "";
            }
            else
            { max = "max=" + ArticleVendors.MaximumOrderQuantity.ToString(); }

            if (ArticleVendors.MinimumOrderQuantity == null || ArticleVendors.MinimumOrderQuantity == 0)
            {
                if (ArticleVendors.Lot != 0)
                {
                    min = "min=" + ArticleVendors.Lot.ToString();
                }
                else

                {
                    min = "min=1";
                }

                ArticleVendors.MinimumOrderQuantity = 1;
            }
            else
            { min = "min=" + ArticleVendors.MinimumOrderQuantity.ToString(); }


            decimal prixUnitaire = (decimal)(ArticleVendors.Price * (decimal)1.0 / ArticleVendors.Lot) * (decimal)1.0;


            if (nbdiscount == 0)
            {
                   result =
                        "<div class=\" carteproduit col-lg-4 col-md-12 mb-4    \" > " +
                                    "<div class=\"  card     hoverable card-ecommerce\">" +
                "                            <!-- Card image -->" +
                "                            <div class=\"view overlay\">" +
    "<div class=\"view overlay zoom\"> <img id=\"myImg\" class=\"img-fluid\" src =" + ArticleVendors.Image1 + " width='180' height='180'   alt=\"" + ArticleVendors.Name + "\" style=\"min-width: 200px;min-height: 200px;max-width: 200px;max-height: 200px;\"> <div class=\"mask flex-center waves-effect waves-light\"> </div> </div>" +


                "                                <a  href=\"/Market/ArticleDetails?articleVendorsId=" + ArticleVendors.Id + "\"> " +
                "                                    <div class=\"mask rgba-white-slight\"></div>" +
                "                                </a>" +
                "                            </div>" +
                "                            <!-- Card image -->" +
                "                            <!-- Card content -->" +
                "                            <div class=\"card-body\">" +
                "                                <!-- Category & Title -->" +
                "                                <h5 class=\" mb-1\" style =\"font-size: medium\">" +
                "                                    <strong>" +
                "                                        <a href=\"/Market/ArticleDetails?articleVendorsId=" + ArticleVendors.Id + "\" class=\"dark-grey-text\">" + "<span style=\"width: 300px; height:60px; display: inline-block;\"> " + ((ArticleVendors.Name.Length <= 60 ? ArticleVendors.Name : ArticleVendors.Name.Substring(0, 60))) + " </span></a>" +
           "                                    </strong>" +
           "                                </h5>" +


                "                                    <div class=\"row\"><span class=\" text-black-50 ml-2\" >" +
    "                                             EAN: " + ArticleVendors.EAN + "" + "" +
           "                                    </span></div>" +
                     "                                    <div class=\"row\"><span class=\" text-black-50 ml-2\" >" +
"                                            "+ _localizer2["SELLER"] + "     : <a asp-page='/Contact/Fiche' asp-route-articleVendorsId=" + ArticleVendors.Id + " > " + contacts.Company + "</a>" +
           "                                    </span></div>" +
                  "                                    <div class=\"row\"><span class=\" text-black-50 ml-2\" >" +
    "                                              " +
           "                                    </span></div>" +
          "                                <div class=\"card-footer   pb-0\" style=\"        \">" +
           "                                    <div class=\"row mb-0\">" +
           "                                            <strong   id=\"PT" + ArticleVendors.Id + "\" class=\"total textevert \" style=\"font-size: xx-large\">" + ArticleVendors.Price + "</strong class=\"total textevert \" style=\"font-size: xx-large\"><strong   class=\"total textevert \" style=\"font-size: xx-large\">&nbsp;" + UserCurrencyObj.symbol + "</strong>" + "<strong class=\"textevert\" style =\"font-size: small\">" + ArticleVendors.PriceType + "</strong>" +
           "<span class=\"float-left text-black-50 ml-2\" data-toggle=\"tooltip\" data-placement=\"top\">" +
           "                                     " + _localizer2["SET_OF"]  + "  "   +  ArticleVendors.Lot + "" + "" +
           "                                        </span>" +
              "  <input  hidden id=\"P" + ArticleVendors.Id + "\" value=\"" + ArticleVendors.Price + "\" > " +
              "  <input hidden id=\"L" + ArticleVendors.Id + "\" value=\"" + ArticleVendors.Lot + "\" > " +
              "  <input hidden id=\"M" + ArticleVendors.Id + "\" value=\"" + ArticleVendors.MinimumOrderQuantity + "\" ></div></div> " +

          "                                <div class=\"card-footer   pb-0\" style=\" height:120px; display: inline-block;\">" +
           "                                    <div class=\"row mb-0\">" +
          "<div    class=\"  align-middle text-black-50 pt-3 ml-2 mr-2\"  style =\" vertical-align: middle\"  >" +
           "                                           <span  id=\"OBJECTIF_HIDDEN" + ArticleVendors.Id + "\"     >    </span> " +
           "                                        </div>" +

           "                                             <strong id=\"TARGETPRICE_HIDDEN" + ArticleVendors.Id + "\" class=\"total textevertbleu \" style=\"font-size: xx-large\">  </strong>" + "<strong id=\"TARGETPRICELABELHT_HIDDEN" + ArticleVendors.Id + "\" class=\"total textevertbleu \" class=\"textevertbleu\" style =\"font-size: small\"> </strong>" + "<s  style =\"font-size: small; color:red\"><strong  id=\"PT1_HIDDEN" + ArticleVendors.Id + "\"  class=\"textevertbleu ml-2\" style =\"font-size: large\">  </strong></s>" +
    "</div></div> " +

           "                                <div id=\"DRQTDIV_HIDDEN" + ArticleVendors.Id + "\" class =\"row mb-1 \" style=\" height:25px;\"><div   id=\"DRDIV_HIDDEN" + ArticleVendors.Id + "\" class =\"col-6 pull-left\"><span id=\"DR_HIDDEN" + ArticleVendors.Id + "\" class=\" text-lg-center   mb-2 \" style=\"  height:25px; display: inline-block; color:red  ; font-size:medium\"></span></div>" +
           "                                <div id=\"QTDIV_HIDDEN" + ArticleVendors.Id + "\"  class =\"col-6 align-items-end    float-right pull-right\"><span id=\"QT_HIDDEN" + ArticleVendors.Id + "\" class=\" text-lg-center align-items-end    float-right pull-right mb-2 \" style=\"width: 140px; height:25px; display: inline-block;font-size:medium ;color:#76B72A;\"></span></div></div>" +
    "<div   class=\" \" style=\"  height:30px; display: inline-block; color:red  ; font-size:large\"> " +
    "  <div id=\"PALL_HIDDEN" + ArticleVendors.Id + "\" class=\"progress-bar    boutonvertbleu progress-bar progress-bar-animated progress-bar-striped\" style=\"\">" +

    "  </div>" +
    "  <div id=\"PUSER_HIDDEN" + ArticleVendors.Id + "\"  class=\"progress-bar  boutonvert   progress-bar progress-bar-animated progress-bar-striped\" style=\"\">" +

    "  </div>" +

    "</div>" +

           "                                <!-- Card footer -->" +
           "                                <div class=\"card-footer   mt-1   d-flex  h-100 justify-content-center align-items-center pb-0\">" +
           "                                    <div class=\"row mb-0\">" +
          "<div class=\"def-number-input number-input mb-1 safari_only\">" +
       "  <button onclick=\"this.parentNode.querySelector('input[type=number]').stepDown();  document.getElementById('TCUR" + ArticleVendors.Id + "').innerHTML ='&nbsp;" + UserCurrencyObj.symbol + "';   document.getElementById('TPRICETYPE" + ArticleVendors.Id + "').innerHTML ='" + ArticleVendors.PriceType + "'; document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value *  parseFloat(document.getElementById('P" + ArticleVendors.Id + "').value .replace(',','.').replace(' ',''))  /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))  ;document.getElementById('PUSER" + ArticleVendors.Id + "').innerHTML= document.getElementById('I" + ArticleVendors.Id + "').value;\"  class=\"minus\"></button>" +
       "  <input id=\"I" + ArticleVendors.Id + "\"  class=\"quantity \"  " + step + " " + max + " " + min + " maxlength=\"6\" name=\"quantity\" value=\"" + ArticleVendors.MinimumOrderQuantity + "\" type=\"number\" oninput=\"if ( parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))<100000000) document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))  ; else {document.getElementById('I" + ArticleVendors.Id + "').value=0;document.getElementById('T" + ArticleVendors.Id + "').innerHTML='0 ';}\"onkeyup=\"if ( parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))<100000000) document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2)) +' ';else {document.getElementById('I" + ArticleVendors.Id + "').value=0;document.getElementById('T" + ArticleVendors.Id + "').innerHTML='0 ';}\">" +
       "  <button onclick=\"this.parentNode.querySelector('input[type=number]').stepUp(); document.getElementById('TCUR" + ArticleVendors.Id + "').innerHTML ='&nbsp;" + UserCurrencyObj.symbol + "';   document.getElementById('TPRICETYPE" + ArticleVendors.Id + "').innerHTML ='" + ArticleVendors.PriceType + "'; document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value *   parseFloat(document.getElementById('P" + ArticleVendors.Id + "').value .replace(',','.').replace(' ',''))  /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))  ;document.getElementById('PUSER" + ArticleVendors.Id + "').innerHTML= document.getElementById('I" + ArticleVendors.Id + "').value; \"    class=\"plus\"></button>" +
       "</div> " +
       "<div class =\"ml-3\">" +

               "                                        <span class=\"float-right\">" +
            "                                            <a class=\"\" data-toggle=\"tooltip\" data-placement=\"top\"    onclick=\"InsertintoChart('" + ArticleVendors.Id + "','" + ArticleVendors.articleId + "','" + UserCurrencyObj.symbol + "','" + ArticleVendors.PriceType + "','" + ArticleVendors.Image1 + "','" + ArticleVendors.Name + "','" + ArticleVendors.Brand + "','" + ArticleVendors.BrandId + "','" + contacts.Company + "','" + ArticleVendors.Lot + "','" + ArticleVendors.MinimumOrderQuantity + "','" + ArticleVendors.MaximumOrderQuantity + "','" + ArticleVendors.VAT + "'); \" title=\"Ajouter au panier\">" +
           "                                                <i class=\"  text-lg-center fas fa-cart-plus ml-3\"  style=\"        font-size: xx-large;  \"></i>" +
    "                                                        </a></Span>" +

       "</div>" +


           "                                    </div>" +
           "                                </div>" +

           "                                <!-- Card footer -->" +
           "                                <div class=\"card-footer pb-0\" >" +
           "                                    <div class=\"row mb-0\" >" +


           "  <strong style=\"font-size: xx-large\" class=\"total textevertbleu  \">Total:&nbsp;</strong><strong style=\"font-size: large\" class=\"total  textevertbleu \" id=\"T" + ArticleVendors.Id + "\" ></strong> <strong style=\"font-size: large\" id=\"TCUR" + ArticleVendors.Id + "\"  class=\"total  textevertbleu \">   </strong>" + "<strong id=\"TPRICETYPE" + ArticleVendors.Id + "\"  class=\"textevertbleu\" style =\"font-size: small\"></strong>" +
                   "                                      <div  style=\"   height:45px; display: inline-block;      \"></div>" +
           "                                       <span class=\"ml-3" + " grey-text\"> </span>" +
           "                        <div id=\"MINUTEUR" + ArticleVendors.Id + "\"   class=\" container-fluid  grey-text d-flex justify-content-end  pull-right \" style=\"    font-family: Digital-7; height:25px; display: inline-block;   font-size: small;  \">" +

    "                            <span class=\"textebleu\"   id =\"days" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +


    "                            <span class=\"textebleu\" id=\"hours" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +


    "                            <span class=\"textebleu\" id=\"minutes" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +


    "                            <span class=\"textebleu\" id=\"seconds" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +

    "                        </div > " +
           "                                    </div>" +
           "                                </div>" +
           "                            </div>" +
           "                            <!-- Card content -->" +
               "                        </div>" +
           "                        </div>";

            }
            else
            {


                result =
                         "<div class=\" carteproduit col-lg-4 col-md-12 mb-4    \" > " +
                                     "<div class=\"  card     hoverable card-ecommerce\">" +
                 "                            <!-- Card image -->" +
                 "                            <div class=\"view overlay\">" +
    "<div class=\"view overlay zoom\"> <img id=\"myImg\" class=\"img-fluid\" src =" + ArticleVendors.Image1 + " width='180' height='180'   alt=\"" + ArticleVendors.Name + "\" style=\"min-width: 200px;min-height: 200px;max-width: 200px;max-height: 200px;\"> <div class=\"mask flex-center waves-effect waves-light\"> </div> </div>" +


                 "                                <a  href=\"/Market/ArticleDetails?articleVendorsId=" + ArticleVendors.Id + "\"> " +
                 "                                    <div class=\"mask rgba-white-slight\"></div>" +
                 "                                </a>" +
                 "                            </div>" +
                 "                            <!-- Card image -->" +
                 "                            <!-- Card content -->" +
                 "                            <div class=\"card-body\">" +
                 "                                <!-- Category & Title -->" +
                 "                                <h5 class=\" mb-1\" style =\"font-size: medium\">" +
                 "                                    <strong>" +
                 "                                        <a href=\"/Market/ArticleDetails?articleVendorsId=" + ArticleVendors.Id + "\" class=\"dark-grey-text\">" + "<span style=\"width: 300px; height:60px; display: inline-block;\"> " + ((ArticleVendors.Name.Length <= 60 ? ArticleVendors.Name : ArticleVendors.Name.Substring(0, 60))) + " </span></a>" +
            "                                    </strong>" +
            "                                </h5>" +


                 "                                    <div class=\"row\"><span class=\" text-black-50 ml-2\" >" +
     "                                             EAN: " + ArticleVendors.EAN + "" + "" +
            "                                    </span></div>" +
                      "                                    <div class=\"row\"><span class=\" text-black-50 ml-2\" >" +
     "                                     "  + _localizer2["SELLER"]               +  "     : <a asp-page='/Contact/Fiche' asp-route-articleVendorsId=" + ArticleVendors.Id + " > " + contacts.Company + "</a>"+
            "                                    </span></div>" +
                   "                                    <div class=\"row\"><span class=\" text-black-50 ml-2\" >" +
     "                                   " + _localizer2["DELIVERY_AREA"]               +" " + ArticleVendors.SaleAreaFullName +
            "                                    </span></div>" +
           "                                <div class=\"card-footer   pb-0\" style=\"        \">" +
            "                                    <div class=\"row mb-0\">" +
            "                                            <strong   id=\"PT" + ArticleVendors.Id + "\" class=\"total textevert \" style=\"font-size: xx-large\"></strong class=\"total textevert \" style=\"font-size: xx-large\"><strong   class=\"total textevert \" style=\"font-size: xx-large\">&nbsp;" + UserCurrencyObj.symbol + "</strong>" + "<strong class=\"textevert\" style =\"font-size: small\">" + ArticleVendors.PriceType + "</strong>" + "<s  style =\"font-size: small; color:red\"><strong  id=\"PP" + ArticleVendors.Id + "\" class=\"textevertbleu ml-2\" style =\"font-size: large\">   </strong></s>" +
            "<span class=\"float-left text-black-50 ml-2\" data-toggle=\"tooltip\" data-placement=\"top\">" +
            "                                     "  + _localizer2["SET_OF"]               + "          " + ArticleVendors.Lot + "" + "" +
            "                                        </span>" +
               "  <input  hidden id=\"P" + ArticleVendors.Id + "\" > " +
               "  <input hidden id=\"L" + ArticleVendors.Id + "\" value=\"" + ArticleVendors.Lot + "\" > " +
               "  <input hidden id=\"M" + ArticleVendors.Id + "\" value=\"" + ArticleVendors.MinimumOrderQuantity + "\" ></div></div> " +

           "                                <div class=\"card-footer   pb-0\" style=\" height:120px; display: inline-block;\">" +
            "                                    <div class=\"row mb-0\">" +
           "<div    class=\"  align-middle text-black-50 row pt-3 ml-2 mr-2\"  style =\" vertical-align: middle\"  >" +
            "                                           <span  id=\"OBJECTIF" + ArticleVendors.Id + "\"     >  "+ _localizer2["TARGET"] + "     </span> " +
            "                                        </div>" +

            "                                            <div  class=\"  align-middle text-black-50 row pt-3 ml-2 mr-2\"  style =\" vertical-align: middle\"  > <strong id=\"TARGETPRICE" + ArticleVendors.Id + "\" class=\"total textevertbleu \" style=\"font-size: large\">  </strong><strong   class=\"total textevertbleu \" style=\"font-size: large\">&nbsp;" + UserCurrencyObj.symbol + " </strong><strong id=\"TARGETPRICELABELHT" + ArticleVendors.Id + "\" class=\"total textevertbleu \" class=\"textevertbleu\" style =\"font-size: small\">" + ArticleVendors.PriceType + "</strong>" + "<s  style =\"font-size: small; color:red\"><strong  id=\"PT1" + ArticleVendors.Id + "\"  class=\"textevertbleu ml-2\" style =\"font-size: large\">  </strong><strong     class=\"textevertbleu ml-2\" style =\"font-size: large\">" + UserCurrencyObj.symbol + " </strong></s></div>" +
     "</div></div> " +

            "                                <div id=\"DRQTDIV" + ArticleVendors.Id + "\" class =\"row mb-1 \" style=\" height:25px;\"><div   id=\"DRDIV" + ArticleVendors.Id + "\" class =\"col-6 pull-left\"><span id=\"DR" + ArticleVendors.Id + "\" class=\" text-lg-center   mb-2 \" style=\"  height:25px; display: inline-block; color:red  ; font-size:medium\"></span><span>  "+ _localizer2["DISCOUNT_LABEL"]+ "    </span></div>" +
            "                                <div id=\"QTDIV" + ArticleVendors.Id + "\"  class =\"col-6 align-items-end    float-right pull-right\"><span id=\"QT" + ArticleVendors.Id + "\" class=\" text-lg-center align-items-end    float-right pull-right mb-2 \" style=\"width: 140px; height:25px; display: inline-block;font-size:medium ;color:#76B72A;\"></span></div></div>" +
    "<div class=\"progress\">" +
    "  <div id=\"PALL" + ArticleVendors.Id + "\" class=\"progress-bar    boutonvertbleu progress-bar progress-bar-animated progress-bar-striped\" style=\"\">" +

    "  </div>" +
    "  <div id=\"PUSER" + ArticleVendors.Id + "\"  class=\"progress-bar  boutonvert   progress-bar progress-bar-animated progress-bar-striped\" style=\"\">" +

    "  </div>" +

    "</div>" +

            "                                <!-- Card footer -->" +
            "                                <div class=\"card-footer   mt-1   d-flex  h-100 justify-content-center align-items-center pb-0\">" +
            "                                    <div class=\"row mb-0\">" +
           "<div class=\"def-number-input number-input mb-1 safari_only\">" +
        "  <button onclick=\"this.parentNode.querySelector('input[type=number]').stepDown();  document.getElementById('TCUR" + ArticleVendors.Id + "').innerHTML ='&nbsp;" + UserCurrencyObj.symbol + "';   document.getElementById('TPRICETYPE" + ArticleVendors.Id + "').innerHTML ='" + ArticleVendors.PriceType + "'; document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value *  parseFloat(document.getElementById('P" + ArticleVendors.Id + "').value .replace(',','.').replace(' ',''))  /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))  ;document.getElementById('PUSER" + ArticleVendors.Id + "').innerHTML= document.getElementById('I" + ArticleVendors.Id + "').value;\"  class=\"minus\"></button>" +
        "  <input id=\"I" + ArticleVendors.Id + "\"  class=\"quantity \"  " + step + " " + max + " " + min + " maxlength=\"6\" name=\"quantity\" value=\"" + ArticleVendors.MinimumOrderQuantity + "\" type=\"number\" oninput=\"if ( parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))<100000000) document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))  ; else {document.getElementById('I" + ArticleVendors.Id + "').value=0;document.getElementById('T" + ArticleVendors.Id + "').innerHTML='0 ';}\"onkeyup=\"if ( parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))<100000000) document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value * document.getElementById('P" + ArticleVendors.Id + "').value /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2)) +' ';else {document.getElementById('I" + ArticleVendors.Id + "').value=0;document.getElementById('T" + ArticleVendors.Id + "').innerHTML='0 ';}\">" +
        "  <button onclick=\"this.parentNode.querySelector('input[type=number]').stepUp(); document.getElementById('TCUR" + ArticleVendors.Id + "').innerHTML ='&nbsp;" + UserCurrencyObj.symbol + "';   document.getElementById('TPRICETYPE" + ArticleVendors.Id + "').innerHTML ='" + ArticleVendors.PriceType + "'; document.getElementById('T" + ArticleVendors.Id + "').innerHTML  = parseFloat((Math.round(document.getElementById('I" + ArticleVendors.Id + "').value *   parseFloat(document.getElementById('P" + ArticleVendors.Id + "').value .replace(',','.').replace(' ',''))  /document.getElementById('L" + ArticleVendors.Id + "').value * 100) / 100).toFixed(2))  ;document.getElementById('PUSER" + ArticleVendors.Id + "').innerHTML= document.getElementById('I" + ArticleVendors.Id + "').value; \"    class=\"plus\"></button>" +
        "</div> " +
        "<div class =\"ml-3\">" +

                "                                        <span class=\"float-right\">" +
            "                                            <a class=\"\" data-toggle=\"tooltip\" data-placement=\"top\"    onclick=\"InsertintoChart('" + ArticleVendors.Id + "','" + ArticleVendors.articleId + "','" + UserCurrencyObj.symbol + "','" + ArticleVendors.PriceType + "','" + ArticleVendors.Image1 + "','" + ArticleVendors.Name + "','" + ArticleVendors.Brand + "','" + ArticleVendors.BrandId + "','" + contacts.Company + "','" + ArticleVendors.Lot + "','" + ArticleVendors.MinimumOrderQuantity + "','" + ArticleVendors.MaximumOrderQuantity + "','" + ArticleVendors.VAT + "'); \" title=\"Ajouter au panier\">" +
            "                                                <i class=\"  text-lg-center fas fa-cart-plus ml-3\"  style=\"        font-size: xx-large;  \"></i>" +
    "                                                        </a></Span>" +

        "</div>" +

            "                                    </div>" +
            "                                </div>" +

            "                                <!-- Card footer -->" +
            "                                <div class=\"card-footer pb-0\" >" +
            "                                    <div class=\"row mb-0\" >" +


            "  <strong style=\"font-size: xx-large\" class=\"total textevertbleu  \">Total:&nbsp;</strong><strong style=\"font-size: large\" class=\"total  textevertbleu \" id=\"T" + ArticleVendors.Id + "\" ></strong> <strong style=\"font-size: large\" id=\"TCUR" + ArticleVendors.Id + "\"  class=\"total  textevertbleu \">   </strong>" + "<strong id=\"TPRICETYPE" + ArticleVendors.Id + "\"  class=\"textevertbleu\" style =\"font-size: small\"></strong>" +
                    "                                      <div  style=\"   height:45px; display: inline-block;      \"></div>" +
            "                                       <span class=\"ml-3" + " grey-text\"> </span>" +
            "                        <div id=\"MINUTEUR" + ArticleVendors.Id + "\"   class=\" container-fluid  grey-text d-flex justify-content-end  pull-right \" style=\"    font-family: Digital-7; height:25px; display: inline-block;   font-size: small;  \">" +

    "                            <span class=\"textebleu\"   id =\"days" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +


    "                            <span class=\"textebleu\" id=\"hours" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +


    "                            <span class=\"textebleu\" id=\"minutes" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +


    "                            <span class=\"textebleu\" id=\"seconds" + ArticleVendors.Id + "\"  style=\"        font-size: large;  font-family: Digital-7; \"></span>" +

    "                        </div > " +
            "                                    </div>" +
            "                                </div>" +
            "                            </div>" +
            "                            <!-- Card content -->" +
                "                        </div>" +
            "                        </div>";

            }
            }
            catch (Exception ex)
            {
                string t = ex.Message;

            }
            return result;
        }

      
    }

}

