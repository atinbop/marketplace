﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using iText.IO.Util;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace MarketPlace.Controllers
{
    [Route("api/sharingbuyingArticleDetails")]
    [ApiController]
    public class sharingbuyingArticleDetailsController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        public List<Article> Articles { get; set; }
        public List<Parameters> Parameters { get; set; }
        public sharingbuyingArticleDetailsController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;

        }

    
 



        [HttpGet]
        public async Task<IActionResult> GetArticle(int ArticleId)
        {
        
            if(_UserCompanyType=="B2B" || _UserCompanyType == "B2C") {
                try { 
     
             Articles = _db.Article.FromSqlRaw<Article>("spGetAutorizedArticlesB2B_B2CByArticle {0} , {1}", ArticleId).ToList();
                }
                catch (Exception ex)
                {
                    string t = ex.Message;
                    t = "";

                }
            }


            if (_UserCompanyType =="PRO")
            {
                try
                {
                    Articles = _db.Article.FromSqlRaw<Article>("GetSharingBuyingArticlesDetails {0}, {1} ", ArticleId, Request.Cookies["ConnectedUserId"]).ToList();
                }
                catch (Exception ex)
                {
                    string t = ex.Message;
                }
            }


            //Listes des Posts Non lus
            try
            {
                List<Parameters> UnreadMessages = null;
                try { 

              UnreadMessages = _db.Parameters.FromSqlRaw<Parameters>("spGetUnreadMessagesNumber {0}", _ConnectedUserId).ToList();
                }
                catch (Exception ex)
                {
                    string t = ex.Message;
                }

                if(Articles != null) { 
                foreach (Article prop in Articles)
                {
                    foreach (Parameters prop1 in UnreadMessages)
                    {
                        if (prop.Id == prop1.Id)
                        { prop.UnreadMessagesNumber = prop1.Value; }
                    }
                }
                }
            }
            catch (Exception ex)
            {
                string t = ex.Message;
            }
  
            return Json(new { data = Articles });

        }


 


    }

}

