﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Controllers
{
    //-----------------------------------------------------------------------  
    // <copyright file="MultiSelectDropDownController.cs" company="None">  
    //     Copyright (c) Allow to distribute this code and utilize this code for personal or commercial purpose.  
    // </copyright>  
    // <author>Asma Khalid</author>  
    //-----------------------------------------------------------------------  

    namespace SaveMultiSelectDropDown.Controllers
    {
        using System;
        using System.Collections.Generic;
        using System.IO;
        using System.Linq;
        using System.Reflection;
        using System.Web;
        using MarketPlace.Model;
        using MarketPlace.Model.SaveMultiSelectDropDown;
        using Microsoft.AspNetCore.Authorization;
        using Microsoft.AspNetCore.Mvc;
        using Microsoft.AspNetCore.Mvc.Rendering;
        using Microsoft.Extensions.Configuration;
        using Models;

        /// <summary>  
        /// Multi select drop down controller class.  
        /// </summary>  
        public class MultiSelectDropDownController : Controller
        {

            private readonly Data.ApplicationDbContext _db;

            public MultiSelectDropDownController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
            {
                _db = db;

            }


            public ActionResult Index()
            {
                // Initialization.  
                MultiSelectDropDownViewModel model = new MultiSelectDropDownViewModel { SelectedMultiCountryId = new List<int>(), SelectedCountryLst = new List<Country>() };

                try
                {
                    // Loading drop down lists.  
                    this.ViewBag.CountryList = this.GetCountryList();
                }
                catch (Exception ex)
                {
                    // Info  
                    Console.Write(ex);
                }

                // Info.  
                return this.View(model);
            }

 

   
            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public ActionResult Index(MultiSelectDropDownViewModel model)
            {
                // Initialization.  
                string filePath = string.Empty;
                string fileContentType = string.Empty;

                try
                {
                    // Verification  
                    if (ModelState.IsValid)
                    {
                        // Initialization.  
                        List<Country> countryList = (from art in _db.Countries select art).ToList();

                        // Selected countries list.  
                        model.SelectedCountryLst = countryList.Where(p => model.SelectedMultiCountryId.Contains(p.Id)).Select(q => q).ToList();
                    }

                    // Loading drop down lists.  
                    this.ViewBag.CountryList = this.GetCountryList();
                }
                catch (Exception ex)
                {
                    // Info  
                    Console.Write(ex);
                }

                // Info  
                return this.View(model);
            }


            private List<Country> LoadData()
            {
                // Initialization.  
                List<Country> lst = new List<Country>();

                try
                {
                    // Initialization.  
                    string line = string.Empty;
                    string rootFolderPath = ""; // Server.MapPath("~/Content/files/");
                    string fileName = "country_list.txt";
                    string fullPath = rootFolderPath + fileName;
                    string srcFilePath = new Uri(fullPath).LocalPath;
                    StreamReader sr = new StreamReader(new FileStream(srcFilePath, FileMode.Open, FileAccess.Read));

                    // Read file.  
                    while ((line = sr.ReadLine()) != null)
                    {
                        // Initialization.  
                        Country infoObj = new Country();
                        string[] info = line.Split(',');

                        // Setting.  
                        infoObj.Id = Convert.ToInt32(info[0].ToString());
                        infoObj.nom_fr_fr = info[1].ToString();

                        // Adding.  
                        lst.Add(infoObj);
                    }

                    // Closing.  
                    sr.Dispose();
                    sr.Close();
                }
                catch (Exception ex)
                {
                    // info.  
                    Console.Write(ex);
                }

                // info.  
                return lst;
            }

  
            private IEnumerable<SelectListItem> GetCountryList()
            {
                // Initialization.  
                SelectList lstobj = null;

                try
                {
                    // Loading.  
                    var list = this.LoadData()
                                      .Select(p =>
                                                new SelectListItem
                                                {
                                                    Value = p.Id.ToString(),
                                                    Text = p.nom_fr_fr
                                                });

                    // Setting.  
                    lstobj = new SelectList(list, "Value", "Text");
                }
                catch (Exception ex)
                {
                    // Info  
                    throw ex;
                }

                // info.  
                return lstobj;
            }

        
        }
    }
}
