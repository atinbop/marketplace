﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Controllers
{
    [Route("api/ArticleProperty")]
    [ApiController]

    public class ArticlePropertyController : Controller
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;

        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        public ArticlePropertyController(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;
            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;

        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
        return Json(new { data = await (from art in _db.ArticleProperty   select art).ToListAsync() });
        }


        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {

            var articleProperty = await _db.ArticleProperty.FindAsync(id);

            var bookFromDb = await _db.ArticleProperty.FirstOrDefaultAsync(u => u.Id == id);
            if (bookFromDb == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }
            string req = "delete from ArticleProperty  where id = " + id;
            //string req = "Update ArticleProperty set Deleted=1 ,  LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "' where id = " + id
            int noOfRowUpdated = _db.Database.ExecuteSqlRaw(req);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Suppression réussie." });

        }


    }


}