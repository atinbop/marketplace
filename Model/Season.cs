﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Season
    {
        public string Name { get; set; }
        public int Mean { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
    }
}
