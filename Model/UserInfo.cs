﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class UserInfo
    {
        public long user_id { get; set; }
        public string email { get; set; }
        public string HealthInsuranceCardFile { get; set; }
        public string HealthInsuranceNumber { get; set; }
        public string Weight { get; set; }
        public string IsPregnant { get; set; }
        public string IsBreasthFeeding { get; set; }
        public string CurrentTreatment { get; set; }
        public string HealthMutualCardFile { get; set; }
        public int app_id { get; set; }
        public string company { get; set; }
        public string address1 { get; set; }
        public string country { get; set; }
        public string professional_number { get; set; }
        public string numero_siret { get; set; }
        public string inscription_rcs { get; set; }
        public string code_ape { get; set; }
        public string whatapp_Number { get; set; }
        public string flag_wholesaler { get; set; }
        public string fax_number { get; set; }
        public string numero_siren { get; set; }
        public string numero_tva { get; set; }
        public string civility { get; set; }
        public string country_code { get; set; }
        public string phone { get; set; }
        public string address2 { get; set; }
        public string ZipCode { get; set; }
        public string city { get; set; }
        public string delivery_first_name { get; set; }
        public string delivery_last_name { get; set; }
        public string delivery_email { get; set; }
        public string delivery_mobilephone { get; set; }
        public string delivery_company { get; set; }
        public string delivery_address1 { get; set; }
        public string delivery_country { get; set; }
        public string delivery_civility { get; set; }
        public string delivery_country_code { get; set; }
        public string delivery_phone { get; set; }
        public string delivery_address2 { get; set; }
        public string delivery_ZipCode { get; set; }
        public string delivery_city { get; set; }
        public string firebase_id { get; set; }
        public string age { get; set; }
        public string mobilephone { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string insert_datetime { get; set; }
        public int active { get; set; }
        public string insuranceType { get; set; }
        public string role { get; set; }
        public string currency { get; set; }
        public string currency_symbol { get; set; }
        public string confirmationCode { get; set; }
    }
}
