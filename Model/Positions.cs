﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Positions
    {
        [Key]
        public int Id { get; set; }
        public  string positionName { get; set; }
    }
}
