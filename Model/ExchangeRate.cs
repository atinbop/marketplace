﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class ExchangeRate
    {
        [Key]
        public int id { get; set; }
            public Nullable<DateTime> ValueDate { get; set; }
        public string Currency { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<DateTime> LastUpdate   { get; set; }
}
}
