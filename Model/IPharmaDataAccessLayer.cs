﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketPlace.Model
{

 
        public interface IPharmaDataAccessLayer
        {
            AppInfo GetAppInfo(string apiID);

            IList<ProductCategory> GetProductCategories(string apiID);

            UserInfo GetUserInfo(GetUserInfoParam getUserInfo);

            Product GetProduct(String appID, String productID);

            IList<Product> GetProducts(GetProductsParam getProductsParam);

            IList<Product> GetProductsCategory(GetProductsCategoryParam getProductsParam);

            IList<Product> GetProductsCategorySearch(GetProductsCategoryIDSearchParam getProductsCategoryIDSearchParam);

            int GetProductsByCategoryIDSearchCount(GetCountProductsByCategoryIDSearchParam getCountProductsByCategoryIDSearchParam);

            int GetProductsByCategoryIDCount(GetCountProductsByCategoryIDParam getCountProductsByCategoryIDParam);

            string GetHealthInsuranceFileData(string fileName);

            string GetBankAccountFileData(string filename);

            string InsertOrder(Order order);

            IList<Order> GetOrdersByUser(GetOrdersByUserIDParam getOrdersByUserIDParam);

            int GetOrdersByUserCount(GetCountOrdersByUserIDParam getOrdersByUserIDParam);

            Order GetOrderByID(GetOrderByIDParam getOrderByIDParam);

            IList<OrderItem> GetOrderItemsByOrderID(GetOrderItemsByOrderIDParam getOrderItemsByOrderIDParam);

            string GetProductDetails(string textLink);

            IList<ZipCodeInfo> GetZipCodeByCountryCode(string CountryCode);

            DeliveryInfo GetDeliveryInfo(string OrderID);
            IList<TrackingInfo> GetTrackingStatusByOrderID(String OrderID, String AppID, String UserID, String OrderConfirmation, String SkybillNumber);

        }
    }
