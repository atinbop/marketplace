﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class GetOrdersByUserIDParam
    {
        public int AppID { get; set; }
        public int UserID { get; set; }
        public int PageID { get; set; }
        public int MaxCount { get; set; }
    }
}
