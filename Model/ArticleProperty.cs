using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarketPlace.Model
{
    public class ArticleProperty
    {
        [Key]
        public int Id { get; set; }
        //public Guid ArticlePropertyId { get; set; }

        //[Required]
        //[MaxLength(128)]
        //public string ArticlePropertyName { get; set; }


   


        public int PropertyValueId { get; set; }


        public int ArticleId { get; set; }

        public string ArticlePropertyLabel  { get; set; }
        public string ArticlePropertyValueLabel { get; set; }


        //public string ArticlePropertyLabel { get; set; }
        //public string ArticlePropertyValueLabel { get; set; }
        //public string EN_ArticlePropertyLabel { get; set; }

        //public string EN_ArticlePropertyValueLabel { get; set; }
        //public string EN_ArticlePropertyName { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; } = DateTime.Now;

        public string PropertyLogo { get; set; }

    }
}