﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Model
{
    public class Property
    {
        [Key]
        [MaxLength(2)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string PropertyName { get; set; }
        public string EN_PropertyName { get; set; }
        public List<PropertyValue> PropertyValues { get; set; }

    }
}