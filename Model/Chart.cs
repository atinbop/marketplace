﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Chart
    {    
        [Key]
        public int Id { get; set; }
        public Nullable<DateTime> chartDate { get; set; }
        public int ArticleVendorsId { get; set; }
        public int supplierContactId { get; set; }
        public int employeeId { get; set; }
        public Nullable<decimal> vat { get; set; }
        public int articleId { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public string  currency { get; set; }
        public string pricetype { get; set; }
        public string chartstate { get; set; }
        public Nullable<int> Deleted { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }

        public string Image1 { get; set; }
        public string  ProductName { get; set; }
        public string  ProductBrand { get; set; }
        public int BrandId { get; set; }
        public string  supplierName { get; set; }
    public decimal  TotalAmount { get; set; }
        public int Lot { get; set; }
        public Nullable<int> MinimumOrderQuantity { get; set; }

        public Nullable<int> MaximumOrderQuantity { get; set; }

        public Nullable<Decimal>  customsTax { get; set; }

    }
}
