﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Brands
    {
        [Key]
        public int Id { get; set; }
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public string Title { get; set; }
        public string BrandLogo { get; set; }
        public string BrandLinkPage { get; set; }
        public string TopBrandFlag { get; set; }
        public string Keywords { get; set; }
        public string BrandShortDescription { get; set; }
        public string BrandLongDescription { get; set; }
        public string EN_Keywords { get; set; }
        public string EN_BrandShortDescription { get; set; }
        public string EN_BrandLongDescription { get; set; }
        public string Slogan { get; set; }
        public Nullable<int>   ContactId { get; set; }
        public Contacts     Contact { get; set; }
        public string ImageBanderole1  { get; set; }
        public string ImageBanderole2  { get; set; }
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
        public string BrandLogoNoirEtBlanc { get; set; }
        public string FacebookPage { get; set; }
        public string InstagramPage { get; set; }
        public string OfficialWebsite{ get; set; }
    public string youtubePage { get; set; }
    }
}
