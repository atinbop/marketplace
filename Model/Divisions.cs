﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Divisions
    {
       public int  id { get; set; }
        public string Code { get; set; }

        public string Name { get; set; }
        public string Region { get; set; }
        public string Place { get; set; }
        public string surface { get; set; }
        public int Population { get; set; }
        public string Density { get; set; }
        public int CountryId { get; set; }


    }
}
