﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class SaleArea
    {
        [Key]
        public int Id  {get;set;}
        public string SaleAreaName  {get;set;}
    public int CountryId { get;set;}
    public string CountryName {get;set;}

    }
}
