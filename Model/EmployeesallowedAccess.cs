﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class EmployeesallowedAccess
    {
        [Key]
    public int Id { get; set; }
        public string UserPreferedLanguage { get; set; }
    public string Contactcode { get; set; }
       public string collaboratorCode { get; set; }
        public string Civility { get; set; }

       public string userRole { get; set; }
        public string usercountry { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Functions { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Portable { get; set; }
        public string Emaillogin { get; set; }
        public string pwd { get; set; }
        public string  Bulletin { get; set; }
        public string Authorizedaccess { get; set; }
        public string Validation { get; set; }
        public string Contactbydefault { get; set; }
        public Contacts Contact { get; set; }
        public string company { get; set; }
        public int ContactId { get; set; }
        public string Picture { get; set; }
        
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public string   ActivitySector { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }

        [StringLength(10)]
        public string Pseudo{ get; set; }
       
        public string PreferedLanguage { get; set; }
        public string PreferedCurrency { get; set; }
        
    }
}
