﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Model
{
    public class Payout
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime PaymentDate { get; set; }

        [Required]
        public Decimal Amount { get; set; }

        public string TransactionNumber { get; set; }

        public string PaymentDocument { get; set; }

        public string OrderCode { get; set; }

        public string PaymentStatus { get; set; }

        public string PaymentComment { get; set; }
        [Required]
        public int SellerId  { get; set; }
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }


    }
}
