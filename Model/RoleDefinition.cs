﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class RoleDefinition
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string description { get; set; }
    }
}
