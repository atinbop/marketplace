﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Product
    {
        public long AppID { get; set; }
        public long ProductID { get; set; }
        public string ProductName { get; set; }
        public int CategoryID { get; set; }
        public int ManufacturerID { get; set; }
        public string FileName1 { get; set; }
        public string FileName2 { get; set; }
        public string FileName3 { get; set; }
        public string Solution { get; set; }
        public string Specificities { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public string Usages { get; set; }
        public string Composition { get; set; }
        public string Contraindication { get; set; }
        public string Posologie { get; set; }
        public string Notice { get; set; }
        public string Taille { get; set; }
        public string Inventory { get; set; }
        public double Price { get; set; }
        public double GrossPrice { get; set; }
        public double TauxTva { get; set; }
        public int Active { get; set; }
        public string InsertDatetime { get; set; }
        public int GrossMinimumQuantity { get; set; }
        public string Promotion { get; set; }
    }
}
