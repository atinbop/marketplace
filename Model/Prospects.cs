﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Prospects
    {
        [Key]
       public  int id { get; set; }
      [Required]
        public string Name { get; set; }
        [Required]

        public string FirstName { get; set; }



        [Required]
        public string emailAddress { get; set; }
        [Required]
        public string TelephoneNumber { get; set; }
    
        [Required]
        public string Company { get; set; }
        [Required]
        public string Message { get; set; }
        
        public string Remarque { get; set; }
        public string ActivitySector { get; set; }
        public string country { get; set; }
    }
}
