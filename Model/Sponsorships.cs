﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
 

namespace MarketPlace.Model
{
    public class Sponsorships
    {
        [Key]
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public Nullable<decimal> GodfatherAmount { get; set; }
        public Nullable<decimal> GodsonpAmount { get; set; }

   
        public DateTime SponsorshipstartDate { get; set; }
        public DateTime SponsorshipsEndDate { get; set; }
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
 
   
        public string UserCompanyType { get; set; }
        public Nullable<int> ContactId { get; set; }
    }
}
