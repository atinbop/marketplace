﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class ProductCategory : IEquatable<ProductCategory>
    {
        public int CategoryID { get; set; }
        public string Label { get; set; }
        public int parentCategoryID { get; set; }
        public IList<ProductCategory> SubCategories { get; set; }
        public int CategoryKey { get; set; }

        public bool Equals(ProductCategory other)
        {
            return other != null &&
                this.CategoryID == other.CategoryID &&
                this.Label.Equals(other.Label) &&
                this.parentCategoryID == other.parentCategoryID;
        }
    }
}
