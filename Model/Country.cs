﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Country
    {
        [Key]
        public int Id { get;  set; }
        public string code { get; set; }
        public string alpha2 { get; set; }
        public string alpha3 { get; set; }
         public string nom_en_gb { get; set; }
        public string nom_fr_fr { get; set; }
        public string flag { get; set; }
        public string Continent { get; set; }
        public string Capital { get; set; }
        public string langName { get; set; }
        public string langCode { get; set; }
        public string currency { get; set; }
        public string langname1 { get; set; }
        public string langcode1 { get; set; }
        public Nullable<int> currencyId   { get; set; }
}
}
