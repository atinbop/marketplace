﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MarketPlace;
using MarketPlace.Utils;
using ChronopostServices;


namespace MarketPlace.Model
{
  public class PharmaDataAccessLayer : IPharmaDataAccessLayer
  {

    private MySqlConnectionStringBuilder connectionStringBuilder;

      private const string PHARMA_RELEASE_BASE_PATH = @"C:\Release\PharmaService\files\";
      private const string PHARMA_PRODUCTS_IMAGE_PATH = @"C:\Release\PharmaService\files\products\";
      private const string PHARMA_DATA_PATH = @"C:\__DATA__\Pharma\";
      private const string PHARMA_BANK_PATH = @"C:\__DATA__\Pharma\rib\";
      private const string PHARMA_HEALTH_INSURANCE_PATH = @"C:\__DATA__\Pharma\healthinsurance";
      private const string PHARMA_PRODUCT_DETAIL_FILES = @"C:\Release\PharmaService\files\products\text_files\";
      private const string PHARMA_PRODUCT_BILLINGS_FILES = @"C:\Release\PharmaService\files\bills\";
      private const string PHARMA_WEBSERVICE_PRODUCTS_LINK = "http://154.16.135.195:53229/files/products/";



      private string Now
      {
          get
          {
              return DateTime.Now.ToString("yyyyMMddHHmmss");
          }
      }

      public PharmaDataAccessLayer()
      {
          connectionStringBuilder = new MySqlConnectionStringBuilder();
          connectionStringBuilder.Server = "154.16.135.195";
          connectionStringBuilder.Port = 3306;
          connectionStringBuilder.UserID = "root";
          connectionStringBuilder.Password = "Edmd2k18";
          connectionStringBuilder.Database = "healthbook_db";
      }

      public AppInfo GetAppInfo(string appID)
      {
          AppInfo result = null;
          using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
          {
              MySqlCommand command = mySqlConnection.CreateCommand();
              command.CommandText = "getAppInfo";
              command.CommandType = System.Data.CommandType.StoredProcedure;

              if (!string.IsNullOrEmpty(appID))
              {
                  command.Parameters.AddWithValue("@param_app_id", appID);
                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      while (mysqlReader.Read())
                      {
                          result =
                              new AppInfo
                              {
                                  AppID = mysqlReader.GetInt32("app_id"),
                                  FirstName = mysqlReader.GetStringSafe("first_name"),
                                  LastName = mysqlReader.GetStringSafe("last_name"),
                                  Email = mysqlReader.GetStringSafe("email"),
                                  PhoneNumber = mysqlReader.GetStringSafe("mobilephone"),
                                  Company = mysqlReader.GetStringSafe("company"),
                                  Address = mysqlReader.GetStringSafe("address1"),
                                  Country = mysqlReader.GetStringSafe("country"),
                                  ProfessionalNumber = mysqlReader.GetStringSafe("professional_number"),
                                  BankAccountFile = mysqlReader.GetStringSafe("bank_account_file"),
                                  StripePK = mysqlReader.GetStringSafe("stripe_publishablekey"),
                                  StripeAK = mysqlReader.GetStringSafe("stripe_apikey"),
                                  numero_siret = mysqlReader.GetStringSafe("numero_siret"),
                                  inscription_rcs = mysqlReader.GetStringSafe("inscription_rcs"),
                                  code_ape = mysqlReader.GetStringSafe("code_ape"),
                                  site_web = mysqlReader.GetStringSafe("site_web"),
                                  photo_principale_file = mysqlReader.GetStringSafe("photo_principale_file"),
                                  whatapp_Number = mysqlReader.GetStringSafe("whatapp_Number"),
                                  flag_wholesaler = mysqlReader.GetStringSafe("flag_wholesaler"),
                                  logo = mysqlReader.GetStringSafe("logo"),
                                  fax_number = mysqlReader.GetStringSafe("fax_number"),
                                  numero_siren = mysqlReader.GetStringSafe("numero_siren"),
                                  numero_tva = mysqlReader.GetStringSafe("numero_tva"),
                                  civility = mysqlReader.GetStringSafe("civility"),
                                  country_code = mysqlReader.GetStringSafe("country_code"),
                                  chronopost_account_number = mysqlReader.GetInt32("chronopost_account_number"),
                                  chronopost_account_password = mysqlReader.GetStringSafe("chronopost_account_password"),
                                  phone = mysqlReader.GetStringSafe("phone"),
                                  address2 = mysqlReader.GetStringSafe("address2"),
                                  ZipCode = mysqlReader.GetStringSafe("ZipCode"),
                                  city = mysqlReader.GetStringSafe("city"),

                              };

                          //result.BankAccountFile = FileHelper.GetBase64File(PHARMA_BANK_PATH, result.BankAccountFile);
                      }
                  }
              }
          }
          return result;
      }

      public IList<ZipCodeInfo> GetZipCodeByCountryCode(string countryCode)
      {
          ZipCodeInfo result = null;
          IList<ZipCodeInfo> tabresult = null;
          using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
          {

              MySqlCommand command = mySqlConnection.CreateCommand();
              command.CommandText = "getZipCodeByCountryCode";
              command.CommandType = System.Data.CommandType.StoredProcedure;

              tabresult = new List<ZipCodeInfo>();

              if (!string.IsNullOrEmpty(countryCode))
              {
                  command.Parameters.AddWithValue("@param_countryCode", countryCode);
                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      while (mysqlReader.Read())
                      {
                          result =
                              new ZipCodeInfo
                              {
                                  country_code = mysqlReader.GetStringSafe("country_code"),
                                  country_name = mysqlReader.GetStringSafe("country_name"),
                                  zip_code = mysqlReader.GetStringSafe("zip_code"),
                                  flag_path = mysqlReader.GetStringSafe("flag_path"),
                                  currency = mysqlReader.GetStringSafe("currency"),
                                  intphonecode = mysqlReader.GetStringSafe("intphonecode"),
                                  country_code3 = mysqlReader.GetStringSafe("country_code3"),
                                  latitude = mysqlReader.GetFloat("latitude"),
                                  longitude = mysqlReader.GetFloat("longitude"),
                                  admin_name1 = mysqlReader.GetStringSafe("admin_name1"),
                                  admin_name2 = mysqlReader.GetStringSafe("admin_name2"),
                                  admin_name3 = mysqlReader.GetStringSafe("admin_name3"),
                                  place_name = mysqlReader.GetStringSafe("place_name"),

                              };
                          tabresult.Add(result);
                      }
                  }
              }
          }
          return tabresult;
      }

      public DeliveryInfo GetDeliveryInfo(string OrderID)
      {

          DeliveryInfo result = null;
          using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
          {
              MySqlCommand command = mySqlConnection.CreateCommand();
              command.CommandText = "getDeliveryInfo";
              command.CommandType = System.Data.CommandType.StoredProcedure;

              if (!string.IsNullOrEmpty(OrderID))
              {
                  command.Parameters.AddWithValue("@param_order_id", OrderID);
                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      while (mysqlReader.Read())
                      {
                          result =
                              new DeliveryInfo
                              {
                                  app_id = mysqlReader.GetInt32("app_id"),
                                  OrderID = mysqlReader.GetStringSafe("order_id"),
                                  orderNumber = mysqlReader.GetStringSafe("orderNumber"),
                                  user_id = mysqlReader.GetInt32("user_id"),
                                  reservationNumber = mysqlReader.GetStringSafe("reservationNumber"),
                                  pickupDate = mysqlReader.GetDateTimeSafe("pickupDate"),
                                  errorCode = mysqlReader.GetStringSafe("errorCode"),
                                  errorMessage = mysqlReader.GetStringSafe("errorMessage"),
                                  ESDFullNumber = mysqlReader.GetStringSafe("ESDFullNumber"),
                                  ESDNumber = mysqlReader.GetStringSafe("ESDNumber"),
                                  DSort = mysqlReader.GetStringSafe("DSort"),
                                  OSort = mysqlReader.GetStringSafe("OSort"),
                                  codeDepot = mysqlReader.GetStringSafe("codeDepot"),
                                  codeService = mysqlReader.GetStringSafe("codeService"),
                                  geoPostNumeroColis = mysqlReader.GetStringSafe("geoPostNumeroColis"),
                                  skybillNumber = mysqlReader.GetStringSafe("skybillNumber"),
                                  pdfFileName = mysqlReader.GetStringSafe("pdfFileName"),
                                  delivery_date = mysqlReader.GetDateTimeSafe("delivery_date"),
                                  delivery_cost = mysqlReader.GetFloat("delivery_cost"),
                                  combine_skybill_bill_file = mysqlReader.GetStringSafe("combine_skybill_bill_file"),
                              };
                      }
                  }
              }
          }
          return result;
      }

      public IList<ProductCategory> GetProductCategories(string apiID)
      {
          IList<ProductCategory> result = null;

          using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
          {
              MySqlCommand command = mySqlConnection.CreateCommand();
              command.CommandText = "getProductCategory";
              command.CommandType = System.Data.CommandType.StoredProcedure;
              IList<ProductCategoryServer> tempResult = new List<ProductCategoryServer>();
              result = new List<ProductCategory>();

              if (!string.IsNullOrEmpty(apiID))
              {
                  command.Parameters.AddWithValue("@param_app_id", apiID);
                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      while (mysqlReader.Read())
                      {
                          //we have the categories, now time to sort them out
                          tempResult.Add(
                              new ProductCategoryServer
                              {
                                  CategoryKey = mysqlReader.GetInt32("productcategory_id"),
                                  CategoryLabel = mysqlReader.GetStringSafe("categoryLabel"),
                                  SubCategoryLabel = mysqlReader.GetStringSafe("subcategoryLabel"),
                                  SubSubCategoryLabel = mysqlReader.GetStringSafe("subsubcategoryLabel"),
                                  SubCategoryID = mysqlReader.GetInt32("subcategory_id"),
                                  SubSubCategoryID = mysqlReader.GetInt32("subsubcategory_id"),
                                  CategoryID = mysqlReader.GetInt32("category_id")

                              });
                      }

                      if (tempResult != null && tempResult.Count > 0)
                      {
                          //1- split per category IDs : Level 1
                          IDictionary<int, IList<ProductCategoryServer>> categoriesRoot = new Dictionary<int, IList<ProductCategoryServer>>();
                          foreach (ProductCategoryServer tempCategoryRoot in tempResult)
                          {
                              if (!categoriesRoot.ContainsKey(tempCategoryRoot.CategoryID))
                                  categoriesRoot[tempCategoryRoot.CategoryID] = new List<ProductCategoryServer>();
                              categoriesRoot[tempCategoryRoot.CategoryID].Add(tempCategoryRoot);
                          }

                          //2- split per sub categories : Level 2
                          if (categoriesRoot != null && categoriesRoot.Count > 0)
                          {
                              foreach (KeyValuePair<int, IList<ProductCategoryServer>> entryRoot in categoriesRoot)
                              {
                                  IDictionary<int, IList<ProductCategoryServer>> categoriesLevel1 = new Dictionary<int, IList<ProductCategoryServer>>();
                                  IList<ProductCategoryServer> listCategoriesLevel1 = entryRoot.Value;
                                  if (listCategoriesLevel1 != null && listCategoriesLevel1.Count > 0)
                                  {
                                      //a- we create the master(or root) category
                                      result.Add(new ProductCategory
                                      {
                                          CategoryID = entryRoot.Key,
                                          Label = listCategoriesLevel1.ElementAt(0).CategoryLabel,
                                          SubCategories = new List<ProductCategory>(),
                                          parentCategoryID = -1, //they are the root so have no parent
                                          CategoryKey = -1
                                      });

                                      if (result.Count > 0)
                                      {
                                          IList<ProductCategory> listLevel1 = result.ElementAt(result.Count - 1).SubCategories;
                                          foreach (ProductCategoryServer tempCategoryLevel1 in listCategoriesLevel1)
                                          {
                                              if (!categoriesLevel1.ContainsKey(tempCategoryLevel1.SubCategoryID))
                                                  categoriesLevel1[tempCategoryLevel1.SubCategoryID] = new List<ProductCategoryServer>();
                                              categoriesLevel1[tempCategoryLevel1.SubCategoryID].Add(tempCategoryLevel1);
                                          }

                                          //b- we fill in level1 (just below the root)
                                          if (categoriesLevel1 != null && categoriesLevel1.Count > 0)
                                          {
                                              foreach (KeyValuePair<int, IList<ProductCategoryServer>> entryLevel1 in categoriesLevel1)
                                              {
                                                  IDictionary<int, IList<ProductCategoryServer>> categoriesLevel2 = new Dictionary<int, IList<ProductCategoryServer>>();
                                                  IList<ProductCategoryServer> listCategoriesLevel2 = entryLevel1.Value;
                                                  if (listCategoriesLevel2 != null && listCategoriesLevel2.Count > 0)
                                                  {
                                                      listLevel1.Add(new ProductCategory
                                                      {
                                                          CategoryID = entryLevel1.Key,
                                                          Label = listCategoriesLevel2.ElementAt(0).SubCategoryLabel,
                                                          SubCategories = new List<ProductCategory>(),
                                                          parentCategoryID = entryRoot.Key,
                                                          CategoryKey = -1
                                                      });

                                                      if (listLevel1.Count > 0)
                                                      {
                                                          IList<ProductCategory> listLevel2 = listLevel1.ElementAt(listLevel1.Count - 1).SubCategories;

                                                          foreach (ProductCategoryServer tempCategoryLevel2 in listCategoriesLevel2)
                                                          {
                                                              if (!categoriesLevel2.ContainsKey(tempCategoryLevel2.SubSubCategoryID))
                                                                  categoriesLevel2[tempCategoryLevel2.SubSubCategoryID] = new List<ProductCategoryServer>();
                                                              categoriesLevel2[tempCategoryLevel2.SubSubCategoryID].Add(tempCategoryLevel2);
                                                          }

                                                          //b- we fill in level2 (the leaf)
                                                          if (categoriesLevel2 != null && categoriesLevel2.Count > 0)
                                                          {
                                                              foreach (KeyValuePair<int, IList<ProductCategoryServer>> entryLevel2 in categoriesLevel2)
                                                              {
                                                                  listLevel2.Add(new ProductCategory
                                                                  {
                                                                      CategoryID = entryLevel2.Key,
                                                                      Label = entryLevel2.Value.ElementAt(0).SubSubCategoryLabel,
                                                                      SubCategories = null,
                                                                      parentCategoryID = entryLevel1.Key,
                                                                      CategoryKey = entryLevel2.Value.ElementAt(0).CategoryKey
                                                                  });
                                                              }
                                                          }
                                                      }
                                                  }
                                              }
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          }
          return result != null && result.Count > 0 ? result : null;
      }

      public UserInfo GetUserInfo(GetUserInfoParam getUserInfo)
      {
          UserInfo result = null;
          using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
          {
              if (getUserInfo != null && !string.IsNullOrEmpty(getUserInfo.AppID) && !string.IsNullOrEmpty(getUserInfo.UserID.ToString()))
              {
                  MySqlCommand command = mySqlConnection.CreateCommand();
                  command.CommandText = "getUserInfo";
                  command.CommandType = System.Data.CommandType.StoredProcedure;
                  command.Parameters.AddWithValue("@param_app_id", getUserInfo.AppID);
                  command.Parameters.AddWithValue("@param_user_id", getUserInfo.UserID);

                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      while (mysqlReader.Read())
                      {
                          result =
                              new UserInfo
                              {
                                  delivery_civility = mysqlReader.GetStringSafe("delivery_civility"),
                                  civility = mysqlReader.GetStringSafe("civility"),
                                  Weight = mysqlReader.GetStringSafe("Weight"),
                                  user_id = mysqlReader.GetInt32("user_id"),
                                  email = mysqlReader.GetStringSafe("email"),
                                  age = mysqlReader.GetStringSafe("age"),
                                  app_id = mysqlReader.GetInt32("app_id"),
                                  active = mysqlReader.GetInt32("active"),
                                  insert_datetime = mysqlReader.GetStringSafe("insert_datetime"),
                                  mobilephone = mysqlReader.GetStringSafe("mobilephone"),
                                  last_name = mysqlReader.GetStringSafe("last_name"),
                                  first_name = mysqlReader.GetStringSafe("first_name"),
                                  flag_wholesaler = mysqlReader.GetStringSafe("flag_wholesaler"),
                                  IsPregnant = mysqlReader.GetStringSafe("IsPregnant"),
                                  IsBreasthFeeding = mysqlReader.GetStringSafe("IsBreasthFeeding"),
                                  ZipCode = mysqlReader.GetStringSafe("ZipCode"),
                                  whatapp_Number = mysqlReader.GetStringSafe("whatapp_Number"),
                                  professional_number = mysqlReader.GetStringSafe("professional_number"),
                                  phone = mysqlReader.GetStringSafe("phone"),
                                  numero_tva = mysqlReader.GetStringSafe("numero_tva"),
                                  numero_siret = mysqlReader.GetStringSafe("numero_siret"),
                                  numero_siren = mysqlReader.GetStringSafe("numero_siren"),
                                  insuranceType = mysqlReader.GetStringSafe("insuranceType"),
                                  inscription_rcs = mysqlReader.GetStringSafe("inscription_rcs"),
                                  HealthMutualCardFile = mysqlReader.GetStringSafe("HealthMutualCardFile"),
                                  HealthInsuranceNumber = mysqlReader.GetStringSafe("HealthInsuranceNumber"),
                                  HealthInsuranceCardFile = mysqlReader.GetStringSafe("HealthInsuranceCardFile"),
                                  firebase_id = mysqlReader.GetStringSafe("firebase_id"),
                                  fax_number = mysqlReader.GetStringSafe("fax_number"),
                                  delivery_ZipCode = mysqlReader.GetStringSafe("delivery_ZipCode"),
                                  delivery_phone = mysqlReader.GetStringSafe("delivery_phone"),
                                  delivery_mobilephone = mysqlReader.GetStringSafe("delivery_mobilephone"),
                                  delivery_last_name = mysqlReader.GetStringSafe("delivery_last_name"),
                                  delivery_first_name = mysqlReader.GetStringSafe("delivery_first_name"),
                                  delivery_email = mysqlReader.GetStringSafe("delivery_email"),
                                  delivery_country_code = mysqlReader.GetStringSafe("delivery_country_code"),
                                  delivery_country = mysqlReader.GetStringSafe("delivery_country"),
                                  delivery_company = mysqlReader.GetStringSafe("delivery_company"),
                                  delivery_city = mysqlReader.GetStringSafe("delivery_city"),
                                  delivery_address2 = mysqlReader.GetStringSafe("delivery_address2"),
                                  delivery_address1 = mysqlReader.GetStringSafe("delivery_address1"),
                                  CurrentTreatment = mysqlReader.GetStringSafe("CurrentTreatment"),
                                  country_code = mysqlReader.GetStringSafe("country_code"),
                                  country = mysqlReader.GetStringSafe("country"),
                                  company = mysqlReader.GetStringSafe("company"),
                                  code_ape = mysqlReader.GetStringSafe("code_ape"),
                                  city = mysqlReader.GetStringSafe("city"),
                                  address2 = mysqlReader.GetStringSafe("address2"),
                                  address1 = mysqlReader.GetStringSafe("address1"),
                              };

                          //todo: take the link instead of the whole file
                          result.HealthInsuranceCardFile = FileHelper.GetBase64File(PHARMA_HEALTH_INSURANCE_PATH, result.HealthInsuranceCardFile);
                      }
                  }
              }
          }
          return result;
      }

      public IList<Product> GetProducts(GetProductsParam getProductsParam)
      {
          IList<Product> results = new List<Product>();
          if (getProductsParam != null)
          {
              using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
              {
                  MySqlCommand command = mySqlConnection.CreateCommand();
                  command.CommandText = "getProducts";
                  command.CommandType = System.Data.CommandType.StoredProcedure;

                  command.Parameters.AddWithValue("@param_app_id", getProductsParam.AppID);
                  command.Parameters.AddWithValue("@param_page_id", getProductsParam.PageID);
                  command.Parameters.AddWithValue("@param_max_count", getProductsParam.MaxCount);

                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      String directoryExtention = "" + getProductsParam.AppID + @"/";
                      while (mysqlReader.Read())
                      {
                          var item = new Product
                          {
                              AppID = mysqlReader.GetInt32("app_id"),
                              ProductID = mysqlReader.GetInt32("product_id"),
                              ProductName = mysqlReader.GetStringSafe("product_name"),
                              CategoryID = mysqlReader.GetInt32("productcategory_id"),
                              ManufacturerID = mysqlReader.GetInt32("manufacturer_id"),
                              FileName1 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename1")),
                              FileName2 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename2")),
                              FileName3 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename3")),
                              Solution = mysqlReader.GetStringSafe("solution"),
                              Specificities = mysqlReader.GetStringSafe("specificities"),
                              Reference = mysqlReader.GetStringSafe("reference"),
                              Description = mysqlReader.GetStringSafe("description"),
                              Usages = mysqlReader.GetStringSafe("usages"),
                              Composition = mysqlReader.GetStringSafe("composition"),
                              Contraindication = mysqlReader.GetStringSafe("contre_indication"),
                              Posologie = mysqlReader.GetStringSafe("posologie"),
                              Notice = mysqlReader.GetStringSafe("notice"),
                              Taille = mysqlReader.GetStringSafe("taille"),
                              Price = mysqlReader.GetDouble("price"),
                              GrossPrice = mysqlReader.GetDouble("gross_price"),
                              TauxTva = mysqlReader.GetDouble("taux_tva"),
                              Active = mysqlReader.GetInt32("active"),
                              Inventory = mysqlReader.GetStringSafe("inventory"),
                              InsertDatetime = mysqlReader.GetStringSafe("insert_datetime"),
                              GrossMinimumQuantity = mysqlReader.GetInt32("gross_minimum_quantity"),
                              Promotion = mysqlReader.GetStringSafe("promotion")
                          };

                          results.Add(item);
                      }
                  }
              }
          }
          return results;
      }

      public IList<Product> GetProductsCategory(GetProductsCategoryParam getProductsParam)
      {
          IList<Product> results = new List<Product>();
          if (getProductsParam != null)
          {
              using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
              {
                  MySqlCommand command = mySqlConnection.CreateCommand();
                  command.CommandText = "getProductsByCategoryID";
                  command.CommandType = System.Data.CommandType.StoredProcedure;

                  command.Parameters.AddWithValue("@param_app_id", getProductsParam.AppID);
                  command.Parameters.AddWithValue("@param_category_id", getProductsParam.CategoryID);
                  command.Parameters.AddWithValue("@param_page_id", getProductsParam.PageID);
                  command.Parameters.AddWithValue("@param_max_count", getProductsParam.MaxCount);

                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      String directoryExtention = "" + getProductsParam.AppID + @"/";
                      while (mysqlReader.Read())
                      {
                          var item = new Product
                          {
                              AppID = mysqlReader.GetInt32("app_id"),
                              ProductID = mysqlReader.GetInt32("product_id"),
                              ProductName = mysqlReader.GetStringSafe("product_name"),
                              CategoryID = mysqlReader.GetInt32("productcategory_id"),
                              ManufacturerID = mysqlReader.GetInt32("manufacturer_id"),
                              FileName1 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename1")),
                              FileName2 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename2")),
                              FileName3 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename3")),
                              Solution = mysqlReader.GetStringSafe("solution"),
                              Specificities = mysqlReader.GetStringSafe("specificities"),
                              Reference = mysqlReader.GetStringSafe("reference"),
                              Description = mysqlReader.GetStringSafe("description"),
                              Usages = mysqlReader.GetStringSafe("usages"),
                              Composition = mysqlReader.GetStringSafe("composition"),
                              Contraindication = mysqlReader.GetStringSafe("contre_indication"),
                              Posologie = mysqlReader.GetStringSafe("posologie"),
                              Notice = mysqlReader.GetStringSafe("notice"),
                              Taille = mysqlReader.GetStringSafe("taille"),
                              Price = mysqlReader.GetDouble("price"),
                              GrossPrice = mysqlReader.GetDouble("gross_price"),
                              TauxTva = mysqlReader.GetDouble("taux_tva"),
                              Active = mysqlReader.GetInt32("active"),
                              Inventory = mysqlReader.GetStringSafe("inventory"),
                              InsertDatetime = mysqlReader.GetStringSafe("insert_datetime"),
                              GrossMinimumQuantity = mysqlReader.GetInt32("gross_minimum_quantity"),
                              Promotion = mysqlReader.GetStringSafe("promotion")
                          };
                          results.Add(item);
                      }
                  }
              }
          }
          return results;
      }

      public IList<Product> GetProductsCategorySearch(GetProductsCategoryIDSearchParam getProductsCategoryIDSearchParam)
      {
          IList<Product> results = new List<Product>();
          if (getProductsCategoryIDSearchParam != null)
          {
              using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
              {
                  MySqlCommand command = mySqlConnection.CreateCommand();
                  command.CommandText = "getProductsByCategoryIDSearch";
                  command.CommandType = System.Data.CommandType.StoredProcedure;

                  command.Parameters.AddWithValue("@param_app_id", getProductsCategoryIDSearchParam.AppID);
                  command.Parameters.AddWithValue("@param_category_id", getProductsCategoryIDSearchParam.CategoryID);
                  command.Parameters.AddWithValue("@param_search", getProductsCategoryIDSearchParam.SearchValue);
                  command.Parameters.AddWithValue("@param_level", getProductsCategoryIDSearchParam.CategoryLevel);
                  command.Parameters.AddWithValue("@param_page_id", getProductsCategoryIDSearchParam.PageID);
                  command.Parameters.AddWithValue("@param_max_count", getProductsCategoryIDSearchParam.MaxCount);

                  mySqlConnection.Open();
                  using (MySqlDataReader mysqlReader = command.ExecuteReader())
                  {
                      String directoryExtention = "" + getProductsCategoryIDSearchParam.AppID + @"/";
                      while (mysqlReader.Read())
                      {
                          var item = new Product
                          {
                              AppID = mysqlReader.GetInt32("app_id"),
                              ProductID = mysqlReader.GetInt32("product_id"),
                              ProductName = mysqlReader.GetStringSafe("product_name"),
                              CategoryID = mysqlReader.GetInt32("productcategory_id"),
                              ManufacturerID = mysqlReader.GetInt32("manufacturer_id"),
                              FileName1 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename1")),
                              FileName2 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename2")),
                              FileName3 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename3")),
                              Solution = mysqlReader.GetStringSafe("solution"),
                              Specificities = mysqlReader.GetStringSafe("specificities"),
                              Reference = mysqlReader.GetStringSafe("reference"),
                              Description = mysqlReader.GetStringSafe("description"),
                              Usages = mysqlReader.GetStringSafe("usages"),
                              Composition = mysqlReader.GetStringSafe("composition"),
                              Contraindication = mysqlReader.GetStringSafe("contre_indication"),
                              Posologie = mysqlReader.GetStringSafe("posologie"),
                              Notice = mysqlReader.GetStringSafe("notice"),
                              Taille = mysqlReader.GetStringSafe("taille"),
                              Price = mysqlReader.GetDouble("price"),
                              GrossPrice = mysqlReader.GetDouble("gross_price"),
                              TauxTva = mysqlReader.GetDouble("taux_tva"),
                              Active = mysqlReader.GetInt32("active"),
                              Inventory = mysqlReader.GetStringSafe("inventory"),
                              InsertDatetime = mysqlReader.GetStringSafe("insert_datetime"),
                              GrossMinimumQuantity = mysqlReader.GetInt32("gross_minimum_quantity"),
                              Promotion = mysqlReader.GetStringSafe("promotion")
                          };
                          results.Add(item);
                      }
                  }
              }
          }
          return results;
      }

      public string GetHealthInsuranceFileData(string fileName)
      {
          return FileHelper.GetBase64File(PHARMA_HEALTH_INSURANCE_PATH, fileName);
      }

      public string GetBankAccountFileData(string filename)
      {
          return FileHelper.GetBase64File(PHARMA_BANK_PATH, filename);
      }

      public IList<TrackingInfo> GetTrackingStatusByOrderID(String OrderID, String AppID, String UserID, String OrderConfirmation, String SkybillNumber)
      {

          IList<TrackingInfo> result = null;
          AppInfo appinfo = new AppInfo();
          appinfo = GetAppInfo(AppID.ToString());

          var orderInformation = new GetOrderInfoParam();
          orderInformation.app_id = AppID.ToString();
          orderInformation.user_id = UserID.ToString();
          orderInformation.order_id = OrderID.ToString();
          orderInformation.orderNumber = OrderConfirmation;
          orderInformation.skybillNumber = SkybillNumber;
          orderInformation.skybill_pod_file = Path.Combine(PHARMA_PRODUCT_BILLINGS_FILES, "" + AppID.ToString() + @"/" + AppID.ToString() + "_" + UserID.ToString() + "_POD_" + OrderConfirmation + ".pdf");

          var getUserInfo = new GetUserInfoParam();
          getUserInfo.UserID = Int32.Parse(UserID);
          getUserInfo.AppID = AppID;

          UserInfo userinfo = new UserInfo();
          userinfo = GetUserInfo(getUserInfo);

          Tracking instanceWebserviceTracking = new Tracking(appinfo, userinfo, orderInformation);
          resultTrackSkybillV2 ship = instanceWebserviceTracking.getTrackingStatus();
          //   resultSearchPOD pod = instanceWebserviceTracking.getPODdocument();

//1- We insert the result of chronopost delivery request in the database
using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
            {
                MySqlCommand command = mySqlConnection.CreateCommand();
                command = mySqlConnection.CreateCommand();
                command.CommandText = "insertNewTrakingResult";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                result = new List<TrackingInfo>();


                if ((ship.listEventInfoComp.events != null) && (!ship.listEventInfoComp.events.Any()))
                {
                    // Handle null or empty list

                    foreach (var item in ship.listEventInfoComp.events)
                    {
                        TrackingInfo track = new TrackingInfo();
                        track.eventDate = item.eventDate;
                        track.tracking_code = item.code;
                        track.eventDateSpecified = item.eventDateSpecified;
                        track.eventLabel = item.eventLabel;
                        track.highPriority = item.highPriority;
                        track.highPrioritySpecified = item.highPrioritySpecified;
                        //track.infoCompname = item.infoCompList.Length>0 ? item.infoCompList[0].name + "|" + item.infoCompList[0].value:"";
                        track.infoCompname = "";
                        track.NPC = item.NPC;
                        track.officeLabel = item.officeLabel;
                        track.zipCode = item.zipCode;
                        track.tracking_errorCode = ship.errorCode;
                        track.tracking_errorMessage = ship.errorMessage;
                       //  track.pod_errorCode = pod.errorCode;
                         // track.pod_errorMessage = pod.errorMessage;
                         // track.pod_formatPOD = pod.formatPOD;
                          //track.pod_file = orderInformation.skybill_pod_file;
                          //track.pod_Presente = pod.podPresente;
                          //track.pod_statusCode = pod.statusCode;
                        track.order_id = Int32.Parse(OrderID);
                        track.app_id = Int32.Parse(AppID);
                        track.user_id = Int32.Parse(UserID);
                        track.order_confirmation = OrderConfirmation;
                        track.SkybillNumber = SkybillNumber;

                        result.Add(track);

                        command.Parameters.AddWithValue("@eventDate", track.eventDate);
                        command.Parameters.AddWithValue("@tracking_code", track.tracking_code);
                        command.Parameters.AddWithValue("@eventDateSpecified", track.eventDateSpecified);
                        command.Parameters.AddWithValue("@eventLabel", track.eventLabel);
                        command.Parameters.AddWithValue("@highPriority", track.highPriority);
                        command.Parameters.AddWithValue("@highPrioritySpecified", track.highPrioritySpecified);
                        command.Parameters.AddWithValue("@infoCompname", track.infoCompname);
                        command.Parameters.AddWithValue("@NPC", track.NPC);
                        command.Parameters.AddWithValue("@officeLabel", track.officeLabel);
                        command.Parameters.AddWithValue("@zipCode", track.zipCode);
                        command.Parameters.AddWithValue("@tracking_errorCode", track.tracking_errorCode);
                        command.Parameters.AddWithValue("@tracking_errorMessage", track.tracking_errorMessage);
                        command.Parameters.AddWithValue("@pod_errorCode", track.pod_errorCode);
                        command.Parameters.AddWithValue("@pod_errorMessage", track.pod_errorMessage);
                        command.Parameters.AddWithValue("@pod_formatPOD", track.pod_formatPOD);
                        command.Parameters.AddWithValue("@pod_file", track.pod_file);
                        command.Parameters.AddWithValue("@pod_Presente", track.pod_Presente);
                        command.Parameters.AddWithValue("@pod_statusCode", track.pod_statusCode);
                        command.Parameters.AddWithValue("@order_id", track.order_id);
                        command.Parameters.AddWithValue("@app_id", track.app_id);
                        command.Parameters.AddWithValue("@user_id", track.user_id);
                        command.Parameters.AddWithValue("@order_confirmation", track.order_confirmation);
                        command.Parameters.AddWithValue("@SkybillNumber", track.SkybillNumber);

                        mySqlConnection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            return result;
        }

        public string InsertOrder(Order order)
        {
            long orderID = -1;
            if (order != null && order.Items != null && order.Items.Count > 0)
            {
                //no order should be empty (has no OrderItem), please check this first on the client because the server will return empty
                using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
                {
                    order.OrderConfirmation = Guid.NewGuid().ToString();
                    String now = DateTime.UtcNow.ToString("yyyyMMddHHmmss");

                    //1- We insert the order first
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "insertNewOrder";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@param_app_id", order.AppID);
                    command.Parameters.AddWithValue("@param_user_id", order.UserID);
                    command.Parameters.AddWithValue("@param_order_name", order.OrderName);
                    command.Parameters.AddWithValue("@param_order_email", order.OrderEmail);
                    command.Parameters.AddWithValue("@param_order_date", now);
                    command.Parameters.AddWithValue("@param_order_confirmation", order.OrderConfirmation);
                    command.Parameters.AddWithValue("@param_order_status", order.OrderStatus);
                    command.Parameters.AddWithValue("@param_total_before_taxes", order.TotalBeforeTaxes);
                    command.Parameters.AddWithValue("@param_total_after_taxes", order.TotalAfterTaxes);

                    mySqlConnection.Open();
                    command.ExecuteNonQuery();

                    //2- We get the inserted order's ID 
                    object insertedIDObject = MySqlHelper.ExecuteScalar(mySqlConnection, "SELECT max(order_id) from fact_orders;");


                    insertedIDObject = (insertedIDObject == DBNull.Value) ? null : insertedIDObject;
                    orderID = Convert.ToInt32(insertedIDObject);


                    //3- If the insertion was a success we insert the items
                    if (orderID > -1)
                    {

                        var orderBillingInfo = new OrderBillingInfo();
                        orderBillingInfo.orderNumber = order.OrderID.ToString("D" + (order.OrderID.ToString("D").Length + 5).ToString());
                        orderBillingInfo.orderDate = now;
                        orderBillingInfo.productOrdered = new ArrayList();

                        foreach (var item in order.Items)
                        {
                            command = mySqlConnection.CreateCommand();
                            command.CommandText = "insertNewOrderItem";
                            command.CommandType = System.Data.CommandType.StoredProcedure;
                            command.Parameters.AddWithValue("@param_app_id", order.AppID);
                            command.Parameters.AddWithValue("@param_order_id", orderID);
                            command.Parameters.AddWithValue("@param_quantity", item.Quantity);
                            command.Parameters.AddWithValue("@param_product_id", item.Product.ProductID);

                            var price = (item.Quantity >= item.Product.GrossMinimumQuantity) ?
                                item.Product.GrossPrice :
                                item.Product.Price;

                            command.Parameters.AddWithValue("@param_unit_price", price);
                            command.ExecuteNonQuery();

                            var productOrdered = new ProductOrdered();
                            productOrdered.Reference = item.Product.Reference;
                            productOrdered.Designation = item.Product.ProductName;
                            productOrdered.Quantity = item.Quantity.ToString();
                            productOrdered.Unite = "";
                            productOrdered.PrixUHT = price.ToString();
                            productOrdered.Remise = 0.ToString();
                            productOrdered.MontantHT = (item.Quantity * price).ToString();
                            productOrdered.TXTVA = item.Product.TauxTva.ToString();

                            orderBillingInfo.productOrdered.Add(productOrdered);
                            orderBillingInfo.totalHT = orderBillingInfo.totalHT + item.Quantity * price;
                            orderBillingInfo.totalRemise = 0;
                            orderBillingInfo.totalTTC = orderBillingInfo.netApayer + (item.Quantity * price) * (1 + item.Product.TauxTva / 100);
                            orderBillingInfo.netApayer = orderBillingInfo.netApayer + (item.Quantity * price) * (1 + item.Product.TauxTva / 100) - orderBillingInfo.totalRemise;
                        }
                        var orderInformation = new GetOrderInfoParam();
                        orderInformation.app_id = order.AppID.ToString();
                        orderInformation.user_id = order.UserID.ToString();
                        orderInformation.order_id = orderID.ToString();
                        orderInformation.orderNumber = order.OrderConfirmation;
                        orderInformation.pdfFileName = Path.Combine(PHARMA_PRODUCT_BILLINGS_FILES, "" + order.AppID.ToString() + @"/" + order.AppID.ToString() + "_" + order.UserID.ToString() + "_BC_" + order.OrderConfirmation + ".pdf");
                        orderInformation.skybill_file = Path.Combine(PHARMA_PRODUCT_BILLINGS_FILES, "" + order.AppID.ToString() + @"/" + order.AppID.ToString() + "_" + order.UserID.ToString() + "_SB_" + order.OrderConfirmation + ".pdf");
                        orderInformation.combine_skybill_bill_file = Path.Combine(PHARMA_PRODUCT_BILLINGS_FILES, "" + order.AppID.ToString() + @"/" + order.AppID.ToString() + "_" + order.UserID.ToString() + "_SBBC_" + order.OrderConfirmation + ".pdf");

                        AppInfo appinfo = new AppInfo();
                        appinfo = GetAppInfo(order.AppID.ToString());

                        //FileHelper.debugFile(order.AppID.ToString() + " -  " + order.UserID.ToString(), order.UserID.ToString());

                        var getUserInfo = new GetUserInfoParam();
                        getUserInfo.UserID = order.UserID;
                        getUserInfo.AppID = order.AppID.ToString();

                        UserInfo userinfo = new UserInfo();
                        userinfo = GetUserInfo(getUserInfo);

                        //5 appel du service chronopost

                        FileHelper.CreatepdfOrderFile(appinfo, userinfo, orderInformation, orderBillingInfo);
                        List<string> mylistofFileTocombine = new List<string>(new string[] { orderInformation.pdfFileName });

                        getChronopostDeliveryInfo(appinfo, userinfo, orderInformation, mylistofFileTocombine);
                        GetTrackingStatusByOrderID(orderID.ToString(), order.UserID.ToString(), order.AppID.ToString(), order.OrderConfirmation, orderInformation.skybillNumber);

                        //merge bc and skybill file

                        FileHelper.MergePdf(mylistofFileTocombine, orderInformation.combine_skybill_bill_file);
                        //7- Send email to the user and to the seller

                        #region ParallelTasks
                        // Perform three tasks in parallel on the source array
                        Parallel.Invoke(() =>
                        {
                            FileHelper.CreateMessageWithAttachment("smtp.ionos.fr", 587, orderInformation.combine_skybill_bill_file, "noreply@epharmapp.com", "M2uewGYhWVpqjVJYWPWa", appinfo.Email, "Test", "Test body");
                        },  // close first Action

                                       () =>
                                       {

                                           FileHelper.CreateMessageWithAttachment("smtp.ionos.fr", 587, orderInformation.combine_skybill_bill_file, "noreply@epharmapp.com", "M2uewGYhWVpqjVJYWPWa", userinfo.email, "Test", "Test body");
                                       } //close second Action
                                     ); //close parallel.invoke
                        #endregion
                    }
                }
            }
            return (orderID > -1) ? order.OrderConfirmation : "";
        }

        public void getChronopostDeliveryInfo(AppInfo appinfo, UserInfo userinfo, GetOrderInfoParam orderInformation, List<string> mylistofFileTocombine)
        {

            Shipping instanceWebservice = new Shipping(appinfo, userinfo, orderInformation, mylistofFileTocombine);
            //construie une fonction qui estime les valeurs des paramètres w/h/l/w en fonction dumontant total de l'offre et du poid de chaque produit
            orderInformation.width = 50;
            orderInformation.height = 50;
            orderInformation.length = 50;
            orderInformation.weight = 10;

            //calcul du produit
            resultCalculateProducts product = instanceWebservice.getProductCode();

            //  calcul du delai d'envoi 
            resultCalculateDeliveryTime deliveryDate = instanceWebservice.getDeliveryDate();

            //Effectuer la demande de ticket de transport pour une livraison
            resultReservationMultiParcelExpeditionValue ship = instanceWebservice.getTicketInfoV1();

            //1- We insert the result of chronopost delivery request in the database
            using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
            {
                MySqlCommand command = mySqlConnection.CreateCommand();
                command = mySqlConnection.CreateCommand();
                command.CommandText = "insertNewDeliveryResult";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@app_id", orderInformation.app_id);
                command.Parameters.AddWithValue("@user_id", orderInformation.user_id);
                command.Parameters.AddWithValue("@order_id", orderInformation.order_id);
                command.Parameters.AddWithValue("@orderNumber", orderInformation.orderNumber);
                command.Parameters.AddWithValue("@reservationNumber", orderInformation.reservationNumber);
                command.Parameters.AddWithValue("@pickupDate", orderInformation.pickupDate);
                command.Parameters.AddWithValue("@errorCode", orderInformation.errorCode);
                command.Parameters.AddWithValue("@errorMessage", orderInformation.errorMessage);
                command.Parameters.AddWithValue("@ESDFullNumber", orderInformation.ESDFullNumber);
                command.Parameters.AddWithValue("@ESDNumber", orderInformation.ESDNumber);
                command.Parameters.AddWithValue("@pickupDateSpecified", orderInformation.pickupDateSpecified);
                command.Parameters.AddWithValue("@DSort", orderInformation.DSort);
                command.Parameters.AddWithValue("@OSort", orderInformation.OSort);
                command.Parameters.AddWithValue("@codeDepot", orderInformation.codeDepot);
                command.Parameters.AddWithValue("@codeService", orderInformation.codeService);
                command.Parameters.AddWithValue("@destinationDepot", orderInformation.destinationDepot);
                command.Parameters.AddWithValue("@geoPostCodeBarre", orderInformation.geoPostCodeBarre);
                command.Parameters.AddWithValue("@geoPostNumeroColis", orderInformation.geoPostNumeroColis);
                command.Parameters.AddWithValue("@groupingPriorityLabel", orderInformation.groupingPriorityLabel);
                command.Parameters.AddWithValue("@RMPV_reservationNumber", orderInformation.RMPV_reservationNumber);
                command.Parameters.AddWithValue("@serviceMark", orderInformation.serviceMark);
                command.Parameters.AddWithValue("@serviceName", orderInformation.serviceName);
                command.Parameters.AddWithValue("@signaletiqueProduit", orderInformation.signaletiqueProduit);
                command.Parameters.AddWithValue("@skybillNumber", orderInformation.skybillNumber);
                command.Parameters.AddWithValue("@pdfFileName", orderInformation.pdfFileName);
                command.Parameters.AddWithValue("@delivery_date", orderInformation.delivery_date);
                command.Parameters.AddWithValue("@delivery_cost", orderInformation.delivery_cost);
                command.Parameters.AddWithValue("@delivery_product_code", orderInformation.delivery_product_code);
                command.Parameters.AddWithValue("@skybill_file", orderInformation.skybill_file);
                command.Parameters.AddWithValue("@combine_skybill_bill_file", orderInformation.skybill_file);

                mySqlConnection.Open();
                command.ExecuteNonQuery();

            }


        }

        public IList<Order> GetOrdersByUser(GetOrdersByUserIDParam getOrdersByUserIDParam)
        {
            IList<Order> result = null;
            using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
            {
                if (getOrdersByUserIDParam != null && getOrdersByUserIDParam.AppID > -1)
                {
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "getOrdersByUserID";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@param_app_id", getOrdersByUserIDParam.AppID);
                    command.Parameters.AddWithValue("@param_user_id", getOrdersByUserIDParam.UserID);
                    command.Parameters.AddWithValue("@param_page_id", getOrdersByUserIDParam.PageID);
                    command.Parameters.AddWithValue("@param_max_count", getOrdersByUserIDParam.MaxCount);

                    mySqlConnection.Open();
                    using (MySqlDataReader mysqlReader = command.ExecuteReader())
                    {
                        result = new List<Order>();
                        while (mysqlReader.Read())
                        {
                            Order order = new Order
                            {
                                OrderID = mysqlReader.GetInt32("order_id"),
                                UserID = mysqlReader.GetInt32("user_id"),
                                OrderDate = mysqlReader.GetStringSafe("order_date"), //convert to DateTime format
                                OrderName = mysqlReader.GetStringSafe("order_name"),
                                OrderEmail = mysqlReader.GetStringSafe("order_email"),
                                OrderStatus = mysqlReader.GetStringSafe("order_status"),
                                OrderConfirmation = mysqlReader.GetStringSafe("order_confirmation"),
                                TotalBeforeTaxes = mysqlReader.GetFloat("total_before_taxes"),
                                TotalAfterTaxes = mysqlReader.GetFloat("total_after_taxes"),
                                AppID = mysqlReader.GetInt32("app_id")
                            };
                            if (order != null)
                            {
                                order.Items = GetOrderItemsByOrderID(new GetOrderItemsByOrderIDParam { AppID = order.AppID, OrderID = order.OrderID });
                                result.Add(order);
                            }
                        }
                    }
                }
            }
            return result;
        }

        public int GetOrdersByUserCount(GetCountOrdersByUserIDParam getOrdersByUserIDParam)
        {
            int amount = 0;
            if (getOrdersByUserIDParam != null)
            {
                using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
                {
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "getCountOrdersByUserID";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@param_app_id", getOrdersByUserIDParam.AppID);
                    command.Parameters.AddWithValue("@param_user_id", getOrdersByUserIDParam.UserID);

                    mySqlConnection.Open();
                    amount = int.Parse(command.ExecuteScalar().ToString());
                }
            }
            return amount;
        }

        public Order GetOrderByID(GetOrderByIDParam getOrderByIDParam)
        {
            Order result = null;
            using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
            {
                if (getOrderByIDParam != null && getOrderByIDParam.AppID > -1)
                {
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "getOrderByID";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@param_app_id", getOrderByIDParam.AppID);
                    command.Parameters.AddWithValue("@param_order_id", getOrderByIDParam.OrderID);

                    mySqlConnection.Open();
                    using (MySqlDataReader mysqlReader = command.ExecuteReader())
                    {
                        while (mysqlReader.Read())
                        {
                            result =
                                new Order
                                {
                                    OrderID = mysqlReader.GetInt32("order_id"),
                                    UserID = mysqlReader.GetInt32("user_id"),
                                    OrderDate = mysqlReader.GetStringSafe("order_date"), //convert to DateTime format
                                    OrderName = mysqlReader.GetStringSafe("order_name"),
                                    OrderEmail = mysqlReader.GetStringSafe("order_email"),
                                    OrderStatus = mysqlReader.GetStringSafe("order_status"),
                                    OrderConfirmation = mysqlReader.GetStringSafe("order_confirmation"),
                                    TotalBeforeTaxes = mysqlReader.GetFloat("total_before_taxes"),
                                    TotalAfterTaxes = mysqlReader.GetFloat("total_after_taxes"),
                                    AppID = mysqlReader.GetInt32("app_id")
                                };

                            //we download the order's items
                            if (result != null)
                                result.Items = GetOrderItemsByOrderID(new GetOrderItemsByOrderIDParam { AppID = result.AppID, OrderID = result.OrderID });
                        }
                    }
                }
            }
            return result;
        }

        public IList<OrderItem> GetOrderItemsByOrderID(GetOrderItemsByOrderIDParam getOrderItemsByOrderIDParam)
        {
            IList<OrderItem> result = null;
            using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
            {
                if (getOrderItemsByOrderIDParam != null && getOrderItemsByOrderIDParam.AppID > -1)
                {
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "getOrderItemsByOrderID";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@param_app_id", getOrderItemsByOrderIDParam.AppID);
                    command.Parameters.AddWithValue("@param_order_id", getOrderItemsByOrderIDParam.OrderID);

                    mySqlConnection.Open();
                    using (MySqlDataReader mysqlReader = command.ExecuteReader())
                    {
                        String directoryExtention = "" + getOrderItemsByOrderIDParam.AppID + @"/";
                        result = new List<OrderItem>();
                        while (mysqlReader.Read())
                        {
                            result.Add(new OrderItem
                            {
                                OrderItemID = mysqlReader.GetInt32("orders_items_id"),
                                OrderID = mysqlReader.GetInt32("order_id"),
                                Quantity = mysqlReader.GetInt32("quantity"),
                                Product = new Product
                                {
                                    AppID = mysqlReader.GetInt32("app_id"),
                                    ProductID = mysqlReader.GetInt32("product_id"),
                                    ProductName = mysqlReader.GetStringSafe("product_name"),
                                    CategoryID = mysqlReader.GetInt32("productcategory_id"),
                                    ManufacturerID = mysqlReader.GetInt32("manufacturer_id"),
                                    FileName1 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename1")),
                                    FileName2 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename2")),
                                    FileName3 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename3")),
                                    Solution = mysqlReader.GetStringSafe("solution"),
                                    Specificities = mysqlReader.GetStringSafe("specificities"),
                                    Reference = mysqlReader.GetStringSafe("reference"),
                                    Description = mysqlReader.GetStringSafe("description"),
                                    Usages = mysqlReader.GetStringSafe("usages"),
                                    Composition = mysqlReader.GetStringSafe("composition"),
                                    Contraindication = mysqlReader.GetStringSafe("contre_indication"),
                                    Posologie = mysqlReader.GetStringSafe("posologie"),
                                    Notice = mysqlReader.GetStringSafe("notice"),
                                    Taille = mysqlReader.GetStringSafe("taille"),
                                    Price = mysqlReader.GetDouble("price"),
                                    GrossPrice = mysqlReader.GetDouble("gross_price"),
                                    TauxTva = mysqlReader.GetDouble("taux_tva"),
                                    Active = mysqlReader.GetInt32("active"),
                                    Inventory = mysqlReader.GetStringSafe("inventory"),
                                    InsertDatetime = mysqlReader.GetStringSafe("insert_datetime"),
                                    GrossMinimumQuantity = mysqlReader.GetInt32("gross_minimum_quantity"),
                                    Promotion = mysqlReader.GetStringSafe("promotion")
                                },

                            });
                        }
                    }
                }
            }
            return result;
        }

        public Product GetProduct(String appID, String productID)
        {
            Product result = null;
            if (!string.IsNullOrEmpty(appID) && !string.IsNullOrEmpty(productID))
            {
                using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
                {
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "getProduct";
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@param_app_id", appID);
                    command.Parameters.AddWithValue("@param_product_id", productID);

                    mySqlConnection.Open();
                    using (MySqlDataReader mysqlReader = command.ExecuteReader())
                    {
                        String directoryExtention = "" + appID + @"/";
                        while (mysqlReader.Read())
                        {
                            result = new Product
                            {
                                AppID = mysqlReader.GetInt32("app_id"),
                                ProductID = mysqlReader.GetInt32("product_id"),
                                ProductName = mysqlReader.GetStringSafe("product_name"),
                                CategoryID = mysqlReader.GetInt32("productcategory_id"),
                                ManufacturerID = mysqlReader.GetInt32("manufacturer_id"),
                                FileName1 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename1")),
                                FileName2 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename2")),
                                FileName3 = FileHelper.GetLinkPath(PHARMA_WEBSERVICE_PRODUCTS_LINK + directoryExtention, mysqlReader.GetStringSafe("filename3")),
                                Solution = mysqlReader.GetStringSafe("solution"),
                                Specificities = mysqlReader.GetStringSafe("specificities"),
                                Reference = mysqlReader.GetStringSafe("reference"),
                                Description = mysqlReader.GetStringSafe("description"),
                                Usages = mysqlReader.GetStringSafe("usages"),
                                Composition = mysqlReader.GetStringSafe("composition"),
                                Contraindication = mysqlReader.GetStringSafe("contre_indication"),
                                Posologie = mysqlReader.GetStringSafe("posologie"),
                                Notice = mysqlReader.GetStringSafe("notice"),
                                Taille = mysqlReader.GetStringSafe("taille"),
                                Price = mysqlReader.GetDouble("price"),
                                GrossPrice = mysqlReader.GetDouble("gross_price"),
                                TauxTva = mysqlReader.GetDouble("taux_tva"),
                                Active = mysqlReader.GetInt32("active"),
                                Inventory = mysqlReader.GetStringSafe("inventory"),
                                InsertDatetime = mysqlReader.GetStringSafe("insert_datetime"),
                                GrossMinimumQuantity = mysqlReader.GetInt32("gross_minimum_quantity"),
                                Promotion = mysqlReader.GetStringSafe("promotion")
                            };
                        }
                    }
                }
            }
            return result;
        }

        public int GetProductsByCategoryIDSearchCount(GetCountProductsByCategoryIDSearchParam getCountProductsByCategoryIDSearchParam)
        {
            int amount = 0;
            if (getCountProductsByCategoryIDSearchParam != null)
            {
                using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
                {
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "getCountProductsByCategoryIDSearch";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@param_app_id", getCountProductsByCategoryIDSearchParam.AppID);
                    command.Parameters.AddWithValue("@param_category_id", getCountProductsByCategoryIDSearchParam.CategoryID);
                    command.Parameters.AddWithValue("@param_search", getCountProductsByCategoryIDSearchParam.SearchValue);
                    command.Parameters.AddWithValue("@param_level", getCountProductsByCategoryIDSearchParam.CategoryLevel);

                    mySqlConnection.Open();
                    amount = int.Parse(command.ExecuteScalar().ToString());
                }
            }
            return amount;
        }

        public int GetProductsByCategoryIDCount(GetCountProductsByCategoryIDParam getCountProductsByCategoryIDParam)
        {
            int amount = 0;
            if (getCountProductsByCategoryIDParam != null)
            {
                using (var mySqlConnection = new MySqlConnection(connectionStringBuilder.ToString()))
                {
                    MySqlCommand command = mySqlConnection.CreateCommand();
                    command.CommandText = "getCountProductsByCategoryID";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@param_app_id", getCountProductsByCategoryIDParam.AppID);
                    command.Parameters.AddWithValue("@param_category_id", getCountProductsByCategoryIDParam.CategoryID);

                    mySqlConnection.Open();
                    amount = int.Parse(command.ExecuteScalar().ToString());
                }
            }
            return amount;
        }
        //not unit tested
        public string GetProductDetails(string textLink)
        {
            string value = File.ReadAllText(PHARMA_PRODUCT_DETAIL_FILES + textLink);
            return value != null ? value.Replace(@"\", "") : "";
        }
        public class ShippingService
        {
        }
    }
}*/
