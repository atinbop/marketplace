﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace MarketPlace.Model
{
    public class SpecialDiscounts
    {
        [Key]
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public Nullable<decimal> BasePrice { get; set; }
        public string TurnOver { get; set; }

        public string DiscountAmount { get; set; }
        public DateTime DiscountStartDate { get; set; }
        public DateTime DiscountEndDate { get; set; }
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
 
        public int Score { get; set; }
        public string UserCompanyType { get; set; }
        public Nullable<int> ContactId { get; set; }
    }
}
