﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Currency
    {
        [Key]
        public int Id { get; set; }
        public string   Entity {get;set;}
        public string CurrencyName {get;set;}
        public string AlphabeticCode { get;set;}
        public string NumericCode { get;set;}
        public string  MinorUnit { get;set;}
        public string  WithdrawalDate { get;set;}
        public string  symbol { get;set;}
        public string  unicodedecimal { get;set;}
        public string  unicodehex { get;set;}
        


    }
}
