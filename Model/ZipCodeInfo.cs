﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class ZipCodeInfo
    {
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string zip_code { get; set; }
        public string flag_path { get; set; }
        public string currency { get; set; }
        public string intphonecode { get; set; }
        public string country_code3 { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public string admin_name1 { get; set; }
        public string admin_name2 { get; set; }
        public string admin_name3 { get; set; }
        public string place_name { get; set; }


    }
}
