﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Posts
    {
        [Key]
        public int Id { get; set; }
        public string PostName { get; set; }
        public string PostDescription { get; set; }
        public string PostContent { get; set; }


        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
        public int Deleted { get; set; }
        public string PostImage { get; set; }
        public string PostFile { get; set; }
        public int ArticleId { get; set; }
        public int PartnerEmployeeId { get; set; }
        public string UpdateByPseudo { get; set; }
   
        public string slotLastUpdate { get; set; }
    public Posts()
        {
            LastUpdate = DateTime.Now;
        }

        public static implicit operator Posts(List<Posts> v)
        {
            throw new NotImplementedException();
        }
    }
}
