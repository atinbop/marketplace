﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class GetCountProductsByCategoryIDSearchParam : GetCountProductsByCategoryIDParam
    {
        public String SearchValue { get; set; }
        public int CategoryLevel { get; set; }
    }
}
