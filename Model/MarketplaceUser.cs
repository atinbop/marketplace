﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class MarketplaceUser : IdentityUser
    {
        [PersonalData]
        public string Name { get; set; }

        [PersonalData]
        public string FirstName { get; set; }

        [PersonalData]
        public string CompanyName { get; set; }


        [PersonalData]
        public string ActivitySector { get; set; }

        [PersonalData]
        public string Adresse { get; set; }

        [PersonalData]
        public string PostalCode { get; set; }

        [PersonalData]
        public string City { get; set; }

        [PersonalData]
        public string usercountry { get; set; }
        [PersonalData]
        public string Position { get; set; }

        [PersonalData]
        public string Civility { get; set; }
        [PersonalData]
        public string Siren { get; set; }
        [PersonalData]
        public string TVAIntracommunautaire { get; set; }
    }

}
