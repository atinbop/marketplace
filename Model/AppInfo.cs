﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class AppInfo
    {
        public long AppID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string ProfessionalNumber { get; set; }
        public string BankAccountFile { get; set; }
        public string StripePK { get; set; }
        public string StripeAK { get; set; }

        public string inscription_rcs { get; set; }
        public string code_ape { get; set; }
        public string site_web { get; set; }
        public string photo_principale_file { get; set; }
        public string whatapp_Number { get; set; }
        public string flag_wholesaler { get; set; }
        public string logo { get; set; }
        public string fax_number { get; set; }
        public string numero_siren { get; set; }
        public string numero_tva { get; set; }
        public string civility { get; set; }
        public string country_code { get; set; }
        public int chronopost_account_number { get; set; }
        public string chronopost_account_password { get; set; }
        public string phone { get; set; }
        public string address2 { get; set; }
        public string ZipCode { get; set; }
        public string city { get; set; }
        public string numero_siret { get; set; }
        public string presentation { get; set; }
        public string specialite { get; set; }

        public string titulaires { get; set; }
        public string introduction { get; set; }
        public string carousel9 { get; set; }
        public string carousel1 { get; set; }
        public string carousel2 { get; set; }
        public string carousel3 { get; set; }
        public string carousel4 { get; set; }
        public string carousel5 { get; set; }
        public string carousel6 { get; set; }
        public string carousel7 { get; set; }
        public string carousel8 { get; set; }
        public string facebook { get; set; }

        public string twitter { get; set; }
        public string googleplus { get; set; }
        public string linkedin { get; set; }
        public string instagram { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string specialite1 { get; set; }
        public string specialite2 { get; set; }
        public string specialite3 { get; set; }
        public string specialite4 { get; set; }
        public string specialite5 { get; set; }
        public string specialite6 { get; set; }
        public string CGV { get; set; }
        public string politique_confidentialite { get; set; }
        public string mentions_legales { get; set; }
        public string horaire_jour { get; set; }
        public string visit_card_picture { get; set; }
    }
}
