﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class GetCountOrdersByUserIDParam
    {
        public int AppID { get; set; }
        public int UserID { get; set; }
    }
}
