﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class DeliveryInfo
    {
        public int app_id { get; set; }
        public string OrderID { get; set; }
        public string orderNumber { get; set; }
        public int user_id { get; set; }
        public string reservationNumber { get; set; }
        public DateTime pickupDate { get; set; }
        public string errorCode { get; set; }
        public string errorMessage { get; set; }
        public string ESDFullNumber { get; set; }
        public string ESDNumber { get; set; }
        public string DSort { get; set; }
        public string OSort { get; set; }
        public string codeDepot { get; set; }
        public string codeService { get; set; }
        public string geoPostNumeroColis { get; set; }
        public string skybillNumber { get; set; }
        public string pdfFileName { get; set; }
        public DateTime delivery_date { get; set; }
        public float delivery_cost { get; set; }
        public string combine_skybill_bill_file { get; set; }
    }
}
