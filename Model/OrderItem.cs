﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Model
{
    public class OrderItem
    {
        [Key]
        public int OrderItemID { get; set; }
        public int OrderID { get; set; }
 
        public Nullable<int> quantity { get; set; } 
        public Nullable<DateTime> orderDate { get; set; }
        public Nullable<int> articleId  { get; set; }



        public Nullable<int> employeeId { get; set; }

        public Nullable<int> contactId { get; set; }
        public Nullable<int> articleVendorsId { get; set; }
}
}
