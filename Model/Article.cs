﻿using MarketPlace.Pages.CommonCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Article


    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        
        [Required]
        [StringLength(13)]
        /* [CustomRemoteValidation("IsProductNameExist", "Product",
          AdditionalFields = "Id", ErrorMessage = "Product EAN Code already exists")]*/ /*Controle de l'unicite du code AEN a corriger*/
        public string EAN { get; set; }

        [Required]
        public decimal Price { get; set; }
        public Nullable<decimal> B2CPrice { get; set; }
        public Nullable<decimal> VAT { get; set; }

 

        [StringLength(255)]
        public string FirstCategory { get; set; }

        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Notice { get; set; }
        public string SEOTitle { get; set; }
        public string SEODescription { get; set; }
        public string Ingredient { get; set; }
        public string ArticleFile { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }

        [DefaultValue(0)]
        public Nullable<int> Inventory { get; set; }
        public Nullable<int> B2CInventory { get; set; }
        public string YoutubeVideo { get; set; }
        public string FacebookPage { get; set; }
        public string TwitterPage { get; set; }
        public string InstagramPage { get; set; }
        public string OfficialWebsite { get; set; }
        public string youtubePage { get; set; }
        public string Indication { get; set; }
        public string Brand { get; set; }
        public string WarningMessage { get; set; }



        public string ArticleFilesLink { get; set; }
        public string ActiveSubtances { get; set; }


        public string SecondCategory { get; set; }
        public string ThirdCategory { get; set; }
        public Nullable<int> Weight { get; set; }
        public Nullable<int> Volume { get; set; }
        public Nullable<int> Height { get; set; }
        public Nullable<int> Width { get; set; }
        public Nullable<int> Depth { get; set; }
        public Nullable<int> Lot { get; set; }
        public Nullable<int> B2CLot { get; set; }
        public string OriginCountry { get; set; }
        public string CustomsCode { get; set; }
        public Nullable<decimal> BuyingPrice { get; set; }
        public string PriceType { get; set; }
        public Nullable<decimal> RefundRate { get; set; }

        public string SaleManagerName { get; set; }
        public string SaleManagerFirstname { get; set; }
        public string SaleManagerCivility { get; set; }
        public string SaleManagerEmail { get; set; }
        public string SaleManagerTelephone { get; set; }
        public Nullable<decimal> FrancoPortAmount { get; set; }
        public Nullable<int> MinimumOrderQuantity { get; set; }
        public Nullable<int> MaximumOrderQuantity { get; set; }
        public string DropShippingEmail { get; set; }
        public Nullable<decimal> SupplierAdvisedResalePrice { get; set; }

        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; } = DateTime.Now;
        public Nullable<int> PROSellerCompanyId { get; set; }

 
        public string SuplierArticleReference { get; set; }
        public string SalesChannel { get; set; }
        public string ArticleValidatedFlag { get; set; }
        public string EndOfLifeFlag { get; set; }

        public Nullable<DateTime> ProductionDate { get; set; }
        public string NoveltyFlag { get; set; }
        public string AccountingCode { get; set; }
        public string ProductType { get; set; }
        public string ArticleKeywords { get; set; }
        public Nullable<int> DeliveryDelay { get; set; }
        public string PurchaseNomenclature { get; set; }
        public string CommercialTaget { get; set; }
        public Nullable<decimal> EcoTaxAmount { get; set; }
        public string Destockage { get; set; }
        public string SaleManagerFax { get; set; }
        public string EN_Name { get; set; }

        public string EN_ShortDescription { get; set; }
        public string EN_LongDescription { get; set; }
        public string EN_Notice { get; set; }
        public string EN_ArticleKeywords { get; set; }
        public string EN_SEOTitle { get; set; }
        public string EN_SEODescription { get; set; }
        public string EN_Ingredient { get; set; }
        public string EN_ArticleFile { get; set; }
        public string EN_Indication { get; set; }


        public string EN_ActiveSubtances { get; set; }
        public string ProductShopLink { get; set; }
        public string BrandShopLink { get; set; }
        public string SaleAuthorizedInPharmacy { get; set; }
        public string ShelfLife { get; set; }
        public string Internalreference { get; set; }
        public string Variantreference { get; set; }
        public string Groupcode { get; set; }
        public string ArticleOtherName { get; set; }
        public string En_ArticleOtherName { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }
        public string Image6 { get; set; }
        public string Image7 { get; set; }
        public string Image8 { get; set; }
        public string Image9 { get; set; }
        public string Image10 { get; set; }

        public Nullable<DateTime> NextProductionDate { get; set; }
        public string ProductionFrequency { get; set; }
        public Nullable<int> NextProductionEstimateQuantity { get; set; }

        public Nullable<decimal> FirstOrderMinimalAmount { get; set; }
        public Nullable<DateTime> Created { get; set; } = DateTime.Now;


        public string Code_CIP7 { get; set; }
        public string Modele { get; set; }
        public string Gamme { get; set; }
        public string UPC { get; set; }
        public string Contenance { get; set; }
        public string Forme { get; set; }
        public string Taille { get; set; }
        public string Conseiller_pour { get; set; }
        public string Dimension { get; set; }

        public string Autres_caracteristiques { get; set; }
        public string Couleur { get; set; }
        public string Pour_qui { get; set; }
        public string Presentation { get; set; }
        public string Indication_autres { get; set; }
        public string Type_de_cheveux { get; set; }
        public string Type_de_peau { get; set; }
        public string Parfum { get; set; }
        public string Texture { get; set; }
        public string Pour_quel_animal { get; set; }
        public string Type_de_shampooing { get; set; }
        public string Mode_administration { get; set; }

        public string Usage { get; set; }
        public string Video1 { get; set; }
        public string ReturnFlag { get; set; }
        public string OtherInformation { get; set; }
        public Nullable<int> AttractivenessIndex { get; set; }
        public Nullable<int> BrandId { get; set; }
        public Nullable<int> EmployeeId { get; set; }

    
        public Nullable<int> Note { get; set; }

 
        public Nullable<int> UnreadMessagesNumber { get; set; }
        public string barCodeImage { get; set; }

        public string categoryMarketing { get; set; }

        public Nullable<int> SaleAreaId { get; set; }
        public string SaleAreaFullName          { get; set; }
        public string ProductDangerosity { get; set; }

        /*  public static implicit operator Article(ValueTask<Article> v)
          {
              throw new NotImplementedException();
          }*/

        public string DE_Name { get; set; }
        public string DE_ShortDescription { get; set; }
        public string DE_LongDescription { get; set; }
        public string DE_Notice { get; set; }
        public string DE_ArticleKeywords { get; set; }
        public string DE_SEOTitle { get; set; }
        public string DE_SEODescription { get; set; }
        public string DE_Ingredient { get; set; }
        public string DE_ArticleFile { get; set; }
        public string DE_Indication { get; set; }
        public string DE_ActiveSubtances { get; set; }
        public string DE_ArticleOtherName { get; set; }

        public string ES_Name { get; set; }
        public string ES_ShortDescription { get; set; }
        public string ES_LongDescription { get; set; }
        public string ES_Notice { get; set; }
        public string ES_ArticleKeywords { get; set; }
        public string ES_SEOTitle { get; set; }
        public string ES_SEODescription { get; set; }
        public string ES_Ingredient { get; set; }
        public string ES_ArticleFile { get; set; }
        public string ES_Indication { get; set; }
        public string ES_ActiveSubtances { get; set; }
        public string ES_ArticleOtherName { get; set; }



        public string CN_Name { get; set; }
        public string CN_ShortDescription { get; set; }
        public string CN_LongDescription { get; set; }
        public string CN_Notice { get; set; }
        public string CN_ArticleKeywords { get; set; }
        public string CN_SEOTitle { get; set; }
        public string CN_SEODescription { get; set; }
        public string CN_Ingredient { get; set; }
        public string CN_ArticleFile { get; set; }
        public string CN_Indication { get; set; }
        public string CN_ActiveSubtances { get; set; }
        public string CN_ArticleOtherName { get; set; }
    }


}
