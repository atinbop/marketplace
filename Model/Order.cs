﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Order { 
        public long OrderID { get; set; }
    public int UserID { get; set; }
    public String OrderDate { get; set; }
    public String OrderName { get; set; }
    public String OrderEmail { get; set; }
    public String OrderConfirmation { get; set; }
    public String OrderStatus { get; set; } //to be turned into and Enum {PENDING, COMPLETED, CANCELLED}
    public int AppID { get; set; }
    public float TotalBeforeTaxes { get; set; }
    public float TotalAfterTaxes { get; set; }

    //public IList<OrderItem> Items { get; set; }
}
}
