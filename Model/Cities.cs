﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Cities
    {
        public int  id { get; set; }
        public string name { get; set; }
        public string divisionCode { get; set; }
        public string divisionId { get; set; }
        public string Code_commune_INSEE { get; set; }
        public string CodePostal { get; set; }
        public string Secondname { get; set; }
        public string Acheminement { get; set; }
        public string coordonnees_gps { get; set; }
        public string country { get; set; }

    }
}
