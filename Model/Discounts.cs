﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarketPlace.Model
{
    public class Discounts
    {
        [Key]
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public Nullable<decimal> BasePrice { get; set; }
      
        public DateTime DiscountStartDate { get; set; }
        public DateTime DiscountEndDate { get; set; }
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public string Type { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
    
        public string UserCompanyType { get; set; }
        public Nullable<int>  ContactId { get; set; }

        public string DiscountPrice1 { get; set; }

        public string DiscountRate1 { get; set; }

        public Nullable<int> Quantity1 { get; set; }

        public string DiscountPrice2 { get; set; }

        public string DiscountRate2 { get; set; }

        public Nullable<int> Quantity2 { get; set; }



        public string DiscountPrice3 { get; set; }

        public string DiscountRate3 { get; set; }

        public Nullable<int> Quantity3 { get; set; }

      
        public Nullable<int> currentorderQuantity { get; set; }
        public Nullable<int> active { get; set; }


        public Nullable<int> articleVendorsId { get; set; }
        public Nullable<int>   flagDiscount  { get; set; }
       
}
}
