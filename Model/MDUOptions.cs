﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class MDUOptions
    {
        public string ImageRepository { get; set; }
        public string ConnectedUserEmail { get; set; }

        public string UserCompanyType { get; set; }
        public int UserCompanyId { get; set; }
        public string ConnectedUserId { get; set; }
        public string UserCompanyName { get; set; }
        public string EmployeeImageRepository { get; set; }
        public string BrandImageRepository { get; set; }
        public int NbProductAutorized { get; set; }
        public int NbProductAlreadyChoose { get; set; }
        public string ConnectedUserPseudo { get; set; }
        public string UserCurrency { get; set; }
        public string UserLanguage { get; set; }
        public int NbProductInChart { get; set; }


       
    }


}
