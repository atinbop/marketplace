﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class LogConnexions
    {
        [Key]
        public int Id { get; set; }
public int EmployeeId   { get; set; }
    public Nullable<DateTime> LoginDate { get; set; }
        public Nullable<DateTime> LogoutDate { get; set; }
 
        public string  logSystem { get; set; }
        public Nullable<int> ViewedItem { get; set; }
    }
}
