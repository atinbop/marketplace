﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class TrackingInfo
    {
        public DateTime eventDate { get; set; }
        public string tracking_code { get; set; }
        public bool eventDateSpecified { get; set; }
        public string eventLabel { get; set; }
        public Boolean highPriority { get; set; }
        public Boolean highPrioritySpecified { get; set; }
        public string infoCompname { get; set; }
        public string NPC { get; set; }
        public string officeLabel { get; set; }
        public string zipCode { get; set; }
        public int tracking_errorCode { get; set; }
        public string tracking_errorMessage { get; set; }
        public int pod_errorCode { get; set; }
        public string pod_errorMessage { get; set; }
        public string pod_formatPOD { get; set; }
        public string pod_file { get; set; }
        public Boolean pod_Presente { get; set; }
        public int pod_statusCode { get; set; }
        public int order_id { get; set; }
        public int app_id { get; set; }
        public int user_id { get; set; }
        public string order_confirmation { get; set; }
        public string SkybillNumber { get; set; }

    }
}
