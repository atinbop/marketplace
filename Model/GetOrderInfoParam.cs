﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class GetOrderInfoParam
    {
        public String app_id { get; set; }
        public String user_id { get; set; }
        public String order_id { get; set; }
        public String orderNumber { get; set; }
        public String reservationNumber { get; set; }
        public DateTime pickupDate { get; set; }
        public string errorCode { get; set; }
        public String errorMessage { get; set; }
        public String ESDFullNumber { get; set; }
        public String ESDNumber { get; set; }
        public Boolean pickupDateSpecified { get; set; }

        public String DSort { get; set; }
        public String OSort { get; set; }
        public String codeDepot { get; set; }
        public String codeService { get; set; }
        public String destinationDepot { get; set; }
        public String geoPostCodeBarre { get; set; }
        public String geoPostNumeroColis { get; set; }
        public String groupingPriorityLabel { get; set; }
        public String RMPV_reservationNumber { get; set; }
        public String serviceMark { get; set; }
        public String serviceName { get; set; }
        public String signaletiqueProduit { get; set; }
        public String skybillNumber { get; set; }
        public String pdfFileName { get; set; }
        public DateTime delivery_date { get; set; }
        public float delivery_cost { get; set; }
        public string delivery_product_code { get; set; }
        public string skybill { get; set; }

        public string skybill_file { get; set; }
        public string combine_skybill_bill_file { get; set; }

        public int weight { get; set; }
        public int height { get; set; }
        public int length { get; set; }
        public int width { get; set; }
        public string skybill_pod_file { get; set; }
        public string pod { get; set; }


    }
}
