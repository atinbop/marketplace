﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class VendorsNotes
    {
        public int Id { get; set; }
        public int Note { get; set; }
        public int ContactId { get; set; }
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
    }
}
