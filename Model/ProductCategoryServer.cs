﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class ProductCategoryServer
    {
        public int CategoryKey { get; set; }
        public string CategoryLabel { get; set; }
        public string SubCategoryLabel { get; set; }
        public string SubSubCategoryLabel { get; set; }
        public int SubCategoryID { get; set; }
        public int SubSubCategoryID { get; set; }
        public int CategoryID { get; set; }
    }
}
