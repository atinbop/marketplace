﻿using MarketPlace.Pages.CommonCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Variante


    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(13)]
        /* [CustomRemoteValidation("IsProductNameExist", "Product",
          AdditionalFields = "Id", ErrorMessage = "Product EAN Code already exists")]*/ /*Controle de l'unicite du code AEN a corriger*/
        public string EAN { get; set; }

       
    }


}
