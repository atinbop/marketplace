﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class ArticleCountry
    {
        [Key]
        public int Id { get; set; }
         public int CountryId { get; set; }
        public int ArticleId { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; } = DateTime.Now;

    }
}
