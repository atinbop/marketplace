﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Offers
    {
        public int id { get; set; }
        public  DateTime offerDate { get; set; }
        public string offerName { get; set; }
        public string OfferDescription { get; set; }
        public decimal offerPrice { get; set; }
        public int numberofProduct { get; set; }
        public string SupplierType { get; set; }
        public string Period { get; set; }
        public int offerOrder { get; set; }
        public Nullable<int> NumberOfDisplayProduct { get; set; }
}
}
