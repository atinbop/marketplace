﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Visitors
    {
        [Key]
        public int id {get; set;}
        public string emailAddress { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
        public Nullable<int> Deleted { get; set; }
    }
}
