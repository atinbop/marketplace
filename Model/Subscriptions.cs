﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Subscriptions
    {
        public int id { get; set; }
      public Contacts Contact { get; set; }

      public int OfferId { get; set; }
        public Offers Offer { get; set; }
        public Employees Employee { get; set; }
       
        public DateTime startvalidityDate { get; set; }
        public DateTime endValidityDate { get; set; }

        [DefaultValue(0)]
        public Nullable<int>  cancelsubscription { get; set; }
    }
}
