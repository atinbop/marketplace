﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
      public class PersonModel
    {
        [Key]
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<PersonModel> People { get; set; }
    }
}
