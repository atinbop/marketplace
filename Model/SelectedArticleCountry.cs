﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class SelectedArticleCountry
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int CountryId { get; set; }
        public IEnumerable<Country> CountryCollection { get; set; } 


    }
}

