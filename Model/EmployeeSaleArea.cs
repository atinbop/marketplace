﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class EmployeeSaleArea
    {
        [Key]
        public int Id { get; set; }

        public int SaleAreaId { get; set; }

        public int EmployeeId { get; set; }

        public string CountryName { get; set; }
        public string SaleAreaName { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; } = DateTime.Now;
    }
}
