﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class GetUserInfoParam
    {
        public string AppID { get; set; }
        public string FirebaseID { get; set; }
        public int UserID { get; set; }

    }
}
