﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarketPlace.Model
{
    public class PropertyValue
    {
        [Key]
        [MaxLength(2)]
        public int Id { get; set; }

        [Required]
        public string PropertyValueName { get; set; }
        public string EN_PropertyValueName { get; set; }

        [Required]
        public int PropertyId { get; set; }

        [ForeignKey("PropertyId")]
        public Property Property { get; set; }
        public string PropertyName { get; set; }

        public string PropertyLogo { get; set; }
}
}