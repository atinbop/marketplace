﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{


    public class User 
    {
        [Required, Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required, Display(Name = "Last Name")]
        public string LastName { get; set; }

        [EnumDataType(typeof(Gender))]
        public Gender GenderType { get; set; }
    }

    public enum Gender
    {
        Mr = 1,
        Mme = 2
    }

   
}


