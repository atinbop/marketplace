﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class StaticData
    {
        [Key]
       public  int Id { get; set; }
       public string Code { get; set; }
        public string description { get; set; }
        public string Type { get; set; }

}
}
