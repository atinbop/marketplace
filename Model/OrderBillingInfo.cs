﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class OrderBillingInfo
    {
        public ArrayList productOrdered { get; set; }
        public string orderNumber { get; set; }
        public string orderDate { get; set; }
        public string modePaiement { get; set; }
        public double netApayer { get; set; }
        public double totalHT { get; set; }
        public double remise { get; set; }
        public double totalRemise { get; set; }
        public double totalTTC { get; set; }

    }

    public class ProductOrdered
    {
        public string Reference { get; set; }
        public string Designation { get; set; }
        public string Quantity { get; set; }
        public string Unite { get; set; }
        public string PrixUHT { get; set; }
        public string Remise { get; set; }
        public string MontantHT { get; set; }
        public string TXTVA { get; set; }
    }

}
