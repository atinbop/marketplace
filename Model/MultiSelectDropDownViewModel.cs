﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    namespace SaveMultiSelectDropDown
    {
        using System.Collections.Generic;
        using System.ComponentModel.DataAnnotations;
        using System.Web;


        /// <summary>  
        /// Multi select drop down view model class.  
        /// </summary>  
        public class MultiSelectDropDownViewModel
        {
            #region Properties  

            /// <summary>  
            /// Gets or sets choose multiple countries property.  
            /// </summary>  
            [Required]
            [Display(Name = "Choose Multiple Countries")]
            public List<int> SelectedMultiCountryId { get; set; }

            /// <summary>  
            /// Gets or sets selected countries property.  
            /// </summary>  
            public List<Country> SelectedCountryLst { get; set; }

      


            #endregion
        }
    }
}
