﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class FeedBack
    {
        public int AppID { get; set; }
        public long ProductID { get; set; }
        public String Message { get; set; }
        public int Rating { get; set; }
        public String InsertedTime { get; set; }
        public int UserID { get; set; }
        public String UserName { get; set; }

    }
}
