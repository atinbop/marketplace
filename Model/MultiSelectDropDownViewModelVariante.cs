﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    namespace SaveMultiSelectDropDown
    {
        using System.Collections.Generic;
        using System.ComponentModel.DataAnnotations;
        using System.Web;


        /// <summary>  
        /// Multi select drop down view model class.  
        /// </summary>  
        public class MultiSelectDropDownViewModelVariante
        {
            #region Properties  

            /// <summary>  
            /// Gets or sets choose multiple countries property.  
            /// </summary>  
            [Required]
            [Display(Name = "Choose Multiple variantes")]
            public List<int> SelectedMultiVarianteId { get; set; }

            /// <summary>  
            /// Gets or sets selected countries property.  
            /// </summary>  
            public List<Variante> SelectedVarianteLst { get; set; }
 
            #endregion
        }
    }
}
