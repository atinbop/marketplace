﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class GetOrderItemsByOrderIDParam
    {
        public int AppID { get; set; }
        public long OrderID { get; set; }
    }
}
