﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class SharingBuyingArea
    {
        public int  id { get; set; }
        public string name { get; set; }

        public string Code_commune_INSEE { get; set; }
        public string CodePostal { get; set; }
        public string Secondname { get; set; }
        public string Acheminement { get; set; }
        public string coordonnees_gps { get; set; }
        public string country { get; set; }
        public string geonameid { get; set; }
 
        public string asciiname { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string featureclass { get; set; }
        public string featurecode { get; set; }
        public string countrycode { get; set; }
        public string cc2 { get; set; }
        public string admin1code { get; set; }
        public string admin2code { get; set; }
        public string admin3code { get; set; }
        public string admin4code { get; set; }
        public string population { get; set; }
        public string elevation { get; set; }
        public string dem { get; set; }
        public string timezone { get; set; }
        public string modificationdate { get; set; }
        public string division1 { get; set; }
        public string division2 { get; set; }
     
  
       
    


    }
}
