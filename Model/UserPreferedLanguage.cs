﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class UserPreferedLanguage
    {
        int id { get; set; }
        int employeeId { get; set; }
        string languageCode { get; set; }

    }
}
