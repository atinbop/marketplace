﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Authorizations
    {
        [Key]
        public int Id { get; set; }
        public string AppPage { get; set; }
        public string userRole { get; set; }
        public int flag_autorized { get; set; }

    }
}
