﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace MarketPlace.Model
{
    public class PreOrderDiscounts
    {
        [Key]
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public Nullable<decimal> BasePrice { get; set; }
        public string DiscountPrice { get; set; }

        public string DiscountRate { get; set; }
        public DateTime DiscountStartDate { get; set; }
        public DateTime DiscountEndDate { get; set; }
        public Nullable<int> Deleted { get; set; }
        public string UpdateByUserId { get; set; }
        public Nullable<DateTime> LastUpdate { get; set; }
        public DateTime Period { get; set; }
        public string UserCompanyType { get; set; }
        public Nullable<int> ContactId { get; set; }
    }
}
