﻿using iText.Kernel.Pdf.Canvas.Parser.ClipperLib;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
	public class Contacts
	{
		[Key]
		public int Id { get; set; }
		public string ContactCode { get; set; }
		public string ContactType { get; set; }
		[Required]
		public string Company { get; set; }
		public Nullable<int> NbSubscritedUsers { get; set; }
		public string Gender { get; set; }

	public string Name { get; set; }
 
		public string FirstName { get; set; }
		public string address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string Country { get; set; }
		public string EAN { get; set; }
		public string Siren { get; set; }
		public string SIRET { get; set; }
		public string APE { get; set; }
		public string ActivitySector { get; set; }
		public Nullable<DateTime>   DateOfBirth { get; set; }
		public string Category { get; set; }
		public string Phone { get; set; }
		public string Mobile { get; set; }
		public string Fax { get; set; }
		public string Email { get; set; }
		public string Website { get; set; }
		public string Origin { get; set; }
		public string Language { get; set; }
		public string BlockedAccount { get; set; }
		public string BlockedPurchase { get; set; }
		public string Alert { get; set; }
		public string ExcludeLoyalty { get; set; }
		public string Password { get; set; }
		public string PrivateSpace { get; set; }
		public string AuthorizedAccess { get; set; }
		public string NewsletterSubscriber { get; set; }
		public string PaymentChoice { get; set; }
		public string PriceDisplay { get; set; }
		public string PriceCategory { get; set; }
		public string AccountingCode { get; set; }
		public string IntraCommunityVAT { get; set; }
		public string Credit { get; set; }
		public string Risk { get; set; }
		public string MonthlyBill { get; set; }
		public string StartOfMonthlyPayment { get; set; }
		public string ClientAccount { get; set; }
		public string FeedbackContact { get; set; }
		public string InternalCommentary { get; set; }
		public string Commercial { get; set; }
		public string Treatment { get; set; }
		public string archiving { get; set; }
		public string VATExemption { get; set; }
		public string PaidOrdersNotInvoiced { get; set; }
		public string ReceivablesClient { get; set; }
		
		public string BankAccountNumberIBAN { get; set; }
		public string CompanyLogoFile { get; set; }
		public string  BankAccountNumberRIB { get; set; }
		public string address1Livraison1 { get; set; }
		public string Address2Livraison1 { get; set; }
		public string Address3Livraison1 { get; set; }
		public string PostalCodeLivraison1 { get; set; }
		public string CityLivraison1 { get; set; }
		public string CountryLivraison1 { get; set; }
	 
	public string address1Livraison3 { get; set; }
	public string Address2Livraison3 { get; set; }
	public string Address3Livraison3 { get; set; }
	public string PostalCodeLivraison3 { get; set; }
	public string CityLivraison3 { get; set; }
	public string CountryLivraison3 { get; set; }
	 
public string address1Facturation { get; set; }
public string Address2Facturation { get; set; }
public string Address3Facturation { get; set; }
public string PostalCodeFacturation { get; set; }
public string CityFacturation { get; set; }
public string CountryFacturation { get; set; }

		public string UserCompanyType { get; set; }
		public Nullable<int> currencyId { get; set; }
	public string Currency { get; set; }

		public string FacebookPage { get; set; }
public string InstagramPage { get; set; }
public string OfficialWebsite { get; set; }
public string youtubePage { get; set; }
				 
		public string Specialities { get; set; }
		public string LongDescription { get; set; }
		public string categoryMarketing { get; set; }
		public string presentation { get; set; }
		public   Nullable<int> Note { get; set; }

		public string versioncodenaf { get; set; }
		public string codenaf { get; set; }
		public string denomination { get; set; }
		public Nullable<int> SharingBuyingAreaId { get; set; }
 
	}
}

