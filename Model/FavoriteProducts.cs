﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class FavoriteVendors
    {
        [Key]
        public int id { get; set; }
        public int contactId { get; set; }
        public int employeeId { get; set; }
        public DateTime LastUpdate { get; set; }        
       public int Deleted     { get; set; }
    }
}
