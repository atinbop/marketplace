﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class ProductSubscriptions
    {
        public int id { get; set; }
        public Contacts contact { get; set; }
        public Article Article { get; set; }
        public Subscriptions subscription   { get; set; }
        public int contactid { get; set; }
        public int articleid { get; set; }
        public int Subscriptionid { get; set; }
        public Nullable<DateTime> startvalidityDate { get; set; }
        public Nullable<DateTime> endValidityDate { get; set; }

        [DefaultValue(0)]
        public int cancelsubscription { get; set; }
        public int  employeeId { get; set; }
    }
}
