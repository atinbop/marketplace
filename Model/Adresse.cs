﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.Model
{
    public class Adresse
    {
        [Key] 
     public  int  Id  { get; set; }
        public int Idadr { get; set; }
        public string Siren { get; set; }
        public string nic { get; set; }
        public string siret { get; set; }
        public string nrue { get; set; }
        public string nrue0 { get; set; }
        public string adresse { get; set; }
        public string ville { get; set; }
        public string postalecode { get; set; }
        public string postalecode1 { get; set; }
        public string postalecode2 { get; set; }
        public string fullAdress { get; set; }
        public string versioncodenaf { get; set; }
        public string codenaf { get; set; }
        public string denomination { get; set; }
    }
}
