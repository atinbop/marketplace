﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketPlace.DataTables
{
    public class Search
    {
        public string Value { get; set; }

        public bool IsRegex { get; set; }
    }
}
