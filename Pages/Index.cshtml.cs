using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace MarketPlace.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
 
        private readonly IConfiguration configuration;
        private static string _imageRepository;
        public List<Chart> ChartList { get; set; }
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCurrency;
        public static string _UserCompanyType;
        public static string _UserCompanyId;
        public static string _UserLanguage;
        public static String _UserCompanyName;
        public IEnumerable<Article> Articles { get; set; }
        public static String _ConnectedUserPseudo;

        public Article Article { get; set; }
        [BindProperty]
        public Chart Chart { get; set; }


        [BindProperty]
        public Prospects Prospect { get; set; }

        [BindProperty]
      
        public IEnumerable<Brands> Brands { get; set; }
        public IndexModel(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
             
            _imageRepository = options.ImageRepository;
         
 
        }

        public async Task OnGetAsync()
        {
            _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
            _ConnectedUserId = Request.Cookies["ConnectedUserId"];
            _UserCompanyType = Request.Cookies["UserCompanyType"];
            _UserCompanyId = Request.Cookies["UserCompanyId"];
            _UserCompanyName = Request.Cookies["UserCompanyName"];
            _UserCurrency = Request.Cookies["UserCurrency"];
            _UserLanguage = Request.Cookies["PreferedLanguage"];


            if ((User.Identity.IsAuthenticated && _ConnectedUserId == null)|| (!User.Identity.IsAuthenticated && _ConnectedUserId != null))
            {
               
                 await HttpContext.SignOutAsync(
                 CookieAuthenticationDefaults.AuthenticationScheme);

                CookieOptions cookieOptions = new CookieOptions();
                cookieOptions.Expires = DateTime.Now.AddDays(-1);

                HttpContext.Response.Cookies.Append("FullName", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("Lastname", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("Firstname", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("ConnectedUserEmail", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("UserCompanyType", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("ConnectedUserId", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("UserCompanyName", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("CompanyLogoFile", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("PreferedLanguage", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("UserCompanyId", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("UserCurrency", "-", cookieOptions);
                HttpContext.Response.Cookies.Append("preferedCountry", "-", cookieOptions);

            }

            //Listes des logo en ramdom
            Brands =  _db.Brands.FromSqlRaw<Brands>("spGetTop39Brand").ToList();


           //PSC_DEV_API_KEY
//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
 
            //var apiKey = Environment.GetEnvironmentVariable("PSC_DEV_API_KEY");
            //var client = new SendGridClient(apiKey);
           
            /*var client = new SendGridClient("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");
            var from = new EmailAddress("contact@pharmashopcenter.com", "Example User");
            var subject = "Sending with SendGrid is Fun";
            var to = new EmailAddress("atinbop@gmail.com", "Example User");
            var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);*/

            /*
             * dotnet user-secrets set SendGridUser RickAndMSFT
dotnet user-secrets set SendGridKey <key>
*/

        }

         
        /*public async Task<JsonResult> OnPostAsync()
        {

         
      

            var t = new JsonResult(new
            {
                Message = "Bien re�u"                
            });
            return t;
        }

        */
    




    }
}

/*PSC_DEV_API_KEY
 * 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
 * 
 * // using SendGrid's C# Library
// https://github.com/sendgrid/sendgrid-csharp
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

namespace Example
{
    internal class Example
    {
        private static void Main()
        {
            Execute().Wait();
        }

        static async Task Execute()
        {
            var apiKey = Environment.GetEnvironmentVariable("NAME_OF_THE_ENVIRONMENT_VARIABLE_FOR_YOUR_SENDGRID_KEY");
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("test@example.com", "Example User");
            var subject = "Sending with SendGrid is Fun";
            var to = new EmailAddress("test@example.com", "Example User");
            var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}*/
