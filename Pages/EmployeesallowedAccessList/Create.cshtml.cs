﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MarketPlace.Data;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.EmployeesallowedAccessList
{
    public class CreateModel : PageModel
    {
        public IEnumerable<StaticData> StaticData;

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }
        public List<SelectListItem> StaticDataPreOrderDelay { get; set; } = new List<SelectListItem>();

        private readonly MarketPlace.Data.ApplicationDbContext _context;

        [BindProperty]
        public Contacts Contact { get; set; }

        [BindProperty]
        public string startDate { get; set; }
        [BindProperty]
        public string period { get; set; }

        [BindProperty]
        public string rate { get; set; }

        [BindProperty]
        public string price { get; set; }



        [BindProperty]
        public string endDate { get; set; }
        public CreateModel(MarketPlace.Data.ApplicationDbContext context , IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;

            startDate = (DateTime.Now).ToString("yyyy-MM-dd"); 

             endDate = (DateTime.Now).AddMonths(1).ToString("yyyy-MM-dd");
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public async Task<IActionResult> OnGetAsync(int contactId)
        {
            Contact = await _context.Contacts.FindAsync(contactId);
        
                return Page();
        }

        [BindProperty]
        public EmployeesallowedAccess EmployeesallowedAccess { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
              int noOfRowUpdated = _context.Database.ExecuteSqlRaw("Update EmployeesallowedAccess set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "' where Emaillogin='" + EmployeesallowedAccess.Emaillogin + "' and ContactId=" + _UserCompanyId );
              _context.EmployeesallowedAccess.Add(EmployeesallowedAccess);
                await _context.SaveChangesAsync();
                return RedirectToPage("Index", new { contactId = Contact.Id });          
           
        }
    }
}
