﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.EmployeesallowedAccessList
{
    public class IndexModel : PageModel
    {
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public IndexModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public IList<EmployeesallowedAccess> EmployeesallowedAccess { get; set; }

        [BindProperty]
        public Contacts Contact { get; set; }

        public async Task OnGetAsync(int contactId)
        {
            Contact = await _context.Contacts.FindAsync(contactId);
        }

        [BindProperty]
        public DataTables.DataTablesRequest DataTablesRequest { get; set; }

        public async Task<JsonResult> OnPostAsync(int contactId)
        {
            var recordsTotal = _context.EmployeesallowedAccess.Count();
            
           

            var EmployeesallowedQuery = _context.EmployeesallowedAccess.AsQueryable().Where(s => s.ContactId == contactId).Where(s => s.Deleted != 1);

            

            var searchText = DataTablesRequest.Search.Value?.ToUpper();
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                EmployeesallowedQuery = EmployeesallowedQuery.Where(s =>
                   s.Id.Equals(searchText) ||
                    s.Firstname.Equals(searchText) ||
                    s.Lastname.Equals(searchText) ||
                    s.Emaillogin.Equals(searchText) ||
                    s.userRole.Equals(searchText)  
                );
            }

            var recordsFiltered = EmployeesallowedQuery.Count();

            var sortColumnName = DataTablesRequest.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;

            var sortDirection = DataTablesRequest.Order.ElementAt(0).Dir.ToLower();

           

            var skip = DataTablesRequest.Start;
            var take = DataTablesRequest.Length;
            var data = await EmployeesallowedQuery
                .Skip(skip)
                .Take(take)
                .ToListAsync();

            var t= new JsonResult(new
            {
                Draw = DataTablesRequest.Draw,
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsFiltered,
                Data = data
            });
            return t;
        }
    }
}
