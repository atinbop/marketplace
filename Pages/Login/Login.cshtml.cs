using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.Login
{
    public class LoginModel : PageModel
    {
        [BindProperty]
        public Employees Employee { get; set; }
        private readonly Data.ApplicationDbContext _db;
        private readonly IWebHostEnvironment webHostEnvironment;
        private string _ImageRepository;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;
        public const string SessionKeyConnectedUserEmail = "_ConnectedUserEmail";
        public const string SessionKeyUserCompanyType = "_UserCompanyType";

        public void OnGet()
        {
        }

        public LoginModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public  async Task<IActionResult> OnPost()
        {
           
            Employee = (from art in _db.Employees where art.Emaillogin== Employee.Emaillogin && art.pwd == Employee.pwd
                              select art).FirstOrDefault();

            if (Employee != null) 
            { string t = Employee.Firstname;
                string sql = "insert into[dbo].[LogConnexions]([EmployeeId], [LoginDate], [logSystem]) values(" + Employee.Id + ", getdate(), 'LOGIN')";
                await _db.SaveChangesAsync();

                Contacts contacts = (from art in _db.Contacts
                                     where art.Id == Employee.ContactId
                                     select art).FirstOrDefault();


                int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);

                if (string.IsNullOrEmpty(HttpContext.Session.GetString(SessionKeyConnectedUserEmail)))
                {
                    HttpContext.Session.SetString(SessionKeyConnectedUserEmail, Employee.Emaillogin);
                    HttpContext.Session.SetString(SessionKeyUserCompanyType, contacts.UserCompanyType);
                }
                _ConnectedUserEmail = HttpContext.Session.GetString(SessionKeyConnectedUserEmail);
                _UserCompanyType = HttpContext.Session.GetString(SessionKeyUserCompanyType);

              
                if (contacts.UserCompanyType == "PRO") {
                    return RedirectToPage("/ArticleList/Index");
                }
                else if (contacts.UserCompanyType == "B2B")
                {
                    return RedirectToPage("/Market/Index");
                }
                else if (contacts.UserCompanyType == "B2C")
                {
                    return RedirectToPage("/Market/Index");
                }
                else
                { return RedirectToPage("/Market/Index"); 
                }

            }
            else
            { string t = Employee.Firstname; }

             return RedirectToPage();
        }
    }
}

