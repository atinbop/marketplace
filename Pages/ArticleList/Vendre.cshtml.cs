﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.ArticleList
{
    public class VendreModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IWebHostEnvironment webHostEnvironment;

        private readonly string _ImageRepository;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;


        [BindProperty]
        public int _UserCompanyId { get; set; }

        public String _UserCompanyName;


        [BindProperty]
        public int _NbProductAutorized { get; set; }

        [BindProperty]
        public int _NbProductAlreadyChoose { get; set; }


        public VendreModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
            _NbProductAutorized = options.NbProductAutorized;

          /*  if (_UserCompanyType == "B2C")
            {
                DateTime localDate = DateTime.Now;
                  _NbProductAlreadyChoose = (from e1 in _db.Article
                                where !_db.ProductSubscriptions.Where(c => c.cancelsubscription != 1).Any(e2 => e2.articleid == e1.Id) && e1.Deleted != 1
                                select e1).Count();

                //Subscriptions en cours
                Subscriptions currentSubscriptions = (from art in _db.Subscriptions
                                                      where art.Contact.Id.ToString() == _UserCompanyId && art.endValidityDate > localDate && art.cancelsubscription != 1
                                                      orderby art.startvalidityDate descending
                                                      select art).FirstOrDefault();

                Offers offre = (from art in _db.Offers
                                where art.id == currentSubscriptions.OfferId
                                select art).FirstOrDefault();
                _NbProductAutorized = offre.numberofProduct;
               
                 
                _NbProductAutorized = options.NbProductAutorized;
                _NbProductAlreadyChoose = _NbProductAutorized - currentSubscriptions.;
            }*/
        }

     

    }
}