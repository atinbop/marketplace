using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using iText.IO.Util;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.ArticleList
{
    public class ChatModel : PageModel
    {
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        [BindProperty]
        public string _ConnectedUserId { get; set; }

        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;

        public String _ConnectedUserPseudo;
        public Article Article { get; set; }
        [BindProperty]


        public IEnumerable<Posts> Posts { get; set; }

        [BindProperty]
        public IEnumerable<ConnectedUser> ConnectedUsers { get; set; }
        public class ConnectedUser
        {
            public int EmployeeId { get; set; }
            public DateTime LoginDate { get; set; }
            public string Pseudo { get; set; }
            public string Picture { get; set; }
        }

        private readonly MarketPlace.Data.ApplicationDbContext _context;

        [HttpGet]
        public async Task OnGet(int ArticleId, string postContent)
        {

            Article = await (_context.Article.FindAsync(ArticleId));



            ConnectedUsers = (from z in _context.LogConnexions
                              where z.LogoutDate == null
                              join f in _context.Employees on z.EmployeeId equals f.Id
                              select new { f.Pseudo, z.EmployeeId, z.LoginDate, f.Picture } into x
                              group x by new { x.Pseudo, x.EmployeeId, x.Picture } into g
                              select new ConnectedUser
                              {
                                  Picture = g.Key.Picture,
                                  Pseudo = g.Key.Pseudo,
                                  EmployeeId = g.Key.EmployeeId,
                                  LoginDate = (DateTime)g.Max(i => i.LoginDate)
                              }).OrderByDescending(g => g.LoginDate).Take(8);


            if (postContent != null)
            {
                Posts ThePost = new Posts();

                ThePost.ArticleId = ArticleId;
                ThePost.PostContent = postContent;
                ThePost.UpdateByUserId = _ConnectedUserId;
                ThePost.LastUpdate = DateTime.Now;
                ThePost.PartnerEmployeeId = -1; //Group Post
                ThePost.UpdateByPseudo = _ConnectedUserPseudo;
                await _context.Posts.AddAsync(ThePost);
                await _context.SaveChangesAsync();

                //Mise � jour du nombre de post de l'utilisateur
                //var article = await _db.Article.FindAsync(id);
                //article.Note = Convert.ToInt32(await _db.Notes.Where(x => x.ArticleId == id).AverageAsync(x => x.Note));
                //await _db.SaveChangesAsync();




           


            }

            Posts = await (from art in _context.Posts
                           where art.ArticleId == ArticleId && art.Deleted != 1 && art.PartnerEmployeeId == -1
                           select art).ToListAsync();

            foreach (Posts prop in Posts)
            {
                prop.slotLastUpdate = processDate((DateTime)prop.LastUpdate);

            }

            //Log de la connexion du user au chat
            int noOfRowUpdated = _context.Database.ExecuteSqlRaw("insert into [dbo].[LogConnexions]([EmployeeId], [LoginDate], [logSystem], [ViewedItem]) values (" + _ConnectedUserId + ", getdate(), 'CHAT'," + ArticleId + ")");
            await _context.SaveChangesAsync();


        }



        public ChatModel(Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
            _ConnectedUserPseudo = options.ConnectedUserPseudo;
        }

        public string processDate(DateTime d)
        {
            string result = "";
            if ((DateTime.Now - d).TotalHours < 1) result = "Il y'a " + Math.Round((DateTime.Now - d).TotalMinutes,0).ToString() + " Minutes";

            else if ((DateTime.Now - d).Days < 1) result = "Il y'a " + Math.Round((DateTime.Now - d).TotalHours, 0).ToString() + " Heures";
           else if ((DateTime.Now - d).Days > 1 && (DateTime.Now - d).Days < 2)  result = "Hier";
            else if ((DateTime.Now - d).Days > 2 && (DateTime.Now - d).Days < 30) result = "Il y'a " +  (DateTime.Now.Date - d).Days.ToString() + " Jours";

            else result = d.ToString("MMM.dd,yyyy HH:mm:ss");

            return result;
        }


    }


}
