﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using MarketPlace.Model.SaveMultiSelectDropDown;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

 
using iText.Barcodes;
using iText.Kernel.Pdf;
using iText.Kernel.Font;
using iText.IO.Font;
using iText.Layout.Element;

using System.Drawing;

using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace MarketPlace.Pages.ArticleList
{
    public class CreateModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;

        public string getseterror { get; set; }
        public string Country { get; set; }

        [BindProperty]
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public IEnumerable<SelectListItem> PropertyList { get; set; }
        public IEnumerable<SelectListItem> VarianteList { get; set; }
        public MultiSelectDropDownViewModel model { get; set; }
        public MultiSelectDropDownViewModelProperty model2 { get; set; }
        public MultiSelectDropDownViewModelVariante model3 { get; set; }
        public List<SelectListItem> items { get; set; }
        public List<SelectListItem> itemsproperties { get; set; }
        public List<SelectListItem> itemsvariantes { get; set; }

        public IEnumerable<StaticData>  StaticData;
              public IEnumerable<Country> Countries;
        public List<SelectListItem> StaticDataGenders { get; set; } = new List<SelectListItem>();        

        public List<SelectListItem> StaticDataVatType { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataOuiNon { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataTypeProduit { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataCountry { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataFrequency { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataCategory { get; set; } = new List<SelectListItem>();

        private readonly IWebHostEnvironment webHostEnvironment;
        public CreateModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;

        }


        [BindProperty]
        public IFormFile Image1 { get; set; }


        [BindProperty]
        public IFormFile Image2 { get; set; }


        [BindProperty]
        public IFormFile Image3 { get; set; }
        [BindProperty]
        public IFormFile Video1 { get; set; }


        [BindProperty]
        public IFormFile ArticleFile { get; set; }
        [BindProperty]
        public IFormFile EN_ArticleFile { get; set; }

        public IEnumerable<ProductCategories> ProductCategories;
        [BindProperty]
        public ArticleVendors ArticleVendors { get; set; }

        [BindProperty]
        public Article Article { get; set; }

        public async Task OnGet()
        {
            StaticData = await(from art in _db.StaticData
                             select art).ToListAsync();

            Countries = await (from art in _db.Countries
                               select art).ToListAsync();

            ProductCategories = await (from art in _db.ProductCategories
                                       select art).ToListAsync();

            ArticleVendors = new ArticleVendors();

            SelectListItem selListItem = new SelectListItem() { Value = "null", Text = "Selectionnez" };
            
            StaticDataGenders.Add(selListItem);
            StaticDataVatType.Add(selListItem);
            StaticDataOuiNon.Add(selListItem);
            StaticDataTypeProduit.Add(selListItem);
            StaticDataCountry.Add(selListItem);

            foreach (var prop in Countries)
            {
                SelectListItem item = new SelectListItem(prop.nom_fr_fr, prop.Id.ToString());
                StaticDataCountry.Add(item);
            }

            foreach (var prop in ProductCategories)
            {
                SelectListItem item = new SelectListItem(prop.Name, prop.Id.ToString());
                StaticDataCategory.Add(item);
            }

            foreach (var prop in StaticData)
                {
                   
                  string val = (string)prop.Type;
                if (val == "Civility")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataGenders.Add(item);
                }

                if (val == "VAT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataVatType.Add(item);
                }

             

                if (val == "YESNO")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataOuiNon.Add(item);
                }

                if (val == "TYPEPRODUIT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataTypeProduit.Add(item);
                }
                if (val == "FREQUENCY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataFrequency.Add(item);
                }

            }
            try
            {
                // Loading drop down lists.  
                PropertyList = this.GetPropertyList();
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

            try
            {
                // Loading drop down lists.  
                VarianteList = this.GetVarianteList();
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

        }

        public async Task<IActionResult> OnPost()
        {

            ArticleVendors.contactId = _UserCompanyId;
            ArticleVendors.UpdateByUserId = _ConnectedUserId;
            ArticleVendors.UserCompanyType = _UserCompanyType;

            if (_UserCompanyType == "B2C")
                ArticleVendors.PriceType = "TTC";
            else
                ArticleVendors.PriceType = "HT";

           


            foreach (var prop in StaticDataGenders)
            {
                if (prop.Value == Article.SaleManagerCivility)
                {
                    ArticleVendors.SaleManagerCivility = prop.Text;
                }
            }

            //Sauvegarde des données partagé produit par le fournisseur pro
            Article.SaleManagerEmail = ArticleVendors.SaleManagerEmail;
            Article.SaleManagerTelephone = ArticleVendors.SaleManagerTelephone;
            Article.FrancoPortAmount = ArticleVendors.FrancoPortAmount;
            Article.MinimumOrderQuantity = ArticleVendors.MinimumOrderQuantity;
            Article.MaximumOrderQuantity = ArticleVendors.MaximumOrderQuantity;
            Article.DropShippingEmail = ArticleVendors.DropShippingEmail;
            Article.ProductionDate = ArticleVendors.ProductionDate;
            Article.DeliveryDelay = ArticleVendors.DeliveryDelay;
            Article.FirstOrderMinimalAmount = ArticleVendors.FirstOrderMinimalAmount;
            Article.NextProductionDate = ArticleVendors.NextProductionDate;
            Article.NextProductionEstimateQuantity = ArticleVendors.NextProductionEstimateQuantity;
            Article.Price = (decimal)ArticleVendors.Price;
         
            Article.Inventory = ArticleVendors.Inventory;
            Article.ProductionFrequency = ArticleVendors.ProductionFrequency;
            Article.SaleManagerFax = ArticleVendors.SaleManagerFax;
            Article.BuyingPrice = ArticleVendors.BuyingPrice;
            Article.SaleManagerName = ArticleVendors.SaleManagerName;
            Article.SaleManagerFirstname = ArticleVendors.SaleManagerFirstname;

            Article.Name = ArticleVendors.Name;
            Article.EAN = ArticleVendors.EAN;
          
            Article.B2CPrice = ArticleVendors.B2CPrice;
            Article.VAT = ArticleVendors.VAT;
            Article.FirstCategory = ArticleVendors.FirstCategory;
            Article.ShortDescription = ArticleVendors.ShortDescription;
            Article.LongDescription = ArticleVendors.LongDescription;
            Article.Notice = ArticleVendors.Notice;
            Article.SEOTitle = ArticleVendors.SEOTitle;
            Article.SEODescription = ArticleVendors.SEODescription;
            Article.Ingredient = ArticleVendors.Ingredient;
            Article.ArticleFile = ArticleVendors.ArticleFile;
            Article.Image1 = ArticleVendors.Image1;
            Article.Image2 = ArticleVendors.Image2;
            Article.Image3 = ArticleVendors.Image3;
           
            Article.B2CInventory = ArticleVendors.B2CInventory;
            Article.YoutubeVideo = ArticleVendors.YoutubeVideo;
            Article.FacebookPage = ArticleVendors.FacebookPage;
            Article.TwitterPage = ArticleVendors.TwitterPage;
            Article.InstagramPage = ArticleVendors.InstagramPage;
            Article.Indication = ArticleVendors.Indication;
            Article.Brand = ArticleVendors.Brand;
            Article.WarningMessage = ArticleVendors.WarningMessage;
            Article.ArticleFilesLink = ArticleVendors.ArticleFilesLink;
            Article.ActiveSubtances = ArticleVendors.ActiveSubtances;
            Article.SecondCategory = ArticleVendors.SecondCategory;
            Article.ThirdCategory = ArticleVendors.ThirdCategory;
            Article.Weight = ArticleVendors.Weight;
            Article.Volume = ArticleVendors.Volume;
            Article.Height = ArticleVendors.Height;
            Article.Width = ArticleVendors.Width;
            Article.Depth = ArticleVendors.Depth;
            Article.Lot = ArticleVendors.Lot;
            Article.B2CLot = ArticleVendors.B2CLot;
            Article.OriginCountry = ArticleVendors.OriginCountry;
            Article.CustomsCode = ArticleVendors.CustomsCode;
         
            Article.PriceType = ArticleVendors.PriceType;
            Article.RefundRate = ArticleVendors.RefundRate;
          
         
            Article.SaleManagerCivility = ArticleVendors.SaleManagerCivility;
     
           
         
      
         
             
            Article.SupplierAdvisedResalePrice = ArticleVendors.SupplierAdvisedResalePrice;
            Article.Deleted = ArticleVendors.Deleted;
            Article.UpdateByUserId = ArticleVendors.UpdateByUserId;
            Article.LastUpdate = ArticleVendors.LastUpdate;
            Article.PROSellerCompanyId = ArticleVendors.PROSellerCompanyId;
            Article.SuplierArticleReference = ArticleVendors.SuplierArticleReference;
            Article.SalesChannel = ArticleVendors.SalesChannel;
            Article.ArticleValidatedFlag = ArticleVendors.ArticleValidatedFlag;
            Article.EndOfLifeFlag = ArticleVendors.EndOfLifeFlag;
      
            Article.NoveltyFlag = ArticleVendors.NoveltyFlag;
            Article.AccountingCode = ArticleVendors.AccountingCode;
            Article.ProductType = ArticleVendors.ProductType;
            Article.ArticleKeywords = ArticleVendors.ArticleKeywords;
         
            Article.PurchaseNomenclature = ArticleVendors.PurchaseNomenclature;
            Article.CommercialTaget = ArticleVendors.CommercialTaget;
            Article.EcoTaxAmount = ArticleVendors.EcoTaxAmount;
            Article.EN_Name = ArticleVendors.EN_Name;
            Article.EN_ShortDescription = ArticleVendors.EN_ShortDescription;
            Article.EN_LongDescription = ArticleVendors.EN_LongDescription;
            Article.EN_Notice = ArticleVendors.EN_Notice;
            Article.EN_SEOTitle = ArticleVendors.EN_SEOTitle;
            Article.EN_SEODescription = ArticleVendors.EN_SEODescription;
            Article.EN_Ingredient = ArticleVendors.EN_Ingredient;
            Article.EN_ArticleFile = ArticleVendors.EN_ArticleFile;
            Article.EN_Indication = ArticleVendors.EN_Indication;
            Article.EN_ActiveSubtances = ArticleVendors.EN_ActiveSubtances;
            Article.ProductShopLink = ArticleVendors.ProductShopLink;
            Article.BrandShopLink = ArticleVendors.BrandShopLink;
            Article.SaleAuthorizedInPharmacy = ArticleVendors.SaleAuthorizedInPharmacy;
            Article.ShelfLife = ArticleVendors.ShelfLife;
            Article.Internalreference = ArticleVendors.Internalreference;
            Article.Variantreference = ArticleVendors.Variantreference;
            Article.Groupcode = ArticleVendors.Groupcode;
            Article.ArticleOtherName = ArticleVendors.ArticleOtherName;
            Article.En_ArticleOtherName = ArticleVendors.En_ArticleOtherName;
            Article.Image4 = ArticleVendors.Image4;
            Article.Image5 = ArticleVendors.Image5;
            Article.Image6 = ArticleVendors.Image6;
            Article.Image7 = ArticleVendors.Image7;
            Article.Image8 = ArticleVendors.Image8;
            Article.Image9 = ArticleVendors.Image9;
            Article.Image10 = ArticleVendors.Image10;
           
          
          
            Article.Created = ArticleVendors.Created;
            Article.EN_ArticleKeywords = ArticleVendors.EN_ArticleKeywords;
          
            Article.Destockage = ArticleVendors.Destockage;
           
            Article.Code_CIP7 = ArticleVendors.Code_CIP7;
            Article.Modele = ArticleVendors.Modele;
            Article.Gamme = ArticleVendors.Gamme;
            Article.UPC = ArticleVendors.UPC;
            Article.Contenance = ArticleVendors.Contenance;
            Article.Forme = ArticleVendors.Forme;
            Article.Taille = ArticleVendors.Taille;
            Article.Conseiller_pour = ArticleVendors.Conseiller_pour;
            Article.Dimension = ArticleVendors.Dimension;
            Article.Autres_caracteristiques = ArticleVendors.Autres_caracteristiques;
            Article.Couleur = ArticleVendors.Couleur;
            Article.Pour_qui = ArticleVendors.Pour_qui;
            Article.Presentation = ArticleVendors.Presentation;
            Article.Indication_autres = ArticleVendors.Indication_autres;
            Article.Type_de_cheveux = ArticleVendors.Type_de_cheveux;
            Article.Type_de_peau = ArticleVendors.Type_de_peau;
            Article.Parfum = ArticleVendors.Parfum;
            Article.Texture = ArticleVendors.Texture;
            Article.Pour_quel_animal = ArticleVendors.Pour_quel_animal;
            Article.Type_de_shampooing = ArticleVendors.Type_de_shampooing;
            Article.Mode_administration = ArticleVendors.Mode_administration;
            Article.Video1 = ArticleVendors.Video1;
            Article.Usage = ArticleVendors.Usage;
            Article.ReturnFlag = ArticleVendors.ReturnFlag;
            Article.OtherInformation = ArticleVendors.OtherInformation;
            Article.AttractivenessIndex = ArticleVendors.AttractivenessIndex;
            Article.BrandId = ArticleVendors.BrandId;
            Article.EmployeeId = ArticleVendors.EmployeeId;
            Article.Note = ArticleVendors.Note;
            Article.UnreadMessagesNumber = ArticleVendors.UnreadMessagesNumber;
            Article.barCodeImage = ArticleVendors.barCodeImage;
            Article.categoryMarketing = ArticleVendors.categoryMarketing;
       
            Article.SaleAreaFullName = ArticleVendors.SaleAreaFullName;
            Article.SaleAreaId = ArticleVendors.SaleAreaId;
            Article.youtubePage = ArticleVendors.youtubePage;
            Article.OfficialWebsite = ArticleVendors.OfficialWebsite;       
            Article.ProductDangerosity = ArticleVendors.ProductDangerosity;




            StaticData = await (from art in _db.StaticData
                                select art).ToListAsync();


            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "Civility")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataGenders.Add(item);
                }

                if (val == "VAT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataVatType.Add(item);
                }

                if (val == "Country")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataCountry.Add(item);
                }

                if (val == "YESNO")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataOuiNon.Add(item);
                }

                if (val == "TYPEPRODUIT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataTypeProduit.Add(item);
                }
                if (val == "COUNTRY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataCountry.Add(item);
                }
                if (val == "FREQUENCY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataFrequency.Add(item);
                }
            }


            foreach (var prop in StaticDataVatType)
            {
                if (Convert.ToDecimal(prop.Value, new CultureInfo("fr-FR")) == Convert.ToDecimal(Article.VAT, new CultureInfo("fr-FR")))
                {
                    Article.VAT = Math.Round(Convert.ToDecimal(prop.Text, new CultureInfo("fr-FR")), 2);
                }
            }


            foreach (var prop in StaticDataFrequency)
            {
                if (prop.Value == Article.ProductionFrequency)
                {
                    Article.ProductionFrequency = prop.Text;
                }
            }


            foreach (var prop in StaticDataCountry)
            {
                if (prop.Value == Article.OriginCountry)
                {
                    Article.OriginCountry = prop.Text;
                }
            }

            foreach (var prop in StaticDataGenders)
            {
                if (prop.Value == Article.SaleManagerCivility)
                {
                    Article.SaleManagerCivility = prop.Text;
                }
            }

            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == Article.ArticleValidatedFlag)
                {
                    Article.ArticleValidatedFlag = prop.Text;
                }
            }

            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == Article.SaleAuthorizedInPharmacy)
                {
                    Article.SaleAuthorizedInPharmacy = prop.Text;
                }
            }

            

            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == Article.EndOfLifeFlag)
                {
                    Article.EndOfLifeFlag = prop.Text;
                }
            }


            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == Article.NoveltyFlag)
                {
                    Article.NoveltyFlag = prop.Text;
                }
            }
            foreach (var prop in StaticDataTypeProduit)
            {
                if (prop.Value == Article.ProductType)
                {
                    Article.ProductType = prop.Text;
                }
            }


            if (Image1 != null)
            {
                Article.Image1 = ProcessUploadingWaterMark(Image1);
            }

            if (Image2 != null)
            {
                Article.Image2 = ProcessUploadingWaterMark(Image2);
            }

            if (Image3 != null)
            {
                Article.Image3 = ProcessUploadingWaterMark(Image3);
            }

            if (ArticleFile != null)
            {
                Article.ArticleFile = ProcessUploading(ArticleFile);
            }
            if (EN_ArticleFile != null)
            {
                Article.EN_ArticleFile = ProcessUploading(EN_ArticleFile);
            }
            if (Video1 != null)
            {
                Article.Video1 = ProcessUploading(Video1);
            }

            Article.BrandId = _db.Brands.Where(p => p.BrandName.Equals(Article.Brand))
                                               .Select(p => p.Id).FirstOrDefault();

            Article.EmployeeId = _db.Employees.Where(p => p.Emaillogin.Equals(Article.SaleManagerEmail )).Where( p => p.Firstname.Equals(Article.SaleManagerFirstname))
                                            .Select(p => p.Id).FirstOrDefault();


            //if (ModelState.IsValid)
            //{
            if (_UserCompanyType == "B2B")
                {
                    Article.PROSellerCompanyId = _UserCompanyId;
                    Article.PriceType = "HT";
                    Article.UpdateByUserId = _ConnectedUserId;

                }
                else
                { 
                    Article.UpdateByUserId = _ConnectedUserId;
                }

                // For article countries
                var selectedCountries = Request.Form["sellingcountry"];

            //For articles properties
            var selectedProperties = Request.Form["properties"];

            //For articles properties
            var selectedVariantes = Request.Form["variantes"];

            int intIdt;
            try
            {
                  intIdt = _db.Article.Max(u => u.Id);
            }
            catch {
                intIdt = 1; //Pour gerer le cas rare e la table article vide
            }
            intIdt++;

            foreach (var prop in selectedCountries)
            {
    
                string sql = "insert into  ArticleCountries (ArticleId, CountryId,	UpdateByUserId,LastUpdate) values( " + intIdt + "," + prop + ", " + _ConnectedUserId + ",'" + DateTime.Now + "')";
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
            }

            foreach (var prop in selectedVariantes)
            {

                string sql = "insert into  ArticleVariantes (ArticleId, VarianteArticleId,	UpdateByUserId,LastUpdate) values( " + intIdt + "," + prop + ", " + _ConnectedUserId + ",'" + DateTime.Now + "')";
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
            }



            foreach (var prop in selectedProperties)
            {
                PropertyValue MyPropertyValue = await _db.PropertyValues.FindAsync(int.Parse(prop));
                string sql = "insert into  ArticleProperty (ArticleId, PropertyValueId,ArticlePropertyLabel,ArticlePropertyValueLabel,	UpdateByUserId,LastUpdate) values( " + intIdt + "," + prop + ", '" + MyPropertyValue.PropertyName + "', '" + MyPropertyValue.PropertyValueName + "', " + _ConnectedUserId + ",'" + DateTime.Now + "')";
                int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
            }



            Article.PROSellerCompanyId = _UserCompanyId;
              await _db.Article.AddAsync(Article);
            await _db.SaveChangesAsync();
            int intIdtArticle;
            try
            {
                intIdtArticle = _db.Article.Max(u => u.Id);
            }
            catch
            {
                intIdtArticle = 1; //Pour gerer le cas rare e la table article vide
            }
     
            ArticleVendors.articleId = intIdtArticle;


   


            await _db.ArticleVendors.AddAsync(ArticleVendors);
            await _db.SaveChangesAsync();
                return RedirectToPage("Index");
            //}
            //else
            //{
            //    return page();
            //}
        }

        private List<SelectListItem> GetPropertyList()
        {
            itemsproperties = new List<SelectListItem>();
            List<SelectListGroup> groupItemsproperties = new List<SelectListGroup>();

            var listArticleProperties = (from art in _db.Properties select art).ToList();
            foreach (var prop in listArticleProperties)
            {
                groupItemsproperties.Add(new SelectListGroup() { Name = prop.PropertyName });
            }


            try
            {
                // Loading.  
                var listProperty = (from art in _db.PropertyValues select art).ToList();

                var listArticleProperty = (from art in _db.ArticleProperty  select art).ToList();

                var groupproperties = new SelectListGroup();
                bool selectedProperty = false;
                foreach (var prop in listProperty)
                {
                    foreach (var groupselected in groupItemsproperties)
                    {

                        if (groupselected.Name == prop.PropertyName)
                        {
                            groupproperties = groupselected;
                        }
                    }

                    foreach (var propselected in listArticleProperty)
                    {
                        if (propselected.PropertyValueId == prop.Id)
                        {
                            selectedProperty = true;
                        }
                    }
                    itemsproperties.Add(new SelectListItem() { Text = prop.PropertyValueName, Group = groupproperties,  Value = prop.Id.ToString() });
                    selectedProperty = false;
                }

            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // info.  

            return itemsproperties;
        }

        private List<SelectListItem> GetVarianteList()
        {
            itemsvariantes = new List<SelectListItem>();
            List<SelectListGroup> groupItemsvariantes = new List<SelectListGroup>();


            groupItemsvariantes.Add(new SelectListGroup() { Name = "Variantes produit" });


            try 
            {
                // Loading.  
                var listVariante = (from art in _db.Article join artb in _db.Article on art.PROSellerCompanyId equals artb.PROSellerCompanyId
                                    where art.PROSellerCompanyId == _UserCompanyId
                                    select art).ToList();
                 
                var listArticleVariante = (from art in _db.ArticleVariantes     select art).ToList();

           


                var groupvariantes = new SelectListGroup();
                bool selectedVariante = false;
                foreach (var prop in listVariante)
                {
                    foreach (var groupselected in groupItemsvariantes)
                    {

                        if (groupselected.Name == prop.Name + "-" + prop.EAN)
                        {
                            groupvariantes = groupselected;
                        }
                    }

                    foreach (var propselected in listArticleVariante)
                    {
                        if (propselected.VarianteArticleId == prop.Id)
                        {
                            selectedVariante = true;
                        }
                    }
                    itemsvariantes.Add(new SelectListItem() { Text = prop.Name + "-" + prop.EAN, Group = groupvariantes, Selected = selectedVariante, Value = prop.Id.ToString() });
                    selectedVariante = false;
                }

            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // info.  

            return itemsvariantes;
        }
        private string ProcessUploadingWaterMark(IFormFile ImageToLoad)
        {

            string uniqueFileName = null;

            if (ImageToLoad != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, _ImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + ImageToLoad.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                ImageToLoad.CopyTo(fileStream);
                fileStream.Dispose();

                //Whatermark NOT WORKING
                int percent;
                System.Drawing.Image resultat;
                string FullPathLogo = Path.GetFullPath(@"../marketplace/wwwroot/images/logo_pharmashopcenter_COULEURS_S.jpg");
                Bitmap watermark2 = new Bitmap(FullPathLogo);
                try
                {
                    string FullPath = Path.GetFullPath(@"../marketplace/wwwroot/" + _ImageRepository + uniqueFileName);
                    Bitmap bitmap = new Bitmap(FullPath);



                    //Image watermark2 = Image.FromStream(new MemoryStream(System.IO.File.ReadAllBytes(FullPathLogo)));

                    percent = Convert.ToInt32(((bitmap.Width * bitmap.Height) / (2160.0 * 2160.0)) * 100);

                    if (percent == 0) percent = 100;

                    if (percent > 40 && percent <= 50) percent = 50;
                    if (percent > 30 && percent <= 40) percent = 40;
                    if (percent > 20 && percent <= 30) percent = 30;
                    if (percent > 10 && percent <= 20) percent = 20;
                    if (percent > 0 && percent <= 10) percent = 15;

                    resultat = ScaleByPercent(watermark2, percent);

                    bitmap = WatermarkImage(bitmap, new Bitmap(resultat), percent);
                    bitmap.Save(@"../marketplace/wwwroot/" + _ImageRepository + Article.EAN + "_" + uniqueFileName);
                }


                catch (Exception exception)
                {
                    Console.WriteLine(exception);

                }

            }
            return _ImageRepository + Article.EAN + "_" + uniqueFileName;
        }


        public string UploadUserFile(IFormFile Image)
        {
            try
            {
                // supported extensions
                // you can add any of extension,if you want pdf file validation then add .pdf in 
                // variable supportedTypes.

                var supportedTypes = new[] { "jpg", "jpeg", "png" };

                // following will fetch the extension of posted file.

                var fileExt = System.IO.Path.GetExtension(Image.FileName).Substring(1);

                // Image datatype is included in System.Drawing librery.will get the image properties 
                //  like height, width.



                //variable will get the ratio of image 
                // (600 x 400),ratio will be 1.5 



                if (Image.Length > (1000 * 1024))
                {
                    getseterror = "filesize will be upto " + 1000 + "KB";
                    return getseterror;
                }
                else if (!supportedTypes.Contains(fileExt))
                {
                    getseterror = "file extension is not valid";
                    return getseterror;
                }

                {
                    getseterror = "success";
                    return getseterror;
                }
            }
            catch (Exception ex)
            {
                getseterror = ex.Message;
                return getseterror;
            }


        }
        public static Bitmap WatermarkImage(Bitmap image, Bitmap watermark, int percent)
        {
            try
            {
                using (Graphics imageGraphics = Graphics.FromImage(image))
                {
                    watermark.SetResolution(imageGraphics.DpiX, imageGraphics.DpiY);

                    int x = (image.Width - watermark.Width) - 30 * percent / 100;
                    int y = (image.Height - watermark.Height) - 30 * percent / 100;

                    imageGraphics.DrawImage(watermark, x, y, watermark.Width, watermark.Height);
                }
            }
            catch { }

            return image;
        }

        private static System.Drawing.Image ScaleByPercent(Bitmap imgPhoto, int Percent)
        {
            float nPercent = ((float)Percent / 100);
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);
            Bitmap bmPhoto = new Bitmap(destWidth, destHeight,
                                     PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                    imgPhoto.VerticalResolution);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBilinear;
            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);
            grPhoto.Dispose();
            return bmPhoto;
        }
        private string ProcessUploading(IFormFile Image)
        {

            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
               Path.Combine(webHostEnvironment.WebRootPath, _ImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                Image.CopyTo(fileStream);
            }
            return _ImageRepository + uniqueFileName;
        }


    }
}
