using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Events;
using iText.Layout.Properties;
using iText.Kernel.Geom;
using iText.Kernel.Colors;
using iText.Layout.Layout;
using iText.Layout.Renderer;
using iText.IO.Image;
using System.Net;
using iText.Kernel.Font;
using iText.IO.Font;
using iText.Layout.Borders;
using iText.Barcodes;
using Image = iText.Layout.Element.Image;
using iText.StyledXmlParser.Jsoup.Nodes;
using HtmlAgilityPack;

namespace MarketPlace.Pages.ArticleList
{
    public class FicheModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IWebHostEnvironment webHostEnvironment;

        private string _ImageRepository;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;

        public IEnumerable<StaticData> StaticData;
        public List<SelectListItem> StaticDataGenders { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataVatType { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataOuiNon { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataTypeProduit { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataCountry { get; set; } = new List<SelectListItem>();


        public string getseterror { get; set; }



        public FicheModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        [BindProperty]
        public IFormFile Image1 { get; set; }


        [BindProperty]
        public IFormFile Image2 { get; set; }


        [BindProperty]
        public IFormFile Image3 { get; set; }

        [BindProperty]
        public IFormFile ArticleFile { get; set; }
        public IEnumerable<ArticleProperty> ArticleProperty { get; set; }


        [BindProperty]
        public ArticleVendors ArticleVendors { get; set; }
        public Contacts Contact { get; set; }
        public Brands Brand { get; set; }
        public Employees Employee { get; set; }
        public IEnumerable<Discounts> Discount { get; set; }
        public IEnumerable<PreOrderDiscounts> PreOrderDiscount { get; set; }
        public IEnumerable<SpecialDiscounts> SpecialDiscount { get; set; }
        public virtual void CreatePdf(String dest)
        {
            //Initialize PDF writer
            PdfWriter writer = new PdfWriter(dest);
            //Initialize PDF document
            PdfDocument pdf = new PdfDocument(writer);
            // Initialize document
            iText.Layout.Document document = new iText.Layout.Document(pdf);
            //Add paragraph to the document
            document.Add(new Paragraph(ArticleVendors.Name));
            //Close document
            document.Close();
        }




        public ActionResult OnGet(int articleVendorId)
        {
            StaticData = (from art in _db.StaticData
                          select art).ToList();

            ArticleVendors = (from art in _db.ArticleVendors
                              where art.Id == articleVendorId && art.UserCompanyType == _UserCompanyType && art.contactId == _UserCompanyId
                              select art).FirstOrDefault();



           
            Contact = _db.Contacts.Find(ArticleVendors.contactId);
            Brand = _db.Brands.Find(ArticleVendors.BrandId);
            Employee = _db.Employees.Find(ArticleVendors.EmployeeId);
            Discount = (from art in _db.Discounts where art.ArticleId == ArticleVendors.articleId select art).ToList();
            PreOrderDiscount = (from art in _db.PreOrderDiscounts where art.ArticleId == ArticleVendors.articleId select art).ToList();
            SpecialDiscount = (from art in _db.SpecialDiscounts where art.ArticleId == ArticleVendors.articleId select art).ToList();
            ArticleProperty = (from art in _db.ArticleProperty where art.ArticleId == ArticleVendors.articleId select art).ToList();

            String destination = "wwwroot/FichesPDF/" + ArticleVendors.EAN + ".PDF";
            FileInfo file = new FileInfo(destination);
            file.Directory.Create();
            //CreatePdfInMemory( Article, Contact, Brand, Employee, Discount, PreOrderDiscount, SpecialDiscount);
            //System.Diagnostics.Process.Start("https://localhost:44390/FichesPDF/" + Article.EAN + ".PDF");
            //  System.Diagnostics.Process.Start("https://localhost:44390/fichespdf/555.pdf");

            byte[] byte1 = ManipulatePdf();
            return File(byte1, "application/pdf", ArticleVendors.EAN + ".PDF");
        }

        [Obsolete]
        protected byte[] ManipulatePdf()
        {
            var stream = new MemoryStream();
            var writer = new PdfWriter(stream);
            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(stream));
            PageSize pageSize = pdfDoc.GetDefaultPageSize();

            //  InitTable();

            iText.Layout.Document doc = new iText.Layout.Document(pdfDoc, PageSize.A4);
            doc.SetMargins(30, 30, 30, 30);

            /* Table table = new Table(1).UseAllAvailableWidth();

             if (Contact !=null) { 
             Cell cell = new Cell().Add(new Paragraph(Contact.Company));
           //  cell.SetBackgroundColor(ColorConstants.ORANGE);
             table.AddCell(cell);
             }

             if (Brand != null)
             {
                 Cell cell = new Cell().Add(new Paragraph(Brand.BrandName));
            // cell.SetBackgroundColor(ColorConstants.LIGHT_GRAY);
             table.AddCell(cell);
             }
             table.SetBorder(Border.NO_BORDER);
             doc.Add(table);*/

            // pdfDoc.AddEventHandler(PdfDocumentEvent.END_PAGE, new TableFooterEventHandler(table));

            //float height = GetTableHeight();

            PdfFont fontSaira = PdfFontFactory.CreateFont("wwwroot/font/Saira/Saira-Regular.ttf", PdfEncodings.IDENTITY_H, true);
            PdfFont fontSairaLight = PdfFontFactory.CreateFont("wwwroot/font/Saira/Saira-Light.ttf", PdfEncodings.IDENTITY_H, true);
            PdfFont fontMetropolis = PdfFontFactory.CreateFont("wwwroot/font/metropolis/Metropolis-Regular.otf", PdfEncodings.IDENTITY_H, true);
            iText.Kernel.Colors.Color myColor = new DeviceRgb(118, 183, 42);
            iText.Kernel.Colors.Color myColorBlueGreen = new DeviceRgb(9, 82, 96);
            PdfPage page = pdfDoc.AddNewPage();
            Table table0 = new Table(1);
            if (ArticleVendors != null)
            {
                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                Text title = new Text(ArticleVendors.Name).SetBold();
                Text brand = new Text(ArticleVendors.Brand);
                Paragraph p = new Paragraph().Add(new Text("\n")).Add("Fiche produit").Add(new Text("\n"));
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                p.SetBold();
                p.SetFontSize(24);
                p.SetFontColor(myColorBlueGreen);
                p.SetFont(fontMetropolis);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table0.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                table0.AddCell(cell);

            }
            doc.Add(table0);

            Table table = new Table(2);
            if (ArticleVendors != null)
            {
                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                Text title = new Text(ArticleVendors.Name).SetBold();
                Text brand = new Text(ArticleVendors.Brand);
                Paragraph p = new Paragraph().Add(new Text("\n")).Add(new Text("\n")).Add(new Text("\n")).Add(title).Add(new Text("\n")).Add(brand);
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                p.SetBold();
                p.SetFontSize(14);
                p.SetFont(fontMetropolis);
                p.SetFontColor(myColorBlueGreen);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);

                table.AddCell(cell);

            }

            if (ArticleVendors != null)
            {
                String code = ArticleVendors.EAN;
                BarcodeEAN codeEAN = new BarcodeEAN(pdfDoc);
                codeEAN.SetFont(fontSaira);
                codeEAN.SetCode(code);
                codeEAN.SetCodeType(BarcodeEAN.EAN13);

                Image code128Image = new Image(codeEAN.CreateFormXObject(pdfDoc)).SetAutoScale(true);


                Cell cell = new Cell();
                cell.Add(code128Image);
                cell.SetBorder(Border.NO_BORDER);
                table.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                table.AddCell(cell);

            }

            doc.Add(table);
            Paragraph pLineBreak = new Paragraph().Add(new Text("\n"));
            doc.Add(pLineBreak);

            Table table1 = new Table(2);
            if (ArticleVendors != null)
            {


                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/
                var HtmlDocument = new HtmlAgilityPack.HtmlDocument();
                HtmlDocument.LoadHtml(ArticleVendors.ShortDescription.Replace("<br>", Environment.NewLine).Replace("&nbsp;", " "));
                HtmlDocument.OptionWriteEmptyNodes = true;

                /*string text=""
                foreach (HtmlNode p in HtmlDocument.DocumentNode.SelectNodes("//br"))
                {
                    text = text + p.PreviousSibling.InnerText.Trim());
                }*/

                var plainText = HtmlDocument.DocumentNode.InnerText;
                Text title = new Text(plainText);
       

              
                Paragraph p = new Paragraph().Add(title);
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.JUSTIFIED);
                p.SetFontColor(myColorBlueGreen);
                p.SetBold();
                p.SetFontSize(12);
                p.SetFont(fontSaira);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table1.AddCell(cell);
            }

            if (ArticleVendors != null)
            {
                try
                {
                    Image image = new Image(ImageDataFactory.Create(convertUrlImagetobyte("wwwroot/" + ArticleVendors.Image1)));
                    image.ScaleToFit(200, 200);
                   
                    Cell cell = new Cell();
                    cell.Add(image);
                    cell.SetBorder(Border.NO_BORDER);
                    table1.AddCell(cell);
                }
                catch (Exception ex)
                {
                }

            }
            doc.Add(table1);
            doc.Add(pLineBreak);

            Table table2 = new Table(2);
            if (ArticleVendors != null)
            {


                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                Text title = new Text(ArticleVendors.Price.ToString()).SetBold();
                Paragraph p = new Paragraph().Add("Prix: ").Add(title).Add("� HT");
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                p.SetBold();
                p.SetFontColor(myColor);
                p.SetFontSize(12);
                p.SetFont(fontSaira);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table2.AddCell(cell);
            }

            if (ArticleVendors != null)
            {


                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                Text title = new Text(ArticleVendors.Lot.ToString()).SetBold();
                Paragraph p = new Paragraph().Add("Par lot de ").Add(title);
                p.SetFontColor(myColor);
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                p.SetBold();
                p.SetFontSize(12);
                p.SetFont(fontSaira);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table2.AddCell(cell);
            }
            doc.Add(table2);
            doc.Add(pLineBreak);



            Table table3_0 = new Table(1);
            if (ArticleVendors != null)
            {


                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                Paragraph p = new Paragraph().Add("DESCRIPTION");
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                p.SetBold();
                p.SetFontSize(10);
                p.SetFont(fontSairaLight);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table3_0.SetBackgroundColor(myColor);
                table3_0.AddCell(cell);
            }
            doc.Add(table3_0);
            doc.Add(pLineBreak);


            Table table3 = new Table(1);
            if (ArticleVendors != null)
            {


                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                var HtmlDocument = new HtmlAgilityPack.HtmlDocument();
                HtmlDocument.LoadHtml(ArticleVendors.LongDescription.Replace("<br>", Environment.NewLine).Replace("&nbsp;", " "));
                HtmlDocument.OptionWriteEmptyNodes = true;
                
                /*string text=""
                foreach (HtmlNode p in HtmlDocument.DocumentNode.SelectNodes("//br"))
                {
                    text = text + p.PreviousSibling.InnerText.Trim());
                }*/

                var plainText = HtmlDocument.DocumentNode.InnerText;
                Text title = new Text(plainText);
                Paragraph p = new Paragraph().Add(title);
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.JUSTIFIED);
                p.SetFontColor(myColorBlueGreen);
                p.SetBold();
                p.SetFontSize(10);
                p.SetFont(fontSairaLight);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table3.AddCell(cell);
            }
            doc.Add(table3);
            doc.Add(pLineBreak);

            Table table4_0 = new Table(1);
            if (ArticleVendors != null)
            {


                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                Paragraph p = new Paragraph().Add("INDICATIONS");
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                p.SetBold();
                p.SetFontSize(10);
                p.SetFont(fontSairaLight);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table4_0.SetBackgroundColor(myColor);
                table4_0.AddCell(cell);
            }
            doc.Add(table4_0);
            doc.Add(pLineBreak);

            Table table4 = new Table(1);
            if (ArticleVendors != null)
            {
                List list = new List();

                /* PdfCanvas pdfCanvas = new PdfCanvas(page);
                 iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                 Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/


                string sArticleProperty = "";
                foreach (ArticleProperty prop in ArticleProperty)
                {
                     sArticleProperty = "";
                    sArticleProperty = sArticleProperty + "" + prop.ArticlePropertyLabel + ": " + prop.ArticlePropertyValueLabel + "";
                    ListItem item = new ListItem(sArticleProperty);
                    list.Add(item);
                }
                sArticleProperty = sArticleProperty + "";


                Paragraph p = new Paragraph().Add(list);
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.LEFT);
                p.SetBold();
                p.SetFontSize(10);
                p.SetFont(fontSairaLight);
                p.SetFontColor(myColorBlueGreen);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table4.AddCell(cell);
            }
            doc.Add(table4);
            doc.Add(pLineBreak);

            Table table5_0 = new Table(1);
            if (ArticleVendors != null)
            {


                /*PdfCanvas pdfCanvas = new PdfCanvas(page);
                iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/

                Paragraph p = new Paragraph().Add("REMISES");
                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
                p.SetBold();
                p.SetFontSize(10);
                p.SetFont(fontSairaLight);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table5_0.SetBackgroundColor(myColor);
                table5_0.AddCell(cell);
            }
            doc.Add(table5_0);
            doc.Add(pLineBreak);


            Table table5 = new Table(1);
            if (ArticleVendors != null)
            {
                List list = new List();




                /*  PdfCanvas pdfCanvas = new PdfCanvas(page);
                  iText.Kernel.Geom.Rectangle rectangle = new iText.Kernel.Geom.Rectangle(36, 650, 100, 100);
                  Canvas canvas = new Canvas(pdfCanvas, pdfDoc, rectangle);*/


                string sDiscount = "";
                foreach (Discounts prop in Discount)
                {
                      sDiscount = "";
                    sDiscount =  "Taux de remise : " + prop.DiscountRate1 + "%";
                    sDiscount = sDiscount+ " Prix remis� : " + prop.DiscountPrice1 + "";
                    sDiscount = sDiscount+ " Quantiti� : " + prop.Quantity1 + "";
                    ListItem item = new ListItem(sDiscount);
                    list.Add(item);
                }


                Paragraph p = new Paragraph().Add(list);

                p.SetTextAlignment(iText.Layout.Properties.TextAlignment.LEFT);
                p.SetBold();
                p.SetFontSize(10);
                p.SetFont(fontSairaLight);
                p.SetFontColor(myColorBlueGreen);
                Cell cell = new Cell();
                cell.SetBorder(Border.NO_BORDER);
                cell.Add(p);
                table5.AddCell(cell);
            }
            doc.Add(table5);
            doc.Add(pLineBreak);

            //Header
            /* for (int i = 1; i <= pdfDoc.GetNumberOfPages(); i++)
             {
                 PdfPage pdfPage1 = pdfDoc.GetPage(i);

                 // When "true": in case the page has a rotation, then new content will be automatically rotated in the
                 // opposite direction. On the rotated page this would look as if new content ignores page rotation.
                 pdfPage1.SetIgnorePageRotationForContent(true);

                 iText.Kernel.Geom.Rectangle pageSize1 = pdfPage1.GetPageSize();
                 float x, y;
                 if (pdfPage1.GetRotation() % 180 == 0)
                 {
                     x = pageSize1.GetWidth() / 2;
                     y = pageSize1.GetTop() - 20;
                 }
                 else
                 {
                     x = pageSize1.GetHeight() / 2;
                     y = pageSize1.GetRight() - 20;
                 }

                 doc.ShowTextAligned(header(), x, y, i, TextAlignment.CENTER, VerticalAlignment.BOTTOM, 0);
             }*/
      
            doc.Close();
            return stream.ToArray();
        }
        private Paragraph footer()
        {
            PdfFont fontMetropolis = PdfFontFactory.CreateFont("wwwroot/font/metropolis/Metropolis-Regular.otf", PdfEncodings.IDENTITY_H, true);

            Paragraph p = new Paragraph("Pharma Shop Center ");
            p.SetFont(fontMetropolis);
            p.SetFontSize(10);
            return p;
        }
        private Paragraph header()
        {
            PdfFont fontMetropolis = PdfFontFactory.CreateFont("wwwroot/font/metropolis/Metropolis-Regular.otf", PdfEncodings.IDENTITY_H, true);

            Paragraph p = new Paragraph("Pharma Shop Center ");
            p.SetFont(fontMetropolis);
            p.SetFontSize(10);
            return p;
        }


        public byte[] convertUrlImagetobyte(string someUrl)
        {
            var webClient = new WebClient();
            byte[] imageBytes = webClient.DownloadData(someUrl);

            return imageBytes;
        }



        private class TableFooterEventHandler : IEventHandler
        {
            private Table table;
            private float tableHeight;
            private iText.Layout.Document doc;

            public TableFooterEventHandler(Table table)
            {
                this.table = table;
            }

            public void HandleEvent(Event currentEvent)
            {
                PdfDocumentEvent docEvent = (PdfDocumentEvent)currentEvent;
                PdfDocument pdfDoc = docEvent.GetDocument();
                PdfPage page = docEvent.GetPage();
                PdfCanvas canvas = new PdfCanvas(page.NewContentStreamBefore(), page.GetResources(), pdfDoc);

                new Canvas(canvas, new iText.Kernel.Geom.Rectangle(36, 20, page.GetPageSize().GetWidth() - 72, 50))
                    .Add(table)
                    .Close();
            }
            public void TableHeaderEventHandler(iText.Layout.Document doc)
            {
                this.doc = doc;
                InitTable();

                TableRenderer renderer = (TableRenderer)table.CreateRendererSubTree();
                renderer.SetParent(new DocumentRenderer(doc));

                // Simulate the positioning of the renderer to find out how much space the header table will occupy.
                LayoutResult result = renderer.Layout(new LayoutContext(new LayoutArea(0, PageSize.A4)));
                tableHeight = result.GetOccupiedArea().GetBBox().GetHeight();
            }

            public float GetTableHeight()
            {
                return this.tableHeight;
            }

            private void InitTable()
            {
                table = new Table(1).UseAllAvailableWidth();
                table.AddCell("Header row 1");
                table.AddCell("Header row 2");
                table.AddCell("Header row 3");
            }
           


        }

    }


    
    }
        
  
      