﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using MarketPlace.Model.SaveMultiSelectDropDown;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace MarketPlace.Pages.ArticleList
{
    public class EditModel : PageModel
    {
        public int[] SelectedValues { get; set; }
        private readonly Data.ApplicationDbContext _db;
        private readonly IWebHostEnvironment webHostEnvironment;

        private readonly string _ImageRepository;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;


        [BindProperty]
        public IFormFile BrandLogo { get; set; }

        [BindProperty]
        public IFormFile ImageBanderole1 { get; set; }

        public IEnumerable<StaticData> StaticData;
        public IEnumerable<Country> Countries;
        public IEnumerable<ProductCategories> ProductCategories;
        public List<SelectListItem> StaticDataGenders { get; set; } = new List<SelectListItem>();

        [BindProperty]
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public IEnumerable<SelectListItem> PropertyList { get; set; }
        public IEnumerable<SelectListItem> VarianteList { get; set; }

        public MultiSelectDropDownViewModel model { get; set; }
        public MultiSelectDropDownViewModelProperty model2 { get; set; }
        public MultiSelectDropDownViewModelVariante model3 { get; set; }
        
        public List<SelectListItem> items { get; set; }
        public List<SelectListItem> itemsproperties { get; set; }
        public List<SelectListItem> itemsvariantes { get; set; }
        public List<SelectListItem> StaticDataVatType { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataOuiNon { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataTypeProduit { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataCountry { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataFrequency { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataCategory { get; set; } = new List<SelectListItem>();

        public string getseterror { get; set; }

        public EditModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        [BindProperty]
        public IFormFile Image1 { get; set; }

        [BindProperty]
        public IFormFile Video1 { get; set; }
        

        [BindProperty]
        public IFormFile Image2 { get; set; }


        [BindProperty]
        public IFormFile Image3 { get; set; }

        [BindProperty]
        public IFormFile ArticleFile { get; set; }
        [BindProperty]
        public IFormFile EN_ArticleFile { get; set; }
        [BindProperty]
        public  ArticleVendors ArticleVendors { get; set; }

        [BindProperty]
        public Article Article { get; set; }

        public async Task OnGet(int id)
        {
            StaticData = await (from art in _db.StaticData
                                select art).ToListAsync();

            Countries = await (from art in _db.Countries
                               select art).ToListAsync();

            ProductCategories = await (from art in _db.ProductCategories
                                       select art).ToListAsync();

            ArticleVendors = (from art in _db.ArticleVendors
                                    where art.articleId == id && art.contactId == _UserCompanyId && art.UserCompanyType == _UserCompanyType
                              select art).ToList().FirstOrDefault() ;

            Article= await _db.Article.FirstOrDefaultAsync(u => u.Id == id);


            if (ArticleVendors == null) {

                ArticleVendors = new ArticleVendors();
                ArticleVendors.articleId = id;
                ArticleVendors.contactId = _UserCompanyId;
                ArticleVendors.UpdateByUserId = _ConnectedUserId;
                ArticleVendors.UserCompanyType = _UserCompanyType;

                ArticleVendors.Name = Article.Name;
                ArticleVendors.EAN = Article.EAN;
                ArticleVendors.Price = 0;
                ArticleVendors.B2CPrice = Article.B2CPrice;
                ArticleVendors.VAT = Article.VAT;
                ArticleVendors.FirstCategory = Article.FirstCategory;
                ArticleVendors.ShortDescription = Article.ShortDescription;
                ArticleVendors.LongDescription = Article.LongDescription;
                ArticleVendors.Notice = Article.Notice;
                ArticleVendors.SEOTitle = Article.SEOTitle;
                ArticleVendors.SEODescription = Article.SEODescription;
                ArticleVendors.Ingredient = Article.Ingredient;
                ArticleVendors.ArticleFile = Article.ArticleFile;
                ArticleVendors.Image1 = Article.Image1;
                ArticleVendors.Image2 = Article.Image2;
                ArticleVendors.Image3 = Article.Image3;
               ArticleVendors.Inventory = 0;
                ArticleVendors.B2CInventory = Article.B2CInventory;
                ArticleVendors.YoutubeVideo = Article.YoutubeVideo;
                ArticleVendors.FacebookPage = Article.FacebookPage;
                ArticleVendors.TwitterPage = Article.TwitterPage;
                ArticleVendors.InstagramPage = Article.InstagramPage;
                ArticleVendors.Indication = Article.Indication;
                ArticleVendors.Brand = Article.Brand;
                ArticleVendors.WarningMessage = Article.WarningMessage;
                ArticleVendors.ArticleFilesLink = Article.ArticleFilesLink;
                ArticleVendors.ActiveSubtances = Article.ActiveSubtances;
                ArticleVendors.SecondCategory = Article.SecondCategory;
                ArticleVendors.ThirdCategory = Article.ThirdCategory;
                ArticleVendors.Weight = Article.Weight;
                ArticleVendors.Volume = Article.Volume;
                ArticleVendors.Height = Article.Height;
                ArticleVendors.Width = Article.Width;
                ArticleVendors.Depth = Article.Depth;
                ArticleVendors.Lot =1;
                ArticleVendors.B2CLot = Article.B2CLot;
                ArticleVendors.OriginCountry = Article.OriginCountry;
                ArticleVendors.CustomsCode = Article.CustomsCode;
                ArticleVendors.BuyingPrice = Article.BuyingPrice;
                ArticleVendors.PriceType = Article.PriceType;
                ArticleVendors.RefundRate = Article.RefundRate;
                ArticleVendors.SaleManagerName = Article.SaleManagerName;
                ArticleVendors.SaleManagerFirstname = Article.SaleManagerFirstname;
                ArticleVendors.SaleManagerCivility = Article.SaleManagerCivility;
                ArticleVendors.SaleManagerEmail = Article.SaleManagerEmail;
                ArticleVendors.SaleManagerTelephone = Article.SaleManagerTelephone;
                ArticleVendors.FrancoPortAmount = Article.FrancoPortAmount;
                ArticleVendors.MinimumOrderQuantity = Article.MinimumOrderQuantity;
                ArticleVendors.MaximumOrderQuantity = Article.MaximumOrderQuantity;
                ArticleVendors.DropShippingEmail = Article.DropShippingEmail;
                ArticleVendors.SupplierAdvisedResalePrice = Article.SupplierAdvisedResalePrice;
    
                ArticleVendors.LastUpdate = Article.LastUpdate;
                ArticleVendors.PROSellerCompanyId = Article.PROSellerCompanyId;
                ArticleVendors.SuplierArticleReference = Article.SuplierArticleReference;
                ArticleVendors.SalesChannel = Article.SalesChannel;
                ArticleVendors.ArticleValidatedFlag = Article.ArticleValidatedFlag;
                ArticleVendors.EndOfLifeFlag = Article.EndOfLifeFlag;
                ArticleVendors.ProductionDate = Article.ProductionDate;
                ArticleVendors.NoveltyFlag = Article.NoveltyFlag;
                ArticleVendors.AccountingCode = Article.AccountingCode;
                ArticleVendors.ProductType = Article.ProductType;
                ArticleVendors.ArticleKeywords = Article.ArticleKeywords;
                ArticleVendors.DeliveryDelay = Article.DeliveryDelay;
                ArticleVendors.PurchaseNomenclature = Article.PurchaseNomenclature;
                ArticleVendors.CommercialTaget = Article.CommercialTaget;
                ArticleVendors.EcoTaxAmount = Article.EcoTaxAmount;
                ArticleVendors.EN_Name = Article.EN_Name;
                ArticleVendors.EN_ShortDescription = Article.EN_ShortDescription;
                ArticleVendors.EN_LongDescription = Article.EN_LongDescription;
                ArticleVendors.EN_Notice = Article.EN_Notice;
                ArticleVendors.EN_SEOTitle = Article.EN_SEOTitle;
                ArticleVendors.EN_SEODescription = Article.EN_SEODescription;
                ArticleVendors.EN_Ingredient = Article.EN_Ingredient;
                ArticleVendors.EN_ArticleFile = Article.EN_ArticleFile;
                ArticleVendors.EN_Indication = Article.EN_Indication;
                ArticleVendors.EN_ActiveSubtances = Article.EN_ActiveSubtances;
                ArticleVendors.ProductShopLink = Article.ProductShopLink;
                ArticleVendors.BrandShopLink = Article.BrandShopLink;
                ArticleVendors.SaleAuthorizedInPharmacy = Article.SaleAuthorizedInPharmacy;
                ArticleVendors.ShelfLife = Article.ShelfLife;
                ArticleVendors.Internalreference = Article.Internalreference;
                ArticleVendors.Variantreference = Article.Variantreference;
                ArticleVendors.Groupcode = Article.Groupcode;
                ArticleVendors.ArticleOtherName = Article.ArticleOtherName;
                ArticleVendors.En_ArticleOtherName = Article.En_ArticleOtherName;
                ArticleVendors.Image4 = Article.Image4;
                ArticleVendors.Image5 = Article.Image5;
                ArticleVendors.Image6 = Article.Image6;
                ArticleVendors.Image7 = Article.Image7;
                ArticleVendors.Image8 = Article.Image8;
                ArticleVendors.Image9 = Article.Image9;
                ArticleVendors.Image10 = Article.Image10;
                ArticleVendors.NextProductionDate = Article.NextProductionDate;
                ArticleVendors.ProductionFrequency = Article.ProductionFrequency;
                ArticleVendors.NextProductionEstimateQuantity = Article.NextProductionEstimateQuantity;
                ArticleVendors.Created = Article.Created;
                ArticleVendors.EN_ArticleKeywords = Article.EN_ArticleKeywords;
                ArticleVendors.SaleManagerFax = Article.SaleManagerFax;
                ArticleVendors.Destockage = Article.Destockage;
                ArticleVendors.FirstOrderMinimalAmount = Article.FirstOrderMinimalAmount;
                ArticleVendors.Code_CIP7 = Article.Code_CIP7;
                ArticleVendors.Modele = Article.Modele;
                ArticleVendors.Gamme = Article.Gamme;
                ArticleVendors.UPC = Article.UPC;
                ArticleVendors.Contenance = Article.Contenance;
                ArticleVendors.Forme = Article.Forme;
                ArticleVendors.Taille = Article.Taille;
                ArticleVendors.Conseiller_pour = Article.Conseiller_pour;
                ArticleVendors.Dimension = Article.Dimension;
                ArticleVendors.Autres_caracteristiques = Article.Autres_caracteristiques;
                ArticleVendors.Couleur = Article.Couleur;
                ArticleVendors.Pour_qui = Article.Pour_qui;
                ArticleVendors.Presentation = Article.Presentation;
                ArticleVendors.Indication_autres = Article.Indication_autres;
                ArticleVendors.Type_de_cheveux = Article.Type_de_cheveux;
                ArticleVendors.Type_de_peau = Article.Type_de_peau;
                ArticleVendors.Parfum = Article.Parfum;
                ArticleVendors.Texture = Article.Texture;
                ArticleVendors.Pour_quel_animal = Article.Pour_quel_animal;
                ArticleVendors.Type_de_shampooing = Article.Type_de_shampooing;
                ArticleVendors.Mode_administration = Article.Mode_administration;
                ArticleVendors.Video1 = Article.Video1;
                ArticleVendors.Usage = Article.Usage;
                ArticleVendors.ReturnFlag = Article.ReturnFlag;
                ArticleVendors.OtherInformation = Article.OtherInformation;
                ArticleVendors.AttractivenessIndex = Article.AttractivenessIndex;
                ArticleVendors.BrandId = Article.BrandId;
                ArticleVendors.EmployeeId = Article.EmployeeId;
                ArticleVendors.Note = Article.Note;
                ArticleVendors.UnreadMessagesNumber = Article.UnreadMessagesNumber;
                ArticleVendors.barCodeImage = Article.barCodeImage;
                ArticleVendors.categoryMarketing = Article.categoryMarketing;
         
                ArticleVendors.SaleAreaFullName = Article.SaleAreaFullName;
                ArticleVendors.SaleAreaId = Article.SaleAreaId;
                ArticleVendors.youtubePage = Article.youtubePage;
                ArticleVendors.OfficialWebsite = Article.OfficialWebsite;
        
                ArticleVendors.ProductDangerosity = Article.ProductDangerosity;

                await _db.ArticleVendors.AddAsync(ArticleVendors);
                await _db.SaveChangesAsync();
            }

            Article = await (_db.Article.FindAsync(id));

     

            SelectListItem selListItem = new SelectListItem() { Value = "null", Text = "Selectionnez" };

            StaticDataGenders.Add(selListItem);

            StaticDataVatType.Add(selListItem);
            StaticDataOuiNon.Add(selListItem);
            StaticDataTypeProduit.Add(selListItem);
            StaticDataCountry.Add(selListItem);
            StaticDataFrequency.Add(selListItem);

            foreach (var prop in ProductCategories)
            {
                SelectListItem item = new SelectListItem(prop.Name, prop.Id.ToString());
                StaticDataCategory.Add(item);
            }

            foreach (var prop in Countries)
            {
                SelectListItem item = new SelectListItem(prop.nom_fr_fr, prop.Id.ToString());
                StaticDataCountry.Add(item);
            }

            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "Civility")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataGenders.Add(item);
                }

                if (val == "VAT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataVatType.Add(item);
                }


                if (val == "YESNO")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataOuiNon.Add(item);
                }

                if (val == "TYPEPRODUIT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataTypeProduit.Add(item);
                }

                if (val == "FREQUENCY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataFrequency.Add(item);
                }

            }

            // Initialization.  
            MultiSelectDropDownViewModel model = new MultiSelectDropDownViewModel { SelectedMultiCountryId = new List<int>(), SelectedCountryLst = new List<Country>() };

            // Initialization.  
            MultiSelectDropDownViewModelProperty model2 = new MultiSelectDropDownViewModelProperty { SelectedMultiPropertyId = new List<int>(), SelectedPropertyLst = new List<Property>() };

            // Initialization.  
            MultiSelectDropDownViewModelVariante model3 = new MultiSelectDropDownViewModelVariante { SelectedMultiVarianteId = new List<int>(), SelectedVarianteLst = new List<Variante>() };


            try
            {
                // Loading drop down lists.  
                CountryList = this.GetCountryList(id);
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

            try
            {
                // Loading drop down lists.  
                PropertyList = this.GetPropertyList(id);
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }


            try
            {
                // Loading drop down lists.  
               VarianteList = this.GetVarianteList(id);
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

            

        }
        private List<SelectListItem> GetCountryList(int articleId)
        {

            items = new List<SelectListItem>();
            List<SelectListGroup> groupItems = new List<SelectListGroup>();

            var listArticleContinent = (from art in _db.Continents select art).ToList();
            foreach (var prop in listArticleContinent)
            {
                groupItems.Add(new SelectListGroup() { Name = prop.Name });
            }


            try
            {
                // Loading.  
                var list = (from art in _db.Countries select art).ToList();

                var listArticleCountry = (from art in _db.ArticleCountries where art.ArticleId == articleId select art).ToList();

                var group = new SelectListGroup();
                bool selected = false;
                foreach (var prop in list)
                {
                    foreach (var groupselected in groupItems)
                    {

                        if (groupselected.Name == prop.Continent)
                        {
                            group = groupselected;
                        }
                    }

                    foreach (var propselected in listArticleCountry)
                    {
                        if (propselected.CountryId == prop.Id)
                        {
                            selected = true;
                        }
                    }
                    items.Add(new SelectListItem() { Text = prop.nom_fr_fr, Group = group, Selected = selected, Value = prop.Id.ToString() });
                    selected = false;
                }

            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // info.  
            return items;
        }

        private List<SelectListItem> GetVarianteList(int articleId)
        {
            itemsvariantes = new List<SelectListItem>();
            List<SelectListGroup> groupItemsvariantes = new List<SelectListGroup>();

          
                groupItemsvariantes.Add(new SelectListGroup() { Name = "Variantes produit" });
           

            try
            {
                // Loading.  
                var listVariante = (from art in _db.Article where art.Brand==Article.Brand select art).ToList();

                var listArticleVariante = (from art in _db.ArticleVariantes where art.ArticleId == articleId select art).ToList();

                var groupvariantes = new SelectListGroup();
                bool selectedVariante = false;
                foreach (var prop in listVariante)
                {
                    foreach (var groupselected in groupItemsvariantes)
                    {

                        if (groupselected.Name == prop.Name + "-" + prop.EAN)
                        {
                            groupvariantes = groupselected;
                        }
                    }

                    foreach (var propselected in listArticleVariante)
                    {
                        if (propselected.VarianteArticleId == prop.Id)
                        {
                            selectedVariante = true;
                        }
                    }
                    itemsvariantes.Add(new SelectListItem() { Text = prop.Name + "-" + prop.EAN, Group = groupvariantes, Selected = selectedVariante, Value = prop.Id.ToString() });
                    selectedVariante = false;
                }

            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // info.  

            return itemsvariantes;
        }

        private List<SelectListItem> GetPropertyList(int articleId)
        {
            itemsproperties = new List<SelectListItem>();
            List<SelectListGroup> groupItemsproperties = new List<SelectListGroup>();

            var listArticleProperties = (from art in _db.Properties select art).ToList();
            foreach (var prop in listArticleProperties)
            {
                groupItemsproperties.Add(new SelectListGroup() { Name = prop.PropertyName });
            }


            try
            {
                // Loading.  
                var listProperty = (from art in _db.PropertyValues select art).ToList();

                var listArticleProperty = (from art in _db.ArticleProperty where art.ArticleId == articleId select art).ToList();

                var groupproperties = new SelectListGroup();
                bool selectedProperty = false;
                foreach (var prop in listProperty)
                {
                    foreach (var groupselected in groupItemsproperties)
                    {

                        if (groupselected.Name == prop.PropertyName)
                        {
                            groupproperties = groupselected;
                        }
                    }

                    foreach (var propselected in listArticleProperty)
                    {
                        if (propselected.PropertyValueId == prop.Id)
                        {
                            selectedProperty = true;
                        }
                    }
                    itemsproperties.Add(new SelectListItem() { Text = prop.PropertyValueName, Group = groupproperties, Selected = selectedProperty, Value = prop.Id.ToString() });
                    selectedProperty = false;
                }

            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // info.  

            return itemsproperties;
        }



        public async Task<IActionResult> OnPost()
        {

            bool NewArticleVendorsFromDb = false; //si nouveau produit perso ArticleVendor

            try
            {
                // Verification  
                //  if (ModelState.IsValid)
                //{
                // Initialization.  
                List<Country> countryList = (from art in _db.Countries select art).ToList();

                //  }

                // Loading drop down lists.  
                CountryList = this.GetCountryList(Article.Id);
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }


            StaticData = await (from art in _db.StaticData
                                select art).ToListAsync();

            Countries = await (from art in _db.Countries
                               select art).ToListAsync();


            ProductCategories = await (from art in _db.ProductCategories
                                       select art).ToListAsync();


            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "Civility")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataGenders.Add(item);
                }

                if (val == "VAT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataVatType.Add(item);
                }


                if (val == "YESNO")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataOuiNon.Add(item);
                }

                if (val == "TYPEPRODUIT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataTypeProduit.Add(item);
                }

                if (val == "FREQUENCY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataFrequency.Add(item);
                }

            }

            foreach (var prop in ProductCategories)
            {
                SelectListItem item = new SelectListItem(prop.Name, prop.Id.ToString());
                StaticDataCategory.Add(item);
            }


            foreach (var prop in Countries)
            {
                SelectListItem item = new SelectListItem(prop.nom_fr_fr, prop.Id.ToString());
                StaticDataCountry.Add(item);
            }


            //  if (ModelState.IsValid)
            // {

            ArticleVendors.articleId = Article.Id;
            var ArticleVendorsFromDb = (from art in _db.ArticleVendors
                                        where art.articleId == ArticleVendors.articleId && art.contactId == _UserCompanyId && art.UserCompanyType == _UserCompanyType
                                        select art).ToList().FirstOrDefault();

            if (ArticleVendorsFromDb == null)
            {
                ArticleVendorsFromDb = new ArticleVendors();
                ArticleVendorsFromDb.articleId = ArticleVendors.Id;
                ArticleVendorsFromDb.contactId = _UserCompanyId;
                ArticleVendorsFromDb.UpdateByUserId = _ConnectedUserId;
                ArticleVendorsFromDb.UserCompanyType = _UserCompanyType;

                NewArticleVendorsFromDb = true;
            }


            //var ArticleVendorsFromDb = await _db.Article.FindAsync(Article.Id);
            ArticleVendorsFromDb.Name = ArticleVendors.Name;

            ArticleVendorsFromDb.Brand = ArticleVendors.Brand;
            ArticleVendorsFromDb.EAN = ArticleVendors.EAN;

            // ArticleVendorsFromDb.VAT= ArticleVendors.VAT;
            foreach (var prop in StaticDataVatType)
            {
                if (Convert.ToDecimal(prop.Value, new CultureInfo("fr-FR")) == Convert.ToDecimal(Article.VAT, new CultureInfo("fr-FR")))
                {
                    ArticleVendorsFromDb.VAT = Math.Round(Convert.ToDecimal(prop.Text, new CultureInfo("fr-FR")), 2);
                }
            }

            ArticleVendorsFromDb.FirstCategory = ArticleVendors.FirstCategory;
            ArticleVendorsFromDb.Indication = ArticleVendors.Indication;
            ArticleVendorsFromDb.ShortDescription = ArticleVendors.ShortDescription;
            ArticleVendorsFromDb.LongDescription = ArticleVendors.LongDescription;
            ArticleVendorsFromDb.SEOTitle = ArticleVendors.SEOTitle;
            ArticleVendorsFromDb.SEODescription = ArticleVendors.SEODescription;


            ArticleVendorsFromDb.Notice = ArticleVendors.Notice;

            ArticleVendorsFromDb.YoutubeVideo = ArticleVendors.YoutubeVideo;
            ArticleVendorsFromDb.FacebookPage = ArticleVendors.FacebookPage;

            ArticleVendorsFromDb.youtubePage = ArticleVendors.youtubePage;
            ArticleVendorsFromDb.OfficialWebsite = ArticleVendors.OfficialWebsite;

            ArticleVendorsFromDb.TwitterPage = ArticleVendors.TwitterPage;
            ArticleVendorsFromDb.InstagramPage = ArticleVendors.InstagramPage;
            ArticleVendorsFromDb.WarningMessage = ArticleVendors.WarningMessage;
            ArticleVendorsFromDb.ArticleFilesLink = ArticleVendors.ArticleFilesLink;
            ArticleVendorsFromDb.Ingredient = ArticleVendors.Ingredient;
            ArticleVendorsFromDb.ActiveSubtances = ArticleVendors.ActiveSubtances;

            ArticleVendorsFromDb.SecondCategory = ArticleVendors.SecondCategory;
            ArticleVendorsFromDb.ThirdCategory = ArticleVendors.ThirdCategory;
            ArticleVendorsFromDb.Weight = ArticleVendors.Weight;
            ArticleVendorsFromDb.Volume = ArticleVendors.Volume;
            ArticleVendorsFromDb.Height = ArticleVendors.Height;

            if (_UserCompanyType == "B2C")
                ArticleVendorsFromDb.PriceType = "TTC";
            else
                ArticleVendorsFromDb.PriceType = "HT";

            ArticleVendorsFromDb.Width = ArticleVendors.Width;
            ArticleVendorsFromDb.Depth = ArticleVendors.Depth;
            ArticleVendorsFromDb.UpdateByUserId = _ConnectedUserId;
            ArticleVendorsFromDb.LastUpdate = DateTime.Now;

            ArticleVendorsFromDb.ProductionFrequency = ArticleVendors.ProductionFrequency;

            ArticleVendorsFromDb.ReturnFlag = ArticleVendors.ReturnFlag;
            ArticleVendorsFromDb.Destockage = ArticleVendors.Destockage;
            ArticleVendorsFromDb.SaleManagerFax = ArticleVendors.SaleManagerFax;
            ArticleVendorsFromDb.EN_ArticleKeywords = ArticleVendors.EN_ArticleKeywords;
            ArticleVendorsFromDb.EN_SEOTitle = ArticleVendors.EN_SEOTitle;
            ArticleVendorsFromDb.EN_SEODescription = ArticleVendors.EN_SEODescription;

            ArticleVendorsFromDb.Taille = ArticleVendors.Taille;
            ArticleVendorsFromDb.Dimension = ArticleVendors.Dimension;
            ArticleVendorsFromDb.Texture = ArticleVendors.Texture;
            ArticleVendorsFromDb.Couleur = ArticleVendors.Couleur;
            ArticleVendorsFromDb.Parfum = ArticleVendors.Parfum;
            ArticleVendorsFromDb.Autres_caracteristiques = ArticleVendors.Autres_caracteristiques;
            ArticleVendorsFromDb.Conseiller_pour = ArticleVendors.Conseiller_pour;
            ArticleVendorsFromDb.Pour_qui = ArticleVendors.Pour_qui;
            ArticleVendorsFromDb.Pour_quel_animal = ArticleVendors.Pour_quel_animal;
            ArticleVendorsFromDb.Type_de_shampooing = ArticleVendors.Type_de_shampooing;
            ArticleVendorsFromDb.Mode_administration = ArticleVendors.Mode_administration;
            ArticleVendorsFromDb.Type_de_peau = ArticleVendors.Type_de_peau;
            ArticleVendorsFromDb.Presentation = ArticleVendors.Presentation;
            ArticleVendorsFromDb.Indication_autres = ArticleVendors.Indication_autres;
            ArticleVendorsFromDb.Type_de_cheveux = ArticleVendors.Type_de_cheveux;
            ArticleVendorsFromDb.Modele = ArticleVendors.Modele;
            ArticleVendorsFromDb.Code_CIP7 = ArticleVendors.Code_CIP7;
            ArticleVendorsFromDb.Gamme = ArticleVendors.Gamme;
            ArticleVendorsFromDb.Contenance = ArticleVendors.Contenance;
            ArticleVendorsFromDb.UPC = ArticleVendors.UPC;
            ArticleVendorsFromDb.ShelfLife = ArticleVendors.ShelfLife;
            ArticleVendorsFromDb.Usage = ArticleVendors.Usage;
            ArticleVendorsFromDb.EN_ShortDescription = ArticleVendors.EN_ShortDescription;
            ArticleVendorsFromDb.EN_LongDescription = ArticleVendors.EN_LongDescription;
            ArticleVendorsFromDb.EN_Ingredient = ArticleVendors.EN_Ingredient;
            ArticleVendorsFromDb.EN_Notice = ArticleVendors.EN_Notice;
            ArticleVendorsFromDb.EN_ActiveSubtances = ArticleVendors.EN_ActiveSubtances;
            ArticleVendorsFromDb.ProductDangerosity = ArticleVendors.ProductDangerosity;
            ArticleVendorsFromDb.OtherInformation = ArticleVendors.OtherInformation;
            ArticleVendorsFromDb.EN_Name = ArticleVendors.EN_Name;
            //ArticleVendorsFromDb.OriginCountry = ArticleVendors.OriginCountry;
            foreach (var prop in StaticDataCountry)
            {
                if (prop.Value == ArticleVendors.OriginCountry)
                {
                    ArticleVendorsFromDb.OriginCountry = prop.Text;
                }
            }




            foreach (var prop in StaticDataCategory)
            {
                if (prop.Value == ArticleVendors.FirstCategory)
                {
                    ArticleVendorsFromDb.FirstCategory = prop.Text;
                }

            }

            ArticleVendorsFromDb.CustomsCode = ArticleVendors.CustomsCode;
            ArticleVendorsFromDb.BuyingPrice = ArticleVendors.BuyingPrice;
            ArticleVendorsFromDb.SaleManagerName = ArticleVendors.SaleManagerName;
            ArticleVendorsFromDb.SaleManagerFirstname = ArticleVendors.SaleManagerFirstname;


            foreach (var prop in StaticDataGenders)
            {
                if (prop.Value == ArticleVendors.SaleManagerCivility)
                {
                    ArticleVendorsFromDb.SaleManagerCivility = prop.Text;
                }
            }

            ArticleVendorsFromDb.SaleManagerEmail = ArticleVendors.SaleManagerEmail;
            ArticleVendorsFromDb.SaleManagerTelephone = ArticleVendors.SaleManagerTelephone;

            ArticleVendorsFromDb.FrancoPortAmount = ArticleVendors.FrancoPortAmount;
            ArticleVendorsFromDb.MinimumOrderQuantity = ArticleVendors.MinimumOrderQuantity;
            ArticleVendorsFromDb.MaximumOrderQuantity = ArticleVendors.MaximumOrderQuantity;
            ArticleVendorsFromDb.DropShippingEmail = ArticleVendors.DropShippingEmail;
            ArticleVendorsFromDb.SupplierAdvisedResalePrice = ArticleVendors.SupplierAdvisedResalePrice;
            ArticleVendorsFromDb.RefundRate = ArticleVendors.RefundRate;


            ArticleVendorsFromDb.SalesChannel = ArticleVendors.SalesChannel;


            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == ArticleVendors.ArticleValidatedFlag)
                {
                    ArticleVendorsFromDb.ArticleValidatedFlag = prop.Text;
                }
            }
            // ArticleVendorsFromDb.EndOfLifeFlag = ArticleVendors.EndOfLifeFlag;
            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == ArticleVendors.EndOfLifeFlag)
                {
                    ArticleVendorsFromDb.EndOfLifeFlag = prop.Text;
                }
            }
            // ArticleVendorsFromDb.PreorderFlag = ArticleVendors.PreorderFlag;
            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == ArticleVendors.SaleAuthorizedInPharmacy)
                {
                    ArticleVendorsFromDb.SaleAuthorizedInPharmacy = prop.Text;
                }
            }



            ArticleVendorsFromDb.ProductionDate = ArticleVendors.ProductionDate;
            //  ArticleVendorsFromDb.NoveltyFlag = ArticleVendors.NoveltyFlag;
            foreach (var prop in StaticDataOuiNon)
            {
                if (prop.Value == ArticleVendors.NoveltyFlag)
                {
                    ArticleVendorsFromDb.NoveltyFlag = prop.Text;
                }
            }

            ArticleVendorsFromDb.AccountingCode = ArticleVendors.AccountingCode;

            //ArticleVendorsFromDb.ProductType = ArticleVendors.ProductType;
            foreach (var prop in StaticDataTypeProduit)
            {
                if (prop.Value == ArticleVendors.ProductType)
                {
                    ArticleVendorsFromDb.ProductType = prop.Text;
                }
            }
            ArticleVendorsFromDb.PurchaseNomenclature = ArticleVendors.PurchaseNomenclature;
            ArticleVendorsFromDb.CommercialTaget = ArticleVendors.CommercialTaget;
            ArticleVendorsFromDb.SuplierArticleReference = ArticleVendors.SuplierArticleReference;
            ArticleVendorsFromDb.ArticleKeywords = ArticleVendors.ArticleKeywords;
            ArticleVendorsFromDb.EcoTaxAmount = ArticleVendors.EcoTaxAmount;

            ArticleVendorsFromDb.DeliveryDelay = ArticleVendors.DeliveryDelay;
            ArticleVendorsFromDb.FirstOrderMinimalAmount = ArticleVendors.FirstOrderMinimalAmount;
            ArticleVendorsFromDb.NextProductionDate = ArticleVendors.NextProductionDate;
            ArticleVendorsFromDb.NextProductionEstimateQuantity = ArticleVendors.NextProductionEstimateQuantity;

            /*  if (_UserCompanyType == "B2B")
              {
                  ArticleVendorsFromDb.Lot = ArticleVendors.Lot;
                  ArticleVendorsFromDb.Price = ArticleVendors.Price;
              }
              else
              {
                  ArticleVendorsFromDb.B2CLot = ArticleVendors.Lot;
                  ArticleVendorsFromDb.B2CPrice = ArticleVendors.Price;
                  ArticleVendorsFromDb.B2CInventory = ArticleVendors.Inventory;
              }*/
            ArticleVendorsFromDb.Price = ArticleVendors.Price;
            ArticleVendorsFromDb.Lot = ArticleVendors.Lot;
            ArticleVendorsFromDb.Inventory = ArticleVendors.Inventory;


            if (Image1 != null)
            {



                ArticleVendorsFromDb.Image1 = ProcessUploadingWaterMark(Image1);
                //string us = fs.UploadUserFile(Image1);
                //ViewBag.ResultMessage = fs.getseterror;
            }

            if (Video1 != null)
            {
                ArticleVendorsFromDb.Video1 = ProcessUploading(Video1);
                //string us = fs.UploadUserFile(Video1);
                //ViewBag.ResultMessage = fs.getseterror;
            }

            if (Image2 != null)
            {
                ArticleVendorsFromDb.Image2 = ProcessUploadingWaterMark(Image2);
            }

            if (Image3 != null)
            {
                ArticleVendorsFromDb.Image3 = ProcessUploadingWaterMark(Image3);
            }

            if (ArticleFile != null)
            {
                ArticleVendorsFromDb.ArticleFile = ProcessUploading(ArticleFile);
            }
            if (EN_ArticleFile != null)
            {
                ArticleVendorsFromDb.EN_ArticleFile = ProcessUploading(EN_ArticleFile);
            }

            ArticleVendorsFromDb.BrandId = _db.Brands.Where(p => p.BrandName.Equals(ArticleVendors.Brand))
                                    .Select(p => p.Id).FirstOrDefault();

            ArticleVendorsFromDb.EmployeeId = _db.Employees.Where(p => p.Emaillogin.Equals(ArticleVendorsFromDb.SaleManagerEmail)).Where(p => p.Firstname.Equals(ArticleVendorsFromDb.SaleManagerFirstname))
                             .Select(p => p.Id).FirstOrDefault();


            // For article countries
            var selectedCountries = Request.Form["country"];

            string sql = "delete from ArticleCountries where articleId= " + Article.Id;
            int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);

            foreach (var prop in selectedCountries)
            {
                sql = "insert into  ArticleCountries (ArticleId, CountryId,	UpdateByUserId,LastUpdate) values( " + Article.Id + "," + prop + ", " + _ConnectedUserId + ",'" + DateTime.Now + "')";
                noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
            }

            // For article variantes
            var selectedVariantes = Request.Form["variante"];

            sql = "delete from ArticleVariantes where articleId= " + Article.Id;
            noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);

            foreach (var prop in selectedVariantes)
            {
                sql = "insert into  ArticleVariantes (ArticleId, VarianteArticleId,	UpdateByUserId,LastUpdate) values( " + Article.Id + "," + prop + ", " + _ConnectedUserId + ",'" + DateTime.Now + "')";
                noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
            }


            //For articles properties
            var selectedProperties = Request.Form["properties"];

            sql = "delete from ArticleProperty where articleId= " + Article.Id;
            noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);



            foreach (var prop in selectedProperties)
            {
                PropertyValue MyPropertyValue = await _db.PropertyValues.FindAsync(int.Parse(prop));
                sql = "insert into  ArticleProperty (ArticleId, PropertyValueId,ArticlePropertyLabel,ArticlePropertyValueLabel,	UpdateByUserId,LastUpdate) values( " + Article.Id + "," + prop + ", '" + MyPropertyValue.PropertyName + "', '" + MyPropertyValue.PropertyValueName + "', " + _ConnectedUserId + ",'" + DateTime.Now + "')";
                noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
            }
            if (NewArticleVendorsFromDb == true)
            {
                ArticleVendorsFromDb.LastUpdate = DateTime.Now;
                await _db.ArticleVendors.AddAsync(ArticleVendorsFromDb);
            }
            //Sauveragde des articles
            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
            //  }
            //return RedirectToPage("Edit", new { id = ArticleVendors.Id });
        }
     

        private string ProcessUploadingWaterMark(IFormFile ImageToLoad)
        {

            string uniqueFileName = null;

            if (ImageToLoad != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, _ImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + ImageToLoad.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                ImageToLoad.CopyTo(fileStream);
                fileStream.Dispose();
                //Whatermark NOT WORKING
                int percent;
                Image resultat;
                string FullPathLogo = Path.GetFullPath(@"../marketplace/wwwroot/images/logo_pharmashopcenter_COULEURS_S.jpg");
                Bitmap watermark2 = new Bitmap(FullPathLogo);
                try
                {
                   string FullPath = Path.GetFullPath(@"../marketplace/wwwroot/" + _ImageRepository + uniqueFileName);
                    Bitmap bitmap = new Bitmap(FullPath);

               
                    
                    //Image watermark2 = Image.FromStream(new MemoryStream(System.IO.File.ReadAllBytes(FullPathLogo)));

                    percent = Convert.ToInt32(((bitmap.Width * bitmap.Height) / (2160.0 * 2160.0)) * 100);

                    if (percent == 0) percent = 100;

                    if (percent > 40 && percent <= 50) percent = 50;
                    if (percent > 30 && percent <= 40) percent = 40;
                    if (percent > 20 && percent <= 30) percent = 30;
                    if (percent > 10 && percent <= 20) percent = 20;
                    if (percent > 0 && percent <= 10) percent = 15;
         
                    resultat = ScaleByPercent(watermark2, percent);

                    bitmap = WatermarkImage(bitmap, new Bitmap(resultat), percent);
                    bitmap.Save(@"../marketplace/wwwroot/"+ _ImageRepository  +Article.EAN+"_"+ uniqueFileName);
                }


                catch (Exception exception) {
                    Console.WriteLine(exception);

                }

            }
            return _ImageRepository + Article.EAN + "_" + uniqueFileName ;
        }


        public string UploadUserFile(IFormFile Image)
        {
            try
            {
                // supported extensions
                // you can add any of extension,if you want pdf file validation then add .pdf in 
                // variable supportedTypes.

                var supportedTypes = new[] { "jpg", "jpeg", "png" };

                // following will fetch the extension of posted file.

                var fileExt = System.IO.Path.GetExtension(Image.FileName).Substring(1);

                // Image datatype is included in System.Drawing librery.will get the image properties 
                //  like height, width.



                //variable will get the ratio of image 
                // (600 x 400),ratio will be 1.5 



                if (Image.Length > (1000 * 1024))
                {
                    getseterror = "filesize will be upto " + 1000 + "KB";
                    return getseterror;
                }
                else if (!supportedTypes.Contains(fileExt))
                {
                    getseterror = "file extension is not valid";
                    return getseterror;
                }

                {
                    getseterror = "success";
                    return getseterror;
                }
            }
            catch (Exception ex)
            {
                getseterror = ex.Message;
                return getseterror;
            }


        }
        public static Bitmap WatermarkImage(Bitmap image, Bitmap watermark, int percent)
        {
            try
            {
                using (Graphics imageGraphics = Graphics.FromImage(image))
                {
                    watermark.SetResolution(imageGraphics.DpiX, imageGraphics.DpiY);

                    int x = (image.Width - watermark.Width) - 30 * percent / 100;
                    int y = (image.Height - watermark.Height) - 30 * percent / 100;

                    imageGraphics.DrawImage(watermark, x, y, watermark.Width, watermark.Height);
                }
            }
            catch { }

            return image;
        }

        private static Image ScaleByPercent(Bitmap imgPhoto, int Percent)
        {
            float nPercent = ((float)Percent / 100);
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);
            Bitmap bmPhoto = new Bitmap(destWidth, destHeight,
                                     PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                    imgPhoto.VerticalResolution);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBilinear;
            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);
            grPhoto.Dispose();
            return bmPhoto;
        }
        private string ProcessUploading(IFormFile Image)
        {

            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
               Path.Combine(webHostEnvironment.WebRootPath, _ImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                Image.CopyTo(fileStream);
            }
            return _ImageRepository + uniqueFileName;
        }

    }
}
