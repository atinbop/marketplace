﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MarketPlace.Pages.ArticleList
{
    public class IndexModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _db;
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
 
        public static String _UserCompanyName;
        public int UserAutorizedToAccessPage { get; set; }
 
        private readonly ILogger _logger; 
        private readonly IHttpContextAccessor _httpContextAccessor;
         
        public List<Authorizations> Authorizations { get; set; }

        [BindProperty]
        public  string _UserCompanyType { get; set; }

        [BindProperty]
        public string _UserCompanyId { get; set; }
        public IndexModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options, ILogger<AboutModel> logger, IHttpContextAccessor httpContextAccessor)
        {
            _db = db;
            _ImageRepository = options.ImageRepository;
            _logger = logger; 
               _httpContextAccessor = httpContextAccessor;

    }
        public IEnumerable<Article> Articles { get; set; }

       public async Task OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
            _ConnectedUserId = Request.Cookies["ConnectedUserId"];

            _UserCompanyType = Request.Cookies["UserCompanyType"];
            _UserCompanyId = Request.Cookies["UserCompanyId"];
            _UserCompanyName = Request.Cookies["UserCompanyName"];
          


            /* string g = _httpContextAccessor.HttpContext.Request.GetDisplayUrl();*/
            string h = _httpContextAccessor.HttpContext.Request.GetEncodedUrl();
         

              Authorizations = await (from art in _db.Authorizations
                                        where art.AppPage == _httpContextAccessor.HttpContext.Request.GetDisplayUrl() && art.flag_autorized == 1
                                        && art.userRole == HttpContext.Request.Cookies["UserCompanyType"]
            select art).ToListAsync();


           

            //Articles = await _db.Article.ToListAsync(); 
           
            if (_UserCompanyType == "B2B" || _UserCompanyType == "B2C")
            {
                try
                {

                    Articles = _db.Article.FromSqlRaw<Article>("spGetAutorizedArticlesB2B_B2C {0} , {1}", _UserCompanyType, _UserCompanyId).ToList();
                }
                catch (Exception ex)
                {
                    string t = ex.Message;
                    t = "";

                }
            }


            if (_UserCompanyType == "PRO")
            {
                try
                {
                    Articles = _db.Article.FromSqlRaw<Article>("spGetAutorizedArticlesPRO {0} , {1}", _UserCompanyType, _UserCompanyId).ToList();
                }
                catch (Exception ex) {
                    string t = ex.Message;

                }
            }


            //Listes des Posts Non lus
            try
            {
                List<Parameters> UnreadMessages = _db.Parameters.FromSqlRaw<Parameters>("spGetUnreadMessagesNumber {0}", _ConnectedUserId).ToList();

                foreach (Article prop in Articles)
                {
                    foreach (Parameters prop1 in UnreadMessages)
                    {
                        if (prop.Id == prop1.Id)
                        { prop.UnreadMessagesNumber = prop1.Value; }
                    }
                }
            }
            catch (Exception ex)
            {
                string t = ex.Message;
            }
            }

        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var article = await _db.Article.FindAsync(id);
            if (article == null)
            {
                return NotFound();
            }
            //   _db.Article.Remove(article);
            int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update article set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "'  where id ="+ id);
            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
        }

        

    }
}