﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.ArticleList
{
    public class IndexModelold : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _db;
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
 
        public static String _UserCompanyName;

        public const string SessionKeyConnectedUserEmail = "_ConnectedUserEmail";
        public const string SessionKeyUserCompanyType = "_UserCompanyType";

        [BindProperty]
        public  string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }
        public IndexModelold(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId =    options.ConnectedUserId;
            _UserCompanyType =    options.UserCompanyType; //or B2C
            _UserCompanyId =      options.UserCompanyId;
            _UserCompanyName =    options.UserCompanyName;
            _ImageRepository =    options.ImageRepository;

        
        }
        public IEnumerable<Article> Articles { get; set; }

       public async Task OnGet()
        {
            //Articles = await _db.Article.ToListAsync(); 
           // if (HttpContext.Session.GetString(SessionKeyConnectedUserEmail )!= null){ 

            if (_UserCompanyType == "B2B" || _UserCompanyType == "B2C")
            {
                try
                {

                    Articles = _db.Article.FromSqlRaw<Article>("spGetAutorizedArticlesB2B_B2C {0} , {1}", _UserCompanyType, _UserCompanyId).ToList();
                }
                catch (Exception ex)
                {
                    string t = ex.Message;
                    t = "";

                }
            }


            if (_UserCompanyType == "PRO")
            {
                try
                {
                    Articles = _db.Article.FromSqlRaw<Article>("spGetAutorizedArticlesPRO {0} , {1}", _UserCompanyType, _UserCompanyId).ToList();
                }
                catch (Exception ex) {
                    string t = ex.Message;

                }
            }


            //Listes des Posts Non lus
            try
            {
                List<Parameters> UnreadMessages = _db.Parameters.FromSqlRaw<Parameters>("spGetUnreadMessagesNumber {0}", _ConnectedUserId).ToList();

                foreach (Article prop in Articles)
                {
                    foreach (Parameters prop1 in UnreadMessages)
                    {
                        if (prop.Id == prop1.Id)
                        { prop.UnreadMessagesNumber = prop1.Value; }
                    }
                }
            }
            catch (Exception ex)
            {
                string t = ex.Message;
            }
         /*   }
            else
            {
                RedirectToPage("Login/Login");
            }*/

        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var article = await _db.Article.FindAsync(id);
            if (article == null)
            {
                return NotFound();
            }
            //   _db.Article.Remove(article);
            int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update article set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "'  where id ="+ id);
            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
        }


      

    }
}