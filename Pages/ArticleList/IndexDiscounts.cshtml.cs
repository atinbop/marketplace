using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace MarketPlace.Pages.ArticleList
{
    public class IndexDiscountsModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
  

      


        [BindProperty]
        public Article Article { get; set; }

        public IndexDiscountsModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment)
        {
          
            _db = db;
         
        }

        public async Task OnGet(int ArticleId)
        {

            Article = await(_db.Article.FindAsync(ArticleId));
        }

      

      

    }
}
