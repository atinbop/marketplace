﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MarketPlace.Data;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.PreOrderDiscountList
{
    public class CreateModel : PageModel
    {
        public IEnumerable<StaticData> StaticData;

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }
        public List<SelectListItem> StaticDataPreOrderDelay { get; set; } = new List<SelectListItem>();

        private readonly MarketPlace.Data.ApplicationDbContext _context;

        [BindProperty]
        public Article Article { get; set; }

        [BindProperty]
        public string startDate { get; set; }
        [BindProperty]
        public string period { get; set; }

        [BindProperty]
        public string rate { get; set; }

        [BindProperty]
        public string price { get; set; }



        [BindProperty]
        public string endDate { get; set; }
        public CreateModel(MarketPlace.Data.ApplicationDbContext context , IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;

            startDate = (DateTime.Now).ToString("yyyy-MM-dd"); 

            endDate = (DateTime.Now).AddMonths(1).ToString("yyyy-MM-dd");
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public async Task<IActionResult> OnGetAsync(int articleId)
        {
        
        
            Article = await _context.Article.FindAsync(articleId);


            StaticData = await (from art in _context.StaticData
                                select art).ToListAsync();

        
            SelectListItem selListItem = new SelectListItem() { Value = "null", Text = "Selectionnez" };

            StaticDataPreOrderDelay.Add(selListItem);
         

            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "PREORDERDELAY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataPreOrderDelay.Add(item);
                }
            }
                return Page();
        }

        [BindProperty]
        public PreOrderDiscounts Discounts { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            /*  if (!ModelState.IsValid)
              {
                  return Page();
              }*/

            StaticData = await (from art in _context.StaticData
                                select art).ToListAsync();


            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "PREORDERDELAY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataPreOrderDelay.Add(item);
                }
            }


       /*     foreach (var prop in StaticDataPreOrderDelay)
            {
                if (prop.Value == Discounts.Period)
                {
                    Discounts.Period = prop.Text;
                }
            }*/


            CultureInfo[] cultures = { new CultureInfo("en-US"),
                                 new CultureInfo("fr-FR") };

            Discounts.UserCompanyType = _UserCompanyType;
            Discounts.UpdateByUserId = _ConnectedUserId;
            Discounts.LastUpdate = DateTime.Now;
            Discounts.ContactId = _UserCompanyId;

            Discounts.ArticleId = Article.Id;
            Discounts.BasePrice =  Article.Price;
            foreach (CultureInfo culture in cultures)
            {
                
                Discounts.DiscountRate = Math.Round(Convert.ToDecimal(Discounts.DiscountRate, culture), 2).ToString();
                Discounts.DiscountPrice = Math.Round(Convert.ToDecimal(Discounts.DiscountPrice, culture), 2).ToString();
            }
            decimal value;
            if (Discounts.DiscountStartDate < Discounts.DiscountEndDate &&  Decimal.TryParse(Discounts.DiscountRate, out value) &&   Decimal.TryParse(Discounts.DiscountPrice, out value))
            {
                int noOfRowUpdated = _context.Database.ExecuteSqlRaw("Update PreOrderDiscounts set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "' where Period='" + Discounts.Period + "' and ContactId=" + _UserCompanyId + " and ArticleId = " + Article.Id);

                _context.PreOrderDiscounts.Add(Discounts);
                await _context.SaveChangesAsync();
                return RedirectToPage("Index", new { articleId = Article.Id });
            }
           
            return RedirectToPage("Create", new { articleId = Article.Id });
        }
    }
}
