﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.PreOrderDiscountList
{
    public class IndexModel : PageModel
    {
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public IndexModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public IList<PreOrderDiscounts> Discounts { get; set; }

        [BindProperty]
        public Article Article { get; set; }

        public async Task OnGetAsync(int articleId)
        {
            Article = await _context.Article.FindAsync(articleId);
        }

        [BindProperty]
        public DataTables.DataTablesRequest DataTablesRequest { get; set; }

        public async Task<JsonResult> OnPostAsync(int articleId)
        {
            var recordsTotal = _context.PreOrderDiscounts.Count();
            int tt = articleId;
            /*  var customersQuery = from Customers in Customers
                          where Customers.ArticleId.AsQueryable().All(m => m = idArticle)
                          select Customers;*/

            var discountsQuery = _context.PreOrderDiscounts.AsQueryable().Where(s => s.ArticleId == articleId).Where(s => s.Deleted != 1).Where(s => s.UserCompanyType == _UserCompanyType).Where(s => s.ContactId == _UserCompanyId);

            //var customersQuery = _context.Customers.FindAsync(idArticle);

            var searchText = DataTablesRequest.Search.Value?.ToUpper();
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                discountsQuery = discountsQuery.Where(s =>
                   s.Id.Equals(searchText) ||
                    s.DiscountRate.Equals(searchText) ||
                    s.DiscountPrice.Equals(searchText) ||
                    s.Period.Equals(searchText) ||
                    s.DiscountStartDate.Equals(searchText) ||
                    s.DiscountEndDate.Equals(searchText)
                );
            }

            var recordsFiltered = discountsQuery.Count();

            var sortColumnName = DataTablesRequest.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;

            var sortDirection = DataTablesRequest.Order.ElementAt(0).Dir.ToLower();

            // using System.Linq.Dynamic.Core
            // customersQuery = customersQuery.OrderBy($"{sortColumnName} {sortDirection}");

            var skip = DataTablesRequest.Start;
            var take = DataTablesRequest.Length;
            var data = await discountsQuery
                .Skip(skip)
                .Take(take)
                .ToListAsync();

            var t= new JsonResult(new
            {
                Draw = DataTablesRequest.Draw,
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsFiltered,
                Data = data
            });
            return t;
        }
    }
}
