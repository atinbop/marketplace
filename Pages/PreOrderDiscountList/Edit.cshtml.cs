﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.PreOrderDiscountList
{
    public class EditModel : PageModel
    {

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }
        [BindProperty]
        public string startDate { get; set; }
        [BindProperty]
        public string endDate { get; set; }

        [BindProperty]
        public string period { get; set; }
        [BindProperty]
        public Article Article { get; set; }


        public IEnumerable<StaticData> StaticData;
 
        public List<SelectListItem> StaticDataPreOrderDelay { get; set; } = new List<SelectListItem>();


        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public EditModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        [BindProperty]
        public PreOrderDiscounts Discount { get; set; }

        public async Task<IActionResult> OnGetAsync(int id, int articleId)
        {
            Article = await _context.Article.FindAsync(articleId);

            StaticData = await (from art in _context.StaticData
                                select art).ToListAsync();


            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "PREORDERDELAY")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataPreOrderDelay.Add(item);

                }
            }


                if (id == 0)
            {
                return NotFound();
            }

            Discount = await _context.PreOrderDiscounts.FirstOrDefaultAsync(m => m.Id == id);
            startDate = (Discount.DiscountStartDate).ToString("yyyy-MM-dd");
            period = (Discount.Period).ToString("yyyy-MM-dd");

            endDate = (Discount.DiscountEndDate).AddMonths(1).ToString("yyyy-MM-dd");
            /* CultureInfo[] cultures = { new CultureInfo("fr-FR"),
                                   };


             foreach (CultureInfo culture in cultures)
             {

                 Discount.DiscountRate = Convert.ToDecimal(Discount.DiscountRate, culture).ToString();
                 Discount.DiscountPrice = Convert.ToDecimal(Discount.DiscountPrice, culture).ToString();
             }*/

            Discount.DiscountRate = Discount.DiscountRate.Replace(",", ".");
            Discount.DiscountPrice = Discount.DiscountPrice.Replace(",", ".");

            if (Discount == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {

            Discount.UserCompanyType = _UserCompanyType;
            /* if (!ModelState.IsValid)
             {
                 return Page();
             }*/
            decimal value;
            CultureInfo[] cultures = { new CultureInfo("en-US"),
                                 new CultureInfo("fr-FR") };

            StaticData = await (from art in _context.StaticData
                                select art).ToListAsync();


            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "Civility")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataPreOrderDelay.Add(item);
                }
            }


            /*foreach (var prop in StaticDataPreOrderDelay)
            {
                if (prop.Value == Discount.Period)
                {
                    Discount.Period = prop.Text;
                }
            }*/



            foreach (CultureInfo culture in cultures)
            {

                Discount.DiscountRate = Math.Round(Convert.ToDecimal(Discount.DiscountRate, culture), 2).ToString();
                Discount.DiscountPrice = Math.Round(Convert.ToDecimal(Discount.DiscountPrice, culture), 2).ToString();
            }

            if (Discount.DiscountStartDate < Discount.DiscountEndDate && Decimal.TryParse(Discount.DiscountRate, out value) && Decimal.TryParse(Discount.DiscountPrice, out value))
            {

                _context.Attach(Discount).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(Discount.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToPage("Index", new {  articleId = Discount.ArticleId });
            }

            else {
                return RedirectToPage("Edit", new { id = Discount.Id, articleId = Discount.ArticleId });
            }
        }

        private bool CustomerExists(int id)
        {
            return _context.Discounts.Any(e => e.Id == id);
        }
    }
}
