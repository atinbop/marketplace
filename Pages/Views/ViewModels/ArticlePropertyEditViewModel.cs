﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MarketPlace.Pages.ViewModels
{
    public class ArticlePropertyEditViewModel
    {
        [Display(Name = "Identifiant de la propriété")]
        public string ArticlePropertyId { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(75)]
        public string ArticlePropertyName { get; set; }

        [Required]
        [Display(Name = "Nom de la propriété")]
        public int SelectedPropertyId { get; set; }
        public IEnumerable<SelectListItem> Properties { get; set; }

        [Required]
        [Display(Name = "Valeur de la propriété")]
        public int SelectedPropertyValueId { get; set; }
        public IEnumerable<SelectListItem> PropertyValues { get; set; }

        public string SelectedArticlePropertyLabel { get; set; }
        public string SelectedArticlePropertyValueLabel { get; set; }
    }
}