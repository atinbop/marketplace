﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MarketPlace.Pages.ViewModels
{
    public class ArticlePropertyDisplayViewModel
    {
        [Display(Name = "Idenfiant de la propriété")]
        public Guid ArticlePropertyId { get; set; }

        [Display(Name = "Description")]
        public string ArticlePropertyName { get; set; }

        [Display(Name = "Nom de la propriété")]
        public string PropertyName { get; set; }

        [Display(Name = "Valeur de la propriété")]
        public string PropertyValueName { get; set; }
        public string SelectedArticlePropertyLabel { get; set; }
        public string SelectedArticlePropertyValueLabel { get; set; }

    }
}