using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MarketPlace.Data;
using MarketPlace.Pages.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.EntityFrameworkCore;


namespace MarketPlace.Pages.Views.ArticleProperties
{
    public class IndexModel : PageModel
    {

        private readonly ApplicationDbContext _context;
        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;

        [BindProperty(SupportsGet = true)]
        public List<ArticlePropertyDisplayViewModel> ArticlePropertiesDisplayList { get; set; }

        public IndexModel(ApplicationDbContext context, MDUOptions options)
        {
            _context = context;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;
        }



        public IActionResult OnGet(int articleId)
        {

            ArticleProperties = (from art in _context.ArticleProperty
                                     /*join p in _context.Properties  on art.PropertyId equals p.PropertyId
                                          join v in _context.PropertyValues on art.PropertyId equals v.PropertyId*/
                                 where art.ArticleId == articleId                                   
                                 select art).ToList();

            Article = _context.Article.Find(articleId);


            //   _db.ArticleProperty.Select(m => new { m.ArticleParam, m.ArticleValue }).Distinct();

            DistinctListOfProperties = _context.ArticleProperty.Where(m => m.ArticleId == articleId).Select(m => m.ArticlePropertyLabel).Distinct();

            var repo = new ArticlePropertiesRepository(_context);
            ArticlePropertiesDisplayList = repo.GetArticleProperties();
            return Page();
        }
        public IEnumerable<ArticleProperty> ArticleProperties { get; set; }
        public Article Article { get; set; }
        public IEnumerable<String> DistinctListOfProperties { get; set; }
 


        public async Task<IActionResult> OnPostDelete(int id, int articleId)
        {
            var articleProperty = await _context.ArticleProperty.FindAsync(id);
            if (articleProperty == null)
            {
                return NotFound();
            }
            int noOfRowUpdated = _context.Database.ExecuteSqlRaw("delete from ArticleProperty where  id = " + id + "and articleId=" + articleId);

            await _context.SaveChangesAsync();

            return RedirectToPage("Index", new { ArticleId = articleId });
        }
    }
}