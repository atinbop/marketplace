using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MarketPlace.Data;
using MarketPlace.Pages.ViewModels;
using MarketPlace.Model;
using Microsoft.AspNetCore.Routing;
using System.Linq;

namespace MarketPlace.Pages.Views.ArticleProperties
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        [BindProperty(SupportsGet = true)]
        public ArticlePropertyEditViewModel ArticlePropertyEditViewModel { get; set; }

        [BindProperty]
        public Article Article { get; set; }
        public CreateModel(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult OnGet(int articleId)
        {
            Article = _context.Article.Find(articleId);

            var repo = new ArticlePropertiesRepository(_context);
            ArticlePropertyEditViewModel = repo.CreateArticleProperty(articleId);
            return Page();
        }

        public IActionResult OnPost()
        {



            ArticlePropertyEditViewModel.SelectedArticlePropertyLabel = (from art in _context.Properties
                                                                         where art.Id == ArticlePropertyEditViewModel.SelectedPropertyId
                                                                         orderby art.Id descending
                                                                         select art).First().PropertyName;


            ArticlePropertyEditViewModel.SelectedArticlePropertyValueLabel = (from art in _context.PropertyValues
                                                                              where art.Id == ArticlePropertyEditViewModel.SelectedPropertyValueId
                                                                              orderby art.Id descending
                                                                              select art).First().PropertyValueName;

            //  bool saved = repo.SaveArticleProperty(ArticlePropertyEditViewModel, Article.Id);

            int id = Article.Id;
            var repo = new ArticlePropertiesRepository(_context);
            bool saved = repo.SaveArticleProperty(ArticlePropertyEditViewModel, Article.Id);

            return RedirectToPage("Index", new { ArticleId = Article.Id });

            /*
                  try
                  {
                     if (ModelState.IsValid)
                     {
                        var repo = new ArticlePropertiesRepository(_context);
                        bool saved = repo.SaveArticleProperty(ArticlePropertyEditViewModel, Article.Id);
                        if (saved)
                        {
                           return RedirectToAction("Index");
                        }
                     }
                     // Handling model state errors is beyond the scope of the demo, so just throwing an ApplicationException when the ModelState is invalid
                     // and rethrowing it in the catch block.
                     throw new ApplicationException("Invalid model");
                  }
                  catch (ApplicationException ex)
                  {
                     Debug.Write(ex.Message);
                     throw ex;
                  }*/
        }

        public IActionResult OnPostPropertyValues()
        {
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyToAsync(stream);
            stream.Position = 0;
            using StreamReader reader = new StreamReader(stream);
            string requestBody = reader.ReadToEnd();
            if (requestBody.Length > 0)
            {
                var repo = new PropertyValuesRepository(_context);

                IEnumerable<SelectListItem> propertyValues = repo.GetPropertyValues(requestBody);
                return new JsonResult(propertyValues);
            }
            return null;
        }
    }
}