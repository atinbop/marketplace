﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.DiscountList
{
    public class CreateModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        [BindProperty]
        public Article Article { get; set; }

        [BindProperty]
        public string startDate { get; set; }

        [BindProperty]
        public string rate { get; set; }

        [BindProperty]
        public string price { get; set; }

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;

        [BindProperty]
        public string ErrorMessage   { get; set; }

       [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }

        [BindProperty]
        public string endDate { get; set; }
        public CreateModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;

            startDate = (DateTime.Now).ToString("yyyy-MM-dd"); 

            endDate = (DateTime.Now).AddMonths(1).ToString("yyyy-MM-dd");
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public async Task<IActionResult> OnGetAsync(int articleId)
        {
        
        
            Article = await _context.Article.FindAsync(articleId);

            //A ajouter sur les autres code de discount
            Article.Price = (decimal)_context.ArticleVendors.Where(p => p.articleId == articleId).Where(p => p.contactId == _UserCompanyId)
                                       .Select(p => p.Price).ToList().FirstOrDefault();

            return Page();
        }

        [BindProperty]
        public Discounts Discounts { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            /*  if (!ModelState.IsValid)
              {
                  return Page();
              }*/

            CultureInfo[] cultures = { new CultureInfo("en-US"),
                                 new CultureInfo("fr-FR") };
            Discounts.UserCompanyType = _UserCompanyType;
            Discounts.UpdateByUserId = _ConnectedUserId;
            Discounts.ContactId = _UserCompanyId;
            Discounts.LastUpdate = DateTime.Now;
            Discounts.Type = "FREE";
            Discounts.ArticleId = Article.Id;
            Discounts.BasePrice =  Article.Price;
            //Nouveau à repliquer sur les autres classe discount
            Discounts.articleVendorsId = _context.ArticleVendors.Where(p => p.articleId == Article.Id).Where(p => p.contactId == _UserCompanyId)
                                        .Select(p => p.Id).ToList().FirstOrDefault();

            foreach (CultureInfo culture in cultures)
            {
                
                Discounts.DiscountRate1 = Math.Round(Convert.ToDecimal(Discounts.DiscountRate1, culture), 2).ToString();
                Discounts.DiscountPrice1 = Math.Round(Convert.ToDecimal(Discounts.DiscountPrice1, culture), 2).ToString();

                Discounts.DiscountRate2 = Math.Round(Convert.ToDecimal(Discounts.DiscountRate2, culture), 2).ToString();
                Discounts.DiscountPrice2 = Math.Round(Convert.ToDecimal(Discounts.DiscountPrice2, culture), 2).ToString();


                Discounts.DiscountRate3 = Math.Round(Convert.ToDecimal(Discounts.DiscountRate3, culture), 2).ToString();
                Discounts.DiscountPrice3 = Math.Round(Convert.ToDecimal(Discounts.DiscountPrice3, culture), 2).ToString();

            }
            decimal value;
            if (Discounts.DiscountStartDate < Discounts.DiscountEndDate 
                &&  Decimal.TryParse(Discounts.DiscountRate1, out value) &&   
                Decimal.TryParse(Discounts.DiscountPrice1, out value)

                                && Decimal.TryParse(Discounts.DiscountRate2, out value) &&
                Decimal.TryParse(Discounts.DiscountPrice2, out value)

                                && Decimal.TryParse(Discounts.DiscountRate2, out value) &&
                Decimal.TryParse(Discounts.DiscountPrice2, out value)&&
                Decimal.Parse(Discounts.DiscountRate1) < Decimal.Parse(Discounts.DiscountRate2) && Decimal.Parse(Discounts.DiscountRate2) < Decimal.Parse(Discounts.DiscountRate3) &&
                Discounts.Quantity1< Discounts.Quantity2 && Discounts.Quantity2 < Discounts.Quantity3
                )
            {

                int noOfRowUpdated =   (from o in _context.Discounts  where o.Type == "FREE" && o.ContactId == _UserCompanyId && o.ArticleId == Article.Id select o).Count();

                if (noOfRowUpdated == 0) {
                    _context.Discounts.Add(Discounts);
                    await _context.SaveChangesAsync();
                    return RedirectToPage("Index", new { articleId = Article.Id });
                }
                else
                {
                    ErrorMessage = "Ce produit dispose déjà d'un programme de remise";
                }
               
            }


            ErrorMessage = "Erreur de saisie - Vérifier les valeurs saisies";
            return RedirectToPage("Create", new { articleId = Article.Id });
        }
    }
}
