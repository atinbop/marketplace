using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.Market
{
    public class ChartModel : PageModel
    {
        public List<Chart> ChartList { get; set; }
        public List<Article> Articles { get; set; }
        public List<Contacts> Contact { get; set; }
  

        private readonly Data.ApplicationDbContext _db;
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;

        public ChartModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
    
        }
        public async Task OnGet()
        {
            Articles = _db.Article.FromSqlRaw<Article>("spGetSharingBuyingArticles").ToList();

            ChartList = _db.Chart.FromSqlRaw<Chart>("spGetChartProductEmployee {0} ", _ConnectedUserId).ToList();


            foreach (Chart prop in ChartList)
            {

                prop.TotalAmount = prop.quantity * prop.price;

            }

         


        }
        public async Task<IActionResult> OnPost( int Id)
        {
            int i = 0;

            //var result = _db.Chart.FromSqlRaw<Chart>("spRemoveItemFromChart {0} ", Id).AsEnumerable().First();
            var result = _db.Chart.FromSqlRaw<Chart>("spRemoveItemFromChart {0} ", Id).AsEnumerable().FirstOrDefault();

            return RedirectToPage("Chart");

        }
    }
}
