using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.Market
{

  

    public class ArticleDetailsModel : PageModel
    {
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        [BindProperty]
        public string _ConnectedUserId { get; set; }

        public static string _UserCompanyType;
        public static string _UserCompanyId;
        public static String _UserCompanyName;
        public static String _UserCurrency;
        public static String _UserLanguage;
        public int sellerNote { get; set; }
        public   Brands Brand { get; set; }

        public Property Properties { get; set; }

        [BindProperty]
        public Chart Chart { get; set; }

        public Contacts Contacts { get; set; }
        public List<Discounts> ParamNextDiscountRateTarget   { get; set; }

        public List<Chart> ChartList { get; set; }
        [BindProperty]
        public Currency UserCurrencyObj { get; set; }
        public List<PropertyValue>  PropertyValues { get; set; }

        [BindProperty]

        public int FlagFavorites { get; set; }

        public int  FlagChart     { get; set; }

    public IEnumerable<ArticleProperty>  ArticleProperties { get; set; }

        public String _ConnectedUserPseudo;
        public ArticleVendors ArticleVendors { get; set; }
        public IEnumerable<Article> ArticleSameBrand { get; set; }
        public IEnumerable<ArticleVendors> ArticleSameVendor { get; set; }
        public IEnumerable<FavoriteProducts> FavoriteProducts { get; set; }
        


        [BindProperty]


        public IEnumerable<Posts> Posts { get; set; }

        [BindProperty]
        public IEnumerable<ConnectedUser> ConnectedUsers { get; set; }
        public class ConnectedUser
        {
            public int EmployeeId { get; set; }
            public DateTime LoginDate { get; set; }
            public string Pseudo { get; set; }
            public string Picture { get; set; }
        }

        private readonly MarketPlace.Data.ApplicationDbContext _context;


   

        private readonly IWebHostEnvironment webHostEnvironment;
        public ArticleDetailsModel(Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            this.webHostEnvironment = webHostEnvironment;

    
            _ImageRepository = options.ImageRepository;
 
        }

 
  
        public string processDate(DateTime d)
        {
            string result = "";
            if ((DateTime.Now - d).TotalHours < 1) result = "Il y'a " + Math.Round((DateTime.Now - d).TotalMinutes, 0).ToString() + " Minutes";

            else if ((DateTime.Now - d).Days < 1) result = "Il y'a " + Math.Round((DateTime.Now - d).TotalHours, 0).ToString() + " Heures";
            else if ((DateTime.Now - d).Days > 1 && (DateTime.Now - d).Days < 2) result = "Hier";
            else if ((DateTime.Now - d).Days > 2 && (DateTime.Now - d).Days < 30) result = "Il y'a " + (DateTime.Now.Date - d).Days.ToString() + " Jours";

            else result = d.ToString("MMM.dd,yyyy HH:mm:ss");

            return result;
        }
        public async Task OnGet(int articleVendorsId, string postContent)
        {

            if (User.Identity.IsAuthenticated)
            {
                _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
                _ConnectedUserId = Request.Cookies["ConnectedUserId"];
                _UserCompanyType = Request.Cookies["UserCompanyType"];
                _UserCompanyId = Request.Cookies["UserCompanyId"];
                _UserCompanyName = Request.Cookies["UserCompanyName"];
                _UserCurrency = Request.Cookies["UserCurrency"];
                _UserLanguage = Request.Cookies["PreferedLanguage"];

                Currency SupplierCurrency = null;

                List<ExchangeRate> ExchangeRates = null;
                List<Discounts> ParamNextDiscountRateTargetTmp = null;

                int articleid = (int)_context.ArticleVendors.Where(p => p.Id == articleVendorsId)
                                            .Select(p => p.articleId).ToList().FirstOrDefault();


                try
                {


                    ArticleVendors = _context.ArticleVendors.FromSqlRaw<ArticleVendors>("spGetSharingBuyingArticlesDetails {0}, {1}", articleVendorsId, Request.Cookies["ConnectedUserId"]).AsEnumerable().First();

                    UserCurrencyObj = (from art in _context.Currency
                                       where art.AlphabeticCode == _UserCurrency
                                       select art).FirstOrDefault();



                    Contacts = (from art in _context.Contacts
                                where art.Id == ArticleVendors.contactId
                                select art).FirstOrDefault();

                    SupplierCurrency = (from art in _context.Currency
                                        where art.AlphabeticCode == Contacts.Currency
                                        select art).FirstOrDefault();



                    ExchangeRates = _context.ExchangeRate.FromSqlRaw<ExchangeRate>("spGetExchangerate {0}, {1}", SupplierCurrency.AlphabeticCode, UserCurrencyObj.AlphabeticCode).ToList();

                    try
                    {
                        sellerNote = Convert.ToInt32(await _context.VendorsNotes.Where(x => x.ContactId == Contacts.Id).AverageAsync(x => x.Note));
                    }
                    catch (Exception e)
                    {
                        string t = e.Message;
                    }


                    string uploadsFolder =
                     Path.Combine(webHostEnvironment.WebRootPath, _ImageRepository);
                    string filePath = Path.Combine(uploadsFolder, ArticleVendors.Image2).Replace("/", "\\");
                    try
                    {
                        var fileStream = new FileStream(filePath, FileMode.Open);
                    }
                    catch (Exception e)
                    {
                        ArticleVendors.Image2 = ArticleVendors.Image1;
                    }


                    filePath = Path.Combine(uploadsFolder, ArticleVendors.Image3).Replace("/", "\\");
                    try
                    {
                        var fileStream = new FileStream(filePath, FileMode.Open);
                    }
                    catch (Exception e)
                    {
                        ArticleVendors.Image3 = ArticleVendors.Image1;
                    }




                    ParamNextDiscountRateTargetTmp = _context.Discounts.FromSqlRaw<Discounts>("spGetActiveDiscountRateDetails {0}", articleVendorsId).ToList();
                    ParamNextDiscountRateTarget = new List<Discounts>();




                    foreach (Discounts pro in ParamNextDiscountRateTargetTmp)
                    {
                        Discounts dis = new Discounts();
                        dis.Id = pro.Id;
                        dis.ArticleId = pro.ArticleId;
                        dis.DiscountStartDate = pro.DiscountStartDate;
                        dis.DiscountEndDate = pro.DiscountEndDate;
                        dis.Deleted = pro.Deleted;
                        dis.UpdateByUserId = pro.UpdateByUserId;
                        dis.Type = pro.Type;
                        dis.LastUpdate = pro.LastUpdate;
                        dis.UserCompanyType = pro.UserCompanyType;
                        dis.ContactId = pro.ContactId;

                        dis.DiscountRate1 = pro.DiscountRate1;
                        dis.Quantity1 = pro.Quantity1;

                        dis.DiscountRate2 = pro.DiscountRate2;
                        dis.Quantity2 = pro.Quantity2;

                        dis.DiscountRate3 = pro.DiscountRate3;
                        dis.Quantity3 = pro.Quantity3;
                        dis.currentorderQuantity = pro.currentorderQuantity;
                        dis.active = pro.active;
                        dis.articleVendorsId = articleVendorsId;
                        dis.flagDiscount = pro.flagDiscount;

                        dis.BasePrice = Math.Round((decimal)(pro.BasePrice * ExchangeRates.First().Rate), 2);
                        dis.DiscountPrice1 = Math.Round((decimal)((Decimal.Parse(pro.DiscountPrice1) * ExchangeRates.First().Rate)), 2).ToString();
                        dis.DiscountPrice2 = Math.Round((decimal)((Decimal.Parse(pro.DiscountPrice2) * ExchangeRates.First().Rate)), 2).ToString();
                        dis.DiscountPrice3 = Math.Round((decimal)((Decimal.Parse(pro.DiscountPrice3) * ExchangeRates.First().Rate)), 2).ToString();

                        ParamNextDiscountRateTarget.Add(dis);
                    }



                    FavoriteProducts = _context.FavoriteProducts.FromSqlRaw<FavoriteProducts>("spGetFavoriteProduct {0} , {1}", articleid, Request.Cookies["ConnectedUserId"]).ToList();


                    FlagFavorites = FavoriteProducts.Count();

                    ChartList = _context.Chart.FromSqlRaw<Chart>("spGetChartProductEmployee {0} ", Request.Cookies["ConnectedUserId"]).ToList();


                    FlagChart = ChartList.Count();




                }
                catch (Exception e)
                {

                    string t = e.Message;
                }

                try
                {

                    ArticleProperties = (from z in _context.ArticleProperty
                                         where z.ArticleId == ArticleVendors.articleId && z.PropertyLogo != null
                                         select z).ToList().Take(3);
                }
                catch (Exception e)
                {

                    string t = e.Message;
                }


                Brand = (from z in _context.Brands
                         where z.Id == ArticleVendors.BrandId
                         select z).FirstOrDefault();

                ArticleSameBrand = (from z in _context.Article
                                    where z.BrandId == Brand.Id
                                    select z).Take(3);
                //A faire
                ArticleSameVendor = (from z in _context.ArticleVendors
                                     where z.contactId == ArticleVendors.contactId
                                     select z).Take(3);

                ConnectedUsers = (from z in _context.LogConnexions
                                  where z.LogoutDate == null
                                  join f in _context.Employees on z.EmployeeId equals f.Id
                                  select new { f.Pseudo, z.EmployeeId, z.LoginDate, f.Picture } into x
                                  group x by new { x.Pseudo, x.EmployeeId, x.Picture } into g
                                  select new ConnectedUser
                                  {
                                      Picture = g.Key.Picture,
                                      Pseudo = g.Key.Pseudo,
                                      EmployeeId = g.Key.EmployeeId,
                                      LoginDate = (DateTime)g.Max(i => i.LoginDate)
                                  }).OrderByDescending(g => g.LoginDate).Take(8);


                if (postContent != null)
                {
                    Posts ThePost = new Posts();

                    ThePost.ArticleId = articleid;
                    ThePost.PostContent = postContent;
                    ThePost.UpdateByUserId = Request.Cookies["ConnectedUserId"];
                    ThePost.LastUpdate = DateTime.Now;
                    ThePost.PartnerEmployeeId = -1; //Group Post
                    ThePost.UpdateByPseudo = _ConnectedUserPseudo;
                    await _context.Posts.AddAsync(ThePost);
                    await _context.SaveChangesAsync();

                    //Mise � jour du nombre de post de l'utilisateur
                    //var article = await _db.Article.FindAsync(id);
                    //article.Note = Convert.ToInt32(await _db.Notes.Where(x => x.ArticleId == id).AverageAsync(x => x.Note));
                    //await _db.SaveChangesAsync();


                }

                Posts = await (from art in _context.Posts
                               where art.ArticleId == articleid && art.Deleted != 1 && art.PartnerEmployeeId == -1
                               select art).ToListAsync();

                foreach (Posts prop in Posts)
                {
                    prop.slotLastUpdate = processDate((DateTime)prop.LastUpdate);

                }

                //Log de la connexion du user au chat
                int noOfRowUpdated = _context.Database.ExecuteSqlRaw("insert into [dbo].[LogConnexions]([EmployeeId], [LoginDate], [logSystem], [ViewedItem]) values (" + Request.Cookies["ConnectedUserId"] + ", getdate(), 'CHAT'," + articleid + ")");
                await _context.SaveChangesAsync();
            }


        }

        public async Task<IActionResult> OnPostAsync()
        {

            
            Chart.employeeId = Int32.Parse(_ConnectedUserId);
            Chart.chartDate = DateTime.Now;
            Chart.LastUpdate = DateTime.Now;
            Chart.chartstate = "StandBy";



            await _context.Chart.AddAsync(Chart);
            await _context.SaveChangesAsync();


            return RedirectToPage("ArticleDetails" , new { articleVendorsId = Chart.ArticleVendorsId });
        }

    }
}
