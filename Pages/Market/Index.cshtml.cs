using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Pages.Market
{
  
    public class IndexModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;

        private readonly IConfiguration configuration;
        private static string _imageRepository;
        public List<Chart> ChartList { get; set; }
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
    
        public static String _UserCompanyName;

        public IEnumerable<Article> Articles { get; set; }
        public IEnumerable<Translations> Translations { get; set; }

        public static String _ConnectedUserPseudo;

        public string _UserLanguage { get; set; }
        public Article Article { get; set; }
        [BindProperty]
        public Chart Chart { get; set; }


        [BindProperty]
        public Prospects Prospect { get; set; }

        [BindProperty]

        public IEnumerable<Brands> Brands { get; set; }

        public IndexModel(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;
            _ConnectedUserPseudo = options.ConnectedUserPseudo;
            _UserLanguage = options.UserLanguage;


        }
        public void OnGet()
        {

            Translations = (from art in _db.Translations
                            where art.language == Request.Cookies["ConnectedUserId"]
                            select art).ToList();
        }


    }
}
