using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MarketPlace.Pages.Tarif
{
    public class SubscriptionB2CModel : PageModel
    {

        [BindProperty]
        public Subscriptions Subscription { get; set; }

        [BindProperty]
        public Offers Offer { get; set; }
        public Contacts Contact { get; set; }
        public Employees Employee { get; set; }

        private readonly Data.ApplicationDbContext _db;
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        [BindProperty]
        public  string _UserCompanyType { get; set; }

        public static int _UserCompanyId;
        public static String _UserCompanyName;




        private readonly IWebHostEnvironment webHostEnvironment;
        public SubscriptionB2CModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;

        }

        public void OnGet()
        {
        }


    }
}