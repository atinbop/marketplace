using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Pages.FavoritesVendor { 
    public class IndexModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        public IEnumerable<Article> Articles { get; set; }
        public IndexModel(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _imageRepository = options.ImageRepository;

        }

        public async Task<IActionResult> OnGet(int contactId)
        {
            try
            {

                // _db.SqlQuery<FavoriteProducts>("spAddFavoriteProduct {0} , {1}", articleId, _ConnectedUserId);

                

                var createdPath = _db.FavoriteProducts.FromSqlRaw("spAddFavoriteVendor  {0}, {1}", contactId, _ConnectedUserId).ToList();

                int i = 0;

            }
            catch (Exception ex)
            {
                string t = ex.Message;
               

            }

            return RedirectToPage("/Contact/Fiche", new { contactId = contactId });
            
        }
    }
}

