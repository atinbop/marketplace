﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MarketPlace.Pages.PayoutList
{
    public class EditModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IWebHostEnvironment webHostEnvironment;

        public EditModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;
        }

       

        [BindProperty]
        public IFormFile PaymentDocument { get; set; }

        

        [BindProperty]
        public Payout Payout { get; set; }

        public async Task OnGet(int id)
        {
            Payout = await _db.Payout.FindAsync(id);
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var PayoutFromDb = await _db.Payout.FindAsync(Payout.Id);
                PayoutFromDb.TransactionNumber = Payout.TransactionNumber;
              
                PayoutFromDb.PaymentDate = Payout.PaymentDate;
                PayoutFromDb.Amount = Payout.Amount;
                PayoutFromDb.PaymentStatus = Payout.PaymentStatus;
                PayoutFromDb.PaymentComment = Payout.PaymentComment;
                PayoutFromDb.OrderCode = Payout.OrderCode;
                PayoutFromDb.SellerId = Payout.SellerId;



                if (PaymentDocument != null)
                {
                    PayoutFromDb.PaymentDocument = ProcessUploading(PaymentDocument);
                }



                await _db.SaveChangesAsync();

                return RedirectToPage("Index");
            }
            return RedirectToPage();
        }

        private string ProcessUploading(IFormFile Image)
        {
        
            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    Image.CopyTo(fileStream);
                }
            }
            return "https://epharmapp.com/pharmashopcenter/produits/" + uniqueFileName;
        }

    }
}