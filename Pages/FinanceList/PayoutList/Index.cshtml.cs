﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.PayoutList
{
    public class IndexModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _db;

        public IndexModel(MarketPlace.Data.ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Payout> Payouts { get; set; }

        public async Task OnGet()
        {
            Payouts = await (from art in _db.Payout where art.Deleted != 1 select art).ToListAsync();
        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var payout = await _db.Payout.FindAsync(id);
            if (payout == null)
            {
                return NotFound();
            }
            //_db.Payout.Remove(payout);
            int noOfRowUpdated = _db.Database.ExecuteSqlRaw("Update payout set deleted=1 where id = id");

            await _db.SaveChangesAsync();

            return RedirectToPage("Index");
        }
    }
}