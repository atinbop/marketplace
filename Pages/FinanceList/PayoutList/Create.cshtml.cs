﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MarketPlace.Pages.PayoutList
{
    public class CreateModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;

        public CreateModel(Data.ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Payout Payout { get; set; }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                await _db.Payout.AddAsync(Payout);
                await _db.SaveChangesAsync();
                return RedirectToPage("Index");
            }
            else
            {
                return Page();
            }
        }
    }
}