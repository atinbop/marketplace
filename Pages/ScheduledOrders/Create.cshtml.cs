﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MarketPlace.Data;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.ScheduledOrders
{
    public class CreateModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        [BindProperty]
        public Article Article { get; set; }

        [BindProperty]
        public string startDate { get; set; }

        [BindProperty]
        public string rate { get; set; }

        [BindProperty]
        public string price { get; set; }

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }

        [BindProperty]
        public string endDate { get; set; }
        public CreateModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;

            startDate = (DateTime.Now).ToString("yyyy-MM-dd"); 

            endDate = (DateTime.Now).AddMonths(1).ToString("yyyy-MM-dd");
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public async Task<IActionResult> OnGetAsync(int articleId)
        {
        
        
            Article = await _context.Article.FindAsync(articleId);
          
            return Page();
        }

        [BindProperty]
        public Discounts Discounts { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            /*  if (!ModelState.IsValid)
              {
                  return Page();
              }*/

            CultureInfo[] cultures = { new CultureInfo("en-US"),
                                 new CultureInfo("fr-FR") };
            Discounts.UserCompanyType = _UserCompanyType;
            Discounts.UpdateByUserId = _ConnectedUserId;
            Discounts.ContactId = _UserCompanyId;
            Discounts.LastUpdate = DateTime.Now;
            Discounts.Type = "SCHEDULED";
            Discounts.ArticleId = Article.Id;
            Discounts.BasePrice =  Article.Price;
            foreach (CultureInfo culture in cultures)
            {
                
                Discounts.DiscountRate1 = Math.Round(Convert.ToDecimal(Discounts.DiscountRate1, culture), 2).ToString();
                Discounts.DiscountPrice1 = Math.Round(Convert.ToDecimal(Discounts.DiscountPrice1, culture), 2).ToString();
            }
            decimal value;
            if (Discounts.DiscountStartDate < Discounts.DiscountEndDate &&  Decimal.TryParse(Discounts.DiscountRate1, out value) &&   Decimal.TryParse(Discounts.DiscountPrice1, out value))
            {
                int noOfRowUpdated = _context.Database.ExecuteSqlRaw("Update Discounts set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "' where type='SCHEDULED' and DiscountRate='" + Discounts.DiscountRate1 + "' and ContactId=" + _UserCompanyId + " and ArticleId = " + Article.Id);

                _context.Discounts.Add(Discounts);
                await _context.SaveChangesAsync();
                return RedirectToPage("Index", new { articleId = Article.Id });
            }
           
            return RedirectToPage("Create", new { articleId = Article.Id });
        }
    }
}
