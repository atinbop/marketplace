﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.ScheduledOrders
{
    public class EditModel : PageModel
    {

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }

        [BindProperty]
        public Article Article { get; set; }

        [BindProperty]
        public string startDate { get; set; }
        [BindProperty]
        public string endDate { get; set; }


        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public EditModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        
    }

        [BindProperty]
        public Discounts Discount { get; set; }

        public async Task<IActionResult> OnGetAsync(int id, int articleId)
        {
            Article = await _context.Article.FindAsync(articleId);

         


            if (id == 0)
            {
                return NotFound();
            }
            

            Discount = await (from art in _context.Discounts  where art.Type == "FREE" && art.Id == id
            select art).FirstOrDefaultAsync();

            startDate = (Discount.DiscountStartDate).ToString("yyyy-MM-dd");

            endDate = (Discount.DiscountEndDate).AddMonths(1).ToString("yyyy-MM-dd");

            /* CultureInfo[] cultures = { new CultureInfo("fr-FR"),
                                   };


             foreach (CultureInfo culture in cultures)
             {

                 Discount.DiscountRate = Convert.ToDecimal(Discount.DiscountRate, culture).ToString();
                 Discount.DiscountPrice = Convert.ToDecimal(Discount.DiscountPrice, culture).ToString();
             }*/

            Discount.DiscountRate1 = Discount.DiscountRate1.Replace(",", ".");
            Discount.DiscountPrice1 = Discount.DiscountPrice1.Replace(",", ".");

            if (Discount == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {


            /* if (!ModelState.IsValid)
             {
                 return Page();
             }*/
            Discount.UserCompanyType = _UserCompanyType;
            Discount.Type = "SCHEDULED";
            decimal value;
            CultureInfo[] cultures = { new CultureInfo("en-US"),
                                 new CultureInfo("fr-FR") };

      
            foreach (CultureInfo culture in cultures)
            {

                Discount.DiscountRate1 = Math.Round(Convert.ToDecimal(Discount.DiscountRate1, culture), 2).ToString();
                Discount.DiscountPrice1 = Math.Round(Convert.ToDecimal(Discount.DiscountPrice1, culture), 2).ToString();
            }

            if (Discount.DiscountStartDate < Discount.DiscountEndDate && Decimal.TryParse(Discount.DiscountRate1, out value) && Decimal.TryParse(Discount.DiscountPrice1, out value))
            {

                _context.Attach(Discount).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(Discount.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToPage("Index", new {  articleId = Discount.ArticleId });
            }

            else {
                return RedirectToPage("Edit", new { id = Discount.Id, articleId = Discount.ArticleId });
            }
        }

        private bool CustomerExists(int id)
        {
            return _context.Discounts.Any(e => e.Id == id);
        }
    }
}
