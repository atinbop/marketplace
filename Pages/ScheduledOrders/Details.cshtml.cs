﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;

namespace MarketPlace.Pages.ScheduledOrders
{
    public class DetailsModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public DetailsModel(MarketPlace.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Discounts Discount { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Discount = await _context.Discounts.FirstOrDefaultAsync(m => m.Id == id);

            if (Discount == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
