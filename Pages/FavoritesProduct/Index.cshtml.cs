using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MarketPlace.Pages.FavoritesProduct
{
    public class IndexModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private static string _imageRepository;

        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static string _UserCompanyId;
        public static String _UserCompanyName;
        public static string _UserCurrency;
        public static String _UserLanguage;
        public IEnumerable<Article> Articles { get; set; }
        public IndexModel(Data.ApplicationDbContext db, IConfiguration iConfig, MDUOptions options)
        {
            _db = db;

            configuration = iConfig;
 
            _imageRepository = options.ImageRepository;

        }

        public async Task<IActionResult> OnGet(int articleId ,int articleVendorsId)
        {
            _ConnectedUserEmail = Request.Cookies["ConnectedUserEmail"];
            _ConnectedUserId = Request.Cookies["ConnectedUserId"];
            _UserCompanyType = Request.Cookies["UserCompanyType"];
            _UserCompanyId = Request.Cookies["UserCompanyId"];
            _UserCompanyName = Request.Cookies["UserCompanyName"];
            _UserCurrency = Request.Cookies["UserCurrency"];
            _UserLanguage = Request.Cookies["PreferedLanguage"];

            try
            {

                // _db.SqlQuery<FavoriteProducts>("spAddFavoriteProduct {0} , {1}", articleId, _ConnectedUserId);

                

                var createdPath = _db.FavoriteProducts.FromSqlRaw("spAddFavoriteProduct  {0}, {1}", articleId, _ConnectedUserId).ToList();

                int i = 0;

            }
            catch (Exception ex)
            {
                string t = ex.Message;
               

            }

            return RedirectToPage("/Market/ArticleDetails", new { articleVendorsId = articleVendorsId });
            
        }
    }
}

