﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using MarketPlace.Model.SaveMultiSelectDropDown;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Query.Internal;


namespace MarketPlace.Pages.BrandList
{
    public class CreateModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        [BindProperty]
        public Contacts Contact { get; set; }
        public IFormFile BrandLogo { get; set; }
        [BindProperty]

        public IFormFile ImageBanderole1 { get; set; }
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly string _BrandImageRepository;
        private readonly string _ImageRepository;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;



        public CreateModel(Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
            _BrandImageRepository = options.BrandImageRepository;
        }

        public async Task<IActionResult> OnGetAsync(int contactId)
        {


            Contact = await _context.Contacts.FindAsync(contactId);

            return Page();
        }

        [BindProperty]
        public Brands Brands { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {

            if (BrandLogo != null)
            {
                Brands.BrandLogo = ProcessUploading(BrandLogo);
            }

            if (ImageBanderole1 != null)
            {
                Brands.ImageBanderole1 = ProcessUploading(ImageBanderole1);
            }



            /*  if (!ModelState.IsValid)
              {
                  return Page();
              }*/
            Brands.ContactId = Contact.Id;


            _context.Brands.Add(Brands);
            await _context.SaveChangesAsync();

            return RedirectToPage("Index", new { contactId = Contact.Id });
        }
        private string ProcessUploading(IFormFile Image)
        {

            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, _BrandImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                Image.CopyTo(fileStream);
            }
            return _ImageRepository + uniqueFileName;
        }
    }
}