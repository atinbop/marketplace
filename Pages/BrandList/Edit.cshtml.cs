﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.BrandList
{
    public class EditModel : PageModel
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly string _BrandImageRepository;
        private readonly string _ImageRepository;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;

        [BindProperty]
        public Contacts Contact { get; set; }

        [BindProperty]
        public IFormFile BrandLogo { get; set; }
        [BindProperty]
        public IFormFile ImageBanderole1 { get; set; }

        private readonly MarketPlace.Data.ApplicationDbContext _db;

        public EditModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
            _BrandImageRepository = options.BrandImageRepository;
        }

        [BindProperty]
        public Brands Brand { get; set; }

        public async Task<IActionResult> OnGetAsync(int id, int contactId)
        {
            Contact = await _db.Contacts.FindAsync(contactId);
            if (id==0)
            {
                return NotFound();
            }

            Brand = await _db.Brands.FirstOrDefaultAsync(m => m.Id == id);

            if (Brand == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {
            if (BrandLogo != null)
            {
                Brand.BrandLogo = ProcessUploading(BrandLogo);
            }

            if (ImageBanderole1 != null)
            {
                Brand.ImageBanderole1 = ProcessUploading(ImageBanderole1);
            }

            

            /* if (!ModelState.IsValid)
             {
                 return Page();
             }*/

            _db.Attach(Brand).State = EntityState.Modified;

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(Brand.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("Index", new { contactId = Brand.ContactId });
        }

        private bool CustomerExists(int id)
        {
            return _db.Brands.Any(e => e.Id == id);
        }
        private string ProcessUploading(IFormFile Image)
        {

            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, _BrandImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                Image.CopyTo(fileStream);
            }
            return _ImageRepository + uniqueFileName;
        }
    }
}
