﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;

namespace MarketPlace.Pages.BrandList
{
    public class IndexModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public IndexModel(MarketPlace.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Brands> Brands { get; set; }

        [BindProperty]
        public Contacts Contact { get; set; }

        public async Task OnGetAsync(int contactId)
        {
            Contact = await _context.Contacts.FindAsync(contactId);
        }

        [BindProperty]
        public DataTables.DataTablesRequest DataTablesRequest { get; set; }

        public async Task<JsonResult> OnPostAsync(int contactId)
        {
            var recordsTotal = _context.Brands.Count();
            int tt = contactId;
            /*  var customersQuery = from Customers in Customers
                          where Customers.ContactId.AsQueryable().All(m => m = idContact)
                          select Customers;*/

            var brandsQuery = _context.Brands.AsQueryable().Where(s => s.ContactId == contactId).Where(s => s.Deleted != 1);

            //var customersQuery = _context.Customers.FindAsync(idContact);

            var searchText = DataTablesRequest.Search.Value?.ToUpper();
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                brandsQuery = brandsQuery.Where(s =>
                   s.Id.Equals(searchText) ||
                    s.BrandLogo.Equals(searchText) ||
                    s.Title.Equals(searchText) ||
                    s.BrandShortDescription.Equals(searchText) ||
                    s.BrandLinkPage.Equals(searchText) ||
                    s.ImageBanderole1.Equals(searchText)
                );
            }

            var recordsFiltered = brandsQuery.Count();

           // var sortColumnName = DataTablesRequest.Columns.ElementAt(DataTablesRequest.Order.ElementAt(0).Column).Name;

            var sortDirection = DataTablesRequest.Order.ElementAt(0).Dir.ToLower();

            // using System.Linq.Dynamic.Core
            // customersQuery = customersQuery.OrderBy($"{sortColumnName} {sortDirection}");

            var skip = DataTablesRequest.Start;
            var take = DataTablesRequest.Length;
            var data = await brandsQuery
                .Skip(skip)
                .Take(take)
                .ToListAsync();

            var t = new JsonResult(new
            {
                Draw = DataTablesRequest.Draw,
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsFiltered,
                Data = data
            });
            return t;
        }
    }
}
