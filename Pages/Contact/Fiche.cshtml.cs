using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.Market
{ 
    public class FicheModel : PageModel
    {
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        [BindProperty]
        public string _ConnectedUserId { get; set; }

        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
        public static String _UserCurrency;

        public int sellerNote { get; set; }
        public Brands Brand { get; set; }

        public Property Properties { get; set; }

        [BindProperty]
        public Chart Chart { get; set; }

        public Contacts Contacts { get; set; }
        public List<Discounts> ParamNextDiscountRateTarget { get; set; }

        public List<Chart> ChartList { get; set; }
        [BindProperty]
        public Currency UserCurrencyObj { get; set; }
        public List<PropertyValue> PropertyValues { get; set; }

        [BindProperty]

        public int FlagFavorites { get; set; }

        public int FlagChart { get; set; }

        public IEnumerable<ArticleProperty> ArticleProperties { get; set; }

        public String _ConnectedUserPseudo;
        public ArticleVendors ArticleVendors { get; set; }
        public IEnumerable<Article> ArticleSameBrand { get; set; }
        public IEnumerable<ArticleVendors> ArticleSameVendor { get; set; }
        public IEnumerable<FavoriteVendors> FavoriteVendors { get; set; }



        [BindProperty]


        public IEnumerable<Posts> Posts { get; set; }

        [BindProperty]
        public IEnumerable<ConnectedUser> ConnectedUsers { get; set; }
        public class ConnectedUser
        {
            public int EmployeeId { get; set; }
            public DateTime LoginDate { get; set; }
            public string Pseudo { get; set; }
            public string Picture { get; set; }
        }

        private readonly MarketPlace.Data.ApplicationDbContext _context;




        private readonly IWebHostEnvironment webHostEnvironment;
        public FicheModel(Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
            _UserCurrency = options.UserCurrency;
        }


 
        public async Task OnGet(int articleVendorsId)
        {


            Currency SupplierCurrency = null;

           
            int articleid = (int)_context.ArticleVendors.Where(p => p.Id == articleVendorsId)
                                        .Select(p => p.articleId).ToList().FirstOrDefault();


            try
            {


                ArticleVendors = _context.ArticleVendors.FromSqlRaw<ArticleVendors>("spGetSharingBuyingArticlesDetails {0}", articleVendorsId).AsEnumerable().First();

                UserCurrencyObj = (from art in _context.Currency
                                   where art.AlphabeticCode == _UserCurrency
                                   select art).FirstOrDefault();



                Contacts = (from art in _context.Contacts
                            where art.Id == ArticleVendors.contactId
                            select art).FirstOrDefault();

                SupplierCurrency = (from art in _context.Currency
                                    where art.AlphabeticCode == Contacts.Currency
                                    select art).FirstOrDefault();



               

                try
                {
                    sellerNote = Convert.ToInt32(await _context.VendorsNotes.Where(x => x.ContactId == Contacts.Id).AverageAsync(x => x.Note));
                }
                catch (Exception e)
                {
                    string t = e.Message;
                }
                                


                FavoriteVendors = _context.FavoriteVendors.FromSqlRaw<FavoriteVendors>("spAddFavoriteVendor {0} , {1}", Contacts.Id, _ConnectedUserId).ToList();


                FlagFavorites = FavoriteVendors.Count();

                ChartList = _context.Chart.FromSqlRaw<Chart>("spGetChartProductEmployee {0} ", _ConnectedUserId).ToList();


                FlagChart = ChartList.Count();




            }
            catch (Exception e)
            {

                string t = e.Message;
            }



            Brand = (from z in _context.Brands
                     where z.Id == ArticleVendors.BrandId
                     select z).FirstOrDefault();


            //A faire
            ArticleSameVendor = (from z in _context.ArticleVendors
                                 where z.contactId == ArticleVendors.contactId
                                 select z).Take(3);

           


        }

      

    }
}
