﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Drawing;

namespace MarketPlace.Pages.Contact
{
    public class EditModel : PageModel
    {
        private readonly Data.ApplicationDbContext _db;
        private readonly IWebHostEnvironment webHostEnvironment;

        private string _ImageRepository;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;

        public IEnumerable<StaticData> StaticData;
        public IEnumerable<Country> Countries;
        public List<SelectListItem> StaticDataGenders { get; set; } = new List<SelectListItem>();


    


        public List<SelectListItem> StaticDataContactType { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataOuiNon { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataTypeProduit { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> StaticDataCountry { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataMapColor { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> StaticDataSaleArea { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> listSaleArea { get; set; } = new List<SelectListItem>();
        
        public string getseterror { get; set; }

        public string tt;

        public EditModel(Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _db = db;
            this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }


        [BindProperty]
        public Contacts Contact { get; set; }

        public async Task OnGet(int contactId)
        {
            StaticData = await (from art in _db.StaticData
                                select art).ToListAsync();

            Countries = await (from art in _db.Countries
                               select art).ToListAsync();


            Contact = await _db.Contacts.FindAsync(contactId);



            SelectListItem selListItem = new SelectListItem() { Value = "null", Text = "Selectionnez" };

            StaticDataGenders.Add(selListItem);

            StaticDataContactType.Add(selListItem);
            StaticDataOuiNon.Add(selListItem);
            StaticDataTypeProduit.Add(selListItem);
            StaticDataCountry.Add(selListItem);

            foreach (var prop in Countries)
            {
                SelectListItem item = new SelectListItem(prop.nom_fr_fr, prop.Id.ToString());
                StaticDataCountry.Add(item);
            }


            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "Civility")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataGenders.Add(item);
                }

                if (val == "CONTACT_TYPE")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataContactType.Add(item);
                }

            

                if (val == "YESNO")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataOuiNon.Add(item);
                }

                if (val == "TYPEPRODUIT")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataTypeProduit.Add(item);
                }

                if (val == "MAPCOLOR")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataMapColor.Add(item);
                }

            }

            List<String> listSaleArea = (from art in _db.EmployeeSaleArea
                                         join em in _db.Employees on art.EmployeeId equals em.Id
                                         join co in _db.Contacts on em.ContactId equals co.Id
                                         join sa in _db.SharingBuyingArea on art.SaleAreaId equals sa.id
                                         join ct in _db.Countries on sa.country equals ct.nom_fr_fr
                                         where co.Id == contactId && em.Deleted != 1
                                         select ct.alpha2).Distinct().ToList() ;

                foreach ( var prop1 in listSaleArea)
                {
                tt = tt + "\""+ prop1 + "\"" + ":" + "\'#5478ab\',";

               }
                //Lorsque le module commercial sera en place elaborer une map sur les pays ou il y a le plus de vente avec 3 couleurs du top 1 au top 3 pays
          //  tt = "\"IN\"" + ":" + "\'#5478ab\'" + "," + "\"FR\"" + ":" + "\'#88b7d6\'" + "," + "\"PL\"" + ":" + "\'#5478ab\'";
                            





        }
        public async Task<IActionResult> OnPost()
        {

            StaticData = await (from art in _db.StaticData
                                select art).ToListAsync();

            Countries = await (from art in _db.Countries
                               select art).ToListAsync();

            foreach (var prop in StaticData)
            {

                string val = (string)prop.Type;
                if (val == "Civility")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataGenders.Add(item);
                }

                if (val == "CONTACT_TYPE")
                {
                    SelectListItem item = new SelectListItem(prop.description, prop.Id.ToString());
                    StaticDataContactType.Add(item);
                }
            }

            foreach (var prop in Countries)
            {
                SelectListItem item = new SelectListItem(prop.nom_fr_fr, prop.Id.ToString());
                StaticDataCountry.Add(item);
            }

            var ContactFromDb = await _db.Contacts.FindAsync(Contact.Id);

            ContactFromDb.Name = Contact.Name;

            

            ContactFromDb.ContactCode = Contact.ContactCode;
            ContactFromDb.ContactType = Contact.ContactType;
            ContactFromDb.Company = Contact.Company;

 

         

            foreach (var prop in StaticDataGenders)
            {
                if (prop.Value == Contact.Gender) { 
                 
                    ContactFromDb.Gender = prop.Text;
                }
            }

            ContactFromDb.Name = Contact.Name;
            ContactFromDb.FirstName = Contact.FirstName;
            ContactFromDb.address1 = Contact.address1;
            ContactFromDb.Address2 = Contact.Address2;
            ContactFromDb.Address3 = Contact.Address3;
            ContactFromDb.PostalCode = Contact.PostalCode;
            ContactFromDb.City = Contact.City;
            ContactFromDb.Country = Contact.Country;
            ContactFromDb.EAN = Contact.EAN;
            ContactFromDb.Siren = Contact.Siren;
            ContactFromDb.SIRET = Contact.SIRET;
            ContactFromDb.APE = Contact.APE;
            ContactFromDb.ActivitySector = Contact.ActivitySector;
            ContactFromDb.DateOfBirth = Contact.DateOfBirth;
            ContactFromDb.Category = Contact.Category;
            ContactFromDb.Phone = Contact.Phone;
            ContactFromDb.Mobile = Contact.Mobile;
            ContactFromDb.Fax = Contact.Fax;
            ContactFromDb.Email = Contact.Email;
            ContactFromDb.Website = Contact.Website;
            ContactFromDb.Origin = Contact.Origin;
            ContactFromDb.Language = Contact.Language;
            ContactFromDb.BlockedAccount = Contact.BlockedAccount;
            ContactFromDb.BlockedPurchase = Contact.BlockedPurchase;
            ContactFromDb.Alert = Contact.Alert;
            ContactFromDb.ExcludeLoyalty = Contact.ExcludeLoyalty;
            ContactFromDb.Password = Contact.Password;
            ContactFromDb.PrivateSpace = Contact.PrivateSpace;
            ContactFromDb.AuthorizedAccess = Contact.AuthorizedAccess;
            ContactFromDb.NewsletterSubscriber = Contact.NewsletterSubscriber;
            ContactFromDb.PaymentChoice = Contact.PaymentChoice;
            ContactFromDb.PriceDisplay = Contact.PriceDisplay;
            ContactFromDb.PriceCategory = Contact.PriceCategory;
            ContactFromDb.AccountingCode = Contact.AccountingCode;
            ContactFromDb.IntraCommunityVAT = Contact.IntraCommunityVAT;
            ContactFromDb.Credit = Contact.Credit;
            ContactFromDb.Risk = Contact.Risk;
            ContactFromDb.MonthlyBill = Contact.MonthlyBill;
            ContactFromDb.StartOfMonthlyPayment = Contact.StartOfMonthlyPayment;
            ContactFromDb.ClientAccount = Contact.ClientAccount;
            ContactFromDb.FeedbackContact = Contact.FeedbackContact;
            ContactFromDb.InternalCommentary = Contact.InternalCommentary;
            ContactFromDb.Commercial = Contact.Commercial;
            ContactFromDb.Treatment = Contact.Treatment;
            ContactFromDb.archiving = Contact.archiving;
            ContactFromDb.VATExemption = Contact.VATExemption;
            ContactFromDb.PaidOrdersNotInvoiced = Contact.PaidOrdersNotInvoiced;
            ContactFromDb.ReceivablesClient = Contact.ReceivablesClient;
            ContactFromDb.CompanyLogoFile = Contact.CompanyLogoFile;
            ContactFromDb.BankAccountNumberIBAN = Contact.BankAccountNumberIBAN;
            ContactFromDb.ReceivablesClient = Contact.ReceivablesClient;
            ContactFromDb.BankAccountNumberRIB = Contact.BankAccountNumberRIB;

            await _db.SaveChangesAsync();
 
            return RedirectToPage("/ArticleList/Index");
      }

        private string ProcessUploading(IFormFile Image)
        {
            
            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                Image.CopyTo(fileStream);
            }
            return _ImageRepository + uniqueFileName;
        }


            public string UploadUserFile(IFormFile Image)
            {
                try
                {
                    // supported extensions
                    // you can add any of extension,if you want pdf file validation then add .pdf in 
                    // variable supportedTypes.

                    var supportedTypes = new[] { "jpg", "jpeg", "png" };

                    // following will fetch the extension of posted file.

                    var fileExt = System.IO.Path.GetExtension(Image.FileName).Substring(1);

                    // Image datatype is included in System.Drawing librery.will get the image properties 
                    //  like height, width.

                    

                    //variable will get the ratio of image 
                    // (600 x 400),ratio will be 1.5 

                    

                    if (Image.Length > (1000 * 1024))
                    {
                        getseterror = "filesize will be upto " + 1000 + "KB";
                        return getseterror;
                    }
                    else if (!supportedTypes.Contains(fileExt))
                    {
                        getseterror = "file extension is not valid";
                        return getseterror;
                    }
                   
                    {
                        getseterror = "success";
                        return getseterror;
                    }
                }
                catch (Exception ex)
                {
                    getseterror = ex.Message;
                    return getseterror;
                }
            }        }
    }
