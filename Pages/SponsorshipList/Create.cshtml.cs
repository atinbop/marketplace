﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MarketPlace.Data;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Pages.SponsorshipList
{
    public class CreateModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        [BindProperty]
        public Article Article { get; set; }

        [BindProperty]
        public string startDate { get; set; }

        [BindProperty]
        public string rate { get; set; }

        [BindProperty]
        public string price { get; set; }

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }

        [BindProperty]
        public string endDate { get; set; }
        public CreateModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;

            startDate = (DateTime.Now).ToString("yyyy-MM-dd"); 

            endDate = (DateTime.Now).AddMonths(1).ToString("yyyy-MM-dd");
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        public async Task<IActionResult> OnGetAsync(int articleId)
        {
        
        
            Article = await _context.Article.FindAsync(articleId);
          
            return Page();
        }

        [BindProperty]
        public Sponsorships Sponsorships { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            /*  if (!ModelState.IsValid)
              {
                  return Page();
              }*/

            CultureInfo[] cultures = { new CultureInfo("en-US"),
                                 new CultureInfo("fr-FR") };
            Sponsorships.UserCompanyType = _UserCompanyType;
            Sponsorships.UpdateByUserId = _ConnectedUserId;
            Sponsorships.ContactId = _UserCompanyId;
            Sponsorships.LastUpdate = DateTime.Now;

            Sponsorships.ArticleId = Article.Id;
      
            foreach (CultureInfo culture in cultures)
            {
                
               // Sponsorships.GodfatherAmount = Math.Round(Convert.ToDecimal(Sponsorships.GodfatherAmount, culture), 2).ToString();
                //Sponsorships.GodsonpAmount = Math.Round(Convert.ToDecimal(Sponsorships.GodsonpAmount, culture), 2).ToString();
            }
            decimal value;
            if (Sponsorships.SponsorshipstartDate < Sponsorships.SponsorshipsEndDate /*&&  Decimal.TryParse(Sponsorships.GodfatherAmount, out value) &&   Decimal.TryParse(Sponsorships.GodfatherAmount, out value)*/)
            {
                int noOfRowUpdated = _context.Database.ExecuteSqlRaw("Update Sponsorships set Deleted=1 , LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "' where ContactId="+ _UserCompanyId + " and ArticleId = " + Article.Id );

                _context.Sponsorships.Add(Sponsorships);
                await _context.SaveChangesAsync();
                return RedirectToPage("Index", new { articleId = Article.Id });
            }
           
            return RedirectToPage("Create", new { articleId = Article.Id });
        }
    }
}
