﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;

namespace MarketPlace.Pages.SponsorshipList
{
    public class DetailsModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public DetailsModel(MarketPlace.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Discounts Sponsorship { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Sponsorship = await _context.Discounts.FirstOrDefaultAsync(m => m.Id == id);

            if (Sponsorship == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
