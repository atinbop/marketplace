﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using System.Globalization;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.SponsorshipList
{
    public class EditModel : PageModel
    {

        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;

        public static String _UserCompanyName;


        [BindProperty]
        public string _UserCompanyType { get; set; }

        [BindProperty]
        public int _UserCompanyId { get; set; }

        [BindProperty]
        public Article Article { get; set; }

        [BindProperty]
        public string startDate { get; set; }
        [BindProperty]
        public string endDate { get; set; }


        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public EditModel(MarketPlace.Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        
    }

        [BindProperty]
        public Sponsorships Sponsorship { get; set; }

        public async Task<IActionResult> OnGetAsync(int id, int articleId)
        {
            Article = await _context.Article.FindAsync(articleId);

         


            if (id == 0)
            {
                return NotFound();
            }

            Sponsorship = await _context.Sponsorships.FirstOrDefaultAsync(m => m.Id == id);
            startDate = (Sponsorship.SponsorshipstartDate).ToString("yyyy-MM-dd");

            endDate = (Sponsorship.SponsorshipstartDate).AddMonths(1).ToString("yyyy-MM-dd");

            /* CultureInfo[] cultures = { new CultureInfo("fr-FR"),
                                   };


             foreach (CultureInfo culture in cultures)
             {

                 Sponsorship.DiscountRate = Convert.ToDecimal(Sponsorship.DiscountRate, culture).ToString();
                 Sponsorship.DiscountPrice = Convert.ToDecimal(Sponsorship.DiscountPrice, culture).ToString();
             }*/

            //Sponsorship.GodfatherAmount = Sponsorship.GodfatherAmount.Replace(",", ".");
            //Sponsorship.GodsonpAmount = Sponsorship.GodsonpAmount.Replace(",", ".");

            if (Sponsorship == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int id)
        {

            /* if (!ModelState.IsValid)
             {
                 return Page();
             }*/
            Sponsorship.UserCompanyType = _UserCompanyType;
            Sponsorship.ContactId = _UserCompanyId;
            Sponsorship.UpdateByUserId = _ConnectedUserId;
            Sponsorship.LastUpdate = DateTime.Now;

            decimal value;
            CultureInfo[] cultures = { new CultureInfo("en-US"),
                                 new CultureInfo("fr-FR") };

      
            foreach (CultureInfo culture in cultures)
            {

               // Sponsorship.GodfatherAmount = Math.Round(Convert.ToDecimal(Sponsorship.GodfatherAmount, culture), 2).ToString();
                //Sponsorship.GodsonpAmount = Math.Round(Convert.ToDecimal(Sponsorship.GodsonpAmount, culture), 2).ToString();
            }

            if (Sponsorship.SponsorshipstartDate < Sponsorship.SponsorshipsEndDate /*&& Decimal.TryParse(Sponsorship.GodfatherAmount, out value) && Decimal.TryParse(Sponsorship.GodsonpAmount], out value)*/)
            {

                _context.Attach(Sponsorship).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(Sponsorship.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToPage("Index", new {  articleId = Sponsorship.ArticleId });
            }

            else {
                return RedirectToPage("Edit", new { id = Sponsorship.Id, articleId = Sponsorship.ArticleId });
            }
        }

        private bool CustomerExists(int id)
        {
            return _context.Sponsorships.Any(e => e.Id == id);
        }
    }
}
