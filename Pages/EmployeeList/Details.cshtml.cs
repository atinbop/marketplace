﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;

namespace MarketPlace.Pages.EmployeeList
{
    public class DetailsModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;

        public DetailsModel(MarketPlace.Data.ApplicationDbContext context)
        {
            _context = context;
      
        }

        public Employees Employee { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Employee = await _context.Employees.FirstOrDefaultAsync(m => m.Id == id);

            if (Employee == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
