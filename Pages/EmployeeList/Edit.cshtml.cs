﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace MarketPlace.Pages.EmployeeList
{
    public class EditModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;
        private readonly string _EmployeeImageRepository;
        private readonly IWebHostEnvironment webHostEnvironment;
        public IEnumerable<SelectListItem> PropertyList { get; set; }
        private readonly string _ImageRepository;
        public List<SelectListItem> itemsproperties { get; set; }
        [BindProperty]
        public Contacts Contact { get; set; }

        public IFormFile Picture { get; set; }




        public EditModel(Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;

            this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
            _EmployeeImageRepository = options.EmployeeImageRepository;
        }

        [BindProperty]
        public Employees Employee { get; set; }

        public async Task<IActionResult> OnGetAsync(int id, int contactId)
        {
            Contact = await _context.Contacts.FindAsync(contactId);
            if (id == 0)
            {
                return NotFound();
            }

            Employee = await _context.Employees.FirstOrDefaultAsync(m => m.Id == id);

            if (Employee == null)
            {
                return NotFound();
            }

            try
            {
                // Loading drop down lists.  
                PropertyList = this.GetEmployeeSaleZoneList(id);
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {

            if (Picture != null)
            {
                Employee.Picture = ProcessUploading(Picture);
                //string us = fs.UploadUserFile(Image1);
                //ViewBag.ResultMessage = fs.getseterror;
            }


            /* if (!ModelState.IsValid)
             {
                 return Page();
             }*/

            // For article salesarea
            var selectedCountries = Request.Form["salearea"];

            string sql = "delete from EmployeeSaleArea where EmployeeId=" + Employee.Id;
            int noOfRowUpdated = _context.Database.ExecuteSqlRaw(sql);

            foreach (var prop in selectedCountries)
            {
                SharingBuyingArea theSalesArea = await (from art in _context.SaleArea where art.id == Int32.Parse(prop) select art).FirstOrDefaultAsync();

                  sql = "insert into  EmployeeSaleArea ( EmployeeId,	SaleAreaId,CountryName,SaleAreaName, UpdateByUserId,LastUpdate ) values( " + Employee.Id + "," + prop + ",'" + theSalesArea.country + "'" + ",'" + theSalesArea.asciiname + "'" + ", " + _ConnectedUserId + ",getdate())";


                  noOfRowUpdated = _context.Database.ExecuteSqlRaw(sql);
            }
             
            _context.Attach(Employee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(Employee.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("Index", new { contactId = Employee.ContactId });
        }

        private bool CustomerExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }


        private List<SelectListItem> GetEmployeeSaleZoneList(int employeeId)
        {
            itemsproperties = new List<SelectListItem>();
            List<SelectListGroup> groupItemsproperties = new List<SelectListGroup>();

            var listArticleProperties = (from art in _context.Countries select art).ToList();
            foreach (var prop in listArticleProperties)
            {
                groupItemsproperties.Add(new SelectListGroup() { Name = prop.nom_fr_fr });
            }


            try
            {
                // Loading.  
                var listProperty = (from art in _context.SaleArea select art).ToList();

                var listArticleProperty = (from art in _context.EmployeeSaleArea where art.EmployeeId == employeeId select art).ToList();

                var groupproperties = new SelectListGroup();
                bool selectedProperty = false;
                foreach (var prop in listProperty)
                {
                    foreach (var groupselected in groupItemsproperties)
                    {

                        if (groupselected.Name == prop.country)
                        {
                            groupproperties = groupselected;
                        }
                    }

                    foreach (var propselected in listArticleProperty)
                    {
                        if (propselected.SaleAreaId == prop.id)
                        {
                            selectedProperty = true;
                        }
                    }
                    itemsproperties.Add(new SelectListItem() { Text = prop.name, Group = groupproperties, Selected = selectedProperty, Value = prop.id.ToString() });
                    selectedProperty = false;
                }

            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // info.  

            return itemsproperties;
        }
        private string ProcessUploading(IFormFile Image)
        {

            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, _EmployeeImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                Image.CopyTo(fileStream);
            }
            return _ImageRepository + uniqueFileName;
        }

    }
}