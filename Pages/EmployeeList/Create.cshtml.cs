﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MarketPlace.Data;
using MarketPlace.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace MarketPlace.Pages.EmployeeList
{
    public class CreateModel : PageModel
    {
        private readonly MarketPlace.Data.ApplicationDbContext _context;
        public string _ConnectedUserEmail;
        public string _ConnectedUserId;
        public string _UserCompanyType;
        public int _UserCompanyId;
        public String _UserCompanyName;
        public List<SelectListItem> itemsproperties { get; set; }
        private readonly IWebHostEnvironment webHostEnvironment;
        public IEnumerable<SelectListItem> PropertyList { get; set; }
        private readonly string _ImageRepository;
        private readonly string _EmployeeImageRepository;
        [BindProperty]
        public Contacts Contact { get; set; }
        [BindProperty]
        public IFormFile Picture { get; set; }
        public CreateModel(Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
     

                    this.webHostEnvironment = webHostEnvironment;

            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
            _EmployeeImageRepository = options.EmployeeImageRepository;

        }

        public async Task<IActionResult> OnGetAsync(int contactId)
        {


            Contact = await _context.Contacts.FindAsync(contactId);
            try
            {
                // Loading drop down lists.  
                PropertyList = this.GetEmployeeSaleZoneList(contactId);
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

            


            return Page();
        }

        [BindProperty]
        public Employees Employees { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (Picture != null)
            {
                Employees.Picture = ProcessUploading(Picture);
            }

            /*  if (!ModelState.IsValid)
              {
                  return Page();
              }*/
            Employees.ContactId = Contact.Id;

            _context.Employees.Add(Employees);

            // For article salesarea
            var selectedCountries = Request.Form["salearea"];

            //string sql = "delete from ContactSaleArea where EmployeeId= ";
            //int noOfRowUpdated = _context.Database.ExecuteSqlRaw(sql);

            int intIdt = _context.Employees.Max(u => u.Id);
            intIdt++;

            foreach (var prop in selectedCountries)
            { 
                SharingBuyingArea theSalesArea = await(from art in _context.SharingBuyingArea where art.id == Int32.Parse(prop) select art).FirstOrDefaultAsync();

                string sql = "insert into  EmployeeSaleArea ( EmployeeId,	SaleAreaId,CountryName,SaleAreaName, UpdateByUserId,LastUpdate ) values( " + intIdt + ","+ prop + ",'" + theSalesArea.country + "'" + ",'" + theSalesArea.asciiname + "'" + ", " + _ConnectedUserId + ",getdate())";
                int noOfRowUpdated = _context.Database.ExecuteSqlRaw(sql);
            }


            await _context.SaveChangesAsync();

            return RedirectToPage("Index", new { contactId = Contact.Id });
        }

        private List<SelectListItem> GetEmployeeSaleZoneList(int employeeId)
        {
            itemsproperties = new List<SelectListItem>();
            List<SelectListGroup> groupItemsproperties = new List<SelectListGroup>();

            var listArticleProperties = (from art in _context.Countries select art).ToList();
            foreach (var prop in listArticleProperties)
            {
                groupItemsproperties.Add(new SelectListGroup() { Name = prop.nom_fr_fr });
            }


            try
            {
                // Loading.  
                var listProperty = (from art in _context.SharingBuyingArea select art).ToList();

                var listArticleProperty = (from art in _context.EmployeeSaleArea where art.EmployeeId == employeeId select art).ToList();

                var groupproperties = new SelectListGroup();
                bool selectedProperty = false;
                foreach (var prop in listProperty)
                {
                    foreach (var groupselected in groupItemsproperties)
                    {

                        if (groupselected.Name == prop.country)
                        {
                            groupproperties = groupselected;
                        }
                    }

                    foreach (var propselected in listArticleProperty)
                    {
                        if (propselected.SaleAreaId == prop.id)
                        {
                            selectedProperty = true;
                        }
                    }
                    itemsproperties.Add(new SelectListItem() { Text = prop.asciiname, Group = groupproperties, Selected = selectedProperty, Value = prop.id.ToString() });
                    selectedProperty = false;
                }

            }
            catch (Exception ex)
            {
                // Info  
                throw ex;
            }

            // info.  

            return itemsproperties;
        }
        private string ProcessUploading(IFormFile Image)
        {

            string uniqueFileName = null;

            if (Image != null)
            {
                string uploadsFolder =
                Path.Combine(webHostEnvironment.WebRootPath, _EmployeeImageRepository);
                uniqueFileName = Guid.NewGuid().ToString() + "_" + Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using var fileStream = new FileStream(filePath, FileMode.Create);
                Image.CopyTo(fileStream);
            }
            return _ImageRepository + uniqueFileName;
        }

    }
}