﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Data;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;

namespace MarketPlace.Pages.EmployeeList
{
    public class DeleteModel : PageModel
    {
        private static string _ImageRepository;
        public static string _ConnectedUserEmail;
        public static string _ConnectedUserId;
        public static string _UserCompanyType;
        public static int _UserCompanyId;
        public static String _UserCompanyName;
 
        private readonly MarketPlace.Data.ApplicationDbContext _context;

 

        [BindProperty]
        public Contacts Contact { get; set; }
        public DeleteModel(Data.ApplicationDbContext context, IWebHostEnvironment webHostEnvironment, MDUOptions options)
        {
            _context = context;
      
            _ConnectedUserEmail = options.ConnectedUserEmail;
            _ConnectedUserId = options.ConnectedUserId;;
            _UserCompanyType = options.UserCompanyType; //or B2C
            _UserCompanyId = options.UserCompanyId;
            _UserCompanyName = options.UserCompanyName;
            _ImageRepository = options.ImageRepository;
        }

        [BindProperty]
        public Employees Employee { get; set; }

        public async Task<IActionResult> OnGetAsync(int id, int contactId)
        {
            Contact = await _context.Contacts.FindAsync(contactId);

            if (id == 0)
            {
                return NotFound();
            }

            Employee = await _context.Employees.FirstOrDefaultAsync(m => m.Id == id);

            if (Employee == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            Employee = await _context.Employees.FindAsync(id);

            if (Employee != null)
            {
                int noOfRowUpdated = _context.Database.ExecuteSqlRaw("Update Employees set Deleted=1,  LastUpdate=getdate(), UpdateByUserId='" + _ConnectedUserId + "'  where id =" + id);

                // _context.Employees.Remove(Employee);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("Index", new { contactId = Employee.ContactId });
        }
    }
}
