using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MarketPlace.Model;
using MarketPlace.Pages.ViewModels;

namespace MarketPlace.Data
{
    public class ArticlePropertiesRepository
    {
        private readonly ApplicationDbContext _context;
        public ArticlePropertiesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<ArticlePropertyDisplayViewModel> GetArticleProperties()
        {

            List<ArticleProperty> articleProperties = new List<ArticleProperty>();

          // articleProperties = _context.ArticleProperties.AsNoTracking()
            //    .Include(x => x.Property)
                //.Include(x => x.PropertyValue)
                //.ToList();

            if (articleProperties != null)
            {
                List<ArticlePropertyDisplayViewModel> articlePropertiesDisplay = new List<ArticlePropertyDisplayViewModel>();
                foreach (var x in articleProperties)
                {
                    var articlePropertyDisplay = new ArticlePropertyDisplayViewModel()
                    {
                        //ArticlePropertyId = x.ArticlePropertyId,
                        //ArticlePropertyName = x.ArticlePropertyName,
                       // PropertyName = x.Property.PropertyName,
                        //PropertyValueName = x.PropertyValue.PropertyValueName,

                    };
                    articlePropertiesDisplay.Add(articlePropertyDisplay);
                }
                return articlePropertiesDisplay;
            }
            return null;
        }


        public ArticlePropertyEditViewModel CreateArticleProperty(int articleId)
        {
            var cRepo = new PropertiesRepository(_context);
            var rRepo = new PropertyValuesRepository(_context);
            var articleProperty = new ArticlePropertyEditViewModel()
            {
                ArticlePropertyId = Guid.NewGuid().ToString(),
                Properties = cRepo.GetProperties(articleId),
                PropertyValues = rRepo.GetPropertyValues()
            };
            return articleProperty;
        }

        public bool SaveArticleProperty(ArticlePropertyEditViewModel articlePropertyedit, int ArticleId)
        {
            ArticlePropertyEditViewModel articlePropertyedit1 = articlePropertyedit;
            if (articlePropertyedit != null)
            {
                // if (Guid.TryParse(articlePropertyedit.ArticlePropertyId, out Guid newGuid))
                //{
                var articleProperty = new ArticleProperty()
                {
                    //ArticlePropertyId = new Guid(),
                    //ArticlePropertyName = articlePropertyedit.ArticlePropertyName,
                    //PropertyId = articlePropertyedit.SelectedPropertyId,
                    PropertyValueId = articlePropertyedit.SelectedPropertyValueId,
                    ArticleId = ArticleId,
                    //ArticlePropertyLabel = articlePropertyedit.SelectedArticlePropertyLabel,
                    //ArticlePropertyValueLabel = articlePropertyedit.SelectedArticlePropertyValueLabel,
                };
                //articleProperty.Property = _context.Properties.Find(articlePropertyedit.SelectedPropertyId);
                //articleProperty.PropertyValue = _context.PropertyValues.Find(articlePropertyedit.SelectedPropertyValueId);

                //_context.ArticleProperties.Add(articleProperty);
                try { _context.SaveChanges(); }
                catch { }
                return true;
                //}
            }
            // Return false if articlePropertyedit == null or ArticlePropertyID is not a guid
            return false;
        }
    }
}