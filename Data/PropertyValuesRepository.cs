using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace MarketPlace.Data
{
    public class PropertyValuesRepository
    {
        private readonly ApplicationDbContext _context;

        public PropertyValuesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetPropertyValues()
        {
            List<SelectListItem> propertyValues = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Value = null,
                    Text = " "
                }
            };
            return propertyValues;
        }

        public IEnumerable<SelectListItem> GetPropertyValues(string propertyId)
        {
            if (!String.IsNullOrWhiteSpace(propertyId))
            {
                IEnumerable<SelectListItem> propertyValues = _context.PropertyValues.AsNoTracking()
                    .OrderBy(n => n.PropertyValueName)
                    .Where(n => n.PropertyId.ToString() == propertyId)
                    .Select(n =>
                        new SelectListItem
                        {
                            Value = n.Id.ToString(),
                            Text = n.PropertyValueName
                        }).ToList();
                return new SelectList(propertyValues, "Value", "Text");
            }
            return null;
        }
    }
}