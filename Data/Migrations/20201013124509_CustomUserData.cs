﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketPlace.Data.Migrations
{
    public partial class CustomUserData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AspNetUserTokens",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserTokens",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "ProviderKey",
                table: "AspNetUserLogins",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserLogins",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(128)",
                oldMaxLength: 128);

            migrationBuilder.CreateTable(
                name: "Article",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    EAN = table.Column<string>(maxLength: 13, nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    B2CPrice = table.Column<decimal>(nullable: true),
                    VAT = table.Column<decimal>(nullable: true),
                    B2BSellerCompanyId = table.Column<int>(nullable: true),
                    FirstCategory = table.Column<string>(maxLength: 255, nullable: true),
                    ShortDescription = table.Column<string>(nullable: true),
                    LongDescription = table.Column<string>(nullable: true),
                    Notice = table.Column<string>(nullable: true),
                    SEOTitle = table.Column<string>(nullable: true),
                    SEODescription = table.Column<string>(nullable: true),
                    Ingredient = table.Column<string>(nullable: true),
                    ArticleFile = table.Column<string>(nullable: true),
                    Image1 = table.Column<string>(nullable: true),
                    Image2 = table.Column<string>(nullable: true),
                    Image3 = table.Column<string>(nullable: true),
                    Inventory = table.Column<int>(nullable: true),
                    B2CInventory = table.Column<int>(nullable: true),
                    YoutubeVideo = table.Column<string>(nullable: true),
                    FacebookPage = table.Column<string>(nullable: true),
                    TwitterPage = table.Column<string>(nullable: true),
                    InstagramPage = table.Column<string>(nullable: true),
                    OfficialWebsite = table.Column<string>(nullable: true),
                    youtubePage = table.Column<string>(nullable: true),
                    Indication = table.Column<string>(nullable: true),
                    Brand = table.Column<string>(nullable: true),
                    WarningMessage = table.Column<string>(nullable: true),
                    ArticleFilesLink = table.Column<string>(nullable: true),
                    ActiveSubtances = table.Column<string>(nullable: true),
                    SecondCategory = table.Column<string>(nullable: true),
                    ThirdCategory = table.Column<string>(nullable: true),
                    Weight = table.Column<int>(nullable: true),
                    Volume = table.Column<int>(nullable: true),
                    Height = table.Column<int>(nullable: true),
                    Width = table.Column<int>(nullable: true),
                    Depth = table.Column<int>(nullable: true),
                    Lot = table.Column<int>(nullable: true),
                    B2CLot = table.Column<int>(nullable: true),
                    OriginCountry = table.Column<string>(nullable: true),
                    CustomsCode = table.Column<string>(nullable: true),
                    BuyingPrice = table.Column<decimal>(nullable: true),
                    PriceType = table.Column<string>(nullable: true),
                    RefundRate = table.Column<decimal>(nullable: true),
                    SaleManagerName = table.Column<string>(nullable: true),
                    SaleManagerFirstname = table.Column<string>(nullable: true),
                    SaleManagerCivility = table.Column<string>(nullable: true),
                    SaleManagerEmail = table.Column<string>(nullable: true),
                    SaleManagerTelephone = table.Column<string>(nullable: true),
                    FrancoPortAmount = table.Column<decimal>(nullable: true),
                    MinimumOrderQuantity = table.Column<int>(nullable: true),
                    MaximumOrderQuantity = table.Column<int>(nullable: true),
                    DropShippingEmail = table.Column<string>(nullable: true),
                    SupplierAdvisedResalePrice = table.Column<decimal>(nullable: true),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    PROSellerCompanyId = table.Column<int>(nullable: true),
                    SuplierArticleReference = table.Column<string>(nullable: true),
                    SalesChannel = table.Column<string>(nullable: true),
                    ArticleValidatedFlag = table.Column<string>(nullable: true),
                    EndOfLifeFlag = table.Column<string>(nullable: true),
                    ProductionDate = table.Column<DateTime>(nullable: true),
                    NoveltyFlag = table.Column<string>(nullable: true),
                    AccountingCode = table.Column<string>(nullable: true),
                    ProductType = table.Column<string>(nullable: true),
                    ArticleKeywords = table.Column<string>(nullable: true),
                    DeliveryDelay = table.Column<int>(nullable: true),
                    PurchaseNomenclature = table.Column<string>(nullable: true),
                    CommercialTaget = table.Column<string>(nullable: true),
                    EcoTaxAmount = table.Column<decimal>(nullable: true),
                    Destockage = table.Column<string>(nullable: true),
                    SaleManagerFax = table.Column<string>(nullable: true),
                    EN_Name = table.Column<string>(nullable: true),
                    EN_ShortDescription = table.Column<string>(nullable: true),
                    EN_LongDescription = table.Column<string>(nullable: true),
                    EN_Notice = table.Column<string>(nullable: true),
                    EN_ArticleKeywords = table.Column<string>(nullable: true),
                    EN_SEOTitle = table.Column<string>(nullable: true),
                    EN_SEODescription = table.Column<string>(nullable: true),
                    EN_Ingredient = table.Column<string>(nullable: true),
                    EN_ArticleFile = table.Column<string>(nullable: true),
                    EN_Indication = table.Column<string>(nullable: true),
                    EN_ActiveSubtances = table.Column<string>(nullable: true),
                    ProductShopLink = table.Column<string>(nullable: true),
                    BrandShopLink = table.Column<string>(nullable: true),
                    SaleAuthorizedInPharmacy = table.Column<string>(nullable: true),
                    ShelfLife = table.Column<string>(nullable: true),
                    Internalreference = table.Column<string>(nullable: true),
                    Variantreference = table.Column<string>(nullable: true),
                    Groupcode = table.Column<string>(nullable: true),
                    ArticleOtherName = table.Column<string>(nullable: true),
                    En_ArticleOtherName = table.Column<string>(nullable: true),
                    Image4 = table.Column<string>(nullable: true),
                    Image5 = table.Column<string>(nullable: true),
                    Image6 = table.Column<string>(nullable: true),
                    Image7 = table.Column<string>(nullable: true),
                    Image8 = table.Column<string>(nullable: true),
                    Image9 = table.Column<string>(nullable: true),
                    Image10 = table.Column<string>(nullable: true),
                    NextProductionDate = table.Column<DateTime>(nullable: true),
                    ProductionFrequency = table.Column<string>(nullable: true),
                    NextProductionEstimateQuantity = table.Column<int>(nullable: true),
                    FirstOrderMinimalAmount = table.Column<decimal>(nullable: true),
                    Created = table.Column<DateTime>(nullable: true),
                    Code_CIP7 = table.Column<string>(nullable: true),
                    Modele = table.Column<string>(nullable: true),
                    Gamme = table.Column<string>(nullable: true),
                    UPC = table.Column<string>(nullable: true),
                    Contenance = table.Column<string>(nullable: true),
                    Forme = table.Column<string>(nullable: true),
                    Taille = table.Column<string>(nullable: true),
                    Conseiller_pour = table.Column<string>(nullable: true),
                    Dimension = table.Column<string>(nullable: true),
                    Autres_caracteristiques = table.Column<string>(nullable: true),
                    Couleur = table.Column<string>(nullable: true),
                    Pour_qui = table.Column<string>(nullable: true),
                    Presentation = table.Column<string>(nullable: true),
                    Indication_autres = table.Column<string>(nullable: true),
                    Type_de_cheveux = table.Column<string>(nullable: true),
                    Type_de_peau = table.Column<string>(nullable: true),
                    Parfum = table.Column<string>(nullable: true),
                    Texture = table.Column<string>(nullable: true),
                    Pour_quel_animal = table.Column<string>(nullable: true),
                    Type_de_shampooing = table.Column<string>(nullable: true),
                    Mode_administration = table.Column<string>(nullable: true),
                    Usage = table.Column<string>(nullable: true),
                    Video1 = table.Column<string>(nullable: true),
                    ReturnFlag = table.Column<string>(nullable: true),
                    OtherInformation = table.Column<string>(nullable: true),
                    AttractivenessIndex = table.Column<int>(nullable: true),
                    BrandId = table.Column<int>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: true),
                    Note = table.Column<int>(nullable: true),
                    barCodeImage = table.Column<string>(nullable: true),
                    categoryMarketing = table.Column<string>(nullable: true),
                    SaleAreaId = table.Column<int>(nullable: true),
                    SaleAreaFullName = table.Column<string>(nullable: true),
                    ProductDangerosity = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Article", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArticleCountries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(nullable: false),
                    ArticleId = table.Column<int>(nullable: false),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleCountries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArticleProperty",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PropertyValueId = table.Column<int>(nullable: false),
                    ArticleId = table.Column<int>(nullable: false),
                    ArticlePropertyLabel = table.Column<string>(nullable: true),
                    ArticlePropertyValueLabel = table.Column<string>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    PropertyLogo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleProperty", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArticleVariantes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleId = table.Column<int>(nullable: false),
                    VarianteArticleId = table.Column<int>(nullable: false),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleVariantes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArticleVendors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    articleId = table.Column<int>(nullable: true),
                    contactId = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: true),
                    Lot = table.Column<int>(nullable: true),
                    Inventory = table.Column<int>(nullable: true),
                    PriceType = table.Column<string>(nullable: true),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    UserCompanyType = table.Column<string>(nullable: false),
                    ProductionFrequency = table.Column<string>(nullable: true),
                    NextProductionEstimateQuantity = table.Column<int>(nullable: true),
                    NextProductionDate = table.Column<DateTime>(nullable: true),
                    ProductionDate = table.Column<DateTime>(nullable: true),
                    SaleManagerCivility = table.Column<string>(nullable: true),
                    SaleManagerName = table.Column<string>(nullable: true),
                    SaleManagerFirstname = table.Column<string>(nullable: true),
                    SaleManagerEmail = table.Column<string>(nullable: true),
                    SaleManagerTelephone = table.Column<string>(nullable: true),
                    SaleManagerFax = table.Column<string>(nullable: true),
                    MinimumOrderQuantity = table.Column<int>(nullable: true),
                    MaximumOrderQuantity = table.Column<int>(nullable: true),
                    DropShippingEmail = table.Column<string>(nullable: true),
                    BuyingPrice = table.Column<decimal>(nullable: true),
                    FrancoPortAmount = table.Column<decimal>(nullable: true),
                    DeliveryDelay = table.Column<int>(nullable: true),
                    FirstOrderMinimalAmount = table.Column<decimal>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    EAN = table.Column<string>(maxLength: 13, nullable: false),
                    B2CPrice = table.Column<decimal>(nullable: false),
                    VAT = table.Column<decimal>(nullable: true),
                    B2BSellerCompanyId = table.Column<int>(nullable: true),
                    FirstCategory = table.Column<string>(maxLength: 255, nullable: true),
                    ShortDescription = table.Column<string>(nullable: true),
                    LongDescription = table.Column<string>(nullable: true),
                    Notice = table.Column<string>(nullable: true),
                    SEOTitle = table.Column<string>(nullable: true),
                    SEODescription = table.Column<string>(nullable: true),
                    Ingredient = table.Column<string>(nullable: true),
                    ArticleFile = table.Column<string>(nullable: true),
                    Image1 = table.Column<string>(nullable: true),
                    Image2 = table.Column<string>(nullable: true),
                    Image3 = table.Column<string>(nullable: true),
                    B2CInventory = table.Column<int>(nullable: true),
                    YoutubeVideo = table.Column<string>(nullable: true),
                    FacebookPage = table.Column<string>(nullable: true),
                    TwitterPage = table.Column<string>(nullable: true),
                    InstagramPage = table.Column<string>(nullable: true),
                    OfficialWebsite = table.Column<string>(nullable: true),
                    youtubePage = table.Column<string>(nullable: true),
                    Indication = table.Column<string>(nullable: true),
                    Brand = table.Column<string>(nullable: true),
                    WarningMessage = table.Column<string>(nullable: true),
                    ArticleFilesLink = table.Column<string>(nullable: true),
                    ActiveSubtances = table.Column<string>(nullable: true),
                    SecondCategory = table.Column<string>(nullable: true),
                    ThirdCategory = table.Column<string>(nullable: true),
                    Weight = table.Column<int>(nullable: true),
                    Volume = table.Column<int>(nullable: true),
                    Height = table.Column<int>(nullable: true),
                    Width = table.Column<int>(nullable: true),
                    Depth = table.Column<int>(nullable: true),
                    B2CLot = table.Column<int>(nullable: true),
                    OriginCountry = table.Column<string>(nullable: true),
                    CustomsCode = table.Column<string>(nullable: true),
                    RefundRate = table.Column<decimal>(nullable: true),
                    SupplierAdvisedResalePrice = table.Column<decimal>(nullable: true),
                    PROSellerCompanyId = table.Column<int>(nullable: true),
                    SuplierArticleReference = table.Column<string>(nullable: true),
                    SalesChannel = table.Column<string>(nullable: true),
                    ArticleValidatedFlag = table.Column<string>(nullable: true),
                    EndOfLifeFlag = table.Column<string>(nullable: true),
                    NoveltyFlag = table.Column<string>(nullable: true),
                    AccountingCode = table.Column<string>(nullable: true),
                    ProductType = table.Column<string>(nullable: true),
                    ArticleKeywords = table.Column<string>(nullable: true),
                    PurchaseNomenclature = table.Column<string>(nullable: true),
                    CommercialTaget = table.Column<string>(nullable: true),
                    EcoTaxAmount = table.Column<decimal>(nullable: true),
                    Destockage = table.Column<string>(nullable: true),
                    EN_Name = table.Column<string>(nullable: true),
                    EN_ShortDescription = table.Column<string>(nullable: true),
                    EN_LongDescription = table.Column<string>(nullable: true),
                    EN_Notice = table.Column<string>(nullable: true),
                    EN_ArticleKeywords = table.Column<string>(nullable: true),
                    EN_SEOTitle = table.Column<string>(nullable: true),
                    EN_SEODescription = table.Column<string>(nullable: true),
                    EN_Ingredient = table.Column<string>(nullable: true),
                    EN_ArticleFile = table.Column<string>(nullable: true),
                    EN_Indication = table.Column<string>(nullable: true),
                    EN_ActiveSubtances = table.Column<string>(nullable: true),
                    ProductShopLink = table.Column<string>(nullable: true),
                    BrandShopLink = table.Column<string>(nullable: true),
                    SaleAuthorizedInPharmacy = table.Column<string>(nullable: true),
                    ShelfLife = table.Column<string>(nullable: true),
                    Internalreference = table.Column<string>(nullable: true),
                    Variantreference = table.Column<string>(nullable: true),
                    Groupcode = table.Column<string>(nullable: true),
                    ArticleOtherName = table.Column<string>(nullable: true),
                    En_ArticleOtherName = table.Column<string>(nullable: true),
                    Image4 = table.Column<string>(nullable: true),
                    Image5 = table.Column<string>(nullable: true),
                    Image6 = table.Column<string>(nullable: true),
                    Image7 = table.Column<string>(nullable: true),
                    Image8 = table.Column<string>(nullable: true),
                    Image9 = table.Column<string>(nullable: true),
                    Image10 = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: true),
                    Code_CIP7 = table.Column<string>(nullable: true),
                    Modele = table.Column<string>(nullable: true),
                    Gamme = table.Column<string>(nullable: true),
                    UPC = table.Column<string>(nullable: true),
                    Contenance = table.Column<string>(nullable: true),
                    Forme = table.Column<string>(nullable: true),
                    Taille = table.Column<string>(nullable: true),
                    Conseiller_pour = table.Column<string>(nullable: true),
                    Dimension = table.Column<string>(nullable: true),
                    Autres_caracteristiques = table.Column<string>(nullable: true),
                    Couleur = table.Column<string>(nullable: true),
                    Pour_qui = table.Column<string>(nullable: true),
                    Presentation = table.Column<string>(nullable: true),
                    Indication_autres = table.Column<string>(nullable: true),
                    Type_de_cheveux = table.Column<string>(nullable: true),
                    Type_de_peau = table.Column<string>(nullable: true),
                    Parfum = table.Column<string>(nullable: true),
                    Texture = table.Column<string>(nullable: true),
                    Pour_quel_animal = table.Column<string>(nullable: true),
                    Type_de_shampooing = table.Column<string>(nullable: true),
                    Mode_administration = table.Column<string>(nullable: true),
                    Usage = table.Column<string>(nullable: true),
                    Video1 = table.Column<string>(nullable: true),
                    ReturnFlag = table.Column<string>(nullable: true),
                    OtherInformation = table.Column<string>(nullable: true),
                    AttractivenessIndex = table.Column<int>(nullable: true),
                    BrandId = table.Column<int>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: true),
                    Note = table.Column<int>(nullable: true),
                    barCodeImage = table.Column<string>(nullable: true),
                    categoryMarketing = table.Column<string>(nullable: true),
                    SaleAreaId = table.Column<int>(nullable: true),
                    SaleAreaFullName = table.Column<string>(nullable: true),
                    ProductDangerosity = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleVendors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Chart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    chartDate = table.Column<DateTime>(nullable: true),
                    ArticleVendorsId = table.Column<int>(nullable: false),
                    supplierContactId = table.Column<int>(nullable: false),
                    employeeId = table.Column<int>(nullable: false),
                    vat = table.Column<decimal>(nullable: true),
                    articleId = table.Column<int>(nullable: false),
                    quantity = table.Column<int>(nullable: false),
                    price = table.Column<decimal>(nullable: false),
                    currency = table.Column<string>(nullable: true),
                    pricetype = table.Column<string>(nullable: true),
                    chartstate = table.Column<string>(nullable: true),
                    Deleted = table.Column<int>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    Image1 = table.Column<string>(nullable: true),
                    ProductName = table.Column<string>(nullable: true),
                    ProductBrand = table.Column<string>(nullable: true),
                    BrandId = table.Column<int>(nullable: false),
                    supplierName = table.Column<string>(nullable: true),
                    TotalAmount = table.Column<decimal>(nullable: false),
                    Lot = table.Column<int>(nullable: false),
                    MinimumOrderQuantity = table.Column<int>(nullable: true),
                    MaximumOrderQuantity = table.Column<int>(nullable: true),
                    customsTax = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chart", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ContactCode = table.Column<string>(nullable: true),
                    ContactType = table.Column<string>(nullable: true),
                    Company = table.Column<string>(nullable: false),
                    Gender = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    address1 = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    Address3 = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    EAN = table.Column<string>(nullable: true),
                    SIREN = table.Column<string>(nullable: true),
                    SIRET = table.Column<string>(nullable: true),
                    APE = table.Column<string>(nullable: true),
                    ActivitySector = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true),
                    Origin = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    BlockedAccount = table.Column<string>(nullable: true),
                    BlockedPurchase = table.Column<string>(nullable: true),
                    Alert = table.Column<string>(nullable: true),
                    ExcludeLoyalty = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    PrivateSpace = table.Column<string>(nullable: true),
                    AuthorizedAccess = table.Column<string>(nullable: true),
                    NewsletterSubscriber = table.Column<string>(nullable: true),
                    PaymentChoice = table.Column<string>(nullable: true),
                    PriceDisplay = table.Column<string>(nullable: true),
                    PriceCategory = table.Column<string>(nullable: true),
                    AccountingCode = table.Column<string>(nullable: true),
                    IntraCommunityVAT = table.Column<string>(nullable: true),
                    Credit = table.Column<string>(nullable: true),
                    Risk = table.Column<string>(nullable: true),
                    MonthlyBill = table.Column<string>(nullable: true),
                    StartOfMonthlyPayment = table.Column<string>(nullable: true),
                    ClientAccount = table.Column<string>(nullable: true),
                    FeedbackContact = table.Column<string>(nullable: true),
                    InternalCommentary = table.Column<string>(nullable: true),
                    Commercial = table.Column<string>(nullable: true),
                    Treatment = table.Column<string>(nullable: true),
                    archiving = table.Column<string>(nullable: true),
                    VATExemption = table.Column<string>(nullable: true),
                    PaidOrdersNotInvoiced = table.Column<string>(nullable: true),
                    ReceivablesClient = table.Column<string>(nullable: true),
                    BankAccountNumberIBAN = table.Column<string>(nullable: true),
                    CompanyLogoFile = table.Column<string>(nullable: true),
                    BankAccountNumberRIB = table.Column<string>(nullable: true),
                    address1Livraison1 = table.Column<string>(nullable: true),
                    Address2Livraison1 = table.Column<string>(nullable: true),
                    Address3Livraison1 = table.Column<string>(nullable: true),
                    PostalCodeLivraison1 = table.Column<string>(nullable: true),
                    CityLivraison1 = table.Column<string>(nullable: true),
                    CountryLivraison1 = table.Column<string>(nullable: true),
                    address1Livraison3 = table.Column<string>(nullable: true),
                    Address2Livraison3 = table.Column<string>(nullable: true),
                    Address3Livraison3 = table.Column<string>(nullable: true),
                    PostalCodeLivraison3 = table.Column<string>(nullable: true),
                    CityLivraison3 = table.Column<string>(nullable: true),
                    CountryLivraison3 = table.Column<string>(nullable: true),
                    address1Facturation = table.Column<string>(nullable: true),
                    Address2Facturation = table.Column<string>(nullable: true),
                    Address3Facturation = table.Column<string>(nullable: true),
                    PostalCodeFacturation = table.Column<string>(nullable: true),
                    CityFacturation = table.Column<string>(nullable: true),
                    CountryFacturation = table.Column<string>(nullable: true),
                    UserCompanyType = table.Column<string>(nullable: true),
                    currencyId = table.Column<int>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Continents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Continents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    code = table.Column<string>(nullable: true),
                    alpha2 = table.Column<string>(nullable: true),
                    alpha3 = table.Column<string>(nullable: true),
                    nom_en_gb = table.Column<string>(nullable: true),
                    nom_fr_fr = table.Column<string>(nullable: true),
                    flag = table.Column<string>(nullable: true),
                    Continent = table.Column<string>(nullable: true),
                    Capital = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Entity = table.Column<string>(nullable: true),
                    CurrencyName = table.Column<string>(nullable: true),
                    AlphabeticCode = table.Column<string>(nullable: true),
                    NumericCode = table.Column<string>(nullable: true),
                    MinorUnit = table.Column<string>(nullable: true),
                    WithdrawalDate = table.Column<string>(nullable: true),
                    symbol = table.Column<string>(nullable: true),
                    unicodedecimal = table.Column<string>(nullable: true),
                    unicodehex = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Discounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleId = table.Column<int>(nullable: false),
                    BasePrice = table.Column<decimal>(nullable: true),
                    DiscountStartDate = table.Column<DateTime>(nullable: false),
                    DiscountEndDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    UserCompanyType = table.Column<string>(nullable: true),
                    ContactId = table.Column<int>(nullable: true),
                    DiscountPrice1 = table.Column<string>(nullable: true),
                    DiscountRate1 = table.Column<string>(nullable: true),
                    Quantity1 = table.Column<int>(nullable: true),
                    DiscountPrice2 = table.Column<string>(nullable: true),
                    DiscountRate2 = table.Column<string>(nullable: true),
                    Quantity2 = table.Column<int>(nullable: true),
                    DiscountPrice3 = table.Column<string>(nullable: true),
                    DiscountRate3 = table.Column<string>(nullable: true),
                    Quantity3 = table.Column<int>(nullable: true),
                    currentorderQuantity = table.Column<int>(nullable: true),
                    active = table.Column<int>(nullable: true),
                    articleVendorsId = table.Column<int>(nullable: true),
                    flagDiscount = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Discounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeSaleArea",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SaleAreaId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    CountryName = table.Column<string>(nullable: true),
                    SaleAreaName = table.Column<string>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeSaleArea", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExchangeRate",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ValueDate = table.Column<DateTime>(nullable: true),
                    Currency = table.Column<string>(nullable: true),
                    Rate = table.Column<decimal>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRate", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "FavoriteProducts",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    articleId = table.Column<int>(nullable: false),
                    employeeId = table.Column<int>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FavoriteProducts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "LogConnexions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeId = table.Column<int>(nullable: false),
                    LoginDate = table.Column<DateTime>(nullable: true),
                    LogoutDate = table.Column<DateTime>(nullable: true),
                    logSystem = table.Column<string>(nullable: true),
                    ViewedItem = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogConnexions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Note = table.Column<int>(nullable: false),
                    ArticleId = table.Column<int>(nullable: false),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Offers",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    offerDate = table.Column<DateTime>(nullable: false),
                    offerName = table.Column<string>(nullable: true),
                    OfferDescription = table.Column<string>(nullable: true),
                    offerPrice = table.Column<decimal>(nullable: false),
                    numberofProduct = table.Column<int>(nullable: false),
                    SupplierType = table.Column<string>(nullable: true),
                    Period = table.Column<string>(nullable: true),
                    offerOrder = table.Column<int>(nullable: false),
                    NumberOfDisplayProduct = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Parameters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Payout",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    TransactionNumber = table.Column<string>(nullable: true),
                    PaymentDocument = table.Column<string>(nullable: true),
                    OrderCode = table.Column<string>(nullable: true),
                    PaymentStatus = table.Column<string>(nullable: true),
                    PaymentComment = table.Column<string>(nullable: true),
                    SellerId = table.Column<int>(nullable: false),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payout", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonModel",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Surname = table.Column<string>(nullable: true),
                    PersonModelName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonModel", x => x.Name);
                    table.ForeignKey(
                        name: "FK_PersonModel_PersonModel_PersonModelName",
                        column: x => x.PersonModelName,
                        principalTable: "PersonModel",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostName = table.Column<string>(nullable: true),
                    PostDescription = table.Column<string>(nullable: true),
                    PostContent = table.Column<string>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<int>(nullable: false),
                    PostImage = table.Column<string>(nullable: true),
                    PostFile = table.Column<string>(nullable: true),
                    ArticleId = table.Column<int>(nullable: false),
                    PartnerEmployeeId = table.Column<int>(nullable: false),
                    UpdateByPseudo = table.Column<string>(nullable: true),
                    slotLastUpdate = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PreOrderDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleId = table.Column<int>(nullable: false),
                    BasePrice = table.Column<decimal>(nullable: true),
                    DiscountPrice = table.Column<string>(nullable: true),
                    DiscountRate = table.Column<string>(nullable: true),
                    DiscountStartDate = table.Column<DateTime>(nullable: false),
                    DiscountEndDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    Period = table.Column<DateTime>(nullable: false),
                    UserCompanyType = table.Column<string>(nullable: true),
                    ContactId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreOrderDiscounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AppID = table.Column<long>(nullable: false),
                    ProductName = table.Column<string>(nullable: true),
                    CategoryID = table.Column<int>(nullable: false),
                    ManufacturerID = table.Column<int>(nullable: false),
                    FileName1 = table.Column<string>(nullable: true),
                    FileName2 = table.Column<string>(nullable: true),
                    FileName3 = table.Column<string>(nullable: true),
                    Solution = table.Column<string>(nullable: true),
                    Specificities = table.Column<string>(nullable: true),
                    Reference = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Usages = table.Column<string>(nullable: true),
                    Composition = table.Column<string>(nullable: true),
                    Contraindication = table.Column<string>(nullable: true),
                    Posologie = table.Column<string>(nullable: true),
                    Notice = table.Column<string>(nullable: true),
                    Taille = table.Column<string>(nullable: true),
                    Inventory = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    GrossPrice = table.Column<double>(nullable: false),
                    TauxTva = table.Column<double>(nullable: false),
                    Active = table.Column<int>(nullable: false),
                    InsertDatetime = table.Column<string>(nullable: true),
                    GrossMinimumQuantity = table.Column<int>(nullable: false),
                    Promotion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ProductID);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EN_Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Properties",
                columns: table => new
                {
                    Id = table.Column<int>(maxLength: 2, nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PropertyName = table.Column<string>(maxLength: 50, nullable: false),
                    EN_PropertyName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Properties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Prospects",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    emailAddress = table.Column<string>(nullable: false),
                    TelephoneNumber = table.Column<string>(nullable: false),
                    Company = table.Column<string>(nullable: false),
                    Message = table.Column<string>(nullable: false),
                    Remarque = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prospects", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "SaleArea",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SaleAreaName = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: false),
                    CountryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleArea", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SpecialDiscounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleId = table.Column<int>(nullable: false),
                    BasePrice = table.Column<decimal>(nullable: true),
                    TurnOver = table.Column<string>(nullable: true),
                    DiscountAmount = table.Column<string>(nullable: true),
                    DiscountStartDate = table.Column<DateTime>(nullable: false),
                    DiscountEndDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    Score = table.Column<int>(nullable: false),
                    UserCompanyType = table.Column<string>(nullable: true),
                    ContactId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecialDiscounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sponsorships",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleId = table.Column<int>(nullable: false),
                    GodfatherAmount = table.Column<decimal>(nullable: true),
                    GodsonpAmount = table.Column<decimal>(nullable: true),
                    SponsorshipstartDate = table.Column<DateTime>(nullable: false),
                    SponsorshipsEndDate = table.Column<DateTime>(nullable: false),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    UserCompanyType = table.Column<string>(nullable: true),
                    ContactId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sponsorships", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StaticData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaticData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Translations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    code = table.Column<string>(nullable: true),
                    language = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Translations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Brands",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BrandCode = table.Column<string>(nullable: true),
                    BrandName = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    BrandLogo = table.Column<string>(nullable: true),
                    BrandLinkPage = table.Column<string>(nullable: true),
                    TopBrandFlag = table.Column<string>(nullable: true),
                    Keywords = table.Column<string>(nullable: true),
                    BrandShortDescription = table.Column<string>(nullable: true),
                    BrandLongDescription = table.Column<string>(nullable: true),
                    EN_Keywords = table.Column<string>(nullable: true),
                    EN_BrandShortDescription = table.Column<string>(nullable: true),
                    EN_BrandLongDescription = table.Column<string>(nullable: true),
                    Slogan = table.Column<string>(nullable: true),
                    ContactId = table.Column<int>(nullable: true),
                    ImageBanderole1 = table.Column<string>(nullable: true),
                    ImageBanderole2 = table.Column<string>(nullable: true),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    BrandLogoNoirEtBlanc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Brands_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Contactcode = table.Column<string>(nullable: true),
                    collaboratorCode = table.Column<string>(nullable: true),
                    Civility = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: false),
                    Firstname = table.Column<string>(nullable: true),
                    Functions = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    Portable = table.Column<string>(nullable: true),
                    Emaillogin = table.Column<string>(nullable: true),
                    pwd = table.Column<string>(nullable: true),
                    Bulletin = table.Column<string>(nullable: true),
                    Authorizedaccess = table.Column<string>(nullable: true),
                    Validation = table.Column<string>(nullable: true),
                    Contactbydefault = table.Column<string>(nullable: true),
                    ContactId = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true),
                    SellingArea = table.Column<string>(nullable: true),
                    SellingCountry = table.Column<string>(nullable: true),
                    Deleted = table.Column<int>(nullable: true),
                    UpdateByUserId = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    Pseudo = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderItem",
                columns: table => new
                {
                    OrderItemID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderID = table.Column<long>(nullable: false),
                    ProductID = table.Column<long>(nullable: true),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItem", x => x.OrderItemID);
                    table.ForeignKey(
                        name: "FK_OrderItem_Product_ProductID",
                        column: x => x.ProductID,
                        principalTable: "Product",
                        principalColumn: "ProductID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PropertyValues",
                columns: table => new
                {
                    Id = table.Column<int>(maxLength: 2, nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PropertyValueName = table.Column<string>(nullable: false),
                    EN_PropertyValueName = table.Column<string>(nullable: true),
                    PropertyId = table.Column<int>(nullable: false),
                    PropertyName = table.Column<string>(nullable: true),
                    PropertyLogo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PropertyValues_Properties_PropertyId",
                        column: x => x.PropertyId,
                        principalTable: "Properties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscriptions",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ContactId = table.Column<int>(nullable: true),
                    OfferId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: true),
                    startvalidityDate = table.Column<DateTime>(nullable: false),
                    endValidityDate = table.Column<DateTime>(nullable: false),
                    cancelsubscription = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscriptions", x => x.id);
                    table.ForeignKey(
                        name: "FK_Subscriptions_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscriptions_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscriptions_Offers_OfferId",
                        column: x => x.OfferId,
                        principalTable: "Offers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductSubscriptions",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    contactid = table.Column<int>(nullable: false),
                    articleid = table.Column<int>(nullable: false),
                    Subscriptionid = table.Column<int>(nullable: false),
                    startvalidityDate = table.Column<DateTime>(nullable: true),
                    endValidityDate = table.Column<DateTime>(nullable: true),
                    cancelsubscription = table.Column<int>(nullable: false),
                    employeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSubscriptions", x => x.id);
                    table.ForeignKey(
                        name: "FK_ProductSubscriptions_Subscriptions_Subscriptionid",
                        column: x => x.Subscriptionid,
                        principalTable: "Subscriptions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductSubscriptions_Article_articleid",
                        column: x => x.articleid,
                        principalTable: "Article",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductSubscriptions_Contacts_contactid",
                        column: x => x.contactid,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Brands_ContactId",
                table: "Brands",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_ContactId",
                table: "Employees",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItem_ProductID",
                table: "OrderItem",
                column: "ProductID");

            migrationBuilder.CreateIndex(
                name: "IX_PersonModel_PersonModelName",
                table: "PersonModel",
                column: "PersonModelName");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSubscriptions_Subscriptionid",
                table: "ProductSubscriptions",
                column: "Subscriptionid");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSubscriptions_articleid",
                table: "ProductSubscriptions",
                column: "articleid");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSubscriptions_contactid",
                table: "ProductSubscriptions",
                column: "contactid");

            migrationBuilder.CreateIndex(
                name: "IX_PropertyValues_PropertyId",
                table: "PropertyValues",
                column: "PropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_ContactId",
                table: "Subscriptions",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_EmployeeId",
                table: "Subscriptions",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_OfferId",
                table: "Subscriptions",
                column: "OfferId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArticleCountries");

            migrationBuilder.DropTable(
                name: "ArticleProperty");

            migrationBuilder.DropTable(
                name: "ArticleVariantes");

            migrationBuilder.DropTable(
                name: "ArticleVendors");

            migrationBuilder.DropTable(
                name: "Brands");

            migrationBuilder.DropTable(
                name: "Chart");

            migrationBuilder.DropTable(
                name: "Continents");

            migrationBuilder.DropTable(
                name: "Countries");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropTable(
                name: "Discounts");

            migrationBuilder.DropTable(
                name: "EmployeeSaleArea");

            migrationBuilder.DropTable(
                name: "ExchangeRate");

            migrationBuilder.DropTable(
                name: "FavoriteProducts");

            migrationBuilder.DropTable(
                name: "LogConnexions");

            migrationBuilder.DropTable(
                name: "Notes");

            migrationBuilder.DropTable(
                name: "OrderItem");

            migrationBuilder.DropTable(
                name: "Parameters");

            migrationBuilder.DropTable(
                name: "Payout");

            migrationBuilder.DropTable(
                name: "PersonModel");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "PreOrderDiscounts");

            migrationBuilder.DropTable(
                name: "ProductCategories");

            migrationBuilder.DropTable(
                name: "ProductSubscriptions");

            migrationBuilder.DropTable(
                name: "PropertyValues");

            migrationBuilder.DropTable(
                name: "Prospects");

            migrationBuilder.DropTable(
                name: "SaleArea");

            migrationBuilder.DropTable(
                name: "SpecialDiscounts");

            migrationBuilder.DropTable(
                name: "Sponsorships");

            migrationBuilder.DropTable(
                name: "StaticData");

            migrationBuilder.DropTable(
                name: "Translations");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Subscriptions");

            migrationBuilder.DropTable(
                name: "Article");

            migrationBuilder.DropTable(
                name: "Properties");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Offers");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AspNetUserTokens",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserTokens",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ProviderKey",
                table: "AspNetUserLogins",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "LoginProvider",
                table: "AspNetUserLogins",
                type: "nvarchar(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
