using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace MarketPlace.Data
{
    public class PropertiesRepository
    {
        private readonly ApplicationDbContext _context;
        public PropertiesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<SelectListItem> GetProperties(int articleId)
        {
            //List<SelectListItem> properties;

            List<SelectListItem> properties = _context.Properties.AsNoTracking()
                                      .OrderBy(n => n.PropertyName)
                  .Select(n =>
                      new SelectListItem
                      {
                          Value = n.Id.ToString(),
                          Text = n.PropertyName
                      }).ToList();

            //List<Property> properties = _context.Properties.FromSqlRaw<List<Property>>("getAvailableArticleProperties { 0}", articleId).ToList();

            /*   _context.Properties
                    .FromSqlRaw<Property>("getAvailableArticleProperties {0}", articleId)
                    .ToList();*/

            /*
        List<SelectListItem> properties=  _context.Properties.FromSqlRaw<List<Property>>("EXECUTE dbo.getAvailableArticleProperties {0}", articleId)
                                                                                   .OrderBy(n => n.PropertyName)
                                                                                   .Select(n =>
                                                                                    new SelectListItem
                                                                                    {
                                                                                        Value = n.PropertyId.ToString(),
                                                                                        Text = n.PropertyName
                                                                                    }).ToList();*/



            var propertytip = new SelectListItem()
            {
                Value = null,
                Text = "--- Selectioner une proprieté ---"
            };
            properties.Insert(0, propertytip);
            return new SelectList(properties, "Value", "Text");
        }
    }
}