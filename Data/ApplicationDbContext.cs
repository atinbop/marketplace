﻿using JetBrains.Annotations;
using MarketPlace.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
 


namespace MarketPlace.Data
{
    public class ApplicationDbContext :  IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public Microsoft.EntityFrameworkCore.DbSet<Positions> Positions { get; set; }
        
        
        public Microsoft.EntityFrameworkCore.DbSet<EmployeesallowedAccess> EmployeesallowedAccess { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Adresse> Adresse { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Authorizations> Authorizations { get; set; }
    public Microsoft.EntityFrameworkCore.DbSet<FavoriteVendors> FavoriteVendors { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<VendorsNotes> VendorsNotes { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Visitors> Visitors { get; set; }
    public Microsoft.EntityFrameworkCore.DbSet<SharingBuyingArea> SharingBuyingArea { get; set; }
    public Microsoft.EntityFrameworkCore.DbSet<MarketplaceUser> MarketplaceUser { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Translations> Translations { get; set; }
    public Microsoft.EntityFrameworkCore.DbSet<ExchangeRate>  ExchangeRate { get; set; }
    public Microsoft.EntityFrameworkCore.DbSet<Currency> Currency { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Chart> Chart { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<FavoriteProducts> FavoriteProducts { get; set; }
        
        public Microsoft.EntityFrameworkCore.DbSet<ArticleVariantes> ArticleVariantes { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Sponsorships> Sponsorships { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<ArticleVendors> ArticleVendors { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<LogConnexions> LogConnexions { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet< Parameters> Parameters { get; set; }
          public Microsoft.EntityFrameworkCore.DbSet<Notes> Notes { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<ProductCategories> ProductCategories { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<EmployeeSaleArea> EmployeeSaleArea { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<SharingBuyingArea> SaleArea { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Article> Article { get; set; }

        public Microsoft.EntityFrameworkCore.DbSet<Payout> Payout { get; set; }

        public Microsoft.EntityFrameworkCore.DbSet<ArticleProperty> ArticleProperty { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Discounts> GlobalDiscount { get; set; }

        public Microsoft.EntityFrameworkCore.DbSet<StaticData> StaticData { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Property> Properties { get; set; }

        public Microsoft.EntityFrameworkCore.DbSet<PropertyValue> PropertyValues { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<ContactsStock> ContactsStock { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Contacts> Contacts { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Prospects> Prospects { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Brands> Brands { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Employees> Employees { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<EmployeesStock> EmployeesStock { get; set; }
        
        public Microsoft.EntityFrameworkCore.DbSet<Offers> Offers { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Subscriptions> Subscriptions { get; set; }

        public Microsoft.EntityFrameworkCore.DbSet<ProductSubscriptions> ProductSubscriptions { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Country> Countries { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Discounts> Discounts { get; set; }

        public Microsoft.EntityFrameworkCore.DbSet<OrderItem> OrderItem { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<PersonModel> PersonModel { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<PreOrderDiscounts> PreOrderDiscounts { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<SpecialDiscounts> SpecialDiscounts { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<ArticleCountry> ArticleCountries { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Continents> Continents { get; set; }
        
        public Microsoft.EntityFrameworkCore.DbSet<Posts> Posts { get; set; }
        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            foreach (var relationship in modelbuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelbuilder);
        }
       
    }
}
