﻿USE [test20]
GO
/****** Object:  StoredProcedure [dbo].[spGetAutorizedArticlesB2B_B2C]    Script Date: 02/10/2020 21:45:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER  procedure [dbo].[spGetAutorizedArticlesB2B_B2C] 
@UserCompanyType varchar(3),
@contactId int
as
begin
 
 
  select distinct
 b.Inventory	,
b.Lot	,
 isnull(b.Price,0)   as Price	,
b.BuyingPrice	,
b.PriceType	,
b.SaleManagerName	,
b.SaleManagerFirstname	,
b.SaleManagerCivility	,
b.SaleManagerEmail	,
b.SaleManagerTelephone	,
b.FrancoPortAmount	,
b.MinimumOrderQuantity	,
b.MinimumOrderAmount	,
b.DropShippingEmail	,
b.ProductionDate	,
b.DeliveryDelay	,
b.NextProductionDate	,
b.ProductionFrequency	,
b.NextProductionEstimateQuantity	,
b.SaleManagerFax	,
b.FirstOrderMinimalAmount	,
	
a.Id	,
a.Name	,
a.EAN	,
 
a.B2CPrice	,
a.VAT	,
a.FirstCategory	,
a.ShortDescription	,
a.LongDescription	,
a.Notice	,
a.SEOTitle	,
a.SEODescription	,
a.Ingredient	,
a.ArticleFile	,
a.Image1	,
a.Image2	,
a.Image3	,
a.B2CInventory	,
a.YoutubeVideo	,
a.FacebookPage	,
a.TwitterPage	,
a.InstagramPage	,
a.Indication	,
a.Brand	,
a.WarningMessage	,
a.ArticleFilesLink	,
a.ActiveSubtances	,
a.SecondCategory	,
a.ThirdCategory	,
a.Weight	,
a.Volume	,
a.Height	,
a.Width	,
a.Depth	,
a.B2CLot	,
a.OriginCountry	,
a.CustomsCode	,
a.RefundRate	,
a.SupplierAdvisedResalePrice	,
a.Deleted	,
a.UpdateByUserId	,
a.LastUpdate	,
a.PROSellerCompanyId	,

a.SuplierArticleReference	,
a.SalesChannel	,
a.ArticleValidatedFlag	,
a.EndOfLifeFlag	,
a.NoveltyFlag	,
a.AccountingCode	,
a.ProductType	,
a.ArticleKeywords	,
a.PurchaseNomenclature	,
a.CommercialTaget	,
a.EcoTaxAmount	,
a.EN_Name	,
a.EN_ShortDescription	,
a.EN_LongDescription	,
a.EN_Notice	,
a.EN_SEOTitle	,
a.EN_SEODescription	,
a.EN_Ingredient	,
a.EN_ArticleFile	,
a.EN_Indication	,
a.EN_ActiveSubtances	,
a.ProductShopLink	,
a.BrandShopLink	,
a.SaleAuthorizedInPharmacy	,
a.ShelfLife	,
a.Internalreference	,
a.Variantreference	,
a.Groupcode	,
a.ArticleOtherName	,
a.En_ArticleOtherName	,
a.Image4	,
a.Image5	,
a.Image6	,
a.Image7	,
a.Image8	,
a.Image9	,
a.Image10	,
a.Created	,
a.EN_ArticleKeywords	,
a.Destockage	,
a.Code_CIP7	,
a.Modele	,
a.Gamme	,
a.UPC	,
a.Contenance	,
a.Forme	,
a.Taille	,
a.Conseiller_pour	,
a.Dimension	,
a.Autres_caracteristiques	,
a.Couleur	,
a.Pour_qui	,
a.Presentation	,
a.Indication_autres	,
a.Type_de_cheveux	,
a.Type_de_peau	,
a.Parfum	,
a.Texture	,
a.Pour_quel_animal	,
a.Type_de_shampooing	,
a.Mode_administration	,
a.Video1	,
a.Usage	,
a.ReturnFlag	,
a.OtherInformation	,
a.AttractivenessIndex	,
a.BrandId	,
a.EmployeeId	,
a.Note	,
a.UnreadMessagesNumber	 

 from Article a   join ProductSubscriptions c on  c.articleid= a.Id  and c.contactid=@contactId
 left join [dbo].[ArticleVendors] b 
 on a.Id=b.articleId and b.UserCompanyType= @UserCompanyType and 
 b.contactid= @contactId
 where c.cancelsubscription != 1 and a.Deleted is  null and  b.Deleted is  null
 
 
end

USE [test20]
GO
/****** Object:  StoredProcedure [dbo].[spGetAutorizedArticlesPRO]    Script Date: 02/10/2020 21:47:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER  procedure [dbo].[spGetAutorizedArticlesPRO] 
@UserCompanyType varchar(3),
@contactId int
as
begin
 
 
 
 select distinct
  b.Inventory	,
b.Lot	,

 isnull(b.Price,0)   as Price	,

b.BuyingPrice	,
b.PriceType	,
b.SaleManagerName	,
b.SaleManagerFirstname	,
b.SaleManagerCivility	,
b.SaleManagerEmail	,
b.SaleManagerTelephone	,
b.FrancoPortAmount	,
b.MinimumOrderQuantity	,
b.MinimumOrderAmount	,
b.DropShippingEmail	,
b.ProductionDate	,
b.DeliveryDelay	,
b.NextProductionDate	,
b.ProductionFrequency	,
b.NextProductionEstimateQuantity	,
b.SaleManagerFax	,
b.FirstOrderMinimalAmount	,
	
a.Id	,
a.Name	,
a.EAN	,
 
a.B2CPrice	,
a.VAT	,
a.FirstCategory	,
a.ShortDescription	,
a.LongDescription	,
a.Notice	,
a.SEOTitle	,
a.SEODescription	,
a.Ingredient	,
a.ArticleFile	,
a.Image1	,
a.Image2	,
a.Image3	,
a.B2CInventory	,
a.YoutubeVideo	,
a.FacebookPage	,
a.TwitterPage	,
a.InstagramPage	,
a.Indication	,
a.Brand	,
a.WarningMessage	,
a.ArticleFilesLink	,
a.ActiveSubtances	,
a.SecondCategory	,
a.ThirdCategory	,
a.Weight	,
a.Volume	,
a.Height	,
a.Width	,
a.Depth	,
a.B2CLot	,
a.OriginCountry	,
a.CustomsCode	,
a.RefundRate	,
a.SupplierAdvisedResalePrice	,
a.Deleted	,
a.UpdateByUserId	,
a.LastUpdate	,
a.PROSellerCompanyId	,

a.SuplierArticleReference	,
a.SalesChannel	,
a.ArticleValidatedFlag	,
a.EndOfLifeFlag	,
a.NoveltyFlag	,
a.AccountingCode	,
a.ProductType	,
a.ArticleKeywords	,
a.PurchaseNomenclature	,
a.CommercialTaget	,
a.EcoTaxAmount	,
a.EN_Name	,
a.EN_ShortDescription	,
a.EN_LongDescription	,
a.EN_Notice	,
a.EN_SEOTitle	,
a.EN_SEODescription	,
a.EN_Ingredient	,
a.EN_ArticleFile	,
a.EN_Indication	,
a.EN_ActiveSubtances	,
a.ProductShopLink	,
a.BrandShopLink	,
a.SaleAuthorizedInPharmacy	,
a.ShelfLife	,
a.Internalreference	,
a.Variantreference	,
a.Groupcode	,
a.ArticleOtherName	,
a.En_ArticleOtherName	,
a.Image4	,
a.Image5	,
a.Image6	,
a.Image7	,
a.Image8	,
a.Image9	,
a.Image10	,
a.Created	,
a.EN_ArticleKeywords	,
a.Destockage	,
a.Code_CIP7	,
a.Modele	,
a.Gamme	,
a.UPC	,
a.Contenance	,
a.Forme	,
a.Taille	,
a.Conseiller_pour	,
a.Dimension	,
a.Autres_caracteristiques	,
a.Couleur	,
a.Pour_qui	,
a.Presentation	,
a.Indication_autres	,
a.Type_de_cheveux	,
a.Type_de_peau	,
a.Parfum	,
a.Texture	,
a.Pour_quel_animal	,
a.Type_de_shampooing	,
a.Mode_administration	,
a.Video1	,
a.Usage	,
a.ReturnFlag	,
a.OtherInformation	,
a.AttractivenessIndex	,
a.BrandId	,
a.EmployeeId	,
a.Note	,
a.UnreadMessagesNumber	 
	from Article a   left join [dbo].[ArticleVendors] b 
 on a.Id=b.articleId and b.UserCompanyType= @UserCompanyType and 
 b.contactid= @contactId
 where a.Deleted is  null and PROSellerCompanyId=@contactId and  b.Deleted is  null
 

 
end
USE [test20]
GO
/****** Object:  StoredProcedure [dbo].[spGetUnreadMessagesNumber]    Script Date: 02/10/2020 21:47:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  procedure [dbo].[spGetUnreadMessagesNumber] 
@EmployeeId int
as
begin

select articleid as Id,'' as Name , COUNT(*) as Value  from posts left join 
 (select max(logindate) logindate,employeeid,vieweditem from [LogConnexions] where employeeid =@EmployeeId and  logsystem='CHAT' group by   employeeid,vieweditem    )b 
on articleid=vieweditem and updatebyuserid=employeeid 
where         updatebyuserid=@EmployeeId and lastupdate >logindate
 group by articleid

 
end
