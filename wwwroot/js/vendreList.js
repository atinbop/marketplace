﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {

 

    

    dataTable = $('#DT_load').DataTable({
        "language": {
            "url": "dataTables.french.lang"
        },
        "ajax": {
            "url": "/api/vendre",
            "type": "GET",
            "datatype": "json" 
           
        },
        "columns": [
            {
                "data": "name",
                "render": function (data, type, row) {
                    return '<a  style="font-size: 18px; color:blue" href="' + row['ProductShopLink'] + '" >'
                        + data +
                        '</a> '
                },

                "width": "35%"
            },
            {
                "data": "brand"

                ,
                "render": function (data, type, row) {
                    return '<a  style="font-size: 18px; color:blue" href="' + row['BrandShopLink'] + '" >'
                        + data +
                        '</a> '
                },


                "width": "10%"
            },
            {
                "data": "ean",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + row['ean'] + '</div>'
                },

                "width": "5%"
            },
            {
                "data": "price",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + row['price'] + ' ' + row['priceType'] + '</div>'
                },

                "width": "5%"
            },
            {
                "data": "lot",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + row['lot'] + '</div>'
                },

                "width": "5%"
            },
            /*{
                "data": "vat",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:blue">' + row['vat'] + '</div>'
                },

                "width": "5%"
            },*/
            {
                "data": "inventory",
                "render": function (data, type, row) {
                    return row['inventory'] > 0 ? '<div style="font-size: 15px; color:black">' + row['inventory'] + '</div>' : ''
                }, "width": "5%"
            },
            {
                "data": "image1",
                "render": function (data) {
                    return `<div  > <img id="myImg" src=${data} width='80' height='80'   ></div>
                    <div id="myModal" class="modal">
                    <span class="close">&times;</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>

` },


                "width": "10%"
            },
            {
                "data": "firstCategory",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + (row['firstCategory']) + '</div>'
                },

                "width": "10%"
            },
            {
                "data": "id",
                "render": function (data) {
                    return ` <div class="text-center">
                      <a class="mb-1" Alt='Ajouter à mon catalogue' onclick=Add('/api/vendre?id=${data}') aria-label="Add" style='cursor:pointer; width:70px;color:green'>
                              <i class='fas fa-plus-circle' aria-hidden="true" style='font-size:24px'></i>

                            </a> 

                          

                      `

                }, "width": "20%"
            }
        ],
        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            },
            "emptyTable": "Votre formule d'abonnement ne vous permet pas d'ajouter des produits dans votre catalogue. Vous pouvez souscrire à l'offre <a href='https://localhost:44378/Tarif'>ESSENTIEL<a>"
        },
        "width": "100%"
    });
}


function Add(url) {
    swal({
        title: "Vous êtes sur le point d'ajouter cet article dans votre catalogue Pharmashopcenter?",
        text: "Devenez ainsi le vendeur exclusif de cet article sur la Market Place Pharmashopcenter (Pour 4.99€/mois). voir CGV.",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}

