﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        "ajax": {
            "url": "/api/brand",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "name",
                "render": function (data, type, row ) {
                    return '<a  style="font-size: 15px; color:blue" href="'+ row['ProductShopLink'] + '" >'
                              + data +
                    '</a> '
                },

                "width": "40%"
            },
            {
                "data": "brand"
                
                ,
                "render": function (data, type, row) {
                    return '<a  style="font-size: 15px; color:blue" href="' + row['BrandShopLink'] + '" >'
                        + data +
                        '</a> '
                },


                "width": "10%"
            },
            {
                "data": "ean",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + row['ean']   + '</div>'
                },

                "width": "10%"
            },
            {
                "data": "price",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">'+ row['price']+ ' ' + row['priceType']  +'</div>'
                },

                "width": "5%"
 },
            {
                "data": "lot",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + row['lot'] + '</div>'
                },

                "width": "5%"
            },
             
       
        ],
        "language": {
            "emptyTable": "no data found"
        },
        "width": "100%"
    });
}



function Delete(url) {
    swal({
        title: "Etes-vous certain de vouloir retirer cet article de votre catalogue Pharmashopcenter?",
        text: "Une fois retiré, l'article ne sera plus disponible en vente sur la plateforme Pharmashopcenter au nom de votre enseigne.",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}
