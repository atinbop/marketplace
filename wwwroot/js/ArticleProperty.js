﻿function Delete(url) {
    swal.fire({
        title: "Voulez-vous supprimer cette   propriété ?",
        text: "Une fois supprimée, cette propriété ne sera plus disponible sur cet article",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Supprimer',
        cancelButtonText: 'Annuler'

    }).then((result) => {
        if (result.isConfirmed) {

            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        //dataTable.ajax.reload();
                          window.location.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
            Swal.fire('Supprimé!',
                'La propriété produit a étée supprimée avec succès.',
                'success'
            )

        }
    });
}