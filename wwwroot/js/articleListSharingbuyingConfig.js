﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
 
    dataTable = $('#DT_load').DataTable({           
        "ajax": {
            "url": "/api/article",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "name",
                "render": function (data, type, row ) {
                    return row['productShopLink'] === null ? data : '<a  style="font-size: 18px; color:blue" href="'+ row['productShopLink'] + '" >'
                              + data +
                        '</a> <br>'+ 
                        (row['note'] === 1 ? '<i class="fas fa-star blue-text   aria-hidden=" true" style="font-size:16px;"></i>' : 
                        row['note'] === 2 ? '<i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i>' :
                            row['note'] === 3 ? '<i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i>' :
                                row['note'] === 4 ? '<i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i>' :
                                    row['note'] === 5 ? '<i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i><i class="fas fa-star blue-text aria-hidden=" true" style="font-size:16px;"></i>' :"")
                },

                "width": "35%"
            },
            {
                "data": "brand"
                
                ,
                "render": function (data, type, row) {
                    return row['brandShopLink'] === null ? data : '<a  style="font-size: 18px; color:blue" href="' + row['brandShopLink'] + '" >'
                        + data +
                        '</a> ' 
                },


                "width": "10%"
            },
            {
                "data": "ean",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + row['ean']   + '</div>'
                },

                "width": "5%"
            },
            {
                "data": "price",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + (row['price'] === 0 ? '' : row['price']) + ' ' + (row['priceType'] === null ? '' : row['priceType'])    +'</div>'
                },

                "width": "5%"
 },
            {
                "data": "lot",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + (row['lot'] === null ? '' : row['lot'])  + '</div>'
                },

                "width": "5%"
            },
            /*{
                "data": "vat",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:blue">' + row['vat'] + '</div>'
                },

                "width": "5%"
            },*/
            {
                "data": "inventory",
                "render": function (data, type, row) {
                    return row['inventory']>0 ?'<div style="font-size: 15px; color:black">' + row['inventory'] + '</div>': ''
                }, "width": "5%" },
            {
                "data": "image1",
                "render": function (data) {
                    return `<div class="view overlay zoom"> <img id="myImg" class="img-fluid" src=${data} width='80' height='80'   > <div class="mask flex-center waves-effect waves-light">
                
              </div>
            </div
` },


                "width": "10%"
            },
            {
                "data": "firstCategory",
                "render": function (data, type, row) {
                    return '<div style="font-size: 15px; color:black">' + (row['firstCategory'] === null || row['firstCategory']==='null' ? '' : row['firstCategory'])  + '</div>'
                },

                "width": "10%"
            },
            {
                "data": "id",
                "render": function (data, type, row) {
                    return ` 
<div class="text-center row">

    <div class="col-4">
        <a href="/DiscountListV2/SharingbuyingConfig?articleId=${data}" class="mb-1" Alt='Gerer les remises par pallier' aria-label="Remove" style='cursor:pointer; width:70px;color:green'>
           
 
                                    <span class="table-next">
                                        <button type="button"
                                                class="btn boutonbleu btn-rounded btn-sm my-0">
                                            Sharing&nbsp;Buying
                                        </button>
                                    </span>
                             
        </a>
    </div>
    
</div> `
                  
                }, "width": "30%"
            }
        ],
        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage du produit _START_ à _END_ sur _TOTAL_ produits",
            "sInfoEmpty": "Affichage du produit 0 à 0 sur 0 produit",
            "sInfoFiltered": "(filtré à partir de _MAX_ produits au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ produits",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun produit correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            },
            "emptyTable": "Vous n'avez pas encore de produits dans votre catalogue Pharmashopcenter"
        },
        "width": "100%"
    });
}



function Delete(url) {
    swal.fire({
        title: 'Retirer cet article de votre catalogue?',
        text: "Une fois retiré, l'article ne sera plus disponible en vente sur la plateforme Pharmashopcenter au nom de votre enseigne.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Supprimer',
        cancelButtonText: 'Annuler'

    }).then((result) => {
        if (result.isConfirmed) {
          
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                       // window.location.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
            Swal.fire('Supprimé!',
                'Le produit a été supprimé avec succès.',
                'success'
            )

        }
    });
}

function setNotation(url) {
    /* inputOptions can be an object or Promise */
    const inputOptions = new Promise((resolve) => {
        setTimeout(() => {
            resolve({
                '1': '1 étoile ',
                '2': '2 étoiles',
                '3': '3 étoiles',
                '4': '4 étoiles',
                '5': '5 étoiles'
            })
        }, 1000)
    })

    const { value: color } =  Swal.fire({
        title: 'Notez le produit',
        input: 'radio',
        inputOptions: inputOptions,
        inputValidator: (value) => {
            if (!value) {
                return 'Faites un choix!'
            }
            else {
                $.ajax({
                    type: "POST",
                    url: url + '&note=' + value,
                    success: function (data) {
                        if (data.success) {
                            toastr.success(data.message);
                            dataTable.ajax.reload();
                            window.location.reload();
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            }
        }
    })

    if (color) {
        alert(1);
        //Swal.fire({ html: `Vous avez sélectionné: ${color}` })
    }
}
