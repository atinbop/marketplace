﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        "ajax": {
            "url": "/api/payout",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "transactionNumber",
                "render": function (data, type, row) {
                    return '<a href="/PayoutList/Edit?id=' + row['id'] + '" >'
                        + data +
                        '</a> '
                },

                "width": "20%"
            },
            {
                "data": "paymentDate", "width": "10%"
            }, 
            
            {"data": "amount", "width": "10%" },
            {     "data": "paymentDocument",
                "render": function (data) {
                    return `
                     <div class="row justify-content-center"><a href ="${data}" download="${data}" class="btn btn-secondary" Alt='Document'     aria-label="Delete">
                            <i class="fa fa-file-text" aria-hidden="true"></i>
                            </a></div>`
                }, "width": "10%"
            },
            { "data": "paymentStatus", "width": "5%" },
            { "data": "orderCode", "width": "10%" },
            { "data": "paymentComment", "width": "35%" }, 
            { "data": "sellerId", "width": "35%" }, 
            {
                "data": "id",
                "render": function (data) {
                    return `
                      <div class="row justify-content-center"><a class="btn btn-danger" Alt='Document' onclick=Delete('/api/payout?id='+${data}) aria-label="Delete">
                            <i class="fa fa-trash-o fa-lg"></i>
                            </a></div>`
                }, "width": "10%"
            }
             ],
        "language": {
            "emptyTable": "no data found"
        },
        "width": "100%"
    });
}



function Delete(url) {
    swal({
        title: "Etes vous certain de vouloir supprimer ce paiement?",
        text: "Une fois supprimée, le paiement ne sera plus en vente sur la plateforme",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}