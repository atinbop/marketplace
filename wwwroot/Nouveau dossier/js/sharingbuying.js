﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#DT_load').DataTable({           
        "ajax": {
            "url": "/api/sharingbuying",
            "type": "GET",
            "datatype": "json" 
        },
        "columns": [
                {
                "data": "name" ,
                },      
        ],
        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage du produit _START_ à _END_ sur _TOTAL_ produits",
            "sInfoEmpty": "Affichage du produit 0 à 0 sur 0 produit",
            "sInfoFiltered": "(filtré à partir de _MAX_ produits au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ produits",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun produit correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            },
            "emptyTable": "Vous n'avez pas encore de produits dans votre catalogue Pharmashopcenter"
        },
        "width": "100%"
    });
}

