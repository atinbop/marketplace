﻿using System;
using MarketPlace.Data;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(MarketPlace.Areas.Identity.IdentityHostingStartup))]
namespace MarketPlace.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {


                services.AddDbContext<Data.ApplicationDbContext>(options =>
                           options.UseSqlServer(
                               context.Configuration.GetConnectionString("DefaultConnection")));

                services.AddIdentity<MarketplaceUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
                 .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders(); 

            });


        }
    }
}