﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using MarketPlace.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Net;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using MarketPlace.Resources;
using System.Reflection;

namespace MarketPlace.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly UserManager<MarketplaceUser> _userManager;
        private readonly SignInManager<MarketplaceUser> _signInManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;

        public string _ConnectedUserEmail;
        public string _UserCompanyType;
    
        public string _ConnectedUserId;
        public string _UserCompanyId;
        public String _UserCompanyName;

      
        private readonly IStringLocalizer _identityLocalizer;


        public LoginModel(SignInManager<MarketplaceUser> signInManager,
            ILogger<LoginModel> logger,
            UserManager<MarketplaceUser> userManager,
            IEmailSender emailSender
                  


        , Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment, IStringLocalizerFactory factory
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _db = db;

            var type = typeof(IdentityResource);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _identityLocalizer = factory.Create("IdentityResource", assemblyName.Name);

        }

        [BindProperty]
        public InputModel Input { get; set; }
        [BindProperty]
        public Employees Employee { get; set; }
        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "EMAIL_REQUIRED")]
            [EmailAddress(ErrorMessage = "EMAIL_INVALID")]
            public string Email { get; set; }

            [Required(ErrorMessage = "PASSWORD_REQUIRED")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            // Clear the existing external cookie
            #region snippet2
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);
            #endregion

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
        }
        private static string GetAccessToken(string userId)
        {
            const string issuer = "localhost";
            const string audience = "localhost";

            var identity = new ClaimsIdentity(new List<Claim>
                                                      {
                                                        new Claim("sub", userId)
                                                      });

            var bytes = Encoding.UTF8.GetBytes(userId);
            var key = new SymmetricSecurityKey(bytes);
            var signingCredentials = new SigningCredentials(
              key, SecurityAlgorithms.HmacSha256);

            var now = DateTime.UtcNow;
            var handler = new JwtSecurityTokenHandler();

            var token = handler.CreateJwtSecurityToken(
              issuer, audience, identity,
              now, now.Add(TimeSpan.FromHours(1)),
              now, signingCredentials);

            return handler.WriteToken(token);
        }

            public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, lockoutOnFailure: true);              
                            

                if (result.Succeeded)
                {
                    // var userName = User.FindFirst(ClaimTypes.Name).Value;
                

                    Employees Employee = await (from art in _db.Employees
                                                where art.Emaillogin == Input.Email
                                                select art).FirstOrDefaultAsync();

                    Contacts Contacts = await (from art in _db.Contacts
                                               where art.Id == Employee.ContactId
                                               select art).FirstOrDefaultAsync();


                    Brands Brands = await (from art in _db.Brands
                                           where art.ContactId == Contacts.Id
                                           select art).FirstOrDefaultAsync();


                    CookieOptions cookieOptions = new CookieOptions();
                    cookieOptions.Expires = DateTime.Now.AddDays(7);

                    HttpContext.Response.Cookies.Append("UserCompanyName", Contacts.Company, cookieOptions);
                    HttpContext.Response.Cookies.Append("ConnectedUserId", Employee.Id.ToString(), cookieOptions);
                    HttpContext.Response.Cookies.Append("UserCompanyId", Contacts.Id.ToString(), cookieOptions);
                    HttpContext.Response.Cookies.Append("Firstname", Employee.Firstname, cookieOptions);
                    HttpContext.Response.Cookies.Append("FullName", Employee.Firstname + " "+  Employee.Lastname, cookieOptions);
                    HttpContext.Response.Cookies.Append("UserCompanyType", Contacts.UserCompanyType, cookieOptions);
                    HttpContext.Response.Cookies.Append("ConnectedUserEmail", Input.Email, cookieOptions);
                    HttpContext.Response.Cookies.Append("UserCompanyLogo", Contacts.CompanyLogoFile, cookieOptions);
                    HttpContext.Response.Cookies.Append("Lastname", Employee.Lastname, cookieOptions);
                    HttpContext.Response.Cookies.Append("UserCurrency", Employee.PreferedCurrency, cookieOptions);
                    HttpContext.Response.Cookies.Append("SharingBuyingAreaId", Contacts.SharingBuyingAreaId.ToString(), cookieOptions);
                    HttpContext.Response.Cookies.Append("Slogan", Brands.Slogan.ToString(), cookieOptions);
                 
                    //    #region snippet1
                    //var claims = new List<Claim>
                    //  {
                    // new Claim(ClaimTypes.Email, Input.Email),
                    // new Claim("FullName", Employee.Firstname + Employee.Lastname),
                    // new Claim("Lastname",  Employee.Lastname),
                    // new Claim("Firstname", Employee.Firstname ),
                    // new Claim("ConnectedUserEmail", Input.Email),
                    // new Claim("UserCompanyType", Contacts.UserCompanyType),
                    // new Claim("ConnectedUserId", Employee.ContactId.ToString()),
                    // new Claim("UserCompanyId", Contacts.Id.ToString()),
                    // new Claim("UserCompanyName",Contacts.Company),
                    // new Claim("UserCompanyLogo",Contacts.CompanyLogoFile), 
                    //  };






                    /*CookieOptions cookieOptions = new CookieOptions();
                    Cookie.Options.Expires = DateTime.Now.AddDays(7);
                    Responses.Cookies.Append(key, value, cookieOptions)*/


                    //var claimsIdentity = new ClaimsIdentity(
                    //    claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    //var authProperties = new AuthenticationProperties
                    //{
                    //    //AllowRefresh = <bool>,
                    //    // Refreshing the authentication session should be allowed.

                    //    ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                    //    // The time at which the authentication ticket expires. A 
                    //    // value set here overrides the ExpireTimeSpan option of 
                    //    // CookieAuthenticationOptions set with AddCookie.

                    //    IsPersistent = true,
                    //    // Whether the authentication session is persisted across 
                    //    // multiple requests. When used with cookies, controls
                    //    // whether the cookie's lifetime is absolute (matching the
                    //    // lifetime of the authentication ticket) or session-based.

                    //    //IssuedUtc = <DateTimeOffset>,
                    //    // The time at which the authentication ticket was issued.

                    //    //RedirectUri = <string>
                    //    // The full path or absolute URI to be used as an http 
                    //    // redirect response value.
                    //};

                    //await HttpContext.SignInAsync(
                    //    CookieAuthenticationDefaults.AuthenticationScheme,
                    //    new ClaimsPrincipal(claimsIdentity),
                    //      new AuthenticationProperties
                    //      {
                    //         //Cookies persist accross brosers
                    //          IsPersistent = true,                             
                    //      });
                    //#endregion

                    ///  string value = HttpContext.Request.Cookies["UserCompanyType"];

                    _logger.LogInformation("User {Email} logged in at {Time}.",
                    Input.Email, DateTime.UtcNow);


                    /* var userId = Guid.NewGuid().ToString();
                     var claims = new List<Claim>
                         {
                           new Claim(ClaimTypes.Name, userId),
                           new Claim("access_token", GetAccessToken(userId))
                         };*/


                    Employee = (from art in _db.Employees
                                where art.Emaillogin == Input.Email  
                                select art).FirstOrDefault();

                    if (Employee != null)
                    {
                        string t = Employee.Firstname;
                        string sql = "insert into[dbo].[LogConnexions]([EmployeeId], [LoginDate], [logSystem]) values(" + Employee.Id + ", getdate(), 'LOGIN')";
                        await _db.SaveChangesAsync();

                        Contacts contacts = (from art in _db.Contacts
                                             where art.Id == Employee.ContactId
                                             select art).FirstOrDefault();


                        int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);

                   /*  if (string.IsNullOrEmpty(HttpContext.Session.GetString(SessionKeyConnectedUserEmail)))
                        {
                            HttpContext.Session.SetString(SessionKeyConnectedUserEmail, Employee.Emaillogin);
                            HttpContext.Session.SetString(SessionKeyUserCompanyType, contacts.UserCompanyType);
                            
                            HttpContext.Session.SetString(SessionKeyConnectedUserId, Employee.Id.ToString());
                            HttpContext.Session.SetString(SessionKeyUserCompanyId, contacts.Id.ToString());
                            HttpContext.Session.SetString(SessionKeyUserCompanyName, contacts.Company);
                        }
                        _ConnectedUserEmail = HttpContext.Session.GetString(SessionKeyConnectedUserEmail);
                        _UserCompanyType = HttpContext.Session.GetString(SessionKeyUserCompanyType);

                        _ConnectedUserId = HttpContext.Session.GetString(SessionKeyConnectedUserId);
                        _UserCompanyId = HttpContext.Session.GetString(SessionKeyUserCompanyId);
                        _UserCompanyName = HttpContext.Session.GetString(SessionKeyUserCompanyName);*/



                        if (contacts.UserCompanyType == "PRO")
                        {
                            return RedirectToPage("/ArticleList/Index");
                        }
                        else if (contacts.UserCompanyType == "B2B")
                        {
                            return RedirectToPage("/Market/Index");
                        }
                        else if (contacts.UserCompanyType == "B2C")
                        {
                            return RedirectToPage("/Market/Index");
                        }
                        else
                        {
                            return RedirectToPage("/Market/Index");
                        }
                    }


                       // return LocalRedirect(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToPage("./Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, _identityLocalizer["INVALID_LOGIN_ATTEMPT"]);
                    return Page();
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.FindByEmailAsync(Input.Email);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Verification email sent. Please check your email.");
            }

            var userId = await _userManager.GetUserIdAsync(user);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { userId = userId, code = code },
                protocol: Request.Scheme);
            await _emailSender.SendEmailAsync(
                Input.Email,
                "Confirm your email",
                $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

            ModelState.AddModelError(string.Empty, "Verification email sent. Please check your email.");
            return Page();
        }


    }
}
