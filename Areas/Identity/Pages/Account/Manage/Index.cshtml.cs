﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using MarketPlace.Model;
using MarketPlace.Resources;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;

namespace MarketPlace.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<MarketplaceUser> _userManager;
        private readonly SignInManager<MarketplaceUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IStringLocalizer _identityLocalizer;

        public IndexModel(
            UserManager<MarketplaceUser> userManager,
            SignInManager<MarketplaceUser> signInManager , IEmailSender emailSender,
            IStringLocalizerFactory factory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;

            var type = typeof(IdentityResource);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _identityLocalizer = factory.Create("IdentityResource", assemblyName.Name);

        }

        public string Username { get; set; }

               public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "EMAIL_REQUIRED")]
            [EmailAddress(ErrorMessage = "EMAIL_INVALID")]
            public string Email { get; set; }


            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Nom")]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Prénom")]
            [DataType(DataType.Text)]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Société")]
            [DataType(DataType.Text)]
            public string CompanyName { get; set; }

            [Required]
            [Display(Name = "Activity sector")]
            [DataType(DataType.Text)]
            public string ActivitySector { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Adresse")]
            public string Adresse { get; set; }



            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "PostalCode")]
            public string PostalCode { get; set; }



            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "City")]
            public string City { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "usercountry")]
            public string usercountry { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Position")]
            public string Position { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Position")]
            public string Civility { get; set; }



       
            [DataType(DataType.Text)]
            [Display(Name = "Siren")]
            public string Siren { get; set; }

            [DataType(DataType.Text)]
            [Display(Name = "TVAIntracommunautaire")]
            public string TVAIntracommunautaire { get; set; }

            

        }

        private async Task LoadAsync(MarketplaceUser user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);

            Username = userName;

            Input = new InputModel
            {
                PhoneNumber = phoneNumber,
                Name = user.Name,
                FirstName = user.FirstName,
                CompanyName = user.CompanyName,
                ActivitySector = user.ActivitySector,
                Adresse = user.Adresse,
                PostalCode = user.PostalCode,
                City = user.City,
                usercountry = user.usercountry,
                Position = user.Position,
                Civility = user.Civility,
                Siren = user.Siren,
                TVAIntracommunautaire=user.TVAIntracommunautaire

            };
            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);

        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(_identityLocalizer["USER_NOTFOUND", _userManager.GetUserId(User)]);
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting phone number for user with ID '{userId}'.");
                }
            }

        if (Input.Name != user.Name)
        {
            user.Name = Input.Name;
        }

        if (Input.FirstName != user.FirstName)
        {
            user.FirstName = Input.FirstName;
        }

            if (Input.PhoneNumber != user.PhoneNumber)
            {
                user.PhoneNumber = Input.PhoneNumber;
            }
            if (Input.CompanyName != user.CompanyName)
            {
                user.CompanyName = Input.CompanyName;
            }

            if (Input.ActivitySector != user.ActivitySector)
            {
                user.ActivitySector = Input.ActivitySector;
            }

            if (Input.Adresse != user.Adresse)
            {
                user.Adresse = Input.Adresse;
            }
            if (Input.PostalCode != user.PostalCode)
            {
                user.PostalCode = Input.PostalCode;
            }
            if (Input.City != user.City)
            {
                user.City = Input.City;
            }
           if (Input.usercountry != user.usercountry)
            {
                user.usercountry = Input.usercountry;
            }

            if (Input.Position != user.Position)
            {
                user.Position = Input.Position;
            }


            if (Input.Civility != user.Civility)
            {
                user.Civility = Input.Civility;
            }
            if (Input.Siren != user.Siren)
            {
                user.Siren = Input.Siren;
            }

            if (Input.TVAIntracommunautaire != user.TVAIntracommunautaire)
            {
                user.TVAIntracommunautaire = Input.TVAIntracommunautaire;
            }
            


            await _userManager.UpdateAsync(user);


        await _signInManager.RefreshSignInAsync(user);
            StatusMessage = _identityLocalizer["STATUS_PROFILE_UPDATED", _userManager.GetUserId(User)];
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(_identityLocalizer["USER_NOTFOUND", _userManager.GetUserId(User)]);
            }


            var userId = await _userManager.GetUserIdAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { userId = userId, code = code },
                protocol: Request.Scheme);
            await _emailSender.SendEmailAsync(
                email,
                _identityLocalizer["CONFIRM_YOUR_EMAIL"],
                _identityLocalizer["CONFIRM_YOUR_EMAIL_TEXT", HtmlEncoder.Default.Encode(callbackUrl)]);

            StatusMessage = _identityLocalizer["STATUS_UPDATE_PROFILE_EMAIL_SEND"];
            return RedirectToPage();
        }

    }
}
