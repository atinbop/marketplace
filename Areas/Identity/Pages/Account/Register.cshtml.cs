﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using MarketPlace.Model;
using MarketPlace.Resources;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;


namespace MarketPlace.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<MarketplaceUser> _signInManager;
        private readonly UserManager<MarketplaceUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly Data.ApplicationDbContext _db;
        private readonly IConfiguration configuration;
        private readonly IStringLocalizer _identityLocalizer;

        public RegisterModel(
            UserManager<MarketplaceUser> userManager,
            SignInManager<MarketplaceUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender, Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment,
            IStringLocalizerFactory factory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _db = db;

            var type = typeof(IdentityResource);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _identityLocalizer = factory.Create("IdentityResource", assemblyName.Name);

        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "EMAIL_REQUIRED")]
            [EmailAddress(ErrorMessage = "EMAIL_INVALID")]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required(ErrorMessage = "PASSWORD_REQUIRED")]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "CONFIRM_PASSWORD_NOT_MATCHING")]
            public string ConfirmPassword { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "FirstName")]
            public string Name { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Last Name")]
            public string FirstName { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Phone Number")]
            public string PhoneNumber { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Company Name")]
            public string CompanyName { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Activity Sector")]
            public string ActivitySector { get; set; }



            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Adresse")]
            public string Adresse { get; set; }



            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "PostalCode")]
            public string PostalCode { get; set; }



            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "City")]
            public string City { get; set; }



            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "usercountry")]
            public string usercountry { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Position")]
            public string Position { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Civility")]
            public string Civility { get; set; }


            [DataType(DataType.Text)]
            [Display(Name = "Siren")]
            public string Siren { get; set; }


            [DataType(DataType.Text)]
            [Display(Name = "TVAIntracommunautaire")]
            public string TVAIntracommunautaire { get; set; }
            


        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {

            returnUrl = returnUrl ?? Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {

                var user = new MarketplaceUser
                {
                    UserName = Input.Email,
                    Email = Input.Email,
                    Name = Input.Name,
                    FirstName = Input.FirstName,
                    PhoneNumber = Input.PhoneNumber,
                    CompanyName = Input.CompanyName,
                    ActivitySector = Input.ActivitySector,
                    City = Input.City,
                    PostalCode = Input.PostalCode,
                    Adresse = Input.Adresse,
                    usercountry = Input.usercountry,
                    Position = Input.Position,
                    Civility = Input.Civility,
                    Siren = Input.Siren,
                TVAIntracommunautaire= Input.TVAIntracommunautaire

                };
                Employees Employee = null;
                EmployeesallowedAccess EmployeesallowedAccess = null;
                Contacts Contact = await (from con in _db.Contacts
                                          where con.Company == user.CompanyName && con.PostalCode == user.PostalCode
                                          select con).FirstOrDefaultAsync();

                if (Contact != null)
                {
                    Employee = await (from con in _db.Employees
                                      where con.ContactId == Contact.Id
                                      select con).FirstOrDefaultAsync();

                    EmployeesallowedAccess = await (from con in _db.EmployeesallowedAccess
                                                    where con.ContactId == Contact.Id && con.Emaillogin == user.Email
                                                    select con).FirstOrDefaultAsync();
                }


                //le user n'hesite pas ou bien a été autorisé par le master account a creer un compte
                if (Contact != null && (Employee == null || EmployeesallowedAccess != null) || Contact == null)
                {
                    var result = await _userManager.CreateAsync(user, Input.Password);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User created a new account with password.");
                        //Create account

                        Employee = new Employees();
                        Employee.Lastname = user.Name;
                        Employee.Firstname = user.FirstName;
                        Employee.Phone = user.PhoneNumber;
                        Employee.Emaillogin = user.Email;
                        Employee.company = user.CompanyName;
                        Employee.usercountry = user.usercountry;
                        Employee.ActivitySector = Input.ActivitySector;
                        Employee.Functions = user.Position;
                        Employee.Civility = Input.Civility;

                        Employee.Pseudo = (user.Name.Substring(0, 1) + user.FirstName).ToLower();


                        Country Country = await (from con in _db.Countries
                                                 where con.nom_fr_fr == user.usercountry
                                                 select con).FirstOrDefaultAsync();

                        string Language = Country.langcode1;
                        string currency = Country.currency;



                        Employee.PreferedLanguage = Language;

                        Employee.PreferedCurrency = currency;



                        Adresse Adresse = await (from con in _db.Adresse
                                                 where con.postalecode == user.PostalCode && con.fullAdress == user.Adresse
                                                 select con).FirstOrDefaultAsync();

                        /*string UserCompanyType = (string)_db.ContactsStock.Where(p => p.Company == user.CompanyName).Where(p => p.PostalCode == user.PostalCode)
                                        .Select(p => p.UserCompanyType).ToList().FirstOrDefault();*/

                        string UserCompanyType;

                        if (Contact == null)
                        {
                            ContactsStock ContactsStock = await (from con in _db.ContactsStock
                                                                 where con.Company == user.CompanyName && con.PostalCode == user.PostalCode
                                                                 select con).FirstOrDefaultAsync();

                            Employee.userRole = "Admin";
                            Contact = new Contacts();
                            Contact.Country = user.usercountry;
                            Contact.Company = user.CompanyName;
                            Contact.address1 = user.Adresse;
                            Contact.PostalCode = user.PostalCode;
                            Contact.City = user.City;
                            Contact.ActivitySector = user.ActivitySector;
                            Contact.Siren = user.Siren;

                            SharingBuyingArea SharingBuyingArea = _db.SharingBuyingArea.FromSqlRaw<SharingBuyingArea>("spGetSharingBuyingArea {0} , {1}", user.PostalCode, user.usercountry).ToList().FirstOrDefault();
                            if (SharingBuyingArea != null) { Contact.SharingBuyingAreaId = SharingBuyingArea.id; }

                            if (Adresse != null)
                            {
                                Contact.Siren = Adresse.Siren;
                                Contact.SIRET = Adresse.siret;
                                Contact.versioncodenaf = Adresse.versioncodenaf;
                                Contact.codenaf = Adresse.codenaf;
                                Contact.denomination = Adresse.denomination;
                            }
                            Contact.Language = Language;
                            Contact.Currency = currency;

                            if (user.ActivitySector == "1") Contact.UserCompanyType = "PRO";
                            else if (user.ActivitySector == "2") Contact.UserCompanyType = "B2C";
                            else if (user.ActivitySector == "3") Contact.UserCompanyType = "B2B";
                            else Contact.UserCompanyType = "EXP";


                            if (user.ActivitySector == "1") Contact.ContactType = "Fournisseur";
                            else if (user.ActivitySector == "2") Contact.ContactType = "Acheteur Vendeur B2C";
                            else if (user.ActivitySector == "3") Contact.ContactType = "Achteur Vendeur B2B";
                            else Contact.ContactType = "Acheteur";




                           


                            if (ContactsStock != null)
                            {
                                Contact.Id = ContactsStock.Id;
                                Contact.ContactCode = ContactsStock.ContactCode;


                                Contact.NbSubscritedUsers = ContactsStock.NbSubscritedUsers;
                                Contact.Gender = ContactsStock.Gender;
                                Contact.Name = ContactsStock.Name;
                                Contact.FirstName = ContactsStock.FirstName;

                                Contact.Address2 = ContactsStock.Address2;
                                Contact.Address3 = ContactsStock.Address3;

                                Contact.EAN = ContactsStock.EAN;

                                Contact.APE = ContactsStock.APE;

                                Contact.DateOfBirth = ContactsStock.DateOfBirth;
                                Contact.Category = ContactsStock.Category;
                                Contact.Phone = ContactsStock.Phone;
                                Contact.Mobile = ContactsStock.Mobile;
                                Contact.Fax = ContactsStock.Fax;
                                Contact.Email = ContactsStock.Email;
                                Contact.Website = ContactsStock.Website;
                                Contact.Origin = ContactsStock.Origin;

                                Contact.BlockedAccount = ContactsStock.BlockedAccount;
                                Contact.BlockedPurchase = ContactsStock.BlockedPurchase;
                                Contact.Alert = ContactsStock.Alert;
                                Contact.ExcludeLoyalty = ContactsStock.ExcludeLoyalty;
                                Contact.Password = ContactsStock.Password;
                                Contact.PrivateSpace = ContactsStock.PrivateSpace;
                                Contact.AuthorizedAccess = ContactsStock.AuthorizedAccess;
                                Contact.NewsletterSubscriber = ContactsStock.NewsletterSubscriber;
                                Contact.PaymentChoice = ContactsStock.PaymentChoice;
                                Contact.PriceDisplay = ContactsStock.PriceDisplay;
                                Contact.PriceCategory = ContactsStock.PriceCategory;
                                Contact.AccountingCode = ContactsStock.AccountingCode;
                                Contact.IntraCommunityVAT = ContactsStock.IntraCommunityVAT;
                                Contact.Credit = ContactsStock.Credit;
                                Contact.Risk = ContactsStock.Risk;
                                Contact.MonthlyBill = ContactsStock.MonthlyBill;
                                Contact.StartOfMonthlyPayment = ContactsStock.StartOfMonthlyPayment;
                                Contact.ClientAccount = ContactsStock.ClientAccount;
                                Contact.FeedbackContact = ContactsStock.FeedbackContact;
                                Contact.InternalCommentary = ContactsStock.InternalCommentary;
                                Contact.Commercial = ContactsStock.Commercial;
                                Contact.Treatment = ContactsStock.Treatment;
                                Contact.archiving = ContactsStock.archiving;
                                Contact.VATExemption = ContactsStock.VATExemption;
                                Contact.PaidOrdersNotInvoiced = ContactsStock.PaidOrdersNotInvoiced;
                                Contact.ReceivablesClient = ContactsStock.ReceivablesClient;
                                Contact.BankAccountNumberIBAN = ContactsStock.BankAccountNumberIBAN;
                                Contact.CompanyLogoFile = ContactsStock.CompanyLogoFile;
                                Contact.BankAccountNumberRIB = ContactsStock.BankAccountNumberRIB;
                                Contact.address1Livraison1 = ContactsStock.address1Livraison1;
                                Contact.Address2Livraison1 = ContactsStock.Address2Livraison1;
                                Contact.Address3Livraison1 = ContactsStock.Address3Livraison1;
                                Contact.PostalCodeLivraison1 = ContactsStock.PostalCodeLivraison1;
                                Contact.CityLivraison1 = ContactsStock.CityLivraison1;
                                Contact.CountryLivraison1 = ContactsStock.CountryLivraison1;
                                Contact.address1Livraison3 = ContactsStock.address1Livraison3;
                                Contact.Address2Livraison3 = ContactsStock.Address2Livraison3;
                                Contact.Address3Livraison3 = ContactsStock.Address3Livraison3;
                                Contact.PostalCodeLivraison3 = ContactsStock.PostalCodeLivraison3;
                                Contact.CityLivraison3 = ContactsStock.CityLivraison3;
                                Contact.CountryLivraison3 = ContactsStock.CountryLivraison3;
                                Contact.address1Facturation = ContactsStock.address1Facturation;
                                Contact.Address2Facturation = ContactsStock.Address2Facturation;
                                Contact.Address3Facturation = ContactsStock.Address3Facturation;
                                Contact.PostalCodeFacturation = ContactsStock.PostalCodeFacturation;
                                Contact.CityFacturation = ContactsStock.CityFacturation;
                                Contact.CountryFacturation = ContactsStock.CountryFacturation;

                                Contact.currencyId = ContactsStock.currencyId;

                                Contact.FacebookPage = ContactsStock.FacebookPage;
                                Contact.InstagramPage = ContactsStock.InstagramPage;
                                Contact.OfficialWebsite = ContactsStock.OfficialWebsite;
                                Contact.youtubePage = ContactsStock.youtubePage;
                                Contact.Specialities = ContactsStock.Specialities;
                                Contact.LongDescription = ContactsStock.LongDescription;
                                Contact.categoryMarketing = ContactsStock.categoryMarketing;
                                Contact.presentation = ContactsStock.presentation;
                                Contact.Note = ContactsStock.Note;


                            }
                            if (Contact != null) { if (Contact.CompanyLogoFile == null) { Contact.CompanyLogoFile = "../images/imagevide.png"; } }

                            using (var transaction = _db.Database.BeginTransaction())
                            {
                                if (Contact.Id == null|| Contact.Id == 0)
                                {

                                    await _db.Contacts.AddAsync(Contact);
                                    await _db.SaveChangesAsync();
                                }

                                else
                                {
                                   _db.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Contacts] ON");

                                    await _db.Contacts.AddAsync(Contact);
                                    await _db.SaveChangesAsync();

                                  _db.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Contacts] OFF");
                                }

                                transaction.Commit();
                            }

                            Contact = await (from con in _db.Contacts
                                             where con.Company == user.CompanyName && con.PostalCode == user.PostalCode
                                             select con).FirstOrDefaultAsync();

                        }
                        else { if (EmployeesallowedAccess != null) Employee.userRole = EmployeesallowedAccess.userRole; }





                        Employee.ContactId = Contact.Id;


                        await _db.Employees.AddAsync(Employee);

                        await _db.SaveChangesAsync();

                        int EmployeeId = (int)_db.Employees.Where(p => p.Emaillogin == user.Email)
                                           .Select(p => p.Id).ToList().FirstOrDefault();

                        //Mise en cookies
                        CookieOptions cookieOptions = new CookieOptions();
                        cookieOptions.Expires = DateTime.Now.AddDays(7);
                        HttpContext.Response.Cookies.Append("PreferedLanguage", Employee.PreferedLanguage, cookieOptions);
                        HttpContext.Response.Cookies.Append("preferedCountry", user.usercountry, cookieOptions);
                        HttpContext.Response.Cookies.Append("UserCurrency", currency, cookieOptions);

                        HttpContext.Response.Cookies.Append("ConnectedUserEmail", user.Email, cookieOptions);

                        HttpContext.Response.Cookies.Append("ConnectedUserId", EmployeeId.ToString(), cookieOptions);
                        HttpContext.Response.Cookies.Append("UserCompanyId", Contact.Id.ToString(), cookieOptions);

                        HttpContext.Response.Cookies.Append("UserCompanyType", Contact.UserCompanyType, cookieOptions);
                        HttpContext.Response.Cookies.Append("UserCompanyName", user.CompanyName, cookieOptions);
                        HttpContext.Response.Cookies.Append("CompanyLogoFile", Contact.CompanyLogoFile, cookieOptions);



                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                        var callbackUrl = Url.Page(
                            "/Account/ConfirmEmail",
                            pageHandler: null,
                            values: new { area = "Identity", userId = user.Id, code = code },
                            protocol: Request.Scheme);

                        await _emailSender.SendEmailAsync(Input.Email,
                            _identityLocalizer["CONFIRM_YOUR_EMAIL"],
                            _identityLocalizer["CONFIRM_YOUR_EMAIL_TEXT", HtmlEncoder.Default.Encode(callbackUrl)]);

                        if (_userManager.Options.SignIn.RequireConfirmedAccount)
                        {

                            return RedirectToPage("RegisterConfirmation", new { email = Input.Email });
                        }
                        else
                        {
                            await _signInManager.SignInAsync(user, isPersistent: false);
                            return LocalRedirect(returnUrl);
                        }
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    // ModelState.AddModelError(string.Empty,"Une erreur s'est produite - Contacter notre support");
                    // If we got this far, something failed, redisplay form
                    return Page();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Une erreur s'est produite - Contacter notre support");
                    //Message d'erreur la societe est deja enregistrée;
                    return Page();
                }

            }
            // ModelState.AddModelError(string.Empty, "Une erreur s'est produite - Contacter notre support");
            //Message d'erreur la societe est deja enregistrée;
            return Page();
        }
    }
}
