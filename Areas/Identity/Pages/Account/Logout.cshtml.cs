﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MarketPlace.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
 
namespace MarketPlace.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LogoutModel : PageModel
    {
        private readonly SignInManager<MarketplaceUser> _signInManager;
        private readonly ILogger<LogoutModel> _logger;

        private readonly Data.ApplicationDbContext _db;


        public const string SessionKeyConnectedUserEmail = "_ConnectedUserEmail";
        public const string SessionKeyUserCompanyType = "_UserCompanyType";
        public const string SessionKeyConnectedUserId = "_ConnectedUserId";
        public const string SessionKeyUserCompanyId = "_UserCompanyId";
        public const string SessionKeyUserCompanyName = "_UserCompanyName";



        public LogoutModel(SignInManager<MarketplaceUser> signInManager, ILogger<LogoutModel> logger, Data.ApplicationDbContext db, IWebHostEnvironment webHostEnvironment)
        {
            _signInManager = signInManager;
            _logger = logger;
            _db = db;


        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost(string returnUrl = null)
        {


            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = DateTime.Now.AddDays(-1);

            HttpContext.Response.Cookies.Append("FullName", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("Lastname", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("Firstname", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("ConnectedUserEmail", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("UserCompanyType", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("ConnectedUserId", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("UserCompanyName", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("CompanyLogoFile", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("PreferedLanguage", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("UserCompanyId", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("UserCurrency", "-", cookieOptions);
            HttpContext.Response.Cookies.Append("preferedCountry", "-", cookieOptions);
            

            await HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);


            if (HttpContext.Session.GetString(SessionKeyConnectedUserEmail) == null)
            {
                if (User.Identity.IsAuthenticated)
                {


                    await _signInManager.SignOutAsync();
                    _logger.LogInformation("User logged out.");
                    int userId =   (_db.Employees.Where(mytable => mytable.Emaillogin == User.Identity.Name).Select(column => column.Id)).FirstOrDefault();

                    string sql = "insert into[dbo].[LogConnexions]([EmployeeId], [LoginDate], [logSystem]) values(" + userId.ToString() + ", getdate(), 'LOGOUT')";

                    int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);
                    return RedirectToPage("/Index");

                }
                return RedirectToPage("/Index");
            }
            else


            {

       


                await _signInManager.SignOutAsync();
                _logger.LogInformation("User logged out.");

                /*_ConnectedUserEmail = HttpContext.Session.GetString(SessionKeyConnectedUserEmail);
                _UserCompanyType = HttpContext.Session.GetString(SessionKeyUserCompanyType);

                _ConnectedUserId = HttpContext.Session.GetString(SessionKeyConnectedUserId);
                _UserCompanyId = HttpContext.Session.GetString(SessionKeyUserCompanyId);
                _UserCompanyName = HttpContext.Session.GetString(SessionKeyUserCompanyName);*/

                string sql = "insert into[dbo].[LogConnexions]([EmployeeId], [LoginDate], [logSystem]) values(" + HttpContext.Session.GetString(SessionKeyConnectedUserId) + ", getdate(), 'LOGOUT')";

                int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);

                HttpContext.Session.Clear();

                //Log de la connexion du user au chat

                // int noOfRowUpdated = _db.Database.ExecuteSqlRaw(sql);

                await _db.SaveChangesAsync();
           

            if (returnUrl != null)
                {
                    return LocalRedirect(returnUrl);

                }
                else
                {
                    return RedirectToPage();
                }

            }

          
        }
    }

}
